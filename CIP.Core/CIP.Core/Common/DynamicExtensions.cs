﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data;
using CIP.Core.Common;
using CIP.Core.Models;
using System.Threading;
using CIP.Core.Repositories.Concretes;

namespace CIP.Core.Common
{

    public static partial class ExceptionExtensions
    {

        /// <summary>
        /// Returns a list of all the exception messages from the top-level
        /// exception down through all the inner exceptions. Useful for making
        /// logs and error pages easier to read when dealing with exceptions.
        /// Usage: Exception.Messages()
        /// </summary>
        public static IEnumerable<string> Messages(this Exception ex)
        {
            // return an empty sequence if the provided exception is null
            if (ex == null) { yield break; }
            // first return THIS exception's message at the beginning of the list
            yield return ex.Message;
            // then get all the lower-level exception messages recursively (if any)
            IEnumerable<Exception> innerExceptions = Enumerable.Empty<Exception>();

            if (ex is AggregateException && (ex as AggregateException).InnerExceptions.Any())
            {
                innerExceptions = (ex as AggregateException).InnerExceptions;
            }
            else if (ex.InnerException != null)
            {
                innerExceptions = new Exception[] { ex.InnerException };
            }

            foreach (var innerEx in innerExceptions)
            {
                foreach (string msg in innerEx.Messages())
                {
                    yield return msg;
                }
            }
        }

        public static string ReplaceUnderscore(this string value)
        {
            return value.Replace("_", " ");
        }

        public static string ReplaceKendoFilter(this string value)
        {
            return value.Substring(value.LastIndexOf("~'") + 2).Replace("'", "");
        }

        public static string ToAlreadyUseMesssageBy(this string value, string by)
        {
            return string.Format(BaseConstants.MESSAGE_DATA_IS_ALREADY_USE, value, by);
        }

        public static string ToGenerateAutoNumber(this string value)
        {

            return string.Format(value +"{0}", DateTime.Now.ToString("ddMMyy"));
        }


        public static decimal ToWelfarePerHour(this decimal value)
        {
            decimal val = 0;
            if (value >= 173)
            {
                val = value / 173;
            }

            return val;
        }

        public static string ReplaceUnderScoreToSpace(this string value)
        {
            return value.Replace("_", " ");
        }

        public static string ReplaceUnderScoreToSpace( this BaseEnums.EnumRegistrationStatus value)
        {
            return value.ToString().Replace("_", " ");
        }

        public static string ReplaceUnderScoreToSpace(this BaseEnums.EnumRegistrationStatusDirektorat value)
        {
            return value.ToString().Replace("_", " ");
        }

        public static string ReplaceUnderScoreToSpace(this BaseEnums.EnumRegistrationStatusEnterprise value)
        {
            return value.ToString().Replace("_", " ");
        }

        public static int GetValue(this BaseEnums.EnumRegistrationStatus value)
        {
            return (int)value;
        }

        public static int GetValue(this BaseEnums.EnumRegistrationStatusDirektorat value)
        {
            return (int)value;
        }

        public static int GetValue(this BaseEnums.EnumRegistrationStatusEnterprise value)
        {
            return (int)value;
        }

        public static string ToCapitalize(this string value)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            return textInfo.ToTitleCase(value);
        }

        public static int GetLastAutoNumber(this string value, GenericContext ctx, string userLogin)
        {
            var rec = ctx.MasterAutoNumber.Find(value);
            if(rec == null)
            {
                var model = new MasterAutoNumber();
                model.Kode = value;
                model.Deskripsi = model.Kode;
                model.Nomor = 1;
                model.CreatedBy = userLogin;
                model.CreatedDate = DateTime.Now;

                ctx.MasterAutoNumber.Add(model);

                return model.Nomor.Value;
            }
            else
            {
                rec.Nomor = Convert.ToInt32(rec.Nomor.Value) + 1; 
                rec.UpdatedBy = userLogin;
                rec.UpdatedDate = DateTime.Now;

                ctx.SaveChanges();
                return rec.Nomor.Value;
            }
        }
    }
}