//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CIP.Core.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AnggotaCIP
    {
        public string Kode { get; set; }
        public string KodeRegistrasi { get; set; }
        public string KodeUser { get; set; }
        public string KodeStatusAnggota { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    
        public virtual RegistrasiCIP RegistrasiCIP { get; set; }
        public virtual MasterStatusAnggota MasterStatusAnggota { get; set; }
    }
}
