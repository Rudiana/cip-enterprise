﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class NilaiPenjurian
    {
        public List<sp_GenerateStreamHeader1_Result> Header1 { set; get; }
        public List<sp_GenerateStreamHeader2_Result> Header2 { set; get; }
        public List<sp_GenerateStreamHeader3_Result> Header3 { set; get; }
        //public List<sp_GenerateStreamDetail1_Result> Detail1 { set; get; }
        //public List<sp_GenerateStreamDetail2_Result> Detail2 { set; get; }
        public List<Nilai> Data { set; get; }

    }

    public class Nilai
    {
        public string Kode { set; get; }
        public string Kriteria { set; get; }
        public int? NoUrut { set; get; }
        public string Langkah { set; get; }
        public double? FT_Prove { set; get; }
        public double? I_Prove { set; get; }
        public double? PC_Prove { set; get; }
        public double?[] Data { set; get; }
    }
}
