﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{

    [JsonObject(IsReference = false)]
    public class Audit
    {
        [JsonProperty(IsReference = false)]
        public AuditPDCAHeader AuditHeader { get; set; }
        [JsonProperty(IsReference = false)]
        public sp_GetAuditPDCADetail_Result[] AuditDetail { get; set; }
    }
}
