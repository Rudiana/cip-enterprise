﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class ParamAdministrasiHakAkses
    {
        public AdministrasiHakAkses HakAkses { get; set; }
        public List<AdministrasiHakAksesRole> Roles { get; set; }
        public List<AdministrasiHakAksesMenu> Menus { get; set; }
        public List<AdministrasiHakAksesTombol> Tombols { get; set; }
    }
}
