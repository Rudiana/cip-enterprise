﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class RegistrasiAnggotaCIP : RegistrasiCIP
    {
        public string Anggota { get; set; }
        public string JenisCIPDes { get; set; }
        public string SubUnitDes { get; set; }
        public string UnitDes { get; set; }
        public string DirektoratDes { get; set; }
        public int KodeStatus { get; set; }
        public List<Anggota> AnggotaCIPDetail { get; set; }

    }

    public class Anggota : AnggotaCIP
    {
            public string NamaAnggota { get; set; }
            public string Email { get; set; }
            public string NoPek { get; set; }
            public string StatusDes { get; set; }
    }
}
