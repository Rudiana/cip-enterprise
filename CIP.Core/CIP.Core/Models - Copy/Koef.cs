﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class Koef
    {
        public List<KoefStream> Stream { get; set;}
        public List<KoefSummary> Summary { get; set; }
        public KoefKoreksi Koreksi { get; set; }
    }

    public class KoefMatrixModel
    {
        public double[,] Summary { get; set; }
        public List<string> Koreksi { get; set; }
        public List<Koefisien> Koefisien { get; set; }
    }

    public class Koefisien {
        public string KodeStream { get; set; }
        public double Nilai { get; set; }
    }

    public class KoefStream {
        public int No { get; set; }
        public string Title { get; set; }
        public double Awal { get; set; }
        public double Kal1 { get; set; }
        public double Kal2 { get; set; }
        public double Kal3 { get; set; }
        public double Kal4 { get; set; }
        public double Kal5 { get; set; }
        public double Final { get; set; }
        public double Koefisien { get; set; }

    }

    public class KoefSummary {
        public int No { get; set; }
        public string Title { get; set; }
        public double Awal { get; set; }
        public double Kal1 { get; set; }
        public double Kal2 { get; set; }
        public double Kal3 { get; set; }
        public double Kal4 { get; set; }
        public double Kal5 { get; set; }
        public double Final { get; set; }
    }

    public class KoefKoreksi
    {
        public int No { get; set; }
        public string Title { get; set; }
        public string Awal { get; set; }
        public string Kal1 { get; set; }
        public string Kal2 { get; set; }
        public string Kal3 { get; set; }
        public string Kal4 { get; set; }
        public string Kal5 { get; set; }
        public string Final { get; set; }
    }
}
