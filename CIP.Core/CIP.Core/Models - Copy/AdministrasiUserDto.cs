﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class AdministrasiUserDto:AdministrasiUser
    {
        public string NamaDirektorat { get; set; }
        public string NamaUnit { get; set; }
    }
}
