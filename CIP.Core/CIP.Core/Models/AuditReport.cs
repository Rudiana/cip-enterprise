﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class AuditReport
    {
        public AuditPDCAHeader PdcaHeader { get; set; }
        public RegistrasiCIP Registrasi { get; set; }
        public List<AnggotaCIP> Anggota { get; set; }
        public List<sp_GetPdca1Report_Result> Pdca1Detail { get; set; }
        public List<sp_GetPdca2Report_Result> Pdca2Detail { get; set; }
    }
}
