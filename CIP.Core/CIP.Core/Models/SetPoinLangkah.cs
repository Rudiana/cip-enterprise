﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class SetPoinLangkah
    {
        public List<MasterJenisCIP> JenisCIP { get; set; }
        public List<sp_GetTemplatePlan_Result> LangkahLangkah { get; set; }
    }

    public class SetPoinLangkahEnter
    {
        public List<MasterJenisCIP> JenisCIP { get; set; }
        public List<sp_GetTemplatePlanEnter_Result> LangkahLangkah { get; set; }
    }
}
