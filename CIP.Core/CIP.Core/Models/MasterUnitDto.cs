﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class MasterUnitDto:MasterUnit
    {
        public String DirektoratName { get; set; }
    }

    public class MasterSubUnitDto : MasterSubUnit
    {
        public String Direktorat { get; set; }
        public String DirektoratDes { get; set; }
        public String UnitDes { get; set; }
    }
}
