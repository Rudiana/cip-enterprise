﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{

    public class PenjurianEnter : Penjurian
    {

        public new PenjurianHeaderEnter PenjurianHeader { get; set; }
        public new List<sp_GetPenjurianDetailEnter_Result> PenjurianDetail { get; set; }
    }

    public class PenjurianDir : Penjurian {

        public new PenjurianHeaderDir PenjurianHeader { get; set; }
        public new List<sp_GetPenjurianDetailDir_Result> PenjurianDetail { get; set; }
    }

    public class Penjurian
    {
        public PenjurianHeader PenjurianHeader { get; set; }
        public List<sp_GetPenjurianDetail_Result> PenjurianDetail { get; set; }
    }

    public class DockingInternalView {
        public List<dynamic> data { get; set; }
        public List<sp_GetDockingInternalNew_Result> kolom { get; set; }
    }

    public class DockingInternalViewDir
    {
        public List<dynamic> data { get; set; }
        public List<sp_GetDockingInternalNewDir_Result> kolom { get; set; }
    }

    public class DockingInternalViewEnter
    {
        public List<dynamic> data { get; set; }
        public List<sp_GetDockingInternalNewEnter_Result> kolom { get; set; }
    }

    public class PenjurianSofi
    {
        public string KodeKriteria { get; set; }
        public string Stream { get; set; }
        public string Kategori { get; set; }
        public string Sofi { get; set; }
    }

    public class PenjurianHeaderView : PenjurianHeader{
        string Direktorat { get; set; }
        string DirektoratDes { get; set; }
        string Unit { get; set; }
        string UnitDes { get; set; }
        string SubUnit { get; set; }
        string SubUnitDes { get; set; }
        string StatusPenjurianDes { get; set; }
    }
}
