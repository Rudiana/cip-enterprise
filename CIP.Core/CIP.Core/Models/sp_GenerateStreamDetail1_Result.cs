//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CIP.Core.Models
{
    using System;
    
    public partial class sp_GenerateStreamDetail1_Result
    {
        public string Kode { get; set; }
        public string Kriteria { get; set; }
        public Nullable<int> NoUrut { get; set; }
        public string Langkah { get; set; }
        public string KodeLangkah { get; set; }
        public double FT_Prove { get; set; }
        public double I_Prove { get; set; }
        public double PC_Prove { get; set; }
        public double RT_Prove { get; set; }
    }
}
