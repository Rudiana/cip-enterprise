﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class DeviasiKoefisien:MasterStdDev
    {
        public String Keterangan { get; set; }
        public String PolicyDesc { get; set; }
    }
}
