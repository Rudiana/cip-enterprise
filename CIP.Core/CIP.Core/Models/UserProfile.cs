﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Models
{
    public class UserProfile
    {
        public string Kode { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Telepon { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public bool IsAdministrator { get; set; }
        public bool Aktif { get; set; }
        public string Roles { get; set; }
        public string HakAkses { get; set; }
        public bool IsLdapAccount { get; set; }

        public string Unit { get; set; }
        public string UnitDes { get; set; }
        public string Direktorat { get; set; }
        public string DirektoratDes { get; set; }
    }
}
