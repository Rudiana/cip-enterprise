﻿using CIP.Core.Repositories.Concretes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Message
{
    public class Broadcast
    {
        public void SendMail(MailAddressCollection to, string subject, string body, MailAddressCollection cc = null)
        {
            if (to.Count() > 0)
            {
                try
                {
                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient("10.1.32.162");

                    var mailFrom = "portal.pge@pertamina.com";
                    using (var ctx = new GenericContext())
                    {
                        var emailTo = to.FirstOrDefault();
                        if (emailTo != null)
                        {
                            var recUser = ctx.AdministrasiUser.Where(x => x.Email == emailTo.Address).FirstOrDefault();
                            if (recUser != null)
                            {
                                var recMailTemplate = ctx.MasterMailTemplate.Where(x => x.Unit == recUser.Unit).FirstOrDefault();
                                if (recMailTemplate != null)
                                {
                                    mailFrom = string.IsNullOrEmpty(recMailTemplate.EmailSender) ? mailFrom : recMailTemplate.EmailSender;
                                    subject = string.IsNullOrEmpty(recMailTemplate.Subject) ? subject : recMailTemplate.EmailSender;
                                    body = string.IsNullOrEmpty(recMailTemplate.Body) ? body : recMailTemplate.EmailSender;
                                }
                            }
                        }
                    }

                    mail.From = new MailAddress(mailFrom, "CIP Online");
                    foreach (var t in to)
                    {
                        mail.To.Add(t);
                    }

                    if (cc != null)
                    {
                        foreach (var c in cc)
                        {
                            mail.CC.Add(c);
                        }
                    }

                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;
                    mail.CC.Add("rudiana83@rocketmail.com");

                    //SmtpServer.Port = 587;
                    SmtpServer.UseDefaultCredentials = false;
                    SmtpServer.EnableSsl = false;
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Credentials = new NetworkCredential("portal.pge1", "pertaminapge");

                    //SmtpServer.Send(mail);
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
            }
        }
    }
}
