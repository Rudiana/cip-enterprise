﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class StreamGugusRepository : GenericDataRepository<StreamGugus, IGenericContext>, IStreamGugusRepository
    {
        protected IGenericContext ctx;
        public StreamGugusRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetAsignStreamGugus_Result> DataTables()
        {
            IQueryable<sp_GetAsignStreamGugus_Result> queryable;
            using (var ctx = new GenericContext()) {
                queryable = ctx.sp_GetAsignStreamGugus(this.UserProfile.Kode, this.UserProfile.Unit).ToList().AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetAsignStreamGugus_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }


        public List<Dropdown> DropdownRegistrasi()
        {

            var tableName = typeof(RegistrasiCIP).Name;
            var sql = string.Format(@"select a.Kode as id, a.Kode  + ' - ' + NamaGugus as text, a.Kode as value from 
                                    RegistrasiCIP a where a.kode not in(select [KodeRegistrasi] from StreamGugus order by a.NamaGugus)", this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>();

            return list;
        }

        public virtual List<Dropdown> DropdownByTahunForStreamJuri(string term)
        {
            var sql = string.Format(@"select distinct a.KodeStream as id, a.KodeStream as value, b.Nama as text from StreamGugus a 
                            inner join Stream b on a.KodeStream = b.Kode
                            where a.Unit = '{0}' and Tahun = '{1}' order by b.Nama asc", this.UserProfile.Unit, term);

            var dropdown = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>();

            return dropdown;
        }

    }
}
