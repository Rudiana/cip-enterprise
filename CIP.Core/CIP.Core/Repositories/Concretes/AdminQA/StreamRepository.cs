﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Abstractions.AdminQA;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CIP.Core.Common;

namespace CIP.Core.Repositories.Concretes.AdminQA
{
    public class StreamDto:Stream {
        
    }

    public class StreamRepository : GenericDataRepository<Stream, IGenericContext>, IStreamRepository
    {
        protected IGenericContext ctx;
        public StreamRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public FormatedList<StreamDto> Datatables()
        {
            IQueryable<StreamDto> queryable;
                queryable = (
                    from a in ctx.Set<Stream>().Where(x => x.Unit.Equals(this.UserProfile.Unit))
                    select new StreamDto() {
                        Kode=a.Kode,
                        Nama=a.Nama,
                        StatusStream =a.StatusStream,
                        CreatedBy=a.CreatedBy,
                        CreatedDate=a.CreatedDate,
                        Tahun=a.Tahun,
                        Unit=a.Unit,
                        UpdatedBy=a.UpdatedBy,
                        UpdatedDate=a.UpdatedDate


                    }
                    ).AsQueryable();
            
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<StreamDto>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;

        }

        public MasterStdDev GetStatusKoefisien(int periode) {
            MasterStdDev stdDev;
            stdDev = ctx.Set<MasterStdDev>().SingleOrDefault(x => x.periode == periode);
            if (stdDev != null)
            {
                return stdDev;
            }
            else {
                return new MasterStdDev();
            }
        }

        public override bool Create(Stream model)
        {
            model.Kode = Guid.NewGuid().ToString().Substring(0, 20);
            model.Unit = this.UserProfile.Unit;
            return base.Create(model);
        }

        public override bool Update(Stream model)
        {
           
            model.Unit = this.UserProfile.Unit;
            return base.Update(model);
        }

        public override bool Delete(params object[] keyValues)
        {
            using(IGenericDataRepository<StreamJuri> repo = new DataRepository<StreamJuri>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                string kodeStream = keyValues.Count() > 0 ? keyValues[0].ToString() : "";
                var rec = repo.FindBy(x => x.KodeStream == kodeStream);
                if(rec.Count() > 0)
                {
                    throw new Exception("Data tidak dapat dihapus karena digunakan oleh Stream Penjurian.");
                }
            }

            using (IGenericDataRepository<StreamGugus> repo = new DataRepository<StreamGugus>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                string kodeStream = keyValues.Count() > 0 ? keyValues[0].ToString() : "";
                var rec = repo.FindBy(x => x.KodeStream == kodeStream);
                if (rec.Count() > 0)
                {
                    throw new Exception("Data tidak dapat dihapus karena digunakan oleh Stream Gugus.");
                }

            }

            return base.Delete(keyValues);
        }

        public virtual List<Dropdown> DropdownByTahun(string term)
        {
            var dropdown = ctx.Set<Stream>().ToList().Where(x=>x.Unit.Equals(this.UserProfile.Unit) && x.Tahun.ToString() == term)
                .Select(x => new Dropdown()
                {
                    id = x.Kode,
                    value = x.Kode,
                    text = x.Nama
                }).OrderBy(x => x.text).ToList();

            return dropdown;
        }
    }
}
