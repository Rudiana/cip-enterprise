﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class DeviasiKoefisienRepository : GenericDataRepository<MasterStdDev, IGenericContext>, IDeviasiKoefisienRepository
    {
        protected IGenericContext ctx;
        public DeviasiKoefisienRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<DeviasiKoefisien> DataTables()
        {
            IQueryable<DeviasiKoefisien> queryable;
            using (var ctx = new GenericContext()) {
                queryable = (from a in ctx.MasterStdDev.ToList()
                            select new DeviasiKoefisien
                            {
                                periode=a.periode,
                                
                                stdDevKoefisien=a.stdDevKoefisien,
                                
                                CreatedBy =a.CreatedBy,
                                CreatedDate=a.CreatedDate
                            }).AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<DeviasiKoefisien>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public override bool Create(MasterStdDev model)
        {
     
            return base.Create(model);
        }

       
        public override bool Update(MasterStdDev model)
        {
     
            return base.Update(model);
        }
    }
}
