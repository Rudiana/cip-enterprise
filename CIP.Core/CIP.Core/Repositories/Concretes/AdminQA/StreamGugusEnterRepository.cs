﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class StreamGugusEnterRepository : GenericDataRepository<StreamGugusEnter, IGenericContext>, IStreamGugusEnterRepository
    {
        protected IGenericContext ctx;
        public StreamGugusEnterRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetAsignStreamGugusEnter_Result> DataTables()
        {
            IQueryable<sp_GetAsignStreamGugusEnter_Result> queryable;
            using (var ctx = new GenericContext()) {
                queryable = ctx.sp_GetAsignStreamGugusEnter(this.UserProfile.Kode).ToList().AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetAsignStreamGugusEnter_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }


       
        public List<Dropdown> DropdownRegistrasiDir()
        {

            var tableName = typeof(RegistrasiCIP).Name;
            var sql = string.Format(@"select a.Kode as id, b.CipDirektorat  + ' - ' + NamaGugus as text, a.Kode as value from 
                                    RegistrasiCIP a join AsignCipDir b on a.KodeRegistrasi=b.KodeRegistrasi where a.kode not in(select [KodeRegistrasi] from StreamGugusEnter)", this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>();

            return list;
        }
        public List<Dropdown> DropdownForStreamGugus(int tahun)
        {
            var gugusInStream = ctx.Set<StreamGugusEnter>().Select(x => x.CipEnterprise).ToList();

            var status = BaseEnums.EnumRegistrationStatusEnterprise.Ready_to_Show_Enterprise.ReplaceUnderScoreToSpace();
            var dropdowns = (from a in (ctx.Set<AsignCipEnter>().Where(x =>
                                     x.Tahun == tahun &&
                                  x.Status == status &&
                                  !gugusInStream.Contains(x.CipEnterprise) &&
                                  x.Direktorat == (this.UserProfile.IsAdministrator ? x.Direktorat : this.UserProfile.Direktorat)))
                             select new Dropdown()
                             {
                                 id = a.CipEnterprise,
                                 value = a.CipEnterprise,
                                 text = a.NamaGugus
                             }).ToList();

            return dropdowns;
        }

        public virtual List<Dropdown> DropdownByTahunForStreamJuri(string term)
        {
            var dropdown = (from a in ctx.Set<StreamGugusEnter>().ToList()
                            join b in ctx.Set<StreamEnter>().ToList() on a.KodeStream equals b.Kode
                            where b.Tahun.ToString() == term
                            orderby b.Nama ascending
                            select new Dropdown()
                            {
                                id = a.KodeStream,
                                value = a.KodeStream,
                                text = b != null ? b.Nama : ""
                            }).OrderBy(x => x.text).ToList();

            return dropdown;
        }
    }
}
