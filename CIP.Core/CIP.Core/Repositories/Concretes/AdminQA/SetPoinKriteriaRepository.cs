﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Abstractions.AdminQA;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CIP.Core.Common;

namespace CIP.Core.Repositories.Concretes.AdminQA
{
    public class SetPoinKriteriaRepository : GenericDataRepository<TemplateAuditPDCAPoin, IGenericContext>, ISetPoinKriteriaRepository
    {
        protected IGenericContext ctx;
        public SetPoinKriteriaRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public override bool Create(TemplateAuditPDCAPoin model)
        {
            return base.Create(model);
        }


        public bool CopyCreateRange(int periode)
        {
            List<sp_GetTemplatePlan_Result> model = null;
            var Result = false;
            using (var ctx=new GenericContext())
            {
                model = ctx.sp_GetTemplatePlan(periode-1).ToList();
                Result= this.Create(model, periode);
            }
           return Result;
        }

        private bool Create(List<sp_GetTemplatePlan_Result> model, int periode) {
            var Result = false;
            try
            {
                using (var ctx = new GenericContext())
                {
                    foreach (sp_GetTemplatePlan_Result itm in model)
                    {

                        TemplateAuditPDCAPoin poin = null;
                        poin = ctx.TemplateAuditPDCAPoin.FirstOrDefault(x => x.tahun == (int?)periode &&
                        x.KodeTemplate.Equals(itm.KodeTemplate) && x.KodeJenis.Equals(itm.KodeJenis));

                        if (poin == null)
                        {
                            poin = new TemplateAuditPDCAPoin()
                            {
                                KodeKriteria = itm.KodeKriteria,
                                KodeTemplate = itm.KodeTemplate,
                                Kriteria = itm.Kriteria,
                                Langkah = itm.Langkah,
                                KodeJenis = itm.KodeJenis,
                                KodeLangkah=itm.KodeLangkah,
                                Poin = itm.Poin,
                                tahun = periode,
                                RequermentTerbaik = itm.RequermentTerbaik,
                                PenilaianSkala = itm.PenilaianSkala,
                                NoUrut = itm.NoUrut,
                                Used = itm.Used
                            };

                            ctx.TemplateAuditPDCAPoin.Add(poin);
                        }
                        else
                        {

                            poin.KodeKriteria = itm.KodeKriteria;
                            poin.KodeTemplate = itm.KodeTemplate;
                            poin.Kriteria = itm.Kriteria;
                            poin.Langkah = itm.Langkah;
                            poin.KodeJenis = itm.KodeJenis;
                            poin.Poin = itm.Poin;
                            poin.tahun = periode;
                            poin.RequermentTerbaik = itm.RequermentTerbaik;
                            poin.PenilaianSkala = itm.PenilaianSkala;
                            poin.NoUrut = itm.NoUrut;
                            poin.Used = itm.Used;
                        }
                    }

                    Result = ctx.SaveChanges() >= 0;
                }
                return Result;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public bool CreateRange(SetPoinLangkah model,int periode)
        {
            return this.Create(model.LangkahLangkah, periode);
        }

        public virtual List<Dropdown> DropdownByTahun()
        {
            var dropdown = ctx.Set<MasterStdDev>().ToList()
                .Select(x => new Dropdown()
                {
                    id = x.periode.ToString(),
                    value = x.periode.ToString(),
                    text = x.periode.ToString()
                }).ToList();

            return dropdown;
        }

        public SetPoinLangkah GetLangkahLangkah(int periode) {
            SetPoinLangkah result =new SetPoinLangkah();
            List<sp_GetTemplatePlan_Result> template =new List<sp_GetTemplatePlan_Result>();
            using (var ctx=new GenericContext())
            {
                template= ctx.sp_GetTemplatePlan(periode).ToList();
                if (template != null)
                {
                    List<string> jenisCIP = template.Select(x => x.KodeJenis).Distinct().ToList();
                    //List<MasterJenisCIP> ls = new List<MasterJenisCIP>();
                    //foreach (string idx in jenisCIP)
                    //{

                    //    MasterJenisCIP jenis =ctx.MasterJenisCIP.FirstOrDefault(x=>x.Kode.Equals(idx));
                    //    if (jenis != null)
                    //        ls.Add(jenis);
                    //}

                    var ls = (from a in jenisCIP
                              join b in ctx.MasterJenisCIP on new { Kode = a } equals new { Kode = b.Kode }
                              select b).OrderBy(x => x.NoUrut).ToList();

                    result.JenisCIP = ls;
                    result.LangkahLangkah=template;

                }
            }

            

            return result;
        }

    }
}
