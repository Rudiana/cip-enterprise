﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class StreamGugusDirRepository : GenericDataRepository<StreamGugusDir, IGenericContext>, IStreamGugusDirRepository
    {
        protected IGenericContext ctx;
        public StreamGugusDirRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetAsignStreamGugusDir_Result> DataTables()
        {
            IQueryable<sp_GetAsignStreamGugusDir_Result> queryable;
            using (var ctx = new GenericContext()) {
                queryable = ctx.sp_GetAsignStreamGugusDir(this.UserProfile.Kode, this.UserProfile.Direktorat).ToList().AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetAsignStreamGugusDir_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }


        public List<Dropdown> DropdownRegistrasi()
        {
             var sql = string.Format(@"select a.CipDirektorat as id, a.CipDirektorat  + ' - ' + NamaGugus as text, a.CipDirektorat as value from 
                                    AsignCipDir a where a.CipDirektorat not in(select [CipDirektorat] from StreamGugusDir)");
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>();

            return list;
        }

        public List<Dropdown> DropdownForStreamGugus(int tahun)
        {
            var gugusInStream = ctx.Set<StreamGugusDir>().Select(x => x.CipDirektorat).ToList();

            var status = BaseEnums.EnumRegistrationStatusDirektorat.Ready_to_Show_Direktorat.ReplaceUnderScoreToSpace();
            var dropdowns =(from a in (ctx.Set<AsignCipDir>().Where(x =>
                                    x.Tahun == tahun &&
                                 x.Status == status &&
                                 !gugusInStream.Contains(x.CipDirektorat) &&
                                 x.Direktorat == (this.UserProfile.IsAdministrator ? x.Direktorat : this.UserProfile.Direktorat)))
                            select new Dropdown()
                                    {
                                        id =a.CipDirektorat,
                                        value = a.CipDirektorat,
                                        text = a.NamaGugus
                            }).ToList();

            return dropdowns;
        }


        public virtual List<Dropdown> DropdownByTahunForStreamJuri(string term)
        {
            var dropdown = (from a in ctx.Set<StreamGugusDir>().ToList()
                            join b in ctx.Set<StreamDir>().ToList() on a.KodeStream equals b.Kode
                            where a.Direktorat == this.UserProfile.Direktorat && b.Tahun.ToString() == term
                            orderby b.Nama ascending
                            select new Dropdown()
                            {
                                id = a.KodeStream,
                                value = a.KodeStream,
                                text = b != null ? b.Nama : ""
                            }).OrderBy(x => x.text).ToList();

            return dropdown;
        }

    }
}
