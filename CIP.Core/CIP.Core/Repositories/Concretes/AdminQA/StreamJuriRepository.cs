﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class StreamJuriRepository : GenericDataRepository<StreamJuri, IGenericContext>, IStreamJuriRepository
    {
        protected IGenericContext ctx;
        public StreamJuriRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetAsignStreamJuri_Result> DataTables()
        {
            IQueryable<sp_GetAsignStreamJuri_Result> queryable;
            using (var ctx = new GenericContext()) {
                queryable = ctx.sp_GetAsignStreamJuri(this.UserProfile.Kode, this.UserProfile.Unit).ToList().AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetAsignStreamJuri_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public List<Dropdown> DropdownTahun()
        {
            var sql = string.Format("select distinct convert(varchar(4),tahun) as id, convert(varchar(4),tahun)  as text, convert(varchar(4),tahun) as [value] from Stream where unit='{0}' order by text asc",this.UserProfile.Unit);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>().OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownTahunPenjurian()
        {
            var sql = string.Format("exec sp_GetDropdownTahunPenjurian '{0}'", this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStream(int tahun)
        {
            var sql = string.Format("select distinct Kode as id,Nama  as text, Kode as [value] from Stream where tahun=@p and unit='{0}' order by text asc",this.UserProfile.Unit);
            var list = ctx.Database.SqlQuery<Dropdown>(sql, new SqlParameter("@p", tahun)).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamByJuri(int tahun)
        {
            var sql = string.Format("exec sp_GetDropdownStreamByJuri {0}, '{1}'", tahun, this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamGugusAvail(int tahun, string stream)
        {
            var sql = string.Format("select KodeRegistrasi as id, KodeRegistrasi as [value], NamaGugus as text from stream s " +
                "join streamgugus sg on s.kode = sg.kodestream " +
                "join registrasicip r on r.kode = sg.koderegistrasi " +
                "where s.tahun={0} and sg.kodestream = '{1}' and s.unit='{2}' order by text asc", tahun, stream,this.UserProfile.Unit);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamGugusDocking(int tahun, string stream)
        {
            var status = string.Format("'{0}','{1}','{2}'", BaseEnums.EnumRegistrationStatus.Ready_to_Show.ReplaceUnderScoreToSpace(), 
                BaseEnums.EnumRegistrationStatus.Forum_Presentasi_CIP.ReplaceUnderScoreToSpace(), BaseEnums.EnumRegistrationStatus.Result_Forum_CIP.ReplaceUnderScoreToSpace());
            var sql = string.Format("select KodeRegistrasi as id, KodeRegistrasi as [value], NamaGugus as text from stream s " +
                "join streamgugus sg on s.kode = sg.kodestream " +
                "join registrasicip r on r.kode = sg.koderegistrasi and Status in(" + status  + ")" +
                "where s.tahun={0} and sg.kodestream = '{1}' order by text asc", tahun, stream);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownJenisCip()
        {
            var sql = string.Format("select Kode as id,Deskripsi as text, Kode as [value] from MasterJenisCIP order by NoUrut asc");
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();
            //list.Add(new Dropdown() { id = "%", text = "All", value = "%" });
            return list;
        }
    }
}
