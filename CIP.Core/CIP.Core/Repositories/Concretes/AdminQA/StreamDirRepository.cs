﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Abstractions.AdminQA;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CIP.Core.Common;
using CIP.Core.Repositories.Concretes;

namespace CIP.Core.Repositories.Concretes.AdminQA
{
    public class StreamDirRepository : GenericDataRepository<StreamDir, IGenericContext>, IStreamDirRepository
    {
        protected IGenericContext ctx;
        public StreamDirRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public FormatedList<StreamDir> Datatables()
        {
            IQueryable<StreamDir> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.StreamDir.Where(x => x.Direktorat.Equals(this.UserProfile.Direktorat)).AsQueryable();
            }
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<StreamDir>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;

        }

        public override bool Create(StreamDir model)
        {
            model.Kode = Guid.NewGuid().ToString().Substring(0, 20);
            model.Direktorat = this.UserProfile.Direktorat;
            return base.Create(model);
        }

        public override bool Delete(params object[] keyValues)
        {
            using (IGenericDataRepository<StreamJuriDir> repo = new DataRepository<StreamJuriDir>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                string kodeStream = keyValues.Count() > 0 ? keyValues[0].ToString() : "";
                var rec = repo.FindBy(x => x.KodeStream == kodeStream);
                if (rec.Count() > 0)
                {
                    throw new Exception("Data tidak dapat dihapus karena digunakan oleh Stream Penjurian.");
                }
            }

            using (IGenericDataRepository<StreamGugusDir> repo = new DataRepository<StreamGugusDir>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                string kodeStream = keyValues.Count() > 0 ? keyValues[0].ToString() : "";
                var rec = repo.FindBy(x => x.KodeStream == kodeStream);
                if (rec.Count() > 0)
                {
                    throw new Exception("Data tidak dapat dihapus karena digunakan oleh Stream Gugus.");
                }

            }

            return base.Delete(keyValues);
        }

        public virtual List<Dropdown> DropdownByTahun(string term)
        {
            var dropdown = ctx.Set<StreamDir>().ToList().Where(x=>x.Direktorat.Equals(this.UserProfile.Direktorat) && x.Tahun.ToString() == term)
                .Select(x => new Dropdown()
                {
                    id = x.Kode,
                    value = x.Kode,
                    text = x.Nama
                }).OrderBy(x => x.text).ToList();

            return dropdown;
        }

    }
}
