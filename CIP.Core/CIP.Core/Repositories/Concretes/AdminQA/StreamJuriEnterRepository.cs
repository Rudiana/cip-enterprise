﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class StreamJuriEnterRepository : GenericDataRepository<StreamJuriEnter, IGenericContext>, IStreamJuriEnterRepository
    {
        protected IGenericContext ctx;
        public StreamJuriEnterRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetAsignStreamJuriEnter_Result> DataTables()
        {
            IQueryable<sp_GetAsignStreamJuriEnter_Result> queryable;
            using (var ctx = new GenericContext()) {
                queryable = ctx.sp_GetAsignStreamJuriEnter().ToList().AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetAsignStreamJuriEnter_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public List<Dropdown> DropdownTahun()
        {
            var sql = string.Format("select distinct convert(varchar(4),tahun) as id, convert(varchar(4),tahun)  as text, convert(varchar(4),tahun) as [value] from StreamEnter order by text asc");
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>().OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownTahunPenjurian()
        {
            var sql = string.Format("exec sp_GetDropdownTahunPenjurianEnter '{0}'", this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStream(int tahun)
        {
            var sql = string.Format("select distinct Kode as id,Nama  as text, Kode as [value] from StreamEnter where tahun=@p order by text asc");
            var list = ctx.Database.SqlQuery<Dropdown>(sql, new SqlParameter("@p", tahun)).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamByJuri(int tahun)
        {
            var sql = string.Format("exec sp_GetDropdownStreamByJuriEnter {0}, '{1}'", tahun, this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamGugusDocking(int tahun, string stream)
        {
            var status = string.Format("'{0}','{1}','{2}'", BaseEnums.EnumRegistrationStatusEnterprise.Ready_to_Show_Enterprise.ReplaceUnderScoreToSpace(),
                BaseEnums.EnumRegistrationStatusEnterprise.Forum_Presentasi_Enterprise.ReplaceUnderScoreToSpace(), 
                BaseEnums.EnumRegistrationStatusEnterprise.Result_Forum_Enterprise.ReplaceUnderScoreToSpace());
            var sql = string.Format("select sg.CipEnterprise as id, sg.CipEnterprise as [value], r.NamaGugus as text from streamEnter s " +
                "join streamgugusEnter sg on s.kode = sg.kodestream " +
                "join AsignCipEnter r on r.CipEnterprise = sg.CipEnterprise and Status in(" + status + ")" +
                "where s.tahun={0} and sg.kodestream = '{1}' order by text asc", tahun, stream);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamGugusAvail(int tahun, string stream)
        {
            var sql = string.Format("select CipEnterprise as id, CipEnterprise as [value], NamaGugus as text from streamEnter s " +
                "join streamgugus sg on s.kode = sg.kodestream " +
                "join registrasicip r on r.kode = sg.koderegistrasi " +
                "where s.tahun={0} and sg.kodestream = '{1}' order by text asc", tahun, stream);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownJenisCip()
        {
            var sql = string.Format("select distinct Kode as id,Deskripsi as text, Kode as [value] from MasterJenisCIP order by Deskripsi asc");
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();
            //list.Add(new Dropdown() { id = "%", text = "All", value = "%" });
            return list;
        }
    }
}
