﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class StreamJuriDirRepository : GenericDataRepository<StreamJuriDir, IGenericContext>, IStreamJuriDirRepository
    {
        protected IGenericContext ctx;
        public StreamJuriDirRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetAsignStreamJuriDir_Result> DataTables()
        {
            IQueryable<sp_GetAsignStreamJuriDir_Result> queryable;
            using (var ctx = new GenericContext()) {
                queryable = ctx.sp_GetAsignStreamJuriDir(this.UserProfile.Kode, this.UserProfile.Direktorat).ToList().AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetAsignStreamJuriDir_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public List<Dropdown> DropdownTahun()
        {
            var sql = string.Format("select distinct convert(varchar(4),tahun) as id," +
                " convert(varchar(4),tahun)  as text, convert(varchar(4),tahun) as [value] from StreamDir where direktorat='{0}' order by text asc",this.UserProfile.Direktorat);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>().OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownTahunPenjurian()
        {
            var sql = string.Format("exec sp_GetDropdownTahunPenjurianDir '{0}'", this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStream(int tahun)
        {
            var sql = string.Format("select distinct Kode as id,Nama  as text, Kode as [value] from StreamDir where tahun=@p and direktorat='{0}' order by text asc", this.UserProfile.Direktorat);
            var list = ctx.Database.SqlQuery<Dropdown>(sql, new SqlParameter("@p", tahun)).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamByJuri(int tahun)
        {
            var sql = string.Format("exec sp_GetDropdownStreamByJuriDir {0}, '{1}'", tahun, this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamGugusAvail(int tahun, string stream)
        {
            var sql = string.Format("select sg.CipDirektorat as id, sg.CipDirektorat as [value], NamaGugus as text from streamDir s "
                +"join streamgugusDir sg on s.kode = sg.kodestream "
                +"join AsignCipDir r on r.CipDirektorat = sg.CipDirektorat " +
                "where r.tahun={0} and sg.kodestream = '{1}' order by text asc", tahun, stream);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownStreamGugusDocking(int tahun, string stream)
        {
            var status = string.Format("'{0}','{1}','{2}'", BaseEnums.EnumRegistrationStatusDirektorat.Ready_to_Show_Direktorat.ReplaceUnderScoreToSpace(),
                BaseEnums.EnumRegistrationStatusDirektorat.Forum_Presentasi_Direktorat.ReplaceUnderScoreToSpace(), BaseEnums.EnumRegistrationStatusDirektorat.Result_Forum_Direktorat.ReplaceUnderScoreToSpace());
            var sql = string.Format("select sg.CipDirektorat as id, sg.CipDirektorat as [value], r.NamaGugus as text from streamDir s " +
                "join streamgugusDir sg on s.kode = sg.kodestream " +
                "join AsignCipDir r on r.CipDirektorat = sg.CipDirektorat and Status in(" + status + ")" +
                "where s.tahun={0} and sg.kodestream = '{1}' order by text asc", tahun, stream);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();

            return list;
        }

        public List<Dropdown> DropdownJenisCip()
        {
            var sql = string.Format("select distinct Kode as id,Deskripsi as text, Kode as [value] from MasterJenisCIP order by Deskripsi asc");
            var list = ctx.Database.SqlQuery<Dropdown>(sql).OrderBy(x => x.text).ToList();
            //list.Add(new Dropdown() { id = "%", text = "All", value = "%" });
            return list;
        }
    }
}
