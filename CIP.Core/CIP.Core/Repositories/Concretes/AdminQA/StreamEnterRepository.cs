﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Abstractions.AdminQA;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CIP.Core.Common;
using CIP.Core.Repositories.Concretes;

namespace CIP.Core.Repositories.Concretes.AdminQA
{
    public class StreamEnterRepository : GenericDataRepository<StreamEnter, IGenericContext>, IStreamEnterRepository
    {
        protected IGenericContext ctx;
        public StreamEnterRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public FormatedList<StreamEnter> Datatables()
        {
            IQueryable<StreamEnter> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.StreamEnter.AsQueryable();
            }
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<StreamEnter>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;

        }

        public override bool Create(StreamEnter model)
        {
            model.Kode = Guid.NewGuid().ToString().Substring(0, 20);
            return base.Create(model);
        }

        public override bool Delete(params object[] keyValues)
        {
            using (IGenericDataRepository<StreamJuriEnter> repo = new DataRepository<StreamJuriEnter>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                string kodeStream = keyValues.Count() > 0 ? keyValues[0].ToString() : "";
                var rec = repo.FindBy(x => x.KodeStream == kodeStream);
                if (rec.Count() > 0)
                {
                    throw new Exception("Data tidak dapat dihapus karena digunakan oleh Stream Penjurian.");
                }
            }

            using (IGenericDataRepository<StreamGugusEnter> repo = new DataRepository<StreamGugusEnter>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                string kodeStream = keyValues.Count() > 0 ? keyValues[0].ToString() : "";
                var rec = repo.FindBy(x => x.KodeStream == kodeStream);
                if (rec.Count() > 0)
                {
                    throw new Exception("Data tidak dapat dihapus karena digunakan oleh Stream Gugus.");
                }

            }

            return base.Delete(keyValues);
        }

        public virtual List<Dropdown> DropdownByTahun(string term)
        {
            var dropdown = ctx.Set<StreamEnter>().ToList()
                .Where(x => x.Tahun.ToString() == term)
                .Select(x => new Dropdown()
                {
                    id = x.Kode,
                    value = x.Kode,
                    text = x.Nama
                }).OrderBy(x => x.text).ToList();

            return dropdown;
        }

    }
}
