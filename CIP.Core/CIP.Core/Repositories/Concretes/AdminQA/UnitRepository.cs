﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Abstractions.AdminQA;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CIP.Core.Common;

namespace CIP.Core.Repositories.Concretes.AdminQA
{
    public class UnitRepository : GenericDataRepository<MasterUnit, IGenericContext>, IUnitRepository
    {
        protected IGenericContext ctx;
        public UnitRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<MasterUnitDto> DataTables()
        {
            IQueryable<MasterUnitDto> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = (from a in ctx.MasterUnit.ToList()
                            join b in ctx.MasterDirektorat.ToList() on a.Direktorat equals b.Kode
                            into c
                            from result in c.DefaultIfEmpty()
                            select new MasterUnitDto
                            {
                                Kode=a.Kode,
                                Deskripsi=a.Deskripsi,
                                Direktorat=a.Direktorat,
                                DirektoratName= result != null ? result.Deskripsi : "",
                                CreatedBy=a.CreatedBy,
                                CreatedDate=a.CreatedDate,
                                UpdatedBy=a.UpdatedBy,
                                CompanyCode=a.CompanyCode,
                                UpdatedDate=a.UpdatedDate

                            }).AsQueryable();
            }
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<MasterUnitDto>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }
    }
}
