﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Repositories.Abstractions;

namespace CIP.Core.Repositories.Concretes
{
    public class DataRepository<T> : GenericDataRepository<T, IGenericContext>
        where T : class
    {
        protected new IGenericContext ctx;
        
        public DataRepository(IGenericContext _ctx)
            : base(_ctx)
        {
            this.ctx = _ctx;
        }
    }
}
