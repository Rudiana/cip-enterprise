﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
   
    public class PenjurianEnterRepository : GenericDataRepository<PenjurianHeaderEnter, IGenericContext>, IPenjurianEnterRepository
    {
        private Dictionary<string, int?[]> Data;
        protected IGenericContext ctx;
        public PenjurianEnterRepository(IGenericContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

        public bool SaveKoef(List<KoefStream> KoefStream,string JenisCip) {
            List<StreamKoefisienEnter> koefs = new List<StreamKoefisienEnter>();
            KoefStream.ForEach(x =>
            {
                StreamKoefisienEnter koe = new StreamKoefisienEnter();
                koe=ctx.Set<StreamKoefisienEnter>().SingleOrDefault(i => i.KodeStream.Equals(x.KodeStream) && i.JenisCip.Equals(JenisCip));
                koe.Koefiesien = x.Koefisien;
                koefs.Add(koe);
            });
            return ctx.SaveChanges()>0;
        }

        public bool ResetKoef(List<KoefStream> KoefStream, string JenisCip)
        {
            KoefStream.ForEach(x =>
            {
                StreamKoefisienEnter koe = new StreamKoefisienEnter();
                koe = ctx.Set<StreamKoefisienEnter>().SingleOrDefault(i => i.KodeStream.Equals(x.KodeStream) && i.JenisCip.Equals(JenisCip));
                                ctx.Entry(koe).State= System.Data.Entity.EntityState.Deleted;
            });    
            return ctx.SaveChanges() > 0;
        }


        public List<ValueCreationEnter> GetValueCreation(int tahun,string stream) {
            List<ValueCreationEnter> result = new List<ValueCreationEnter>();
            result = ctx.Set<ValueCreationEnter>().Where(x => x.Tahun == tahun && x.Direktorat.Equals(this.UserProfile.Direktorat) && x.KodeStream.Equals(stream)).ToList();
            return result;
        }

        public DataSet GetDockingPenjurian(int tahun, string gugus)
        {
            using (var ctx = new GenericContext())
            {
                var sql = string.Format("exec sp_GetDockingInternalNewEnter {0}, '{1}'", tahun, gugus);
                using (var connection = ctx.Database.Connection)
                {
                    connection.Open();
                    var command = connection.CreateCommand() as SqlCommand;
                    command.CommandText = sql;

                    var ds = new DataSet();
                    var da = new SqlDataAdapter(command);
                    da.Fill(ds);

                    return ds;
                }

            }

        }

       
        private void SetStreamKoefisien(int tahun, string KodeStream, ref double Koefisien, double StdDev,string JenisCip)
        {
            using (var ctx = new GenericContext())
            {
                StreamKoefisienEnter KoefG = new StreamKoefisienEnter();
                KoefG = ctx.StreamKoefisienEnter.FirstOrDefault(x => x.Tahun == (int?)tahun && x.KodeStream.Equals(KodeStream) && x.JenisCip.Equals(JenisCip));
                if (KoefG != null)
                {
                    Koefisien =(double) KoefG.Koefiesien;
                }
                else
                {
                    KoefG = new StreamKoefisienEnter()
                    {
                        KodeStream = KodeStream,
                        StdDev = StdDev,
                        Tahun = tahun,
                
                        JenisCip = JenisCip,
                        Koefiesien = Koefisien
                    };

                    ctx.StreamKoefisienEnter.Add(KoefG);
                    ctx.SaveChanges();
                }

            }
        }

        private void SetStreamKoefisienEnter(int tahun, string KodeStream, ref double Koefisien, double StdDev, string JenisCip)
        {
            using (var ctx = new GenericContext())
            {
                StreamKoefisienEnter KoefG = new StreamKoefisienEnter();
                KoefG = ctx.StreamKoefisienEnter.FirstOrDefault(x => x.Tahun == (int?)tahun && x.KodeStream.Equals(KodeStream) && x.JenisCip.Equals(JenisCip));
                if (KoefG != null)
                {
                    Koefisien = (double)KoefG.Koefiesien;
                }
                else
                {
                    KoefG = new StreamKoefisienEnter()
                    {
                        KodeStream = KodeStream,
                        StdDev = StdDev,
                        Tahun = tahun,
                        
                        JenisCip = JenisCip,
                        Koefiesien = Koefisien
                    };

                    ctx.StreamKoefisienEnter.Add(KoefG);
                    ctx.SaveChanges();
                }

            }
        }
        public double? GetDefaultStdDev()
        {
            double? res = 0;
            using (var ctx = new GenericContext())
            {
                List<StreamKoefisienEnter> koefStd = ctx.StreamKoefisienEnter.OrderBy(x => x.Tahun).ToList();
                if (koefStd.Count > 0)
                {
                    res = koefStd[0].StdDev;
                }
                else { res = 5; }
            }
            return res;
        }

       

        public Koef GetKoef(int tahun, double stdDev, string direktorat,string KodeJenisCip)
        {
            Koef koef = new Koef();
            try
            {
                using (var ctx = new GenericContext())
                {
                    List<sp_GetKoefEnter_Result> stream = new List<sp_GetKoefEnter_Result>();
                    
                    List<KoefStream> kStream = new List<KoefStream>();
                    List<KoefSummary> kSummary = new List<KoefSummary>();
                    List<sp_GetKoefEnter_Result> totStream = ctx.sp_GetKoefEnter(tahun, direktorat, KodeJenisCip).ToList();
                    KoefMatrixModel koefMatrix = MatrixKoef.CalculateKoef(totStream, stdDev);
                    double[,] matrix = koefMatrix.Summary;
                    var row = matrix.GetLength(0);
                    var col = matrix.GetLength(1);
                    var rowStrean = totStream.Count();
                    int i = 1;

                    for (int rw = 0; rw < rowStrean; rw++)
                    {
                        double KoefValue = koefMatrix.Koefisien.FirstOrDefault(x => x.KodeStream.Equals(totStream[rw].KodeStream)).Nilai;
                        double refKoef = double.IsInfinity(KoefValue) ? 0 : double.IsNaN(KoefValue) ? 0 : KoefValue;
                        SetStreamKoefisienEnter(tahun, totStream[rw].KodeStream, ref refKoef, stdDev, KodeJenisCip);
                        KoefStream s = new KoefStream()
                        {
                            KodeStream= totStream[rw].KodeStream,
                            Title = totStream[rw].stream,
                            No = i,
                            Awal = Math.Round(matrix[rw, 0], 2),
                            Kal1 = Math.Round(matrix[rw, 1], 2),
                            Kal2 = Math.Round(matrix[rw, 2], 2),
                            Kal3 = Math.Round(matrix[rw, 3], 2),
                            Kal4 = Math.Round(matrix[rw, 4], 2),
                            Kal5 = Math.Round(matrix[rw, 5], 2),
                            Final = Math.Round(matrix[rw, 6], 2),
                            Koefisien = Math.Round(refKoef, 2)
                        };



                        i++;
                        kStream.Add(s);
                    }

                    KoefSummary v = new KoefSummary()
                    {
                        Title = "Rata-rata",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean, 5], 2),
                        Final = Math.Round(matrix[rowStrean, 6], 2)
                    };
                    kSummary.Add(v);

                    v = new KoefSummary()
                    {
                        Title = "Range",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean + 1, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean + 1, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean + 1, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean + 1, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean + 1, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean + 1, 5], 2),
                        Final = Math.Round(matrix[rowStrean + 1, 6], 2)
                    };
                    kSummary.Add(v);

                    v = new KoefSummary()
                    {
                        Title = "Deviasi",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean + 2, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean + 2, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean + 2, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean + 2, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean + 2, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean + 2, 5], 2),
                        Final = Math.Round(matrix[rowStrean + 2, 6], 2)
                    };
                    kSummary.Add(v);

                    v = new KoefSummary()
                    {
                        Title = "Max",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean + 3, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean + 3, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean + 3, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean + 3, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean + 3, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean + 3, 5], 2),
                        Final = Math.Round(matrix[rowStrean + 3, 6], 2)
                    };
                    kSummary.Add(v);

                    v = new KoefSummary()
                    {
                        Title = "Min",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean + 4, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean + 4, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean + 4, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean + 4, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean + 4, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean + 4, 5], 2),
                        Final = Math.Round(matrix[rowStrean + 4, 6], 2)
                    };
                    kSummary.Add(v);

                    v = new KoefSummary()
                    {
                        Title = "(Max)-(Rata-rata)",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean + 5, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean + 5, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean + 5, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean + 5, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean + 5, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean + 5, 5], 2),
                        Final = Math.Round(matrix[rowStrean + 5, 6], 2)
                    };
                    kSummary.Add(v);

                    v = new KoefSummary()
                    {
                        Title = "(Rata-rata)-(Min)",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean + 6, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean + 6, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean + 6, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean + 6, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean + 6, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean + 6, 5], 2),
                        Final = Math.Round(matrix[rowStrean + 6, 6], 2)
                    };
                    kSummary.Add(v);

                    v = new KoefSummary()
                    {
                        Title = "Max Absolute",
                        No = i,
                        Awal = Math.Round(matrix[rowStrean + 7, 0], 2),
                        Kal1 = Math.Round(matrix[rowStrean + 7, 1], 2),
                        Kal2 = Math.Round(matrix[rowStrean + 7, 2], 2),
                        Kal3 = Math.Round(matrix[rowStrean + 7, 3], 2),
                        Kal4 = Math.Round(matrix[rowStrean + 7, 4], 2),
                        Kal5 = Math.Round(matrix[rowStrean + 7, 5], 2),
                        Final = Math.Round(matrix[rowStrean + 7, 6], 2)
                    };
                    kSummary.Add(v);

                    //status koreksi
                    KoefKoreksi koreksi = new KoefKoreksi();

                    koreksi.No = 9;
                    koreksi.Title = "Koreksi";
                    koreksi.Awal = (matrix[rowStrean + 8, 0] == Math.Round((double)MapStrValue.Max, 2) ? "Max" : "Min");
                    koreksi.Kal1 = (matrix[rowStrean + 8, 1] == Math.Round((double)MapStrValue.Max, 2) ? "Max" : "Min");
                    koreksi.Kal2 = (matrix[rowStrean + 8, 2] == Math.Round((double)MapStrValue.Max, 2) ? "Max" : "Min");
                    koreksi.Kal3 = (matrix[rowStrean + 8, 3] == Math.Round((double)MapStrValue.Max, 2) ? "Max" : "Min");
                    koreksi.Kal4 = (matrix[rowStrean + 8, 4] == Math.Round((double)MapStrValue.Max, 2) ? "Max" : "Min");
                    koreksi.Kal5 = (matrix[rowStrean + 8, 5] == Math.Round((double)MapStrValue.Max, 2) ? "Max" : "Min");
                    koreksi.Final = (matrix[rowStrean + 8, 6] == Math.Round((double)MapStrValue.Max, 2) ? "Max" : "Min");
                    koef.Stream = kStream;
                    koef.Summary = kSummary;
                    koef.Koreksi = koreksi;

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

            return koef;
        }

        public FormatedList<sp_GetSummaryEnter_Result> GetSummary(int tahun, string direktorat)
        {
            IQueryable<sp_GetSummaryEnter_Result> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.sp_GetSummaryEnter(tahun, direktorat).ToList().AsQueryable();
            }

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetSummaryEnter_Result>(wrapper, queryable);
            var datatable = parser.Parse();


            return datatable;
        }

        public virtual FormatedList<sp_GetSummaryBreakdownEnter_Result> GetSummaryBreakdown(int tahun, string kodeRegistrasi)
        {
            IQueryable<sp_GetSummaryBreakdownEnter_Result> queryable;
            //queryable = ctx.Database.SqlQuery<sp_GetSummaryBreakdown_Result>("exec sp_GetSummaryBreakdown {0}, '{1}'", tahun, kodeRegistrasi).AsQueryable();

            //var list = queryable.ToList();
            using (var ctx = new GenericContext())
            {
                queryable = ctx.sp_GetSummaryBreakdownEnter(tahun, kodeRegistrasi).ToList().AsQueryable();
            }

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetSummaryBreakdownEnter_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public new FormatedList<sp_GetPenjurianHeaderEnter_Result> DataTables()
        {
            var statusRegistrasi = BaseEnums.EnumRegistrationStatusEnterprise.Ready_to_Show_Enterprise.ReplaceUnderScoreToSpace();
            IQueryable<sp_GetPenjurianHeaderEnter_Result> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.sp_GetPenjurianHeaderEnter(this.UserProfile.Kode, statusRegistrasi).ToList().AsQueryable();
            }
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetPenjurianHeaderEnter_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public bool ResetPenjurian(int kodePenjurian)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var header = base.GetSingle(kodePenjurian);
                    if (header != null)
                    {
                        bool res = false;

                        var recDetails = ctx.Set<PenjurianDetailEnter>().Where(x => x.KodePenjurianHeader == kodePenjurian);
                        foreach (var item in recDetails)
                        {
                            item.Skala = 0;
                            item.Nilai = 0;
                            res = ctx.SaveChanges() > 0;
                        }
                    }
                    else
                    {
                        throw new Exception("Data Penjurian tidak ditemukan.");
                    }

                    trans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool UpdatePenjurian(PenjurianHeaderEnter modelHeader, List<sp_GetPenjurianDetailEnter_Result> modelDetail)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var header = base.GetSingle(modelHeader.Kode);
                    if (header != null)
                    {

                        foreach (sp_GetPenjurianDetailEnter_Result item in modelDetail)
                        {
                            var dataDetail = ctx.Set<PenjurianDetailEnter>().Where(x => x.Kode == item.Kode).FirstOrDefault();
                            dataDetail.Point = item.Point;
                            dataDetail.RequirmentTerbaik = item.RequirmentTerbaik;
                            dataDetail.PenilainSkala = item.PenilainSkala;
                            dataDetail.Skala = item.Skala;
                            dataDetail.Nilai = item.Nilai;
                        }

                        ctx.SaveChanges();
                    }

                    //Save SOFI
                    var dataSofi = modelDetail.Where(x => x.idx == 1).ToList();
                    if (dataSofi.Count() > 0)
                    {
                        var kodeHeadrs = base.FindBy(x => x.KodeRegistrasi == modelHeader.KodeRegistrasi && x.KodeStream == modelHeader.KodeStream)
                            .Select(x => x.Kode).ToList();
                        foreach (sp_GetPenjurianDetailEnter_Result itemSofi in dataSofi)
                        {
                            var modelDetailSofi = ctx.Set<PenjurianDetailEnter>().Where(x => kodeHeadrs.Contains(x.KodePenjurianHeader.Value) && x.KodeKriteria == itemSofi.KodeKriteria);
                            foreach (PenjurianDetailEnter dataDetail in modelDetailSofi)
                            {
                                dataDetail.SaranPeningkatan = itemSofi.SaranPeningkatan;
                                dataDetail.Sofi = itemSofi.Sofi;
                            }

                        }

                        ctx.SaveChanges();
                    }

                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }


        public List<Dropdown> DropdownRegistrasi()
        {

            var tableName = typeof(RegistrasiCIP).Name;
            var sql = string.Format(@"select a.Kode as id, a.Kode  + ' - ' + NamaGugus + ' - ' + b.Auditor + '(Auditor)' as text, a.Kode as value from 
                                    AsignCipEnterprise a inner join
									AuditPDCAHeader b on a.KodeRegistrasi=b.KodeRegistrasi
									where 
									b.StatusApproval=1 and a.CipEnterprise not in(
									select isnull(KodeRegistrasi,'') from PenjurianHeaderEnter where  isnull(CreateBy,'')='{0}')", this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>();

            return list;
        }

        public sp_GetRegistrasiCIP_Result GetRegistrasi(string Kode)
        {
            var res = new sp_GetRegistrasiCIP_Result();
            using (var ctx = new GenericContext())
            {
                res = ctx.sp_GetRegistrasiCIP(Kode).FirstOrDefault();
            }
            return res;
        }

        public PenjurianHeaderEnter GetByRegistrasi(string Kode, string kodeJuri)
        {
            var res = new PenjurianHeaderEnter();
            using (var ctx = new GenericContext())
            {
                res = ctx.PenjurianHeaderEnter.Where(x => x.KodeRegistrasi.Equals(Kode) && x.KodeJuri == kodeJuri).FirstOrDefault();
            }

            return res;
        }

        public List<sp_GetPenjurianDetailEnter_Result> GetPenjurianDetail(string kodePenjurianHeader, string kodeJuri)
        {
            List<sp_GetPenjurianDetailEnter_Result> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.sp_GetPenjurianDetailEnter(kodePenjurianHeader, kodeJuri).ToList();
            }

            return queryable;
        }

        public bool StartPenjurian(string kode, string kodeRegistrasi)
        {
            int kodePenjurian = Convert.ToInt32(kode);
            var record = base.FindBy(x => x.Kode == kodePenjurian && x.KodeRegistrasi == kodeRegistrasi).FirstOrDefault();

            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    if (record != null)
                    {
                        record.Status = BaseEnums.EnumPenjurianStatus.Start.ToString();
                        base.Update(record);
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }

                    using (IRegistrasiCIPRepository repoRegistrasiCIP = new RegistrasiCIPRepository(ctx))
                    {
                        repoRegistrasiCIP.UserProfile = this.UserProfile;

                        repoRegistrasiCIP.UpdateStatusCipEnter(kodeRegistrasi, BaseEnums.EnumRegistrationStatusEnterprise.Forum_Presentasi_Enterprise.ReplaceUnderScoreToSpace());
                    }

                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool FinishPenjurian(string kode, string kodeRegistrasi)
        {
            int kodePenjurian = Convert.ToInt32(kode);
            var record = base.FindBy(x => x.Kode == kodePenjurian && x.KodeRegistrasi == kodeRegistrasi).FirstOrDefault();

            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    if (record != null)
                    {
                        record.Status = BaseEnums.EnumPenjurianStatus.Finish.ToString();
                        base.Update(record);
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }

                    using (IRegistrasiCIPRepository repoRegistrasiCIP = new RegistrasiCIPRepository(ctx))
                    {
                        repoRegistrasiCIP.UserProfile = this.UserProfile;

                        repoRegistrasiCIP.UpdateStatusCipDir(kodeRegistrasi, BaseEnums.EnumRegistrationStatusEnterprise.Result_Forum_Enterprise.ReplaceUnderScoreToSpace());
                    }

                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool SetValueCreation(ValueCreationEnter modelHeader)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var header = ctx.Set<PenjurianHeaderEnter>().Where(x=>x.KodeRegistrasi.Equals(modelHeader.KodeRegistrasi));
                    if (header.Count()>0)
                    {
                        foreach (PenjurianHeaderEnter itm in header) {
                            itm.ValueCreationPotensi = modelHeader.ValueCreationPotensi;
                            itm.ValueCreationReal = modelHeader.ValueCreationReal;
                            itm.ValueProyeksi = modelHeader.ValueProyeksi;

                        }
                        ctx.SaveChanges();
                    }
                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

    }
}
