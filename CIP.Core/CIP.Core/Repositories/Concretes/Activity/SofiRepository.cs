﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    
   
    public class SofiRepository : GenericDataRepository<PenjurianDetail, IGenericContext>, ISofiRepository
    {

        protected IGenericContext ctx;
        public SofiRepository(IGenericContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }
        
        public List<Sofi> GetSofi(int tahun,string stream) {
            List<Sofi> result = new List<Sofi>();

            result = (from a in ctx.Set<Sofi>()
                      where a.Tahun == tahun && a.KodeStream == stream
                      select a).ToList();

            return result;
        }

        public List<SofiDetail> GetSofiDetail(string KodeRegistrasi)
        {

            var kodeHeaders =ctx.Set<PenjurianHeader>().Where(x => x.KodeRegistrasi.Equals(KodeRegistrasi)).Select(x=>x.Kode);
            List<SofiDetail> result = new List<SofiDetail>();
            var sofi = ctx.Set<PenjurianDetail>()
                .Where(x => kodeHeaders.Contains(x.KodePenjurianHeader.Value))
                .Select(x=>new { KodeKriteria = x.KodeKriteria,Sofi=x.Sofi, SaranPeningkatan=x.SaranPeningkatan.ToString()})
                .Distinct()
                .ToList();

            result = (from a in ctx.Set<MasterKriteria>().ToList()
                      join b in sofi on a.Kode equals b.KodeKriteria into c
                      from d in c.DefaultIfEmpty()
                      select new SofiDetail
                      {
                          KodeRegistrasi=KodeRegistrasi,
                          KodeKriteria = a.Kode,
                          Kriteria = a.Deskripsi,
                          Sofi=d.Sofi,
                          SaranPeningkatan=d.SaranPeningkatan
                      }).ToList();

            return result;
        }

        public bool SetSofi(List<SofiDetail> model)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                   
                    foreach (SofiDetail itemSofi in model)
                    {

                    var sql = String.Format("update PenjurianDetail " +
                        "set sofi = '{0}', SaranPeningkatan = '{1}'" +
                        " from PenjurianDetail d inner " +
                        " join PenjurianHeader h on d.KodePenjurianHeader = h.Kode " +
                        " where h.KodeRegistrasi ='{2}' and d.KodeKriteria = '{3}'"
                        , itemSofi.Sofi, itemSofi.SaranPeningkatan, itemSofi.KodeRegistrasi, itemSofi.KodeKriteria);
                        ctx.Database.ExecuteSqlCommand(sql);
                    }
                    trans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

    }
}
