﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class TemplatePdcaEnterRepository : GenericDataRepository<TemplateAuditPDCAEnter, IGenericContext>, ITemplatePdcaEnterRepository
    {
        protected IGenericContext ctx;
        public TemplatePdcaEnterRepository(IGenericContext ctx) :base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetTemplateAuditPDCAEnter_Result> DataTables()
        {
            IQueryable<sp_GetTemplateAuditPDCAEnter_Result> queryable;
            using (var ctx = new GenericContext()) {
                queryable = ctx.sp_GetTemplateAuditPDCAEnter().ToList().AsQueryable();
            }       
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetTemplateAuditPDCAEnter_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public override bool Create(TemplateAuditPDCAEnter model)
        {
            model.Kode = Guid.NewGuid().ToString().Substring(0, 20);
            return base.Create(model);
        }
    }
}
