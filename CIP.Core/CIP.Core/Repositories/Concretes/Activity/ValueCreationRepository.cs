﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
   
    public class ValueCreationRepository : GenericDataRepository<PenjurianHeader, IGenericContext>, IValueCreationRepository
    {

        protected IGenericContext ctx;
        public ValueCreationRepository(IGenericContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }
        
        public List<ValueCreation> GetValueCreation(int tahun,string stream) {
            List<ValueCreation> result = new List<ValueCreation>();
            result = ctx.Set<ValueCreation>().Where(x => x.Tahun == tahun && x.Unit.Equals(this.UserProfile.Unit) && x.KodeStream.Equals(stream)).ToList();
            return result;
        }

        public bool SetValueCreation(ValueCreation modelHeader)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var header = ctx.Set<PenjurianHeader>().Where(x=>x.KodeRegistrasi.Equals(modelHeader.KodeRegistrasi));
                    if (header.Count()>0)
                    {
                        foreach (PenjurianHeader itm in header) {
                            itm.ValueCreationPotensi = modelHeader.ValueCreationPotensi;
                            itm.ValueCreationReal = modelHeader.ValueCreationReal;
                            itm.ValueProyeksi = modelHeader.ValueProyeksi;

                        }
                        ctx.SaveChanges();
                    }
                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

    }
}
