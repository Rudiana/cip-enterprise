﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
   
    public class StreamPenjurianDirRepository : GenericDataRepository<PenjurianHeaderDir, IGenericContext>, IStreamPenjurianDirRepository
    {
        private Dictionary<string, int?[]> Data;
        protected IGenericContext ctx;
        public StreamPenjurianDirRepository(IGenericContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

        
        public NilaiPenjurianDir GetNilaiPenjurian(string stream, int tahun)
        {
            var nilaiPenjurian = new NilaiPenjurianDir();
            using (var ctx = new GenericContext())
            {

                nilaiPenjurian.Header1 = ctx.sp_GenerateStreamHeaderDir1(stream, tahun).ToList();
                nilaiPenjurian.Header2 = ctx.sp_GenerateStreamHeaderDir2(stream, tahun).ToList();
                nilaiPenjurian.Header3 = ctx.sp_GenerateStreamHeaderDir3(stream, tahun).ToList();
                nilaiPenjurian.Data = ConstractData(stream, tahun, nilaiPenjurian.Header3);
            }

            return nilaiPenjurian;
        }

        

        private List<Nilai> ConstractData(string stream, int tahun, List<sp_GenerateStreamHeaderDir3_Result> header3)
        {
            try
            {
                using (var ctx = new GenericContext())
                {
                    List<sp_GenerateStreamDetailDir1_Result> detail1 = ctx.sp_GenerateStreamDetailDir1(stream, tahun).ToList();
                    List<sp_GenerateStreamDetailDir2_Result> detail2 = ctx.sp_GenerateStreamDetailDir2(stream, tahun).ToList();
                    List<Nilai> DataNilai = new List<Nilai>();
                    foreach (var itm in detail1)
                    {
                        List<double?> arrPoin = new List<double?>();
                        List<double?> arrSkala = new List<double?>();
                        List<double?> arrNilai = new List<double?>();
                        Nilai nilai = new Nilai()
                        {
                            Kode = itm.Kode,
                            Kriteria = itm.Kriteria,
                            Langkah = itm.Langkah,
                            NoUrut = itm.NoUrut,
                            FT_Prove = itm.FT_Prove,
                            I_Prove = itm.I_Prove,
                            PC_Prove = itm.PC_Prove,
                            RT_Prove=itm.RT_Prove
                        };

                        var prove = ctx.TemplateAuditPDCAPoin.Where(x => x.tahun == tahun 
                        && x.KodeTemplate.Equals(itm.Kode)
                        )
                        .Distinct().OrderBy(x => x.KodeJenis).ToList();

                        List<sp_GenerateStreamDetailDir2_Result> val = detail2.Where(x => x.KodeTemplatePDCA.Equals(itm.Kode)
                        && x.Langkah.Equals(itm.Langkah))
                            .OrderBy(x => x.KodeJenisCIP).OrderBy(x => x.KodeRegistrasi).ToList();


                        var gugus = header3.GroupBy(x => x.Kode).Select(x => new { kode = x.Key, c = x.ToList().Count() });

                        foreach (var v in gugus)
                        {

                            var poin = prove.FirstOrDefault(x => x.KodeJenis.Equals(v.kode));


                            if (poin != null)
                            {
                                arrPoin.Add(poin.Poin ?? 0);
                            }
                            else
                            {

                                var item = val.Where(x => x.KodeRegistrasi.Equals(v.kode)).ToList();
                                if (item.Count() > 0)
                                {
                                    arrSkala.Add(item[0].Skala);
                                    arrNilai.Add(item[0].Nilai);
                                }
                                else
                                {
                                    arrSkala.Add(0);
                                    arrNilai.Add(0);
                                }

                            }

                        }


                        arrPoin.AddRange(arrSkala);
                        arrPoin.AddRange(arrNilai);
                        nilai.Data = arrPoin.ToArray();
                        DataNilai.Add(nilai);

                    }

                    return DataNilai;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }


    }
}
