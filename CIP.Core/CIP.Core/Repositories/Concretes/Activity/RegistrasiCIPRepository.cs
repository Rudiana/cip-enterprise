﻿using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Message;
using CIP.Core.Models;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Concretes.Administrasi;
using System.Net.Mail;
using System.Threading.Tasks;
using static CIP.Core.Common.BaseEnums;
using System.Text;
using CIP.Core.Security;
using System.Globalization;
using System.Threading;
using CIP.Core.Repositories.Concretes;
using Newtonsoft.Json;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class RegistrasiCIPRepository : GenericDataRepository<RegistrasiCIP, IGenericContext>, IRegistrasiCIPRepository
    {
        protected new IGenericContext ctx;

        public RegistrasiCIPRepository(IGenericContext _ctx)
            : base(_ctx)
        {
            this.ctx = _ctx;
        }

        public bool Register(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var rec = base.FindBy(x => x.NamaGugus == model.NamaGugus && x.Tahun == model.Tahun).FirstOrDefault();
                    if (rec != null)
                    {
                        throw new Exception("Nama Gugus untuk Tahun tersebut sudah terdaftar. Untuk mengelola silahkan cek di Menu Historis");
                    }


                    rec = base.FindBy(x => x.JudulCIP == model.JudulCIP && x.Tahun == model.Tahun).FirstOrDefault();
                    if (rec != null)
                    {
                        throw new Exception("Judul untuk Tahun tersebut sudah digunakan. Untuk mengelola silahkan cek di Menu Historis");
                    }

                    model.Kode = Guid.NewGuid().ToString();
                    model.NamaGugus = model.NamaGugus.ToCapitalize();
                    model.JudulCIP = model.JudulCIP.ToCapitalize();
                    //model.Direktorat = this.UserProfile.Direktorat;
                    //model.Unit = this.UserProfile.Unit;
                    model.Status = BaseEnums.EnumRegistrationStatus.Pending_Approval_Admin_QM.ReplaceUnderScoreToSpace();
                    model.IsAktivasi = true;
                    model.IsAktif = true;
                    model.CreatedDate = DateTime.Now;

                    //var recAnggotaCIP = listAnggotaCIP.Where(x => x.KodeStatusAnggota == "1").FirstOrDefault();
                    //if (recAnggotaCIP != null)
                    //{
                    //    var recUserEmailKetua = ctx.Set<AdministrasiUser>().Find(recAnggotaCIP.KodeUser);
                    //    if (recUserEmailKetua != null)
                    //    {
                    //        model.Email = recUserEmailKetua.Email;
                    //        model.Ketua = 
                    //    }
                    //}

                    //Save Registrasi CIP
                    base.Create(model);

                    //Create Anggota CIP
                    this.p_CreateAnggota(model, listAnggotaCIP);

                    // Create Registrasi Log
                    this.p_CreateLog(model.Kode, EnumRegistrationStatus.Registrasi_Peserta_CIP.ToString().ReplaceUnderScoreToSpace());

                    // Create Registrasi Log
                    this.p_CreateLog(model.Kode, model.Status);

                    trans.Commit();

                    // Broadcast Email
                    p_BroadcastEmail(model, listAnggotaCIP);

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool Reregister(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var rec = base.FindBy(x => x.NamaGugus == model.NamaGugus && x.Tahun == model.Tahun && x.Kode != model.Kode).FirstOrDefault();
                    if (rec != null)
                    {
                        throw new Exception("Nama Gugus untuk Tahun tersebut sudah terdaftar");
                    }
                    //else
                    //{
                    //    rec = base.FindBy(x => x.Email == model.Email && x.Tahun == model.Tahun && x.Kode != model.Kode).FirstOrDefault();
                    //    if (rec != null)
                    //    {
                    //        throw new Exception("Email untuk Tahun tersebut sudah digunakan");
                    //    }
                    //}

                    base.Update(model);

                    var kodeUser = model.Kode;

                    //Create anggota CIP
                    p_CreateAnggota(model, listAnggotaCIP);

                    //Create User
                    using (var userRepo = new AdministrasiUserRepository(ctx))
                    {
                        userRepo.UserProfile = this.UserProfile;

                        var user = userRepo.GetSingle(new object[] { kodeUser });
                        var lokasi = ctx.Set<MasterLokasi>().Find(model.Unit);
                        if (user == null)
                        {
                            user = new AdministrasiUser()
                            {
                                Kode = kodeUser,
                                Nama = string.Format("{0}-{1}-{2}", model.Kode, model.NamaGugus, model.Tahun),
                                Alamat = lokasi != null ? lokasi.Deskripsi : "",
                                Email = model.Email,
                                IsAdministrator = false,
                                Aktif = true
                            };

                            userRepo.Create(user);
                        }
                        else
                        {
                            user.Nama = string.Format("{0}-{1}-{2}", model.Kode, model.NamaGugus, model.Tahun);
                            if (lokasi != null)
                            {
                                user.Nama = string.Format("{0}-{1}-{2}", model.Kode, model.NamaGugus, model.Tahun);
                                user.Alamat = lokasi.Deskripsi;
                                user.Email = model.Email;
                            }

                            userRepo.Update(user);
                        }
                    }

                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool UpdateWithDetail(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP)
        {
            var rec = base.FindBy(x => x.Kode == model.Kode).ToList().FirstOrDefault();
            if (rec == null)
            {
                throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
            }

            rec = null;
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    //Create Anggota CIP
                    p_CreateAnggota(model, listAnggotaCIP);

                    //Save Registrasi CIP
                    base.Update(model);

                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool UpdateWithDetailDir(AsignCipDir model, List<AnggotaCIPDir> listAnggotaCIP)
        {
            using (IGenericDataRepository<AsignCipDir> repo = new DataRepository<AsignCipDir>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                var rec = repo.FindBy(x => x.Kode == model.Kode).ToList().FirstOrDefault();
                if (rec == null)
                {
                    throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                }

                rec = null;
                using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        //Create Anggota CIP
                        p_CreateAnggotaDir(model, listAnggotaCIP);

                        //Save Registrasi CIP
                        repo.Update(model);

                        trans.Commit();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();

                        throw new Exception(ex.Message, ex.InnerException);
                    }
                }
            }
        }

        public bool UpdateWithDetailEnter(AsignCipEnter model, List<AnggotaCIPEnter> listAnggotaCIP)
        {
            using (IGenericDataRepository<AsignCipEnter> repo = new DataRepository<AsignCipEnter>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                var rec = repo.FindBy(x => x.Kode == model.Kode).ToList().FirstOrDefault();
                if (rec == null)
                {
                    throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                }

                rec = null;
                using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        //Create Anggota CIP
                        p_CreateAnggotaEnter(model, listAnggotaCIP);

                        //Save Registrasi CIP
                        repo.Update(model);

                        trans.Commit();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();

                        throw new Exception(ex.Message, ex.InnerException);
                    }
                }
            }
        }

        public bool AllowEdit(string kode)
        {
            var allowEdit = false;
            if (!this.UserProfile.IsAdministrator)
            {
                var registrasi = base.FindBy(x => x.Kode == kode).FirstOrDefault();
                if (registrasi != null)
                {
                    var statusLimitEdit = BaseEnums.EnumRegistrationStatus.Forum_Presentasi_CIP.GetValue();
                    var status = BaseEnums.EnumRegistrationStatus.Forum_Presentasi_CIP.ReplaceUnderScoreToSpace();

                    var statusReg = (int)Enum.Parse(typeof(EnumRegistrationStatus), registrasi.Status.Replace(" ", "_"));
                    if (statusReg < statusLimitEdit)
                    {
                        var isAdminUnit = ctx.Set<AdministrasiRoleUser>().Where(x => x.KodeUser == this.UserProfile.Kode && x.KodeRole == "AdminUnit").FirstOrDefault();
                        if (isAdminUnit != null)
                        {
                            allowEdit = true;
                        }
                        else
                        {
                            var rec = ctx.Set<AnggotaCIP>().Where(x => x.KodeRegistrasi == kode && x.KodeUser == this.UserProfile.Kode && x.KodeStatusAnggota == "1").FirstOrDefault();
                            if (rec != null)
                            {
                                allowEdit = true;
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                }
            }
            else
            {
                allowEdit = true;
            }

            return allowEdit;
        }

        public bool AllowEditDir(string kode)
        {
            var allowEdit = false;
            if (!this.UserProfile.IsAdministrator)
            {
                var registrasi = ctx.Set<AsignCipDir>().Where(x => x.CipDirektorat == kode).FirstOrDefault();
                if (registrasi != null)
                {
                    var statusLimitEdit = BaseEnums.EnumRegistrationStatusDirektorat.Forum_Presentasi_Direktorat.GetValue();
                    var status = BaseEnums.EnumRegistrationStatusDirektorat.Forum_Presentasi_Direktorat.ReplaceUnderScoreToSpace();

                    var statusReg = (int)Enum.Parse(typeof(EnumRegistrationStatusDirektorat), registrasi.Status.Replace(" ", "_"));
                    if (statusReg < statusLimitEdit)
                    {
                        var isAdminDirektorat = ctx.Set<AdministrasiRoleUser>().Where(x => x.KodeUser == this.UserProfile.Kode && x.KodeRole == "AdminDirektorat").FirstOrDefault();
                        if (isAdminDirektorat != null)
                        {
                            allowEdit = true;
                        }
                        else
                        {
                            var rec = ctx.Set<AnggotaCIPDir>().Where(x => x.KodeRegistrasi == kode && x.KodeUser == this.UserProfile.Kode && x.KodeStatusAnggota == "1").FirstOrDefault();
                            if (rec != null)
                            {
                                allowEdit = true;
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                }
            }
            else
            {
                allowEdit = true;
            }

            return allowEdit;
        }

        public bool AllowEditEnter(string kode)
        {
            var allowEdit = false;
            if (!this.UserProfile.IsAdministrator)
            {
                var registrasi = ctx.Set<AsignCipEnter>().Where(x => x.CipEnterprise == kode).FirstOrDefault();
                if (registrasi != null)
                {
                    var statusLimitEdit = BaseEnums.EnumRegistrationStatusEnterprise.Forum_Presentasi_Enterprise.GetValue();
                    var status = BaseEnums.EnumRegistrationStatusEnterprise.Forum_Presentasi_Enterprise.ReplaceUnderScoreToSpace();

                    var statusReg = (int)Enum.Parse(typeof(EnumRegistrationStatusEnterprise), registrasi.Status.Replace(" ", "_"));
                    if (statusReg < statusLimitEdit)
                    {
                        var isAdminEnterprise = ctx.Set<AdministrasiRoleUser>().Where(x => x.KodeUser == this.UserProfile.Kode && x.KodeRole == "AdminEnterprise").FirstOrDefault();
                        if (isAdminEnterprise != null)
                        {
                            allowEdit = true;
                        }
                        else
                        {
                            var rec = ctx.Set<AnggotaCIPEnter>().Where(x => x.KodeRegistrasi == kode && x.KodeUser == this.UserProfile.Kode && x.KodeStatusAnggota == "1").FirstOrDefault();
                            if (rec != null)
                            {
                                allowEdit = true;
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                }
            }
            else
            {
                allowEdit = true;
            }

            return allowEdit;
        }
        public bool SetActive(string kode, bool isAktif)
        {
            var rec = base.GetSingle(kode);
            if (rec != null)
            {
                rec.IsAktif = isAktif;
            }
            else
            {
                throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
            }

            return base.Update(rec);
        }

        public bool Activation(string kode, string password)
        {
            password = Cryptography.EncryptString(password);
            var user = ctx.Set<AdministrasiUser>().Where(x => x.Kode == kode
                && x.Password == password).FirstOrDefault();

            if (user == null)
            {
                throw new Exception("Akun anda tidak valid.");
            }

            var model = base.GetSingle(kode);
            if (model != null)
            {
                if (!model.IsAktivasi)
                {
                    model.IsAktivasi = true;

                    var success = base.Update(model);
                    if (success)
                    {
                        p_BroadcastEmail(model);
                    }

                    return success;
                }
                else
                {
                    throw new Exception("Akun anda sudah teraktivasi.");
                }
            }
            else
            {
                throw new Exception("Akun anda tidak terdaftar.");
            }
        }

        public bool UpdateVerifikasiAdminQM(string kode, string status)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var success = false;
                    var model = base.GetSingle(kode);

                    status = status.ReplaceUnderScoreToSpace();
                    model.Status = status;
                    if (status == BaseEnums.EnumRegistrationStatus.Tim_CIP_Approved.ReplaceUnderScoreToSpace())
                    {
                        kode = model.Kode;
                        var autoNumber = p_GenereateAutoNumberUnit(model);

                        this.p_CreateLog(autoNumber, status);

                        var sql = string.Format(@"
                            update RegistrasiCIPLog set KodeRegistrasi ='{0}' where KodeRegistrasi='{1}';
                            update AnggotaCIP set KodeRegistrasi ='{0}' where KodeRegistrasi='{1}';
                            update RegistrasiCIP set Kode ='{0}', Status='{2}' where Kode='{1}';"
                        , autoNumber, kode, model.Status);
                        ctx.Database.ExecuteSqlCommand(sql);

                        model.Kode = autoNumber;
                        p_BroadcastEmail(model);
                    }
                    else
                    {
                        success = p_UpdateVerifikasiAdminQM(model.Kode, model.Status, model.Keterangan);
                    }

                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception("Verifikasi Perserta CIP tidak berhasil.");
                }
            }
        }

        public bool UpdateVerifikasiAdminDir(string kode, string status)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    using (IGenericDataRepository<AsignCipDir> repo = new DataRepository<AsignCipDir>(ctx))
                    {
                        repo.UserProfile = this.UserProfile;

                        var success = false;
                        var model = repo.FindBy(x => x.CipDirektorat == kode).FirstOrDefault();

                        status = status.ReplaceUnderScoreToSpace();
                        model.Status = status;
                        if (status == BaseEnums.EnumRegistrationStatusDirektorat.Tim_CIP_Approved_Direktorat.ReplaceUnderScoreToSpace())
                        {
                            kode = model.CipDirektorat;
                            var autoNumber = p_GenereateAutoNumberDir(model);

                            this.p_CreateLog(autoNumber, status);

                            var sql = string.Format(@"
                            update RegistrasiCIPLog set KodeRegistrasi ='{0}' where KodeRegistrasi='{1}';
                            update AnggotaCIPDir set KodeRegistrasi ='{0}' where KodeRegistrasi='{1}';
                            update AsignCipDir set CipDirektorat ='{0}', Status='{2}' where CipDirektorat='{1}';"
                            , autoNumber, kode, model.Status);
                            ctx.Database.ExecuteSqlCommand(sql);

                            model.CipDirektorat = autoNumber;
                            //p_BroadcastEmail(model);
                        }
                        else
                        {
                            success = p_UpdateVerifikasiAdminDir(model.CipDirektorat, model.Status, model.Keterangan);
                        }

                        trans.Commit();

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception("Verifikasi Perserta CIP tidak berhasil.");
                }
            }
        }

        public bool UpdateVerifikasiAdminEnter(string kode, string status)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    using (IGenericDataRepository<AsignCipEnter> repo = new DataRepository<AsignCipEnter>(ctx))
                    {
                        repo.UserProfile = this.UserProfile;

                        var success = false;
                        var model = repo.FindBy(x => x.CipEnterprise == kode).FirstOrDefault();

                        status = status.ReplaceUnderScoreToSpace();
                        model.Status = status;
                        if (status == BaseEnums.EnumRegistrationStatusEnterprise.Tim_CIP_Approved_Enterprise.ReplaceUnderScoreToSpace())
                        {
                            kode = model.CipEnterprise;
                            var autoNumber = p_GenereateAutoNumberEnter(model);

                            this.p_CreateLog(autoNumber, status);

                            var sql = string.Format(@"
                            update RegistrasiCIPLog set KodeRegistrasi ='{0}' where KodeRegistrasi='{1}';
                            update AnggotaCIPEnter set KodeRegistrasi ='{0}' where KodeRegistrasi='{1}';
                            update AsignCipEnter set CipEnterprise ='{0}', Status='{2}' where CipEnterprise='{1}';"
                            , autoNumber, kode, model.Status);
                            ctx.Database.ExecuteSqlCommand(sql);

                            model.CipEnterprise = autoNumber;
                            //p_BroadcastEmail(model);
                        }
                        else
                        {
                            success = p_UpdateVerifikasiAdminEnter(model.CipEnterprise, model.Status, model.Keterangan);
                        }

                        trans.Commit();

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw new Exception("Verifikasi Perserta CIP tidak berhasil.");
                }
            }
        }

        public override List<dynamic> Browse()
        {
            return p_Browse().ToList<dynamic>();
        }

        public override dynamic DynamicData(string term)
        {
            var listAnggota = (from a in ctx.Set<AnggotaCIP>().ToList()
                               join c in ctx.Set<RegistrasiCIP>().ToList() on a.KodeRegistrasi equals c.Kode
                               join b in ctx.Set<AdministrasiUser>() on a.KodeUser equals b.Kode into _b
                               from b in _b.DefaultIfEmpty()
                               join d in ctx.Set<MasterStatusAnggota>() on a.KodeStatusAnggota equals d.Kode into _d
                               from d in _d.DefaultIfEmpty()
                               where a.KodeRegistrasi == term
                               select new Anggota()
                               {
                                   Email = b == null ? "-" : b.Email,
                                   KodeStatusAnggota = a.KodeStatusAnggota,
                                   KodeUser = a.KodeUser,
                                   NamaAnggota = b == null ? a.KodeUser : b.Nama,
                                   NoPek = b == null ? "" : b.NoPek,
                                   StatusDes = d == null ? "" : d.Deskripsi
                               }).OrderBy(x => x.KodeStatusAnggota).ToList();

            var record = (from a in ctx.Set<RegistrasiCIP>().ToList()
                          join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode
                          join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode
                          join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode
                          join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode
                          where a.Kode == term
                          select new RegistrasiAnggotaCIP()
                          {
                              Abster = a.Abster,
                              AnggotaCIPDetail = listAnggota,
                              CreatedBy = a.CreatedBy,
                              CreatedDate = a.CreatedDate,
                              Email = a.Email,
                              SubUnitDes = e.Deskripsi,
                              JenisCIPDes = b.Deskripsi,
                              JudulCIP = a.JudulCIP,
                              JumlahAnggota = a.JumlahAnggota,
                              DirektoratDes = c.Deskripsi,
                              Keterangan = a.Keterangan,
                              Kode = a.Kode,
                              SubUnit = a.SubUnit,
                              KodeJenisCIP = a.KodeJenisCIP,
                              Direktorat = a.Direktorat,
                              Unit = a.Unit,
                              KOMET = a.KOMET,
                              UnitDes = d.Deskripsi,
                              NamaGugus = a.NamaGugus,
                              Risalah = a.Risalah,
                              Status = a.Status,
                              STK = a.STK,
                              Tahun = a.Tahun,
                              UpdatedBy = a.UpdatedBy,
                              UpdatedDate = a.UpdatedDate,
                              ValueCreationKategori = a.ValueCreationKategori,
                              ValueCreationPotensi = a.ValueCreationPotensi,
                              ValueCreationReal = a.ValueCreationReal,
                              VerifikasiKeuangan = a.VerifikasiKeuangan,
                              Fasilitator = a.Fasilitator,
                              Ketua = a.Ketua,
                              UploadPresentasi = a.UploadPresentasi,
                              IsUploadAbster = a.IsUploadAbster,
                              IsUploadKOMET = a.IsUploadKOMET,
                              IsUploadPresentasi = a.IsUploadPresentasi,
                              IsUploadRisalah = a.IsUploadRisalah,
                              IsUploadSTK = a.IsUploadSTK,
                              IsUploadVerifikasiKeuangan = a.IsUploadVerifikasiKeuangan,
                              FotoPeserta = a.FotoPeserta,
                              IsAktivasi = a.IsAktivasi,
                              IsUploadFotoPeserta = a.IsUploadFotoPeserta,
                              IsAktif = a.IsAktif,
                              KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatus), a.Status.Replace(" ", "_")),
                              AbsterUpdatedDate = a.AbsterUpdatedDate,
                              KOMETUpdatedDate = a.KOMETUpdatedDate,
                              RisalahUpdatedDate = a.RisalahUpdatedDate,
                              STKUpdatedDate = a.STKUpdatedDate,
                              UploadPresentasiUpdatedDate = a.UploadPresentasiUpdatedDate,
                              VerifikasiKeuanganUpdatedDate = a.VerifikasiKeuanganUpdatedDate,
                              LintasDirektorat = a.LintasDirektorat,
                          }).ToList();

            return record.FirstOrDefault();
        }

        public virtual dynamic DynamicDataDir(string term)
        {
            var listAnggota = (from a in ctx.Set<AnggotaCIPDir>().ToList()
                               join c in ctx.Set<AsignCipDir>().ToList() on a.KodeRegistrasi equals c.CipDirektorat
                               join b in ctx.Set<AdministrasiUser>() on a.KodeUser equals b.Kode into _b
                               from b in _b.DefaultIfEmpty()
                               join d in ctx.Set<MasterStatusAnggota>() on a.KodeStatusAnggota equals d.Kode into _d
                               from d in _d.DefaultIfEmpty()
                               where a.KodeRegistrasi == term
                               select new Anggota()
                               {
                                   Email = b == null ? "-" : b.Email,
                                   KodeStatusAnggota = a.KodeStatusAnggota,
                                   KodeUser = a.KodeUser,
                                   NamaAnggota = b == null ? a.KodeUser : b.Nama,
                                   NoPek = b == null ? "" : b.NoPek,
                                   StatusDes = d == null ? "" : d.Deskripsi
                               }).OrderBy(x => x.KodeStatusAnggota).ToList();

            var record = (from a in ctx.Set<AsignCipDir>().ToList()
                          join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode
                          join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode
                          join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode
                          join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode
                          where a.CipDirektorat == term
                          select new RegistrasiAnggotaCIPDir()
                          {
                              Abster = a.Abster,
                              AnggotaCIPDetail = listAnggota,
                              CreatedBy = a.CreatedBy,
                              CreatedDate = a.CreatedDate,
                              Email = a.Email,
                              SubUnitDes = e.Deskripsi,
                              JenisCIPDes = b.Deskripsi,
                              JudulCIP = a.JudulCIP,
                              JumlahAnggota = a.JumlahAnggota,
                              DirektoratDes = c.Deskripsi,
                              Keterangan = a.Keterangan,
                              Kode = a.Kode,
                              SubUnit = a.SubUnit,
                              KodeJenisCIP = a.KodeJenisCIP,
                              Direktorat = a.Direktorat,
                              Unit = a.Unit,
                              KOMET = a.KOMET,
                              UnitDes = d.Deskripsi,
                              NamaGugus = a.NamaGugus,
                              Risalah = a.Risalah,
                              Status = a.Status,
                              STK = a.STK,
                              Tahun = a.Tahun,
                              UpdatedBy = a.UpdatedBy,
                              UpdatedDate = a.UpdatedDate,
                              ValueCreationKategori = a.ValueCreationKategori,
                              ValueCreationPotensi = a.ValueCreationPotensi,
                              ValueCreationReal = a.ValueCreationReal,
                              VerifikasiKeuangan = a.VerifikasiKeuangan,
                              Fasilitator = a.Fasilitator,
                              Ketua = a.Ketua,
                              UploadPresentasi = a.UploadPresentasi,
                              IsUploadAbster = a.IsUploadAbster,
                              IsUploadKOMET = a.IsUploadKOMET,
                              IsUploadPresentasi = a.IsUploadPresentasi,
                              IsUploadRisalah = a.IsUploadRisalah,
                              IsUploadSTK = a.IsUploadSTK,
                              IsUploadVerifikasiKeuangan = a.IsUploadVerifikasiKeuangan,
                              FotoPeserta = a.FotoPeserta,
                              IsAktivasi = a.IsAktivasi,
                              IsUploadFotoPeserta = a.IsUploadFotoPeserta,
                              IsAktif = a.IsAktif,
                              KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusDirektorat), a.Status.Replace(" ", "_")),
                              AbsterUpdatedDate = a.AbsterUpdatedDate,
                              KOMETUpdatedDate = a.KOMETUpdatedDate,
                              RisalahUpdatedDate = a.RisalahUpdatedDate,
                              STKUpdatedDate = a.STKUpdatedDate,
                              UploadPresentasiUpdatedDate = a.UploadPresentasiUpdatedDate,
                              VerifikasiKeuanganUpdatedDate = a.VerifikasiKeuanganUpdatedDate,
                              KodeRegistrasi = a.KodeRegistrasi,
                              CipDirektorat = a.CipDirektorat,
                              LintasDirektorat = a.LintasDirektorat,
                          }).ToList();

            return record.FirstOrDefault();
        }

        public virtual dynamic DynamicDataEnter(string term)
        {
            var listAnggota = (from a in ctx.Set<AnggotaCIPEnter>().ToList()
                               join c in ctx.Set<AsignCipEnter>().ToList() on a.KodeRegistrasi equals c.CipDirektorat
                               join b in ctx.Set<AdministrasiUser>() on a.KodeUser equals b.Kode into _b
                               from b in _b.DefaultIfEmpty()
                               join d in ctx.Set<MasterStatusAnggota>() on a.KodeStatusAnggota equals d.Kode into _d
                               from d in _d.DefaultIfEmpty()
                               where a.KodeRegistrasi == term
                               select new Anggota()
                               {
                                   Email = b == null ? "-" : b.Email,
                                   KodeStatusAnggota = a.KodeStatusAnggota,
                                   KodeUser = a.KodeUser,
                                   NamaAnggota = b == null ? a.KodeUser : b.Nama,
                                   NoPek = b == null ? "" : b.NoPek,
                                   StatusDes = d == null ? "" : d.Deskripsi
                               }).OrderBy(x => x.KodeStatusAnggota).ToList();

            var record = (from a in ctx.Set<AsignCipEnter>().ToList()
                          join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode
                          join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode
                          join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode
                          join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode
                          where a.CipDirektorat == term
                          select new RegistrasiAnggotaCIPEnter()
                          {
                              Abster = a.Abster,
                              AnggotaCIPDetail = listAnggota,
                              CreatedBy = a.CreatedBy,
                              CreatedDate = a.CreatedDate,
                              Email = a.Email,
                              SubUnitDes = e.Deskripsi,
                              JenisCIPDes = b.Deskripsi,
                              JudulCIP = a.JudulCIP,
                              JumlahAnggota = a.JumlahAnggota,
                              DirektoratDes = c.Deskripsi,
                              Keterangan = a.Keterangan,
                              Kode = a.Kode,
                              SubUnit = a.SubUnit,
                              KodeJenisCIP = a.KodeJenisCIP,
                              Direktorat = a.Direktorat,
                              Unit = a.Unit,
                              KOMET = a.KOMET,
                              UnitDes = d.Deskripsi,
                              NamaGugus = a.NamaGugus,
                              Risalah = a.Risalah,
                              Status = a.Status,
                              STK = a.STK,
                              Tahun = a.Tahun,
                              UpdatedBy = a.UpdatedBy,
                              UpdatedDate = a.UpdatedDate,
                              ValueCreationKategori = a.ValueCreationKategori,
                              ValueCreationPotensi = a.ValueCreationPotensi,
                              ValueCreationReal = a.ValueCreationReal,
                              VerifikasiKeuangan = a.VerifikasiKeuangan,
                              Fasilitator = a.Fasilitator,
                              Ketua = a.Ketua,
                              UploadPresentasi = a.UploadPresentasi,
                              IsUploadAbster = a.IsUploadAbster,
                              IsUploadKOMET = a.IsUploadKOMET,
                              IsUploadPresentasi = a.IsUploadPresentasi,
                              IsUploadRisalah = a.IsUploadRisalah,
                              IsUploadSTK = a.IsUploadSTK,
                              IsUploadVerifikasiKeuangan = a.IsUploadVerifikasiKeuangan,
                              FotoPeserta = a.FotoPeserta,
                              IsAktivasi = a.IsAktivasi,
                              IsUploadFotoPeserta = a.IsUploadFotoPeserta,
                              IsAktif = a.IsAktif,
                              KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusEnterprise), a.Status.Replace(" ", "_")),
                              AbsterUpdatedDate = a.AbsterUpdatedDate,
                              KOMETUpdatedDate = a.KOMETUpdatedDate,
                              RisalahUpdatedDate = a.RisalahUpdatedDate,
                              STKUpdatedDate = a.STKUpdatedDate,
                              UploadPresentasiUpdatedDate = a.UploadPresentasiUpdatedDate,
                              VerifikasiKeuanganUpdatedDate = a.VerifikasiKeuanganUpdatedDate,
                              KodeRegistrasi = a.KodeRegistrasi,
                              CipDirektorat = a.CipDirektorat,
                              CipEnterprise = a.CipEnterprise,
                              LintasDirektorat = a.LintasDirektorat,
                          }).ToList();

            return record.FirstOrDefault();
        }

        public virtual FormatedList<RegistrasiAnggotaCIP> DataTablePeserta(int statReg)
        {
            IQueryable<RegistrasiAnggotaCIP> queryable;
            if (statReg == 1)
            {
                queryable = p_Browse().Where(x => x.Status == BaseEnums.EnumRegistrationStatus.Pending_Approval_Admin_QM.ToString().ReplaceUnderScoreToSpace()).AsQueryable();
            }
            else
            {
                queryable = p_Browse().AsQueryable();
            }

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIP>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<PenjurianSofi> DataTablePenjurianSofi(string kodeRegistrasi)
        {
            IQueryable<PenjurianSofi> queryable = ctx.Database.SqlQuery<PenjurianSofi>(string.Format("exec sp_GetPenjurianSofi '{0}'", kodeRegistrasi)).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<PenjurianSofi>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<PenjurianSofi> DataTablePenjurianSofiDir(string cipDirektorat)
        {
            IQueryable<PenjurianSofi> queryable = ctx.Database.SqlQuery<PenjurianSofi>(string.Format("exec sp_GetPenjurianSofiDir '{0}'", cipDirektorat)).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<PenjurianSofi>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<PenjurianSofi> DataTablePenjurianSofiEnter(string cipEnterprise)
        {
            IQueryable<PenjurianSofi> queryable = ctx.Database.SqlQuery<PenjurianSofi>(string.Format("exec sp_GetPenjurianSofiEnter '{0}'", cipEnterprise)).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<PenjurianSofi>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIP> DataTableVerifikasiPeserta()
        {
            var status = new string[] { BaseEnums.EnumRegistrationStatus.Pending_Approval_Admin_QM.ReplaceUnderScoreToSpace()
                , BaseEnums.EnumRegistrationStatus.Tim_CIP_Approved.ReplaceUnderScoreToSpace() };


            IQueryable<RegistrasiAnggotaCIP> queryable =
                p_Browse().Where(x => status.Contains(x.Status)).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIP>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPDir> DataTableVerifikasiPesertaDir()
        {
            var status = new string[] { BaseEnums.EnumRegistrationStatusDirektorat.Pending_Approval_Admin_Direktorat.ReplaceUnderScoreToSpace()
                , BaseEnums.EnumRegistrationStatusDirektorat.Tim_CIP_Approved_Direktorat.ReplaceUnderScoreToSpace() };

            IQueryable<RegistrasiAnggotaCIPDir> queryable =
                p_BrowseDir().Where(x => status.Contains(x.Status)).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPDir>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPEnter> DataTableVerifikasiPesertaEnter()
        {
            var status = new string[] { BaseEnums.EnumRegistrationStatusEnterprise.Pending_Approval_Admin_Enterprise.ReplaceUnderScoreToSpace()
                , BaseEnums.EnumRegistrationStatusEnterprise.Tim_CIP_Approved_Enterprise.ReplaceUnderScoreToSpace() };

            IQueryable<RegistrasiAnggotaCIPEnter> queryable =
                p_BrowseEnter().Where(x => status.Contains(x.Status)).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPEnter>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiCIPLog> DataTableRegisterLog(string kodeRegistrasi)
        {
            IQueryable<RegistrasiCIPLog> queryable = ctx.Set<RegistrasiCIPLog>().Where(x => x.KodeRegistrasi == kodeRegistrasi)
                .OrderBy(x => x.CreatedDate).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiCIPLog>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIP> DatatablesByPeriode(int? periode)
        {
            var unit = this.UserProfile.Unit;
            var isAdministrator = this.UserProfile.IsAdministrator;

            var queryable = (from a in ctx.Set<RegistrasiCIP>().Where(x => x.Tahun == (periode.HasValue ? periode.Value : x.Tahun) && x.Unit == (isAdministrator ? x.Unit : unit)).ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()
                                 //where a.Tahun == (periode.HasValue ? periode.Value : a.Tahun)
                                 //&& a.Unit == (this.UserProfile.IsAdministrator ? a.Unit : unit)
                             select new RegistrasiAnggotaCIP()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatus), a.Status.Replace(" ", "_")),
                             }).AsQueryable();


            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIP>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPDir> DatatablesByPeriodeDir(int? periode)
        {
            var direktorat = this.UserProfile.Direktorat;
            var isAdministrator = this.UserProfile.IsAdministrator;

            var queryable = (from a in ctx.Set<AsignCipDir>().Where(x => x.Tahun == (periode.HasValue ? periode.Value : x.Tahun) && x.Direktorat == (isAdministrator ? x.Direktorat : direktorat)).ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()

                             select new RegistrasiAnggotaCIPDir()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusDirektorat), a.Status.Replace(" ", "_")),
                                 KodeRegistrasi = a.KodeRegistrasi,
                                 CipDirektorat = a.CipDirektorat
                             }).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPDir>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPEnter> DatatablesByPeriodeEnter(int? periode)
        {
            var queryable = (from a in ctx.Set<AsignCipEnter>().ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()
                             where a.Tahun == (periode.HasValue ? periode.Value : a.Tahun)
                             select new RegistrasiAnggotaCIPEnter()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusEnterprise), a.Status.Replace(" ", "_")),
                                 KodeRegistrasi = a.KodeRegistrasi,
                                 CipDirektorat = a.CipDirektorat,
                                 CipEnterprise = a.CipEnterprise
                             }).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPEnter>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPDir> DatatablesByPeriodeHistoryDir(int? periode)
        {
            var unit = this.UserProfile.Unit;
            var isAdministrator = this.UserProfile.IsAdministrator;

            var queryable = (from a in ctx.Set<AsignCipDir>().Where(x => x.Tahun == (periode.HasValue ? periode.Value : x.Tahun) && x.Unit == (isAdministrator ? x.Unit : unit)).ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()
                             select new RegistrasiAnggotaCIPDir()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusDirektorat), a.Status.Replace(" ", "_")),
                                 KodeRegistrasi = a.KodeRegistrasi,
                                 CipDirektorat = a.CipDirektorat
                             }).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPDir>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPEnter> DatatablesByPeriodeHistoryEnter(int? periode)
        {
            var unit = this.UserProfile.Unit;
            var isAdministrator = this.UserProfile.IsAdministrator;

            var queryable = (from a in ctx.Set<AsignCipEnter>().Where(x => x.Tahun == (periode.HasValue ? periode.Value : x.Tahun) && x.Unit == (isAdministrator ? x.Unit : unit)).ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()
                             select new RegistrasiAnggotaCIPEnter()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusEnterprise), a.Status.Replace(" ", "_")),
                                 KodeRegistrasi = a.KodeRegistrasi,
                                 CipDirektorat = a.CipDirektorat,
                                 CipEnterprise = a.CipEnterprise
                             }).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPEnter>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIP> DatatablesByPeriodeAnggota(int? periode)
        {
            var listRegistrasi = ctx.Set<AnggotaCIP>().Where(x => x.KodeUser == this.UserProfile.Kode)
                .Select(x => x.KodeRegistrasi).Distinct().ToList();

            var queryable = (from a in ctx.Set<RegistrasiCIP>().Where(x => x.Tahun == (periode.HasValue ? periode.Value : x.Tahun) && listRegistrasi.Contains(x.Kode)).ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()
                                 //where a.Tahun == (periode.HasValue ? periode.Value : a.Tahun)
                             select new RegistrasiAnggotaCIP()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatus), a.Status.Replace(" ", "_")),
                             }).AsQueryable();


            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIP>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPDir> DatatablesByPeriodeAnggotaDir(int? periode)
        {
            var listRegistrasi = ctx.Set<AnggotaCIPDir>().Where(x => x.KodeUser == this.UserProfile.Kode)
                .Select(x => x.KodeRegistrasi).Distinct().ToList();

            var queryable = (from a in ctx.Set<AsignCipDir>().Where(x => x.Tahun == (periode.HasValue ? periode.Value : x.Tahun) && listRegistrasi.Contains(x.CipDirektorat)).ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()
                                 //where a.Tahun == (periode.HasValue ? periode.Value : a.Tahun)
                             select new RegistrasiAnggotaCIPDir()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusDirektorat), a.Status.Replace(" ", "_")),
                                 KodeRegistrasi = a.KodeRegistrasi,
                                 CipDirektorat = a.CipDirektorat
                             }).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPDir>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiAnggotaCIPEnter> DatatablesByPeriodeAnggotaEnter(int? periode)
        {
            var listRegistrasi = ctx.Set<AnggotaCIPEnter>().Where(x => x.KodeUser == this.UserProfile.Kode)
                .Select(x => x.KodeRegistrasi).Distinct().ToList();

            var queryable = (from a in ctx.Set<AsignCipEnter>().Where(x => x.Tahun == (periode.HasValue ? periode.Value : x.Tahun) && listRegistrasi.Contains(x.CipEnterprise)).ToList()
                             join b in ctx.Set<MasterJenisCIP>() on a.KodeJenisCIP equals b.Kode into _b
                             from b in _b.DefaultIfEmpty()
                             join c in ctx.Set<MasterDirektorat>() on a.Direktorat equals c.Kode into _c
                             from c in _c.DefaultIfEmpty()
                             join d in ctx.Set<MasterUnit>() on a.Unit equals d.Kode into _d
                             from d in _d.DefaultIfEmpty()
                             join e in ctx.Set<MasterSubUnit>() on a.SubUnit equals e.Kode into _e
                             from e in _e.DefaultIfEmpty()
                                 //where a.Tahun == (periode.HasValue ? periode.Value : a.Tahun)
                             select new RegistrasiAnggotaCIPEnter()
                             {
                                 Abster = a.Abster,
                                 CreatedBy = a.CreatedBy,
                                 CreatedDate = a.CreatedDate,
                                 Email = a.Email,
                                 SubUnitDes = e != null ? e.Deskripsi : "",
                                 JenisCIPDes = b != null ? b.Deskripsi : "",
                                 JudulCIP = a.JudulCIP,
                                 JumlahAnggota = a.JumlahAnggota,
                                 DirektoratDes = c != null ? c.Deskripsi : "",
                                 Keterangan = a.Keterangan,
                                 Kode = a.Kode,
                                 SubUnit = a.SubUnit,
                                 KodeJenisCIP = a.KodeJenisCIP,
                                 Direktorat = a.Direktorat,
                                 Unit = a.Unit,
                                 KOMET = a.KOMET,
                                 UnitDes = d != null ? d.Deskripsi : "",
                                 NamaGugus = a.NamaGugus,
                                 Risalah = a.Risalah,
                                 Status = a.Status,
                                 STK = a.STK,
                                 Tahun = a.Tahun,
                                 UpdatedBy = a.UpdatedBy,
                                 UpdatedDate = a.UpdatedDate,
                                 ValueCreationKategori = a.ValueCreationKategori,
                                 ValueCreationPotensi = a.ValueCreationPotensi,
                                 ValueCreationReal = a.ValueCreationReal,
                                 VerifikasiKeuangan = a.VerifikasiKeuangan,
                                 Fasilitator = a.Fasilitator,
                                 Ketua = a.Ketua,
                                 UploadPresentasi = a.UploadPresentasi,
                                 IsAktif = a.IsAktif,
                                 KodeStatus = (int)Enum.Parse(typeof(EnumRegistrationStatusEnterprise), a.Status.Replace(" ", "_")),
                                 KodeRegistrasi = a.KodeRegistrasi,
                                 CipDirektorat = a.CipDirektorat,
                                 CipEnterprise = a.CipEnterprise
                             }).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiAnggotaCIPEnter>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public virtual FormatedList<RegistrasiCIP> DatatablesByStatus(int periode, string unit, string status)
        {
            var inStatus = p_GetRegStatus(status);


            IQueryable<RegistrasiCIP> queryable = ctx.Set<RegistrasiCIP>().Where(x => x.Tahun == periode && inStatus.Contains(x.Status)
                && x.Unit == (this.UserProfile.IsAdministrator ? x.Unit : unit)
            ).OrderBy(x => x.CreatedDate).AsQueryable();

            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<RegistrasiCIP>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public List<Dropdown> DropdownForAuditor(int periode)
        {
            var status = BaseEnums.EnumRegistrationStatus.Wait_to_Audit_PDCA_I.ReplaceUnderScoreToSpace();
            var dropdowns = base.FindBy(x => x.Tahun == periode && x.Status == status
                && x.Unit == (this.UserProfile.IsAdministrator ? x.Unit : this.UserProfile.Unit)
            )
                .Select(x => new Dropdown()
                {
                    id = x.Kode,
                    value = x.Kode,
                    text = x.Kode + " - " + x.NamaGugus
                }).ToList();

            return dropdowns;
        }

        public List<Dropdown> DropdownForStreamGugusDir(int tahun)
        {
            var gugusInStream = ctx.Set<StreamGugusDir>().Select(x => x.CipDirektorat).ToList();

            var status = BaseEnums.EnumRegistrationStatus.Ready_to_Show.ReplaceUnderScoreToSpace();
            var dropdowns = (from a in (ctx.Set<AsignCipDir>().Where(x => x.Tahun == tahun && x.Status == status && !gugusInStream.Contains(x.CipDirektorat)
              && x.Direktorat == (this.UserProfile.IsAdministrator ? x.Direktorat : this.UserProfile.Direktorat))).ToList()
                             select new Dropdown()
                             {
                                 id = a.CipDirektorat,
                                 value = a.CipDirektorat,
                                 text = a.NamaGugus
                             }).ToList();

            return dropdowns;
        }
        public List<Dropdown> DropdownForStreamGugus(int tahun)
        {
            var gugusInStream = ctx.Set<StreamGugus>().Select(x => x.KodeRegistrasi).ToList();

            var status = BaseEnums.EnumRegistrationStatus.Ready_to_Show.ReplaceUnderScoreToSpace();
            var dropdowns = base.FindBy(x => x.Tahun == tahun && x.Status == status && !gugusInStream.Contains(x.Kode)
                    && x.Unit == (this.UserProfile.IsAdministrator ? x.Unit : this.UserProfile.Unit)
                )
                .Select(x => new Dropdown()
                {
                    id = x.Kode,
                    value = x.Kode,
                    text = x.NamaGugus
                }).ToList();

            return dropdowns;
        }

        public bool UpdateStatusCipDir(string kode, string status)
        {
            try
            {
                var model = ctx.Set<AsignCipDir>().SingleOrDefault(x => x.CipDirektorat.Equals(kode));
                if (model != null)
                {
                    return this.UpdateStatusDir(model.Kode.ToString(), status);
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

        }


        public bool UpdateStatusCipEnter(string kode, string status)
        {
            try
            {
                var model = ctx.Set<AsignCipEnter>().SingleOrDefault(x => x.CipEnterprise.Equals(kode));
                if (model != null)
                {
                    return this.UpdateStatusEnter(model.Kode.ToString(), status);
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

        }

        public bool UpdateStatus(string kode, string status)
        {
            try
            {
                var model = base.GetSingle(kode);
                if (model != null)
                {
                    model.Status = status;
                    base.Update(model);

                    //var excludeStatus = new string[]
                    //{
                    //    BaseEnums.EnumRegistrationStatus.
                    //}
                    p_BroadcastEmail(model);

                    return this.p_CreateLog(kode, status);
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public bool UpdateStatusDir(string kode, string status)
        {
            try
            {
                using (IGenericDataRepository<AsignCipDir> repo = new DataRepository<AsignCipDir>(ctx))
                {
                    repo.UserProfile = this.UserProfile;
                    var model = repo.GetSingle(kode);
                    if (model != null)
                    {
                        model.Status = status;
                        repo.Update(model);

                        //p_BroadcastEmail(model);
                        return this.p_CreateLog(model.CipDirektorat, status);
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

        }

        public bool UpdateStatusEnter(string kode, string status)
        {
            try
            {
                using (IGenericDataRepository<AsignCipEnter> repo = new DataRepository<AsignCipEnter>(ctx))
                {
                    repo.UserProfile = this.UserProfile;
                    var model = repo.GetSingle(kode);
                    if (model != null)
                    {
                        model.Status = status;
                        repo.Update(model);

                        //p_BroadcastEmail(model);
                        return this.p_CreateLog(model.CipEnterprise, status);
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

        }

        public int GetCountApprovalStatus(int periode, string unit)
        {
            var count = base.GetList(x => p_GetRegStatus("approval").Contains(x.Status) && x.Tahun == periode && x.Unit == unit).Count();
            return count;
        }

        public int GetCountNewRegStatus(int periode, string unit)
        {
            var count = base.GetList(x => p_GetRegStatus("new").Contains(x.Status) && x.Tahun == periode && x.Unit == unit).Count();
            return count;
        }
        public int GetCountAuditStatus(int periode, string unit)
        {
            var count = base.GetList(x => p_GetRegStatus("audit").Contains(x.Status) && x.Tahun == periode && x.Unit == unit).Count();
            return count;
        }

        public int GetCountPenjurianStatus(int periode, string unit)
        {
            var count = base.GetList(x => p_GetRegStatus("penjurian").Contains(x.Status) && x.Tahun == periode && x.Unit == unit).Count();
            return count;
        }

        public bool SendNotificationUploadDocument(string kodeRegistrasi, string docType)
        {
            var model = base.GetSingle(kodeRegistrasi);
            if (model != null)
            {
                if (!string.IsNullOrEmpty(docType))
                {
                    if (model.GetType().GetProperty(docType) != null)
                    {
                        model.GetType().GetProperty(docType).SetValue(model, false, null);

                        var success = base.Update(model);

                        if (success)
                        {
                            p_BroadcastEmailDocument(model, docType);
                        }

                        return success;
                    }
                    else
                    {
                        throw new Exception(string.Format("Kolom Berkas \"{0}\" tidak ditemukan.", docType[0]));
                    }
                }
                else
                {
                    throw new Exception("Kolom Berkas tidak ditemukan.");
                }
            }
            else
            {
                throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
            }
        }

        public IEnumerable<RegistrasiAnggotaCIP> DaftarPeserta()
        {
            return p_Browse();
        }

        public IEnumerable<RegistrasiAnggotaCIPDir> DaftarPesertaDir()
        {
            return p_BrowseDir();
        }

        public IEnumerable<RegistrasiAnggotaCIPEnter> DaftarPesertaEnter()
        {
            return p_BrowseEnter();
        }

        public RegistrasiCIP GetDocument(string kode)
        {
            var rec = base.FindBy(x => x.Kode == kode).ToList().Select(x => new RegistrasiCIP
            {
                Risalah = x.Risalah,
                VerifikasiKeuangan = x.VerifikasiKeuangan,
                STK = x.STK,
                Abster = x.Abster,
                KOMET = x.KOMET,
                UploadPresentasi = x.UploadPresentasi
            }).FirstOrDefault();

            return rec;
        }

        public FormatedList<sp_GetCipForDirektorat_Result> DataTableForAssignToDir(int tahun, string unit, string direktorat)
        {
            using (var ctx_ = (GenericContext)ctx)
            {
                IQueryable<sp_GetCipForDirektorat_Result> queryable = ctx_.sp_GetCipForDirektorat(tahun, unit, direktorat)
                    .ToList().AsQueryable();

                var request = HttpContext.Current.Request;
                var wrapper = new HttpRequestWrapper(request);
                var parser = new DataTablesParser<sp_GetCipForDirektorat_Result>(wrapper, queryable);
                var datatable = parser.Parse();

                return datatable;

            }
        }

        public FormatedList<sp_GetCipAssignedDirektorat_Result> DataTableAssignDir(int tahun, string unit, string direktorat)
        {
            using (var ctx_ = (GenericContext)ctx)
            {
                IQueryable<sp_GetCipAssignedDirektorat_Result> queryable = ctx_.sp_GetCipAssignedDirektorat(tahun, unit, direktorat)
                    .ToList().AsQueryable();

                var request = HttpContext.Current.Request;
                var wrapper = new HttpRequestWrapper(request);
                var parser = new DataTablesParser<sp_GetCipAssignedDirektorat_Result>(wrapper, queryable);
                var datatable = parser.Parse();

                return datatable;

            }
        }

        public FormatedList<sp_GetCipForEnterprise_Result> DataTableForAssignToEnter(int tahun, string unit, string direktorat)
        {
            using (var ctx_ = (GenericContext)ctx)
            {
                IQueryable<sp_GetCipForEnterprise_Result> queryable = ctx_.sp_GetCipForEnterprise(tahun, unit, direktorat)
                    .ToList().AsQueryable();

                var request = HttpContext.Current.Request;
                var wrapper = new HttpRequestWrapper(request);
                var parser = new DataTablesParser<sp_GetCipForEnterprise_Result>(wrapper, queryable);
                var datatable = parser.Parse();

                return datatable;

            }
        }

        public FormatedList<sp_GetCipAssignedEnterprise_Result> DataTableAssignEnter(int tahun, string unit, string direktorat)
        {
            using (var ctx_ = (GenericContext)ctx)
            {
                IQueryable<sp_GetCipAssignedEnterprise_Result> queryable = ctx_.sp_GetCipAssignedEnterprise(tahun, unit, direktorat)
                    .ToList().AsQueryable();

                var request = HttpContext.Current.Request;
                var wrapper = new HttpRequestWrapper(request);
                var parser = new DataTablesParser<sp_GetCipAssignedEnterprise_Result>(wrapper, queryable);
                var datatable = parser.Parse();

                return datatable;

            }
        }

        public bool CreateAsignCipDir(List<AsignCipDir> models)
        {
            using (IGenericDataRepository<AsignCipDir> repo = new DataRepository<AsignCipDir>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                var listKodeRegistrasi = models.Select(x => x.KodeRegistrasi);
                var registrasiCIPExist = repo.FindBy(x => listKodeRegistrasi.Contains(x.KodeRegistrasi)).Select(x => x.KodeRegistrasi).ToList();
                if (registrasiCIPExist.Count() > 0)
                {
                    string kodeRegistrasiExist = string.Join("," + Environment.NewLine, registrasiCIPExist);
                    throw new Exception(string.Format("No. Gugus: {0}{1} Sudah terdaftar di CIP Direktorat.", Environment.NewLine, kodeRegistrasiExist));
                };

                var registrasiCIPs = base.FindBy(x => listKodeRegistrasi.Contains(x.Kode)).ToList();
                var modelFrom = JsonConvert.SerializeObject(registrasiCIPs);
                modelFrom = modelFrom.Replace("\"Kode\"", "\"KodeRegistrasi\"");
                List<AsignCipDir> modelTo = JsonConvert.DeserializeObject<List<AsignCipDir>>(modelFrom);
                using (var tran = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        foreach (AsignCipDir model in modelTo)
                        {
                            model.CreatedDate = null;
                            model.CreatedBy = null;
                            model.UpdatedBy = null;
                            model.UpdatedDate = null;

                            model.CipDirektorat = Guid.NewGuid().ToString();
                            model.Status = BaseEnums.EnumRegistrationStatusDirektorat.Pending_Approval_Admin_Direktorat.ReplaceUnderScoreToSpace();
                            model.IsAktivasi = true;

                            //Save Registrasi CIP
                            repo.Create(model);

                            //Create Anggota CIP
                            var anggotaUnit = ctx.Set<AnggotaCIP>().Where(x => x.KodeRegistrasi == model.KodeRegistrasi);
                            var anggotaFrom = JsonConvert.SerializeObject(anggotaUnit);
                            anggotaFrom = anggotaFrom.Replace(model.KodeRegistrasi, model.CipDirektorat);
                            var listAnggotaCIP = JsonConvert.DeserializeObject<List<AnggotaCIPDir>>(anggotaFrom);

                            this.p_CreateAnggotaDir(model, listAnggotaCIP);

                            // Create Registrasi Log
                            this.p_CreateLog(model.CipDirektorat, EnumRegistrationStatusDirektorat.Registrasi_Peserta_Direktorat.ReplaceUnderScoreToSpace());

                            // Create Registrasi Log
                            this.p_CreateLog(model.CipDirektorat, model.Status);

                        }

                        tran.Commit();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();

                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        public bool CreateAsignCipEnter(List<AsignCipEnter> models)
        {
            using (IGenericDataRepository<AsignCipEnter> repo = new DataRepository<AsignCipEnter>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                var listCipDirektorat = models.Select(x => x.CipDirektorat);
                var registrasiCIPExist = repo.FindBy(x => listCipDirektorat.Contains(x.CipDirektorat)).Select(x => x.CipDirektorat).ToList();
                if (registrasiCIPExist.Count() > 0)
                {
                    string cipDirektoratExist = string.Join("," + Environment.NewLine, registrasiCIPExist);
                    throw new Exception(string.Format("No. Gugus: {0}{1} Sudah terdaftar di CIP Enterprise.", Environment.NewLine, cipDirektoratExist));
                };

                List<AsignCipEnter> modelTo = new List<AsignCipEnter>();
                using (IGenericDataRepository<AsignCipDir> repoDir = new DataRepository<AsignCipDir>(ctx))
                {
                    repoDir.UserProfile = this.UserProfile;

                    var registrasiCIPs = repoDir.FindBy(x => listCipDirektorat.Contains(x.CipDirektorat)).ToList();
                    var modelFrom = JsonConvert.SerializeObject(registrasiCIPs);
                    //modelFrom = modelFrom.Replace("\"Kode\"", "\"KodeRegistrasi\"");
                    modelTo = JsonConvert.DeserializeObject<List<AsignCipEnter>>(modelFrom);
                }

                using (var tran = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        foreach (AsignCipEnter model in modelTo)
                        {
                            model.Kode = 0;
                            model.CreatedDate = null;
                            model.CreatedBy = null;
                            model.UpdatedBy = null;
                            model.UpdatedDate = null;

                            model.CipEnterprise = Guid.NewGuid().ToString();
                            model.Status = BaseEnums.EnumRegistrationStatusEnterprise.Pending_Approval_Admin_Enterprise.ReplaceUnderScoreToSpace();
                            model.IsAktivasi = true;

                            //Save Registrasi CIP
                            repo.Create(model);

                            //Create Anggota CIP
                            var anggotaDirektorat = ctx.Set<AnggotaCIPDir>().Where(x => x.KodeRegistrasi == model.CipDirektorat);
                            var anggotaFrom = JsonConvert.SerializeObject(anggotaDirektorat);
                            anggotaFrom = anggotaFrom.Replace(model.CipDirektorat, model.CipEnterprise);
                            var listAnggotaCIP = JsonConvert.DeserializeObject<List<AnggotaCIPEnter>>(anggotaFrom);

                            this.p_CreateAnggotaEnter(model, listAnggotaCIP);

                            // Create Registrasi Log
                            this.p_CreateLog(model.CipEnterprise, EnumRegistrationStatusEnterprise.Registrasi_Peserta_Enterprise.ReplaceUnderScoreToSpace());

                            // Create Registrasi Log
                            this.p_CreateLog(model.CipEnterprise, model.Status);
                        }

                        tran.Commit();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();

                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        public bool UndoAsignCipDir(string kodeRegistrasi)
        {
            using (IGenericDataRepository<AsignCipDir> repo = new DataRepository<AsignCipDir>(ctx))
            {
                using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        repo.UserProfile = this.UserProfile;
                        var status = BaseEnums.EnumRegistrationStatusDirektorat.Pending_Approval_Admin_Direktorat.ReplaceUnderScoreToSpace();
                        var model = repo.FindBy(x => x.KodeRegistrasi == kodeRegistrasi && x.Status == status).FirstOrDefault();
                        if (model == null)
                        {
                            throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                        }

                        repo.Delete(model);

                        var listAnggota = ctx.Set<AnggotaCIPDir>().Where(x => x.KodeRegistrasi == model.CipDirektorat);
                        ctx.Set<AnggotaCIPDir>().RemoveRange(listAnggota);
                        ctx.SaveChanges();

                        var listLog = ctx.Set<RegistrasiCIPLog>().Where(x => x.KodeRegistrasi == model.CipDirektorat);
                        ctx.Set<RegistrasiCIPLog>().RemoveRange(listLog);
                        ctx.SaveChanges();

                        trans.Commit();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();

                        throw new Exception(ex.Message);
                    }
                }
            }
        }
        public bool UndoAsignCipEnter(string cipDirektorat)
        {
            using (IGenericDataRepository<AsignCipEnter> repo = new DataRepository<AsignCipEnter>(ctx))
            {

                using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        repo.UserProfile = this.UserProfile;
                        var status = BaseEnums.EnumRegistrationStatusEnterprise.Pending_Approval_Admin_Enterprise.ReplaceUnderScoreToSpace();
                        var model = repo.FindBy(x => x.KodeRegistrasi == cipDirektorat && x.Status == status).FirstOrDefault();
                        if (model == null)
                        {
                            throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                        }

                        repo.Delete(model);

                        var listAnggota = ctx.Set<AnggotaCIPEnter>().Where(x => x.KodeRegistrasi == model.CipEnterprise);
                        ctx.Set<AnggotaCIPEnter>().RemoveRange(listAnggota);
                        ctx.SaveChanges();

                        var listLog = ctx.Set<RegistrasiCIPLog>().Where(x => x.KodeRegistrasi == model.CipEnterprise);
                        ctx.Set<RegistrasiCIPLog>().RemoveRange(listLog);
                        ctx.SaveChanges();

                        trans.Commit();

                        return true;
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();

                        throw new Exception(ex.Message);
                    }
                }
            }
        }

        #region Private Method

        private string p_GenereateAutoNumberUnit(RegistrasiCIP model)
        {
            //{TAHUN}-CIP-{JENIS_CIP}-{DIREKTORAT}-{UNIT}-{URUT}
            var tahun = model.Tahun;
            var jenisCIP = model.KodeJenisCIP;
            var direktorat = model.Direktorat;
            var unit = model.Unit;

            var masterJenisCip = ctx.Set<MasterJenisCIP>().Find(model.KodeJenisCIP);
            var masterDirektorat = ctx.Set<MasterDirektorat>().Find(model.Direktorat);
            var masterUnit = ctx.Set<MasterUnit>().Find(model.Unit);

            if (masterJenisCip != null)
            {
                if (!string.IsNullOrEmpty(masterJenisCip.NamaSingkat))
                {
                    jenisCIP = masterJenisCip.NamaSingkat;
                }
            }
            if (masterDirektorat != null)
            {
                if (!string.IsNullOrEmpty(masterDirektorat.NamaSingkat))
                {
                    direktorat = masterDirektorat.NamaSingkat;
                }
            }
            if (masterUnit != null)
            {
                if (!string.IsNullOrEmpty(masterUnit.NamaSingkat))
                {
                    unit = masterUnit.NamaSingkat;
                }
            }

            string kodeFormat = BaseConstants.REGISTRATION_NUMBER_FORMAT_UNIT;
            string cekKodeFormat = string.Format(kodeFormat, tahun, jenisCIP, direktorat, unit, "");
            string urut = cekKodeFormat.GetLastAutoNumber((GenericContext)this.ctx, this.UserProfile.Kode).ToString();
            switch (urut.Length)
            {
                case 1:
                    urut = "000" + urut;
                    break;
                case 2:
                    urut = "00" + urut;
                    break;
                case 3:
                    urut = "0" + urut;
                    break;
                default:
                    break;
            }
            kodeFormat = string.Format(kodeFormat, tahun, jenisCIP, direktorat, unit, urut);

            return kodeFormat;
        }

        private string p_GenereateAutoNumberDir(AsignCipDir model)
        {
            //{TAHUN}-CIP-{JENIS_CIP}-{DIREKTORAT}-{URUT}
            var tahun = model.Tahun;
            var jenisCIP = model.KodeJenisCIP;
            var direktorat = model.Direktorat;

            var masterJenisCip = ctx.Set<MasterJenisCIP>().Find(model.KodeJenisCIP);
            var masterDirektorat = ctx.Set<MasterDirektorat>().Find(model.Direktorat);

            if (masterJenisCip != null)
            {
                if (!string.IsNullOrEmpty(masterJenisCip.NamaSingkat))
                {
                    jenisCIP = masterJenisCip.NamaSingkat;
                }
            }
            if (masterDirektorat != null)
            {
                if (!string.IsNullOrEmpty(masterDirektorat.NamaSingkat))
                {
                    direktorat = masterDirektorat.NamaSingkat;
                }
            }

            string kodeFormat = BaseConstants.REGISTRATION_NUMBER_FORMAT_DIR;
            string cekKodeFormat = string.Format(kodeFormat, tahun, jenisCIP, direktorat, "");
            string urut = cekKodeFormat.GetLastAutoNumber((GenericContext)this.ctx, this.UserProfile.Kode).ToString();
            switch (urut.Length)
            {
                case 1:
                    urut = "000" + urut;
                    break;
                case 2:
                    urut = "00" + urut;
                    break;
                case 3:
                    urut = "0" + urut;
                    break;
                default:
                    break;
            }
            kodeFormat = string.Format(kodeFormat, tahun, jenisCIP, direktorat, urut);

            return kodeFormat;
        }

        private string p_GenereateAutoNumberEnter(AsignCipEnter model)
        {
            //{TAHUN}-CIP-{JENIS_CIP}-{URUT}
            var tahun = model.Tahun;
            var jenisCIP = model.KodeJenisCIP;

            var masterJenisCip = ctx.Set<MasterJenisCIP>().Find(model.KodeJenisCIP);

            if (masterJenisCip != null)
            {
                if (!string.IsNullOrEmpty(masterJenisCip.NamaSingkat))
                {
                    jenisCIP = masterJenisCip.NamaSingkat;
                }
            }

            string kodeFormat = BaseConstants.REGISTRATION_NUMBER_FORMAT_ENTER;
            string cekKodeFormat = string.Format(kodeFormat, tahun, jenisCIP, "");
            string urut = cekKodeFormat.GetLastAutoNumber((GenericContext)this.ctx, this.UserProfile.Kode).ToString();
            switch (urut.Length)
            {
                case 1:
                    urut = "000" + urut;
                    break;
                case 2:
                    urut = "00" + urut;
                    break;
                case 3:
                    urut = "0" + urut;
                    break;
                default:
                    break;
            }
            kodeFormat = string.Format(kodeFormat, tahun, jenisCIP, urut);

            return kodeFormat;
        }

        private bool p_UpdateVerifikasiAdminQM(string kode, string status, string keterangan)
        {
            try
            {
                var model = base.GetSingle(kode);
                if (model != null)
                {
                    model.Status = status;
                    model.Keterangan = keterangan;
                    base.Update(model);

                    p_BroadcastEmail(model);

                    return this.p_CreateLog(kode, status);
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        private bool p_UpdateVerifikasiAdminDir(string kode, string status, string keterangan)
        {
            try
            {
                using (IGenericDataRepository<AsignCipDir> repo = new DataRepository<AsignCipDir>(ctx))
                {
                    repo.UserProfile = this.UserProfile;

                    var model = repo.FindBy(x => x.CipDirektorat == kode).FirstOrDefault();
                    if (model != null)
                    {
                        model.Status = status;
                        model.Keterangan = keterangan;
                        repo.Update(model);

                        //p_BroadcastEmail(model);
                        return this.p_CreateLog(kode, status);
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        private bool p_UpdateVerifikasiAdminEnter(string kode, string status, string keterangan)
        {
            try
            {
                using (IGenericDataRepository<AsignCipEnter> repo = new DataRepository<AsignCipEnter>(ctx))
                {
                    repo.UserProfile = this.UserProfile;

                    var model = repo.FindBy(x => x.CipEnterprise == kode).FirstOrDefault();
                    if (model != null)
                    {
                        model.Status = status;
                        model.Keterangan = keterangan;
                        repo.Update(model);

                        //p_BroadcastEmail(model);
                        return this.p_CreateLog(kode, status);
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        private void p_BroadcastEmail(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP = null)
        {
            if (model.Status == BaseEnums.EnumRegistrationStatus.Registrasi_Peserta_CIP.ReplaceUnderScoreToSpace())
            {

                Task.Run(() =>
                {
                    var broadcast = new Broadcast();
                    var mailTo = new MailAddressCollection();
                    var subject = string.Format("Registrasi Peserta CIP Berhasil");
                    var body = string.Format(@"Kepada Bapak/Ibu Insan Mutu,</br><b>Selamat! proses registrasi anda telah berhasil dengan rincian pendaftaran sebagai berikut :</b><hr></hr><br/>" +
                    "<table width=\"100%\">" +
                        "<tr><td width=\"20%\">No. Gugus</td><td>:</td><td>" + model.Kode + "</td></tr>" +
                        "<tr><td>Tahun</td><td>:</td><td>" + model.Tahun + "</td></tr>" +
                        "<tr><td>Nama Gugus</td><td>:</td><td>" + model.NamaGugus + "</td></tr>" +
                        //"<tr><td>Kategori Gugus</td><td>:</td><td>" + model.KodeKategori + "</td></tr>" +
                        "<tr><td>Judul CIP</td><td>:</td><td>" + model.JudulCIP + "</td></tr>" +
                    "</table> <br/>");

                    body = body + "</br></br>Terima kasih atas partisipasi dan keikutsertaannya pada Forum Hulu " + model.Tahun + ".<br/></br>" +
                            "Salam,<br/>" +
                            "Panitia Forum Hulu " + model.Tahun + "<br/>";

                    mailTo.Add(model.Email);
                    var mailCC = p_GetMailCC(listAnggotaCIP);

                    broadcast.SendMail(mailTo, subject, body, mailCC);
                });
            }
            else
            {
                Task.Run(() =>
                {
                    var broadcast = new Broadcast();
                    var mailTo = new MailAddressCollection();
                    var subject = string.Format("Status CIP - {0}", model.Status);
                    var body = string.Format(@"<p>Kepada Bapak/Ibu Insan Mutu,</br><b>Selamat! Selamat, anda telah berhasil terdaftar sebagai salah satu Peserta CIP pada Forum Hulu " + model.Tahun + "</p><p>Detail Data Peserta sebagai berikut:</p></b><hr></hr><br/>" + "<br/>" +
                        "<table>" +
                            "<tr><td>No. Gugus</td><td>:</td><td>" + model.Kode + "</td></tr>" +
                            "<tr><td>Tahun</td><td>:</td><td>" + model.Tahun + "</td></tr>" +
                            "<tr><td>Nama Gugus</td><td>:</td><td>" + model.NamaGugus + "</td></tr>" +
                            //"<tr><td>Kategori Gugus</td><td>:</td><td>" + model.KodeKategori + "</td></tr>" +
                            "<tr><td>Judul CIP</td><td>:</td><td>" + model.JudulCIP + "</td></tr>" +
                        "</table>" +
                        "</br>" +
                        "Terima kasih atas partisipasi dan keikutsertaannya pada Forum Hulu" + model.Tahun + ".<br/></br>" +
                        "Salam,<br/>" +
                        "Panitia Forum Hulu " + model.Tahun + "<br/>" +
                        "<hr></hr>"
                    );

                    mailTo.Add(model.Email);
                    var mailCC = p_GetMailCC(listAnggotaCIP);

                    broadcast.SendMail(mailTo, subject, body, mailCC);
                });
            }
        }

        private void p_BroadcastEmailDir(AsignCipDir model, List<AnggotaCIPDir> listAnggotaCIP = null)
        {
            if (model.Status == BaseEnums.EnumRegistrationStatus.Registrasi_Peserta_CIP.ReplaceUnderScoreToSpace())
            {

                Task.Run(() =>
                {
                    var broadcast = new Broadcast();
                    var mailTo = new MailAddressCollection();
                    var subject = string.Format("Registrasi Peserta CIP Berhasil");
                    var body = string.Format(@"Kepada Bapak/Ibu Insan Mutu,</br><b>Selamat! proses registrasi anda telah berhasil dengan rincian pendaftaran sebagai berikut :</b><hr></hr><br/>" +
                    "<table width=\"100%\">" +
                        "<tr><td width=\"20%\">No. Gugus</td><td>:</td><td>" + model.Kode + "</td></tr>" +
                        "<tr><td>Tahun</td><td>:</td><td>" + model.Tahun + "</td></tr>" +
                        "<tr><td>Nama Gugus</td><td>:</td><td>" + model.NamaGugus + "</td></tr>" +
                        //"<tr><td>Kategori Gugus</td><td>:</td><td>" + model.KodeKategori + "</td></tr>" +
                        "<tr><td>Judul CIP</td><td>:</td><td>" + model.JudulCIP + "</td></tr>" +
                    "</table> <br/>");

                    body = body + "</br></br>Terima kasih atas partisipasi dan keikutsertaannya pada Forum Hulu " + model.Tahun + ".<br/></br>" +
                            "Salam,<br/>" +
                            "Panitia Forum Hulu " + model.Tahun + "<br/>";

                    mailTo.Add(model.Email);
                    var mailCC = p_GetMailCCDir(listAnggotaCIP);

                    broadcast.SendMail(mailTo, subject, body, mailCC);
                });
            }
            else
            {
                Task.Run(() =>
                {
                    var broadcast = new Broadcast();
                    var mailTo = new MailAddressCollection();
                    var subject = string.Format("Status CIP - {0}", model.Status);
                    var body = string.Format(@"<p>Kepada Bapak/Ibu Insan Mutu,</br><b>Selamat! Selamat, anda telah berhasil terdaftar sebagai salah satu Peserta CIP pada Forum Hulu " + model.Tahun + "</p><p>Detail Data Peserta sebagai berikut:</p></b><hr></hr><br/>" + "<br/>" +
                        "<table>" +
                            "<tr><td>No. Gugus</td><td>:</td><td>" + model.Kode + "</td></tr>" +
                            "<tr><td>Tahun</td><td>:</td><td>" + model.Tahun + "</td></tr>" +
                            "<tr><td>Nama Gugus</td><td>:</td><td>" + model.NamaGugus + "</td></tr>" +
                            //"<tr><td>Kategori Gugus</td><td>:</td><td>" + model.KodeKategori + "</td></tr>" +
                            "<tr><td>Judul CIP</td><td>:</td><td>" + model.JudulCIP + "</td></tr>" +
                        "</table>" +
                        "</br>" +
                        "Terima kasih atas partisipasi dan keikutsertaannya pada Forum Hulu" + model.Tahun + ".<br/></br>" +
                        "Salam,<br/>" +
                        "Panitia Forum Hulu " + model.Tahun + "<br/>" +
                        "<hr></hr>"
                    );

                    mailTo.Add(model.Email);
                    var mailCC = p_GetMailCCDir(listAnggotaCIP);

                    broadcast.SendMail(mailTo, subject, body, mailCC);
                });
            }
        }

        private void p_BroadcastEmailDocument(RegistrasiCIP model, string docType)
        {
            Task.Run(() =>
            {
                var broadcast = new Broadcast();
                var mailTo = new MailAddressCollection();
                var subject = string.Format("Silahkan Bpk/Ibu melakukan upload kelengkapan Dokumen - {0}", p_AddSpacesToSentence(docType.Replace("IsUpload", ""), true));
                var body =
                    "<h2>" + subject + "</h2><br /><br />" +
                    string.Format(@"<h2>Data Peserta berikut: </h2><br/>" +
                    "<table>" +
                        "<tr><td>No. Gugus</td><td>:</td><td>" + model.Kode + "</td></tr>" +
                        "<tr><td>Tahun</td><td>:</td><td>" + model.Tahun + "</td></tr>" +
                        "<tr><td>Nama Gugus</td><td>:</td><td>" + model.NamaGugus + "</td></tr>" +
                        "<tr><td>Judul CIP</td><td>:</td><td>" + model.JudulCIP + "</td></tr>" +
                    "</table>"
                );

                mailTo.Add(model.Email);

                broadcast.SendMail(mailTo, subject, body);
            });
        }

        private MailAddressCollection p_GetMailCC(List<AnggotaCIP> listAnggotaCIP)
        {
            var mailCC = new MailAddressCollection();
            if (listAnggotaCIP != null)
            {
                var recMails = (from a in listAnggotaCIP
                                join b in ctx.Set<AdministrasiUser>() on a.KodeUser equals b.Kode
                                where a.KodeStatusAnggota != "1"
                                select new
                                {
                                    Email = b.Email
                                }
                        );

                foreach (var rec in recMails)
                {
                    mailCC.Add(rec.Email);
                }
            }

            return mailCC;
        }

        private MailAddressCollection p_GetMailCCDir(List<AnggotaCIPDir> listAnggotaCIP)
        {
            var mailCC = new MailAddressCollection();
            if (listAnggotaCIP != null)
            {
                var recMails = (from a in listAnggotaCIP
                                join b in ctx.Set<AdministrasiUser>() on a.KodeUser equals b.Kode
                                where a.KodeStatusAnggota != "1"
                                select new
                                {
                                    Email = b.Email
                                }
                        );

                foreach (var rec in recMails)
                {
                    mailCC.Add(rec.Email);
                }
            }

            return mailCC;
        }
        string p_AddSpacesToSentence(string text, bool preserveAcronyms)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                    if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                        (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                         i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                        newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }
        private bool p_CreateLog(string kodeRegistrasi, string status)
        {
            using (IGenericDataRepository<RegistrasiCIPLog> repo = new DataRepository<RegistrasiCIPLog>(ctx))
            {
                repo.UserProfile = this.UserProfile;

                RegistrasiCIPLog model = new RegistrasiCIPLog()
                {
                    Kode = Guid.NewGuid().ToString(),
                    KodeRegistrasi = kodeRegistrasi,
                    Status = status
                };

                return repo.Create(model);
            }
        }

        private void p_CreateAnggota(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP)
        {
            //Remove Existing Records
            var sql = string.Format(@"
                            delete from AnggotaCIP where KodeRegistrasi ='{0}';"
                        , model.Kode);
            ctx.Database.ExecuteSqlCommand(sql);

            //Insert New Records
            using (var anggotaRepo = new DataRepository<AnggotaCIP>(ctx))
            {
                anggotaRepo.UserProfile = this.UserProfile;

                var recAnggotas = anggotaRepo.FindBy(x => x.KodeRegistrasi == model.Kode);
                if (recAnggotas.Count() > 0)
                {
                    ctx.Set<AnggotaCIP>().RemoveRange(recAnggotas);
                    ctx.SaveChanges();
                }

                var i = 1;
                foreach (var anggota in listAnggotaCIP)
                {
                    anggota.KodeRegistrasi = model.Kode;
                    var recAnggota = anggotaRepo.FindBy(x => x.KodeUser == anggota.KodeUser && x.KodeRegistrasi == anggota.KodeRegistrasi).FirstOrDefault();
                    if (recAnggota != null)
                    {
                        throw new Exception(string.Format("User: \"{0}\" sudah terdaftar dalam Tim.", anggota.KodeUser));
                    }

                    anggota.Kode = string.Format("{0}-{1}", model.Kode, i);

                    anggotaRepo.Create(anggota);

                    i++;
                }
            }
        }

        private void p_CreateAnggotaDir(AsignCipDir model, List<AnggotaCIPDir> listAnggotaCIP)
        {
            //Remove Existing Records
            var sql = string.Format(@"
                            delete from AnggotaCIPDir where KodeRegistrasi ='{0}';"
                        , model.CipDirektorat);
            ctx.Database.ExecuteSqlCommand(sql);

            //Insert New Records
            using (var anggotaRepo = new DataRepository<AnggotaCIPDir>(ctx))
            {
                anggotaRepo.UserProfile = this.UserProfile;

                var recAnggotas = anggotaRepo.FindBy(x => x.KodeRegistrasi == model.CipDirektorat);
                if (recAnggotas.Count() > 0)
                {
                    ctx.Set<AnggotaCIPDir>().RemoveRange(recAnggotas);
                    ctx.SaveChanges();
                }

                var i = 1;
                foreach (var anggota in listAnggotaCIP)
                {
                    anggota.KodeRegistrasi = model.CipDirektorat;
                    var recAnggota = anggotaRepo.FindBy(x => x.KodeUser == anggota.KodeUser && x.KodeRegistrasi == anggota.KodeRegistrasi).FirstOrDefault();
                    if (recAnggota != null)
                    {
                        throw new Exception(string.Format("User: \"{0}\" sudah terdaftar dalam Tim.", anggota.KodeUser));
                    }

                    anggota.Kode = string.Format("{0}-{1}", model.CipDirektorat, i);

                    anggotaRepo.Create(anggota);

                    i++;
                }
            }
        }

        private void p_CreateAnggotaEnter(AsignCipEnter model, List<AnggotaCIPEnter> listAnggotaCIP)
        {
            //Remove Existing Records
            var sql = string.Format(@"
                            delete from AnggotaCIPEnter where KodeRegistrasi ='{0}';"
                        , model.CipEnterprise);
            ctx.Database.ExecuteSqlCommand(sql);

            //Insert New Records
            using (var anggotaRepo = new DataRepository<AnggotaCIPEnter>(ctx))
            {
                anggotaRepo.UserProfile = this.UserProfile;

                var recAnggotas = anggotaRepo.FindBy(x => x.KodeRegistrasi == model.CipEnterprise);
                if (recAnggotas.Count() > 0)
                {
                    ctx.Set<AnggotaCIPEnter>().RemoveRange(recAnggotas);
                    ctx.SaveChanges();
                }

                var i = 1;
                foreach (var anggota in listAnggotaCIP)
                {
                    anggota.KodeRegistrasi = model.CipDirektorat;
                    var recAnggota = anggotaRepo.FindBy(x => x.KodeUser == anggota.KodeUser && x.KodeRegistrasi == anggota.KodeRegistrasi).FirstOrDefault();
                    if (recAnggota != null)
                    {
                        throw new Exception(string.Format("User: \"{0}\" sudah terdaftar dalam Tim.", anggota.KodeUser));
                    }

                    anggota.Kode = string.Format("{0}-{1}", model.CipEnterprise, i);

                    anggotaRepo.Create(anggota);

                    i++;
                }
            }
        }

        protected IEnumerable<RegistrasiAnggotaCIP> p_Browse()
        {
            //var anggota = ctx.Set<AnggotaCIP>().Select(x => new { x.KodeRegistrasi, x.KodeUser, x.KodeStatusAnggota }).OrderBy(x => x.KodeStatusAnggota);

            var records = from a in ctx.Set<RegistrasiCIP>().ToList()
                          join b in ctx.Set<MasterJenisCIP>().ToList() on a.KodeJenisCIP equals b.Kode
                          join c in ctx.Set<MasterDirektorat>().ToList() on a.Direktorat equals c.Kode
                          join d in ctx.Set<MasterUnit>().ToList() on a.Unit equals d.Kode
                          join e in ctx.Set<MasterSubUnit>().ToList() on a.SubUnit equals e.Kode
                          where a.Unit == (this.UserProfile.IsAdministrator ? a.Unit : this.UserProfile.Unit)
                          select new RegistrasiAnggotaCIP()
                          {
                              Abster = a.Abster,
                              //Anggota = string.Join(",", anggota.Where(x => x.KodeRegistrasi == a.Kode).Select(x => x.KodeUser)),
                              CreatedBy = a.CreatedBy,
                              CreatedDate = a.CreatedDate,
                              Email = a.Email,
                              SubUnitDes = e.Deskripsi,
                              JenisCIPDes = b.Deskripsi,
                              JudulCIP = a.JudulCIP,
                              JumlahAnggota = a.JumlahAnggota,
                              DirektoratDes = c.Deskripsi,
                              Keterangan = a.Keterangan,
                              Kode = a.Kode,
                              SubUnit = a.SubUnit,
                              KodeJenisCIP = a.KodeJenisCIP,
                              Direktorat = a.Direktorat,
                              Unit = a.Unit,
                              KOMET = a.KOMET,
                              UnitDes = d.Deskripsi,
                              NamaGugus = a.NamaGugus,
                              Risalah = a.Risalah,
                              Status = a.Status,
                              STK = a.STK,
                              Tahun = a.Tahun,
                              UpdatedBy = a.UpdatedBy,
                              UpdatedDate = a.UpdatedDate,
                              ValueCreationKategori = a.ValueCreationKategori,
                              ValueCreationPotensi = a.ValueCreationPotensi,
                              ValueCreationReal = a.ValueCreationReal,
                              VerifikasiKeuangan = a.VerifikasiKeuangan,
                              Fasilitator = a.Fasilitator,
                              Ketua = a.Ketua,
                              UploadPresentasi = a.UploadPresentasi
                          };

            return records;
        }

        protected IEnumerable<RegistrasiAnggotaCIPDir> p_BrowseDir()
        {
            var records = from a in ctx.Set<AsignCipDir>().ToList()
                          join b in ctx.Set<MasterJenisCIP>().ToList() on a.KodeJenisCIP equals b.Kode
                          join c in ctx.Set<MasterDirektorat>().ToList() on a.Direktorat equals c.Kode
                          join d in ctx.Set<MasterUnit>().ToList() on a.Unit equals d.Kode
                          join e in ctx.Set<MasterSubUnit>().ToList() on a.SubUnit equals e.Kode
                          where a.Direktorat == (this.UserProfile.IsAdministrator ? a.Direktorat : this.UserProfile.Direktorat)
                          select new RegistrasiAnggotaCIPDir()
                          {
                              Abster = a.Abster,
                              CreatedBy = a.CreatedBy,
                              CreatedDate = a.CreatedDate,
                              Email = a.Email,
                              SubUnitDes = e.Deskripsi,
                              JenisCIPDes = b.Deskripsi,
                              JudulCIP = a.JudulCIP,
                              JumlahAnggota = a.JumlahAnggota,
                              DirektoratDes = c.Deskripsi,
                              Keterangan = a.Keterangan,
                              Kode = a.Kode,
                              SubUnit = a.SubUnit,
                              KodeJenisCIP = a.KodeJenisCIP,
                              Direktorat = a.Direktorat,
                              Unit = a.Unit,
                              KOMET = a.KOMET,
                              UnitDes = d.Deskripsi,
                              NamaGugus = a.NamaGugus,
                              Risalah = a.Risalah,
                              Status = a.Status,
                              STK = a.STK,
                              Tahun = a.Tahun,
                              UpdatedBy = a.UpdatedBy,
                              UpdatedDate = a.UpdatedDate,
                              ValueCreationKategori = a.ValueCreationKategori,
                              ValueCreationPotensi = a.ValueCreationPotensi,
                              ValueCreationReal = a.ValueCreationReal,
                              VerifikasiKeuangan = a.VerifikasiKeuangan,
                              Fasilitator = a.Fasilitator,
                              Ketua = a.Ketua,
                              UploadPresentasi = a.UploadPresentasi,
                              KodeRegistrasi = a.KodeRegistrasi,
                              CipDirektorat = a.CipDirektorat
                          };

            return records;
        }

        protected IEnumerable<RegistrasiAnggotaCIPEnter> p_BrowseEnter()
        {
            var records = from a in ctx.Set<AsignCipEnter>().ToList()
                          join b in ctx.Set<MasterJenisCIP>().ToList() on a.KodeJenisCIP equals b.Kode
                          join c in ctx.Set<MasterDirektorat>().ToList() on a.Direktorat equals c.Kode
                          join d in ctx.Set<MasterUnit>().ToList() on a.Unit equals d.Kode
                          join e in ctx.Set<MasterSubUnit>().ToList() on a.SubUnit equals e.Kode
                          select new RegistrasiAnggotaCIPEnter()
                          {
                              Abster = a.Abster,
                              CreatedBy = a.CreatedBy,
                              CreatedDate = a.CreatedDate,
                              Email = a.Email,
                              SubUnitDes = e.Deskripsi,
                              JenisCIPDes = b.Deskripsi,
                              JudulCIP = a.JudulCIP,
                              JumlahAnggota = a.JumlahAnggota,
                              DirektoratDes = c.Deskripsi,
                              Keterangan = a.Keterangan,
                              Kode = a.Kode,
                              SubUnit = a.SubUnit,
                              KodeJenisCIP = a.KodeJenisCIP,
                              Direktorat = a.Direktorat,
                              Unit = a.Unit,
                              KOMET = a.KOMET,
                              UnitDes = d.Deskripsi,
                              NamaGugus = a.NamaGugus,
                              Risalah = a.Risalah,
                              Status = a.Status,
                              STK = a.STK,
                              Tahun = a.Tahun,
                              UpdatedBy = a.UpdatedBy,
                              UpdatedDate = a.UpdatedDate,
                              ValueCreationKategori = a.ValueCreationKategori,
                              ValueCreationPotensi = a.ValueCreationPotensi,
                              ValueCreationReal = a.ValueCreationReal,
                              VerifikasiKeuangan = a.VerifikasiKeuangan,
                              Fasilitator = a.Fasilitator,
                              Ketua = a.Ketua,
                              UploadPresentasi = a.UploadPresentasi,
                              KodeRegistrasi = a.KodeRegistrasi,
                              CipDirektorat = a.CipDirektorat,
                              CipEnterprise = a.CipEnterprise
                          };

            return records;
        }


        private List<string> p_GetRegStatus(string status)
        {
            List<string> par = new List<string>();

            switch (status)
            {
                case "new":
                    par = new List<string>() {
                        EnumRegistrationStatus.Registrasi_Peserta_CIP.ReplaceUnderScoreToSpace()
                        ,EnumRegistrationStatus.Pending_Approval_Admin_QM.ReplaceUnderScoreToSpace()
                     };
                    break;
                case "approval":
                    par = new List<string>() {
                        EnumRegistrationStatus.Tim_CIP_Approved.ReplaceUnderScoreToSpace()
                        ,EnumRegistrationStatus.Wait_to_Audit_PDCA_I.ReplaceUnderScoreToSpace()
                     };
                    break;
                case "audit":
                    par = new List<string>() {
                        EnumRegistrationStatus.Audit_PDCA_II_On_Progress.ReplaceUnderScoreToSpace()
                        ,EnumRegistrationStatus.Wait_to_Audit_PDCA_II.ReplaceUnderScoreToSpace()
                        ,EnumRegistrationStatus.Audit_PDCA_I_On_Progress.ReplaceUnderScoreToSpace()
                     };
                    break;
                case "penjurian":
                    par = new List<string>() {
                        EnumRegistrationStatus.Forum_Presentasi_CIP.ReplaceUnderScoreToSpace()
                        ,EnumRegistrationStatus.Ready_to_Show.ReplaceUnderScoreToSpace()
                     };
                    break;
                case "winner":
                    break;
                default:
                    break;
            }

            return par;
        }
        #endregion
    }
}
