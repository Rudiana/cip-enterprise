﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.AdminQA
{
    public interface IStreamEnterRepository : IGenericDataRepository<StreamEnter>
    {
        List<Dropdown> DropdownByTahun(string term);
        FormatedList<StreamEnter> Datatables();
    }
}