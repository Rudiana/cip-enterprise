﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.AdminQA
{
    public interface IStreamDirRepository : IGenericDataRepository<StreamDir>
    {
        List<Dropdown> DropdownByTahun(string term);
        FormatedList<StreamDir> Datatables();
    }
}