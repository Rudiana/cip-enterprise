﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIP.Core.Repositories.Abstractions.AdminQA
{
    public interface IUnitRepository : IGenericDataRepository<MasterUnit>
    {
        new FormatedList<MasterUnitDto> DataTables();
    }
}
