﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.AdminQA
{
    public interface ISetPoinKriteriaEnterRepository : IGenericDataRepository<TemplateAuditPDCAPoinEnter>
    {
        List<Dropdown> DropdownByTahun();
        SetPoinLangkahEnter GetLangkahLangkah(int periode);
        bool CreateRange(SetPoinLangkahEnter model, int periode);
        bool CopyCreateRange(int periode);

    }
}