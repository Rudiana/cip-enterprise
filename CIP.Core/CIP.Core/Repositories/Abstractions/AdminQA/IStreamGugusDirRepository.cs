﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IStreamGugusDirRepository : IGenericDataRepository<StreamGugusDir>
    {
        new FormatedList<sp_GetAsignStreamGugusDir_Result> DataTables();
        List<Dropdown> DropdownRegistrasi();
        List<Dropdown> DropdownForStreamGugus(int tahun);
        List<Dropdown> DropdownByTahunForStreamJuri(string term);
    }
}