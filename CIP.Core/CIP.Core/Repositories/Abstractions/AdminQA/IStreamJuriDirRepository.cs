﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IStreamJuriDirRepository : IGenericDataRepository<StreamJuriDir>
    {
        new FormatedList<sp_GetAsignStreamJuriDir_Result> DataTables();
        List<Dropdown> DropdownTahun();
        List<Dropdown> DropdownStream(int tahun);
        List<Dropdown> DropdownStreamGugusAvail(int tahun, string stream);
        List<Dropdown> DropdownJenisCip();
        List<Dropdown> DropdownStreamByJuri(int tahun);
        List<Dropdown> DropdownTahunPenjurian();
        List<Dropdown> DropdownStreamGugusDocking(int tahun, string stream);

    }
}