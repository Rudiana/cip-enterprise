﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.AdminQA
{
    public interface ISetPoinKriteriaRepository : IGenericDataRepository<TemplateAuditPDCAPoin>
    {
        List<Dropdown> DropdownByTahun();
        SetPoinLangkah GetLangkahLangkah(int periode);
        bool CreateRange(SetPoinLangkah model, int periode);
        bool CopyCreateRange(int periode);

    }
}