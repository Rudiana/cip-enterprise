﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IStreamGugusRepository : IGenericDataRepository<StreamGugus>
    {
        new FormatedList<sp_GetAsignStreamGugus_Result> DataTables();
        List<Dropdown> DropdownRegistrasi();
        List<Dropdown> DropdownByTahunForStreamJuri(string term);

    }
}