﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;

namespace CIP.Core.Repositories.Abstractions.Activity
{
   public interface IDeviasiKoefisienRepository: IGenericDataRepository<MasterStdDev>
    {
        new FormatedList<DeviasiKoefisien> DataTables();
    }
}