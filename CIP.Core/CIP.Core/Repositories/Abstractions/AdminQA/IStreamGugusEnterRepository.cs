﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IStreamGugusEnterRepository : IGenericDataRepository<StreamGugusEnter>
    {
        new FormatedList<sp_GetAsignStreamGugusEnter_Result> DataTables();
        List<Dropdown> DropdownRegistrasiDir();
        List<Dropdown> DropdownForStreamGugus(int tahun);
        List<Dropdown> DropdownByTahunForStreamJuri(string term);
    }
}