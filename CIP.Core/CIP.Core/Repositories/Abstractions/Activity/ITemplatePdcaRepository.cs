﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface ITemplatePdcaRepository : IGenericDataRepository<TemplateAuditPDCA>
    {
        new FormatedList<sp_GetTemplateAuditPDCA_Result> DataTables();
    }
}