﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface ITemplatePdcaEnterRepository : IGenericDataRepository<TemplateAuditPDCAEnter>
    {
        new FormatedList<sp_GetTemplateAuditPDCAEnter_Result> DataTables();
    }
}