﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IValueCreationRepository : IGenericDataRepository<PenjurianHeader>
    {
        List<ValueCreation> GetValueCreation(int tahun,string stream);
        bool SetValueCreation(ValueCreation modelHeader);
    }
}