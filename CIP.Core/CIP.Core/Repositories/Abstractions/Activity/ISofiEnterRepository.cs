﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes.Activity;
using DataTablesParser;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface ISofiEnterRepository : IGenericDataRepository<PenjurianDetailEnter>
    {
       
        List<SofiEnter> GetSofi(int tahun,string stream);
        List<SofiDetail> GetSofiDetail(string KodeRegistrasi);
        bool SetSofi(List<SofiDetail> model);

    }
}