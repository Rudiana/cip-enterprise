﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IPenjurianEnterRepository: IGenericDataRepository<PenjurianHeaderEnter>
    {
        new FormatedList<sp_GetPenjurianHeaderEnter_Result> DataTables();
        List<sp_GetPenjurianDetailEnter_Result> GetPenjurianDetail(string kodePenjurianHeader, string kodeJuri);
        bool UpdatePenjurian(PenjurianHeaderEnter modelHeader, List<sp_GetPenjurianDetailEnter_Result> modelDetail);
        sp_GetRegistrasiCIP_Result GetRegistrasi(string Kode);
        List<Dropdown> DropdownRegistrasi();
        PenjurianHeaderEnter GetByRegistrasi(string Kode, string kodeJuri);
        DataSet GetDockingPenjurian(int tahun, string gugus);
        FormatedList<sp_GetSummaryEnter_Result> GetSummary(int tahun, string direktorat);
        Koef GetKoef(int tahun, double stdDev, string direktorat, string KodeJenisCip);
        double? GetDefaultStdDev();
        bool StartPenjurian(string kode, string kodeRegistrasi);
        bool FinishPenjurian(string kode, string kodeRegistrasi);
        bool ResetPenjurian(int kodePenjurian);
        FormatedList<sp_GetSummaryBreakdownEnter_Result> GetSummaryBreakdown(int tahun, string kodeRegistrasi);
        bool SaveKoef(List<KoefStream> KoefStream, string JenisCip);
        bool ResetKoef(List<KoefStream> KoefStream, string JenisCip);
        List<ValueCreationEnter> GetValueCreation(int tahun,string stream);
        bool SetValueCreation(ValueCreationEnter modelHeader);
    }
}