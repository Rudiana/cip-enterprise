﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IValueCreationEnterRepository : IGenericDataRepository<PenjurianHeaderEnter>
    {
        List<ValueCreationEnter> GetValueCreation(int tahun,string stream);
        bool SetValueCreation(ValueCreationEnter modelHeader);
    }
}