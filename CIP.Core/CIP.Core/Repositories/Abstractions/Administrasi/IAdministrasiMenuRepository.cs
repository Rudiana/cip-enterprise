﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.Administrasi
{
    public interface IAdministrasiMenuRepository : IGenericDataRepository<AdministrasiMenu>
    {
        List<MenuItem> GetMenuByUser();
    }
}
