﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace CIP.Core.Repositories.Abstractions.Administrasi
{
    public interface IAdministrasiRoleUserRepository : IGenericDataRepositoryExtended<AdministrasiRoleUser>
    {
        IQueryable<string> DualList(string kode);
    }
}
