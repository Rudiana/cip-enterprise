﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CIP.Core.Repositories.Abstractions.Administrasi
{
    public interface IAdministrasiHakAksesRoleRepository : IGenericDataRepository<AdministrasiHakAksesRole>
    {
        IQueryable<string> DualList(string kode);
    }
}
