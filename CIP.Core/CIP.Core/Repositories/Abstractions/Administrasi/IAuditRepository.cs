﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;
using System.Linq;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IAuditRepository: IGenericDataRepository<AuditPDCAHeader>
    {
        new FormatedList<sp_GetAuditPDCAHeader_Result> DataTables();
        List<sp_GetAuditPDCADetail_Result> GetPdcaDetail(string kodePDCAHeader);
        bool CreateAuditPDCA(AuditPDCAHeader modelHeader, List<sp_GetAuditPDCADetail_Result> modelDetail);
        bool UpdateAuditPDCA(AuditPDCAHeader modelHeader, List<sp_GetAuditPDCADetail_Result> modelDetail);
        sp_GetRegistrasiCIP_Result GetRegistrasi(string Kode);
        List<Dropdown> DropdownRegistrasi();
        AuditPDCAHeader GetByRegistrasi(string Kode);
        bool StartPDCA_1(string kode, string kodeRegistrasi);
        bool FinishPDCA_1(string kode, string kodeRegistrasi);
        bool StartPDCA_2(string kode, string kodeRegistrasi);
        bool FinishPDCA_2(string kode, string kodeRegistrasi);
        IQueryable<string> DualList(string kodeUser);
        bool AssignAuditorGugus(List<AuditPDCAHeader> headers);
        AuditReport Pdca1FormReport(int Periode, string KodeRegistrasi);
        AuditReport Pdca2FormReport(int Periode, string KodeRegistrasi);
        FormatedList<sp_GetPdcaList_Result> PdcaListReport(int Periode);
    }
}