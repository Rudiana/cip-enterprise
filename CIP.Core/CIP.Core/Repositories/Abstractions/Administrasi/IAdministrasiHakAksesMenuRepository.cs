﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;

namespace CIP.Core.Repositories.Abstractions.Administrasi
{
    public interface IAdministrasiHakAksesMenuRepository : IGenericDataRepository<AdministrasiHakAksesMenu>
    {
       IQueryable<string> DualList(string kode);
    }
}
