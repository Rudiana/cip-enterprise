﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using CIP.Core.Common;
using CIP.Core.Models;
using CIP.Core.GenericRepositories.Abstractions;

namespace CIP.Core.Repositories.Abstractions.Administrasi
{
    public interface IAdministrasiHakAksesRepository : IGenericDataRepository<AdministrasiHakAkses>
    {
        bool CreateWithDetail(ParamAdministrasiHakAkses data); 
        bool UpdateWithDetail(ParamAdministrasiHakAkses data);
        bool DeleteWithDetail(string kode);
    }
}
