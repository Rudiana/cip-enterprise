﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.Administrasi
{
    public interface IAdministrasiRoleRepository : IGenericDataRepository<AdministrasiRole>
    {
        bool CreateWithDetail(AdministrasiRole header, List<AdministrasiRoleDtl> detail);
        bool UpdateWithDetail(AdministrasiRole header, List<AdministrasiRoleDtl> detail);
        bool DeleteWithDetail(string kode);

        bool CreateWithDetailUser(AdministrasiRole header, List<AdministrasiRoleUser> detail);
    }
}
