﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;
using System.Linq;

namespace CIP.Core.Repositories.Abstractions.Administrasi
{
    public interface IAdministrasiUserRepository : IGenericDataRepository<AdministrasiUser>
    {
        bool IsAdministrator {get;}
        UserProfile Login(string userName, string password);
        bool ChangePassword(string oldPassword, string newPassword);
        bool ResetPassword();
        bool UploadAvatar(string avatar);
        IQueryable<AdministrasiUser> DatatablesAuditor();
        List<Dropdown> DropdownRoleJuri(string kodeStream);
        List<Dropdown> DropdownRoleJuriDir(string kodeStream);
        List<Dropdown> DropdownRoleJuriEnter(string kodeStream);
        FormatedList<AdministrasiUserDto> DataTablesDto();
        List<Dropdown> DropdownByUnit(string unit);
    }
}
