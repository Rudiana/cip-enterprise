'use strict';

angular.module('app.peserta').controller('RegistrasiController', function ($scope, $http, $state, $stateParams, $window, $location, $filter, Select2Helper, BASE_API, User, authSvc, DTOptionsBuilder, DTColumnBuilder, Language) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;
    var msgTitle = "Registrasi";

    me.init = function (obj) {
        me.undo();
        me.paramKode = $stateParams.kode;

        if (!isNullOrEmpty(me.paramKode)) {
            me.vm = obj;
            me.loadDropdown(me.viewDetail);
            me.fromStateName = $state.fromState.name;
        }
        else {
            me.loadDropdown();
        }
    };

    me.loadDropdown = function (callback) {
        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;
        });

        Select2Helper.GetDataForCombo(BASE_API + 'masterjeniscip/dropdown').then(function (result) {
            me.listJenisCIP = result.Dropdown;
        });

        Select2Helper.GetDataForCombo(BASE_API + 'masterdirektorat/dropdownbyuser').then(function (result) {
            me.listDirektorat = result.Dropdown;

            if (me.listDirektorat.length === 1) {
                me.data.Direktorat = me.listDirektorat[0].value;
                setTimeout(function () {
                    me.listDirektorat[0].obj = angular.copy(me.listDirektorat[0]);
                    $('#Direktorat').select2('data', me.listDirektorat[0]);
                }, 2000);
            }
            else {
                me.data.Direktorat = '';
            }

        });

        Select2Helper.GetDataForCombo(BASE_API + 'masterstatusanggota/dropdown').then(function (result) {
            me.listStatusAnggota = result.Dropdown;
            me.listStatusAnggotaTemp = result.Dropdown;
        });

        if (callback != undefined) {
            callback(me.paramKode);
            me.loadHistory(me.paramKode);
            me.loadSofi(me.paramKode);
        }
    };

    me.loadHistory = function (kode) {
        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', {
                // Either you specify the AjaxDataProp here
                // dataSrc: 'data',
                url: api + 'datatableregisterlog?koderegistrasi=' + kode,
                type: 'POST',
                accepts: "application/json",
                headers: authSvc.headers(),
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Historikal", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            })
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            //.withOption('scrollX', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withOption('rowCallback', me.rowCallback)
            .withBootstrap()
            .withOption('order', [2, 'asc'])
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('KodeRegistrasi').withTitle('No. Gugus').withClass('text-primary').notSortable(),
            DTColumnBuilder.newColumn('Status').withTitle('Status'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Created Date').renderWith(function (data, type, full, meta) {
                return DateTimeFormat(data);
            }),
            DTColumnBuilder.newColumn('CreatedBy').withTitle('Created By')
        ];

        me.vm.dtInstance = {};
    };

    me.loadSofi = function (kode) {
        me.vm.tableOptionsSofi = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', {
                // Either you specify the AjaxDataProp here
                // dataSrc: 'data',
                url: api + 'datatablepenjuriansofi?koderegistrasi=' + kode,
                type: 'POST',
                accepts: "application/json",
                headers: authSvc.headers(),
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Load Data Sofi", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            })
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('height', 200)
            .withOption('colReorder', true)
            .withFixedColumns({
                leftColumns: 2
            })


            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withOption('rowCallback', me.rowCallback)
            .withBootstrap()
            .withOption('order', [1, 'asc'])
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumnsSofi = [
            DTColumnBuilder.newColumn('Juri').withTitle('Juri').withClass('text-primary').notSortable(),
            DTColumnBuilder.newColumn('Stream').withTitle('Stream'),
            DTColumnBuilder.newColumn('Kategori').withTitle('Kategori'),
            DTColumnBuilder.newColumn('Langkah').withTitle('Langkah'),
            DTColumnBuilder.newColumn('SubLangkah').withTitle('Sub Langkah'),
            DTColumnBuilder.newColumn('Sofi').withTitle('Detil/Keterangan')
        ];

        me.vm.dtInstanceSofi = {};
    };

    me.showInputAnggota = function () {
        if (this.frmRegistrasiPeserta.$valid) {
            me.allowInputAnggota = true;
        }
        else {
            me.allowInputAnggota = false;
        }
    };

    me.resetAnggota = function () {
        me.anggota = {
            KodeUser: '',
            NamaAnggota: '',
            Email: '',
            NoPek: '',
            KodeStatusAnggota: '',
            StatusDes: '',
        };
    }

    me.isValidByJenisCIP = function (n) {
        /*Kode	Deskripsi
        1	PC Prove
        2	FT Prove
        3	I Prove
        4	RT Prove
        
        PC Prove Maksimal 8 dengan Fasilitator, FT Prove : Maksimal 6 dengan Fasilitator,
        I Prove : Maksimal 2 Tanpa Fasilitator (fitur fasilitator menjadi hilang apabila dipilih kategori I Prove)
        */
        var countAnggota = me.listAnggota.length;
        switch (me.data.KodeJenisCIP) {
            case 'PC-Prove':
                if (countAnggota < (8 + n)) {
                    return true;
                }
                else {
                    return false;
                }

                break;
            case 'FT-Prove':
                if (countAnggota < (6 + n)) {
                    return true;
                }
                else {
                    return false;
                }

                break;
            case 'I-Prove':
                if (countAnggota < (2 + n)) {
                    return true;
                }
                else {
                    return false;
                }

                break;
            case 'RT-Prove':
                if (countAnggota < (5 + n)) {
                    return true;
                }
                else {
                    return false;
                }

                break;
            default:
                return false;
                break
        }
    };

    me.undoAddAnggota = function () {
        me.resetAnggota();
        me.allowInputAnggota = false;
    };

    me.doAddAnggota = function () {
        if (me.anggota.KodeStatusAnggota === '1') {
            me.data.Email = me.anggota.Email;
            me.data.Ketua = me.anggota.KodeUser;
        }
        if (me.anggota.KodeStatusAnggota === '2') {
            me.data.Fasilitator = me.anggota.KodeUser;
        }

        me.anggota.NamaAnggota = $('#KodeUser').select2('data').text;
        me.anggota.StatusDes = $('#KodeStatusAnggota').select2('data').text;
        me.listAnggota.push(me.anggota);
        me.resetAnggota();
    };

    me.addAnggota = function () {
        if (me.isValidByJenisCIP(0)) {
            var userExist = $filter('filter')(me.listAnggota, { KodeUser: me.anggota.KodeUser }, true);
            if (userExist.length > 0) {
                NotifBoxWarning(msgTitle, "Nama Anggota sudah terdaftar.");

                return;
            }
            else {
                if (me.anggota.KodeStatusAnggota === '1') {
                    var statusKetua = $filter('filter')(me.listAnggota, { KodeStatusAnggota: me.anggota.KodeStatusAnggota }, false);
                    if (statusKetua.length > 0) {
                        NotifBoxWarning(msgTitle, "Status Anggota sebagai Ketua sudah terdaftar.");

                        return;
                    }
                    else {
                        me.doAddAnggota();
                    }
                }
                else {
                    if (me.anggota.KodeStatusAnggota === '2') {
                        var statusFasilitator = $filter('filter')(me.listAnggota, { KodeStatusAnggota: me.anggota.KodeStatusAnggota }, false);
                        if (statusFasilitator.length > 0) {
                            NotifBoxWarning(msgTitle, "Status Anggota sebagai Fasilitator sudah terdaftar.");

                            return;
                        }
                        else {
                            me.doAddAnggota();
                        }
                    }
                    else {
                        me.doAddAnggota();
                    }
                }
            }
        }
        else {
            NotifBoxWarning(msgTitle, "Jumlah Anggota tidak boleh melebihi batas maksimal.");
            return;
        }
    };

    me.deleteAnggota = function (row) {
        $.SmartMessageBox({
            title: "Delete",
            content: "Yakin hapus data Anggota?",
            buttons: '[OK][Batal]',
            theme: 'bg-warning'
        },
            function (action) {
                if (action === "OK") {
                    var index = me.listAnggota.indexOf(row);
                    me.listAnggota.splice(index, 1);
                    if (row.KodeStatusAnggota === '1') {
                        me.data.Email = row.Email;
                        me.data.Ketua = row.KodeUser;
                    }
                    if (row.KodeStatusAnggota === '2') {
                        me.data.Fasilitator = row.KodeUser;
                    }
                    return;
                }
                else {
                    return;
                }
            });
    };

    me.submit = function () {
        var statusKetua = $filter('filter')(me.listAnggota, { KodeStatusAnggota: "1" }, false);
        if (statusKetua.length < 1) {
            NotifBoxWarning(msgTitle, "Ketua Tim belum didaftarkan. Harap daftarkan Ketua Tim terlebih dahulu.");

            return;
        }
        else {
            me.undoAddAnggota();

            $.SmartMessageBox({
                title: "Submit",
                content: "Submit data Registrasi?",
                buttons: '[OK][Batal]',
                theme: 'bg-success'
            },
                function (action) {
                    if (action === "OK") {
                        //if (this.frmRegistrasiPeserta.$valid) {
                        if (me.isValidByJenisCIP(1)) {
                            var msgTitle = 'Simpan data Registrasi.';
                            var data = {
                                header: me.data,
                                detail: me.listAnggota
                            }

                            $http.post(api + "register", data)
                                .success(function (result) {
                                    if (result.Success) {
                                        me.data.Kode = result.Data.Kode;
                                        me.data.Status = result.Data.Status;

                                        NotifBoxSuccess(msgTitle, result.Message + ' Silahkan Upload Kelengkapan Dokumen.');
                                        $('a[href="#tab2"]').click();
                                        me.isSubmited = true;
                                        me.allowEdit = true;
                                        me.allowUpload = true;
                                    }
                                    else {
                                        NotifBoxWarning(msgTitle, result.Message);
                                    }
                                })
                                .error(function (error, status) {
                                    NotifBoxError(msgTitle, status + " - " + error.Message);
                                });
                        }
                        else {
                            NotifBoxWarning(msgTitle, "Jumlah Anggota tidak boleh melebihi batas maksimal.");
                            return;
                        }
                        //}
                    }
                    else {
                        return;
                    }
                });
        }
    };

    me.undo = function () {
        me.data = {};
        me.data.Kode = '';
        me.data.KodeStatus = 0;

        me.anggota = {};
        me.resetAnggota();
        me.listAnggota = [];
        me.listUserAdd = [];
        me.isSubmited = false;
        me.allowInputAnggota = false;
        me.allowEdit = true;
        me.allowUpload = false;

        $('div > input:text').val('');
        $('input:file').val('');
    };

    me.uploadFile = function (el) {
        if (isNullOrEmpty(me.data.Kode)) {
            NotifBoxError(msgTitle, 'Data Registrasi tidak ditemukan. Silahkan input data Registrasi terlebih dahulu.');
            return;
        }
        else {
            const name = el.files[0].name;
            const lastDot = name.lastIndexOf('.');

            const fileName = name.substring(0, lastDot);
            const ext = name.substring(lastDot + 1);

            var fileDoc = el.files[0];
            el.parentNode.nextSibling.value = name;

            //$scope.data[el.id] = me.data.Tahun + "_" + el.id + "_" + name;
            var exts = "jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx, pdf";

            if (!exts.includes(ext)) {
                $.SmartMessageBox({
                    title: "",
                    content: "Unggah Dokumen" + " harus tipe \"" + exts + "\"",
                    buttons: '[OK]',
                    theme: 'bg-warning'
                },
                    function (action) {
                        if (action === "OK") {
                            //me.data[el.id] = '';
                            el.parentNode.nextSibling.value = '';
                            return;
                        }
                        else {
                            el.parentNode.nextSibling.value = '';
                            //me.data[el.id] = '';
                            return;
                        }
                    });
            }
            else {
                if (el.id === "Risalah") {
                    exts = "pdf";
                    if (!exts.includes(ext)) {
                        $.SmartMessageBox({
                            title: "",
                            content: "Unggah Dokumen" + " harus tipe \"" + exts + "\"",
                            buttons: '[OK]',
                            theme: 'bg-warning'
                        },
                            function (action) {
                                if (action === "OK") {
                                    //me.data[el.id] = '';
                                    el.parentNode.nextSibling.value = '';
                                    return;
                                }
                                else {
                                    el.parentNode.nextSibling.value = '';
                                    //me.data[el.id] = '';
                                    return;
                                }
                            });
                    }
                    else {
                        me.fnUpload(el, fileDoc, name);
                    }
                }
                else {
                    me.fnUpload(el, fileDoc, name);
                }
            }
        }
    };

    me.fnUpload = function (el, fileDoc, name) {
        $.SmartMessageBox({
            title: "",
            content: "Yakin Unggah Dokumen \"" + el.id + ": " + name + "\"?",
            buttons: '[OK][Batal]',
            theme: 'bg-success'
        },
            function (action) {
                if (action === "OK") {
                    var model = {
                        Kode: $scope.data.Kode,
                        Tahun: $scope.data.Tahun
                    };

                    var fd = new FormData();
                    fd.append("files", fileDoc);
                    fd.append("model", JSON.stringify(model));
                    fd.append("docType", el.id);

                    var msgTitle = 'Unggah Berkas ' + el.id;
                    $http.post(api + "uploaddocument", fd, {
                        withCredentials: false,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                        .success(function (result) {
                            if (result.Success) {
                                me.data[el.id] = result.More.FileName;

                                var fieldUpdatedDate = el.id + 'UpdatedDate';
                                console.log();
                                me.data[fieldUpdatedDate] = result.More.UpdatedDate;

                                NotifBoxSuccess(msgTitle, result.Message);
                            }
                            else {
                                // me.data[el.id] = '';
                                el.parentNode.nextSibling.value = '';
                                NotifBoxWarning(msgTitle, result.Message);
                            }
                        })
                        .error(function (error, status) {
                            // me.data[el.id] = '';
                            el.parentNode.nextSibling.value = '';
                            NotifBoxError(msgTitle, status + " - " + error.Message);
                        });
                }
                else {
                    //el.parentNode.nextSibling.value = '';
                    return;
                }
            });
    };

    me.PreviewDocument = function (event) {
        var id = event.target.id;
        if (id.toLowerCase() === "risalah") {
            $http.get(api + "downloadrisalah?kode=" + me.data.Kode)
                .success(function (result) {
                    if (result.Success) {
                        $window.open(BASE_API.replace('/api', '') + 'downloads/' + result.Data, '_blank');
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });
        }
        else {
            $window.open(BASE_API.replace('/api', '') + 'uploads/' + me.data[id], 'blank');
        }
    }

    me.viewDetail = function (kode) {
        me.data.Kode = kode;
        $http.get(api + "allowedit?kode=" + kode)
            .success(function (result) {
                if (result.Success) {
                    if ($stateParams.allowEdit) {
                        me.allowEdit = result.More.AllowEdit;
                        me.isSubmited = me.allowEdit ? false : true;
                        me.allowUpload = result.More.AllowEdit;
                    }
                    else {
                        me.allowEdit = $stateParams.allowEdit;
                        me.isSubmited = true;
                        me.allowUpload = false;
                    }

                    $http.get(api + "dynamicdata?term=" + kode)
                        .success(function (result) {
                            if (result.Success) {
                                me.data = result.Data;

                                if (me.data.KodeStatus === 3) {
                                    me.allowEdit = false;
                                    me.isSubmited = true;
                                    me.allowUpload = false;
                                }

                                setTimeout(function () {
                                    var tahun = $filter('filter')(me.listTahun, { id: me.data.Tahun }, false);
                                    if (tahun.length > 0) {
                                        tahun[0].obj = angular.copy(tahun[0]);
                                        $('#Tahun').select2('data', tahun[0]);
                                    }

                                    var jenisCIP = $filter('filter')(me.listJenisCIP, { id: me.data.KodeJenisCIP }, false);
                                    if (jenisCIP.length > 0) {
                                        jenisCIP[0].obj = angular.copy(jenisCIP[0]);
                                        $('#KodeJenisCIP').select2('data', jenisCIP[0]);
                                    }

                                    var direktorat = $filter('filter')(me.listDirektorat, { id: me.data.Direktorat }, false);
                                    if (direktorat.length > 0) {
                                        direktorat[0].obj = angular.copy(direktorat[0]);
                                        $('#Direktorat').select2('data', direktorat[0]);
                                    }

                                    var unit = $filter('filter')(me.listUnit, { id: me.data.Unit }, false);
                                    if (unit.length > 0) {
                                        unit[0].obj = angular.copy(unit[0]);
                                        $('#Unit').select2('data', unit[0]);
                                    }

                                    var subUnit = $filter('filter')(me.listSubUnit, { id: me.data.SubUnit }, false);
                                    if (subUnit.length > 0) {
                                        subUnit[0].obj = angular.copy(subUnit[0]);
                                        $('#SubUnit').select2('data', subUnit[0]);
                                    }
                                }, 3000);

                                me.listAnggota = result.Data.AnggotaCIPDetail;
                            }
                            else {
                                NotifBoxWarning(msgTitle, result.Message);
                            }
                        })
                        .error(function (error, status) {
                            NotifBoxError(msgTitle, status + " - " + error.Message);
                        });
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.update = function () {
        var statusKetua = $filter('filter')(me.listAnggota, { KodeStatusAnggota: "1" }, false);
        if (statusKetua.length < 1) {
            NotifBoxWarning(msgTitle, "Ketua Tim belum didaftarkan. Harap daftarkan Ketua Tim terlebih dahulu.");

            return;
        }
        else {
            $.SmartMessageBox({
                title: "Update",
                content: "Yakin Update data Registrasi?",
                buttons: '[OK][Batal]',
                theme: 'bg-success'
            },
                function (action) {
                    if (action === "OK") {
                        //if (this.frmRegistrasiPeserta.$valid) {
                        if (me.isValidByJenisCIP(1)) {
                            var msgTitle = 'Update data Registrasi.';
                            var data = {
                                header: me.data,
                                detail: me.listAnggota
                            }

                            $http.post(api + "updatewithdetail", data)
                                .success(function (result) {
                                    if (result.Success) {
                                        NotifBoxSuccess(msgTitle, result.Message);
                                    }
                                    else {
                                        NotifBoxWarning(msgTitle, result.Message);
                                    }
                                })
                                .error(function (error, status) {
                                    NotifBoxError(msgTitle, status + " - " + error.Message);
                                });
                        }
                        else {
                            NotifBoxWarning(msgTitle, "Jumlah Anggota tidak boleh melebihi batas maksimal.");
                            return;
                        }
                        //}
                    }
                    else {
                        return;
                    }
                });
        }
    };

    me.back = function () {
        $state.go(me.fromStateName, { tahun: $stateParams.tahun });
    }

    me.refreshHistory = function () {
        //me.vm.dtInstance.DataTable.search('2017').draw();
        me.vm.dtInstance.reloadData(function () { }, true);
    };

    me.refreshSofi = function () {
        //me.vm.dtInstance.DataTable.search('2017').draw();
        me.vm.dtInstanceSofi.reloadData(function () { }, true);
    };


    me.isNullOrEmpty = function (val) {
        return IsNullOrEmpty(val);
    }

    me.dateTimeFormat = function (val) {
        return DateTimeFormat(val);
    }

    // WATCH METHOD

    me.$watch('data.KodeJenisCIP', function (n, o) {
        me.listStatusAnggota = $filter('filter')(me.listStatusAnggotaTemp, { id: '!' + '2' });

        if (!isNullOrEmpty(n)) {
            //3	I Prove
            if (n === 'I-Prove') {
                me.listStatusAnggota = $filter('filter')(me.listStatusAnggotaTemp, { id: '!' + '2' });
            }
            else {
                me.listStatusAnggota = $filter('filter')(me.listStatusAnggotaTemp, {});
            }

            if (n === 'PC-Prove' || n === 'RT-Prove') {
                Select2Helper.GetDataForCombo(BASE_API + 'administrasiuser/dropdown').then(function (result) {
                    me.listUserUnit = result.Dropdown;
                    me.listUserUnitTemp = result.Dropdown;
                });
            }
            else {
                Select2Helper.GetDataForCombo(BASE_API + 'administrasiuser/dropdownbyunit?unit=' + me.data.Unit).then(function (result) {
                    me.listUserUnit = result.Dropdown;
                    me.listUserUnitTemp = result.Dropdown;
                });
            }
        }
    }, true);

    me.$watch('anggota', function (n, o) {
        if (isNullOrEmpty(n.KodeUser) || isNullOrEmpty(n.KodeStatusAnggota)) {
            me.isValidInputAnggota = false;
        }
        else {
            me.isValidInputAnggota = true;
        }
    }, true);

    me.$watch('anggota.KodeUser', function (n, o) {
        if (!isNullOrEmpty(n)) {
            $http.get(BASE_API + 'administrasiuser/getsingle?keyvalues=' + n)
                .success(function (result) {
                    if (result.Success) {
                        me.anggota.Email = result.Data.Email;
                        me.anggota.NoPek = result.Data.NoPek;
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });
        }
        else {
            me.anggota.Email = '';
            me.anggota.NoPek = '';
        }

    }, true);

    me.$watch('listAnggota', function (n, o) {
        me.data.JumlahAnggota = n.length;
    }, true);

    me.$watch('data.Direktorat', function (n, o) {
        if (!isNullorEmpty(n)) {
            Select2Helper.GetDataForCombo(BASE_API + 'masterunit/dropdownbydirektorat?direktorat=' + n).then(function (result) {
                me.listUnit = result.Dropdown;
                if (me.listUnit.length === 1) {
                    me.data.Unit = me.listUnit[0].value;
                    setTimeout(function () {
                        me.listUnit[0].obj = angular.copy(me.listUnit[0]);
                        $('#Unit').select2('data', me.listUnit[0]);
                    }, 3000);
                }
                else {
                    if (!isNullOrEmpty(me.paramKode)) {
                        var unit = $filter('filter')(me.listUnit, { id: me.data.Unit }, false);
                        setTimeout(function () {
                            if (unit.length > 0) {
                                unit[0].obj = angular.copy(unit[0]);
                                $('#Unit').select2('data', unit[0]);
                            }
                        }, 3000);
                    }
                    else {
                        me.data.Unit = ''
                    }
                }
            });
        }
        else {
            me.listUnit = [];
            me.data.Unit = '';
        }
    }, true);

    me.$watch('data.Unit', function (n, o) {
        if (!isNullorEmpty(n)) {
            Select2Helper.GetDataForCombo(BASE_API + 'mastersubunit/dropdownbyunit?unit=' + n).then(function (result) {
                me.listSubUnit = result.Dropdown;
                if (!isNullOrEmpty(me.paramKode)) {
                    var subUnit = $filter('filter')(me.listSubUnit, { id: me.data.SubUnit }, false);
                    setTimeout(function () {
                        if (subUnit.length > 0) {
                            subUnit[0].obj = angular.copy(subUnit[0]);
                            $('#SubUnit').select2('data', subUnit[0]);
                        }
                    }, 3000);
                }
                else {
                    me.data.SubUnit = ''
                }
            });
        }
        else {
            me.listSubUnit = [];
            me.data.SubUnit = '';
        }

        
        if (!isNullorEmpty(n)) {
            if (me.data.KodeJenisCIP === 'PC-Prove' || me.data.KodeJenisCIP === 'RT-Prove') {
                Select2Helper.GetDataForCombo(BASE_API + 'administrasiuser/dropdown').then(function (result) {
                    me.listUserUnit = result.Dropdown;
                    me.listUserUnitTemp = result.Dropdown;
                });
            }
            else {
                Select2Helper.GetDataForCombo(BASE_API + 'administrasiuser/dropdownbyunit?unit=' + n).then(function (result) {
                    me.listUserUnit = result.Dropdown;
                    me.listUserUnitTemp = result.Dropdown;
                });
            }
        }
        else {
            me.listUserUnit = [];
            me.listUserUnitTemp = [];
        }
    }, true);

    me.init(this);
});