﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Reposirories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.Core.Repositories.Concretes.Master;
using CIP.WebApi.Lamp.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Activity
{
    [AllowAnonymous]
    [RoutePrefix(@"api/RegistrasiCIP")]
    public class RegistrasiCIPController : BaseApiController<RegistrasiCIP>
    {
        private GenericContext ctx;
        private IRegistrasiCIPRepository objRepo;

        public RegistrasiCIPController()
        {
            ctx = new GenericContext();
            objRepo = new RegistrasiCIPRepository(ctx);

            base.repo = objRepo;
        }

        [HttpPost, Route("register")]
        public IGenericWebApiResult Register(ParamHeaderDetail<RegistrasiCIP, AnggotaCIP> data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    var model = data.header;
                    var anggotaCIP = data.detail;
                    result.Success = objRepo.Register(model, anggotaCIP);
                    result.Data = model;
                    result.Message =  BaseConstants.MESSAGE_CREATE_SUCCESS;


                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablePeserta(int statReg)
        {
            try
            {
                var datatable = objRepo.DataTablePeserta(statReg);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        public HttpResponseMessage DataTableVerifikasiPeserta()
        {
            try
            {
                var datatable = objRepo.DataTableVerifikasiPeserta();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        public HttpResponseMessage DataTableRegisterLog(string kodeRegistrasi)
        {
            try
            {
                var datatable = objRepo.DataTableRegisterLog(kodeRegistrasi);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownForAuditor([FromBody]int period)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownForAuditor(period);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult UpdateVerifikasiAdminQM(RegistrasiCIP data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateVerifikasiAdminQM(data);
                    result.Data = data;
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult Activation(string kode, string x)
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    result.Success = objRepo.Activation(kode, x);
                    result.Message = result.Success ? BaseConstants.MESSAGE_ACTIVATION_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult SubmitRegister(RegistrasiCIP data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(data.Kode, BaseEnums.EnumRegistrationStatus.Pending_Approval_Admin_QM.ReplaceUnderScoreToSpace());
                    result.Data = data;
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult SubmitAdminQM(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(kode, BaseEnums.EnumRegistrationStatus.Wait_to_Audit_PDCA_I.ReplaceUnderScoreToSpace());
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }
        public IGenericWebApiResult StartPenjurian(RegistrasiCIP data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(data.Kode, BaseEnums.EnumRegistrationStatus.Forum_Presentasi_CIP.ReplaceUnderScoreToSpace());
                    result.Data = data;
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        public IGenericWebApiResult FinishPenjurian(RegistrasiCIP data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(data.Kode, BaseEnums.EnumRegistrationStatus.Result_Forum_CIP.ReplaceUnderScoreToSpace());
                    result.Data = data;
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownForStreamGugus([FromBody]int tahun)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownForStreamGugus(tahun);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost, Route("uploaddocument")]
        public HttpResponseMessage UploadDocument()
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    var forms = HttpContext.Current.Request.Form;
                    var formModel = forms.GetValues("model");
                    var docType = forms.GetValues("docType");

                    //var guidString = Guid.NewGuid().ToString();

                    var model = formModel.Length > 0 ? JsonConvert.DeserializeObject<RegistrasiCIP>(formModel[0]) : new RegistrasiCIP();
                    var docTypeName = docType.Length > 0 ? docType[0] : "";
                    var colDocType = "IsUpload" + docTypeName;

                    if (!string.IsNullOrEmpty(colDocType))
                    {
                        if (model.GetType().GetProperty(colDocType) != null)
                        {
                            model.GetType().GetProperty(colDocType).SetValue(model, true, null);
                        }
                        else
                        {
                            if (model.GetType().GetProperty("IsUploadPresentasi") != null)
                            {
                                model.GetType().GetProperty("IsUploadPresentasi").SetValue(model, true, null);
                            }
                            else
                            {
                                throw new Exception(string.Format("Kolom Berkas \"{0}\" tidak ditemukan.", docType[0]));
                            }
                        }
                    }

                    result.Success = objRepo.Update(model);
                    result.Data = model;
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    if (result.Success)
                    {
                        var fileCount = HttpContext.Current.Request.Files.Count;
                        for (int i = 0; i < fileCount; i++)
                        {
                            HttpPostedFile file = HttpContext.Current.Request.Files[i];

                            //var fileName = Path.GetFileName(file.FileName);
                            //var fileExt = Path.GetExtension(file.FileName);
                            //var guid = Guid.NewGuid().ToString("n");

                            var dir = HttpContext.Current.Server.MapPath("~/Uploads");
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }

                            var fileName = string.Format("{0}_{1}_{2}", model.Kode, docTypeName, file.FileName);
                            var path = Path.Combine(dir, fileName);
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }

                            file.SaveAs(path);
                        }
                    }

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost, Route("sendnotificationuploaddocument")]
        public HttpResponseMessage SendNotificationUploadDocument(string kodeRegistrasi, string docType)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {

                    result.Success = objRepo.SendNotificationUploadDocument(kodeRegistrasi, docType);
                    result.Message = result.Success ? BaseConstants.MESSAGE_NOTIFICATION_UPLOAD_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriode(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriode(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeAnggota(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeAnggota(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        
        [HttpGet]
        public HttpResponseMessage GeneratePeserataCIPExcel(int? periode)
        {
            var BytesFile = ExcelGenerator.GeneratePeserataCIPExcel(objRepo, periode);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Daftar_Peserta_CIP_" + (periode.HasValue ? periode.Value.ToString() : "") + ".xlsx";

            return result;
        }

        [HttpGet]
        public IGenericWebApiResult GetDocument(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Data = objRepo.GetDocument(kode);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

    }
}
