﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Reposirories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Concretes.Master;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Http;
using System.Linq;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterSubUnit")]
    public class MasterSubUnitController : BaseApiController<MasterSubUnit>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterSubUnit> genRepo;

        public MasterSubUnitController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterSubUnit>(ctx);

            base.repo = genRepo;
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByDirektorat(string direktorat)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    List<Dropdown> dropdown = new List<Dropdown>();
                    dropdown = ctx.sp_GetMasterSubUnit(direktorat).Select(x => new Dropdown
                    {
                        id = x.Kode.Value.ToString(),
                        value = x.Kode.Value.ToString(),
                        text = x.Deskripsi
                    }).ToList();

                    result.Dropdown = dropdown;
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByDirektoratUser()
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    List<Dropdown> dropdown = new List<Dropdown>();
                    dropdown = ctx.sp_GetMasterSubUnit(repo.UserProfile.Direktorat).Select(x => new Dropdown
                    {
                        id = x.Kode.Value.ToString(),
                        value = x.Kode.Value.ToString(),
                        text = x.Deskripsi
                    }).ToList();

                    result.Dropdown = dropdown;

                    dynamic more = new ExpandoObject();
                    more.UnitDes = repo.UserProfile.UnitDes;
                    more.DirektoratDes = repo.UserProfile.DirektoratDes;
                    result.More = more;

                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

    }
}
