﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections.Generic;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IRegistrasiCIPRepository : IGenericDataRepository<RegistrasiCIP>
    {
        bool Register(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP);
        bool Reregister(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP);
        FormatedList<RegistrasiAnggotaCIP> DataTablePeserta(int statReg);
        FormatedList<RegistrasiAnggotaCIP> DataTableVerifikasiPeserta();
        FormatedList<RegistrasiCIPLog> DataTableRegisterLog(string kodeRegistrasi);
        List<Dropdown> DropdownForAuditor(int periode);
        List<Dropdown> DropdownForStreamGugus(int tahun);
        bool UpdateVerifikasiAdminQM(string kode, string status);
        bool UpdateStatus(string kode, string status);
        int GetCountPenjurianStatus(int periode);
        int GetCountAuditStatus(int periode);
        int GetCountNewRegStatus(int periode);
        int GetCountApprovalStatus(int periode);
        bool SendNotificationUploadDocument(string kodeRegistrasi, string docType);
        bool Activation(string kode, string password);
        FormatedList<RegistrasiAnggotaCIP> DatatablesByPeriode(int? periode);
        FormatedList<RegistrasiAnggotaCIP> DatatablesByPeriodeAnggota(int? periode);
        FormatedList<RegistrasiCIP> DatatablesByStatus(int periode, string status);
        IEnumerable<RegistrasiAnggotaCIP> DaftarPeserta();
        RegistrasiCIP GetDocument(string kode);
        bool AllowEdit(string kode);
        bool UpdateWithDetail(RegistrasiCIP model, List<AnggotaCIP> listAnggotaCIP);
        bool SetActive(string kode, bool isAktif);
        FormatedList<PenjurianSofi> DataTablePenjurianSofi(string kodeRegistrasi);
        FormatedList<sp_GetCipForDirektorat_Result> DataTableForAssignToDir(int tahun, string unit, string direktorat);
        FormatedList<sp_GetCipAssignedDirektorat_Result> DataTableAssignDir(int tahun, string unit, string direktorat);
        FormatedList<sp_GetCipForEnterprise_Result> DataTableForAssignToEnter(int tahun, string unit, string direktorat);
        FormatedList<sp_GetCipAssignedEnterprise_Result> DataTableAssignEnter(int tahun, string unit, string direktorat);
        bool CreateAsignCipDir(List<AsignCipDir> models);
        bool CreateAsignCipEnter(List<AsignCipEnter> models);
        bool UndoAsignCipDir(string kodeRegistrasi);
        bool UndoAsignCipEnter(string cipDirektorat);
        bool UpdateStatusDir(string kode, string status);
        bool UpdateStatusCipDir(string kode, string status);
        bool UpdateStatusEnter(string kode, string status);
        bool UpdateStatusCipEnter(string kode, string status);
        IEnumerable<RegistrasiAnggotaCIPDir> DaftarPesertaDir();
        IEnumerable<RegistrasiAnggotaCIPEnter> DaftarPesertaEnter();
        bool UpdateWithDetailEnter(AsignCipEnter model, List<AnggotaCIPEnter> listAnggotaCIP);
        bool UpdateWithDetailDir(AsignCipDir model, List<AnggotaCIPDir> listAnggotaCIP);
        bool UpdateVerifikasiAdminEnter(string kode, string status);
        bool UpdateVerifikasiAdminDir(string kode, string status);
        bool AllowEditDir(string kode);
        bool AllowEditEnter(string kode);
        FormatedList<PenjurianSofi> DataTablePenjurianSofiEnter(string cipEnterprise);
        FormatedList<PenjurianSofi> DataTablePenjurianSofiDir(string cipDirektorat);
        FormatedList<RegistrasiAnggotaCIPDir> DatatablesByPeriodeAnggotaDir(int? periode);
        FormatedList<RegistrasiAnggotaCIPEnter> DatatablesByPeriodeAnggotaEnter(int? periode);
        FormatedList<RegistrasiAnggotaCIPDir> DatatablesByPeriodeDir(int? periode);
        FormatedList<RegistrasiAnggotaCIPEnter> DatatablesByPeriodeEnter(int? periode);
        FormatedList<RegistrasiAnggotaCIPDir> DatatablesByPeriodeHistoryDir(int? periode);
        FormatedList<RegistrasiAnggotaCIPEnter> DatatablesByPeriodeHistoryEnter(int? periode);
        FormatedList<RegistrasiAnggotaCIPDir> DataTableVerifikasiPesertaDir();
        FormatedList<RegistrasiAnggotaCIPEnter> DataTableVerifikasiPesertaEnter();
        dynamic DynamicDataDir(string term);
        dynamic DynamicDataEnter(string term);
        List<Dropdown> DropdownForStreamGugusDir(int tahun);

    }
}
