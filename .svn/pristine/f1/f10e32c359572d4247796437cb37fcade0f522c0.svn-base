﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Reposirories.Concretes;
using CIP.Core.Repositories.Concretes.Master;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.Core.Repositories.Abstractions;
using System.Dynamic;
using System.Collections.Generic;
using CIP.Core.Common;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http.Headers;
using DataTablesParser;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/PenjurianDir")]
    public class PenjurianDirController : BaseApiController<PenjurianHeaderDir>
    {
        private GenericContext ctx;
        private IPenjurianDirRepository objRepo;

        public PenjurianDirController()
        {
            ctx = new GenericContext();
            objRepo = new PenjurianDirRepository(ctx);
            base.repo = objRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = objRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownRegistrasi()
        {
            try
            {
                using (var result = new GenericWebApiResult<AsignCipDir>())
                {
                    result.Dropdown = objRepo.DropdownRegistrasi();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AsignCipDir>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpGet, Route("GetRegistrasi")]
        public IGenericWebApiResult GetRegistrasiCIP(string keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<sp_GetRegistrasiCIP_Result>())
                {
                    result.Data = objRepo.GetRegistrasi(keyValues);

                    if (result.Data != null)
                    {
                        result.Success = true;
                    }
                    else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }

        }

        [HttpGet, Route("GetPenjurianHeader")]
        public IGenericWebApiResult GetPenjurianHeaderByRegistrasi(string keyValues, string kodeJuri)
        {
            try
            {
                using (var result = new GenericWebApiResult<PenjurianHeaderDir>())
                {
                    result.Data = objRepo.GetByRegistrasi(keyValues, kodeJuri);

                    if (result.Data != null)
                    {
                        result.Success = true;
                    }
                    else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }

        }

        [HttpGet, Route("GetPenjurianDetail")]
        public IGenericWebApiResult GetPenjurianDetailByKodeHeader(string Kode, string kodeJuri)
        {
            try
            {
                using (var result = new GenericWebApiResult<sp_GetPenjurianDetailDir_Result>())
                {
                    result.DataList = objRepo.GetPenjurianDetail(Kode, kodeJuri);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<sp_GetPenjurianDetailDir_Result>(ex))
                {
                    result.Success = false;
                    result.DataList = new List<sp_GetPenjurianDetailDir_Result>();

                    return result;
                }
            }



        }

        [HttpPost]
        public IGenericWebApiResult Update(HttpRequestMessage request)
        {
            try
            {
                using (var result = new GenericWebApiResult<PenjurianDir>())
                {

                    var content = request.Content;
                    string jsonContent = content.ReadAsStringAsync().Result;
                    PenjurianDir model = JsonConvert.DeserializeObject<PenjurianDir>(jsonContent);

                    result.Success = objRepo.UpdatePenjurian(model.PenjurianHeader, model.PenjurianDetail);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<PenjurianDir>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost, Route("ResetKoef")]
        public virtual IGenericWebApiResult ResetKoef(ParamHeaderDetail<MasterJenisCIP, KoefStream> data)
        {
            try
            {
                using (var result = new GenericWebApiResult<List<KoefStream>>())
                {
                    result.Success = objRepo.ResetKoef(data.detail, data.header.Kode);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<List<KoefStream>>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost,Route("SaveKoef")]
        public virtual IGenericWebApiResult SaveKoef(ParamHeaderDetail<MasterJenisCIP, KoefStream> data)
        {
            try
            {
                using (var result = new GenericWebApiResult<List<KoefStream>>())
                {
                    result.Success = objRepo.SaveKoef(data.detail, data.header.Kode);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<List<KoefStream>>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }
        
        [HttpPost]
        public IGenericWebApiResult ResetNilaiPenjurian(int kodePenjurian)
        {
            try
            {
                using (var result = new GenericWebApiResult<PenjurianHeader>())
                {
                    result.Success = objRepo.ResetPenjurian(kodePenjurian);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<PenjurianHeader>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet, Route("getNilai")]
        public HttpResponseMessage GetNilaiPenjurian(string stream, int tahun)
        {
            try
            {

                var datatable = objRepo.GetNilaiPenjurian(stream, tahun);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost, Route("getDocking")]
        public IGenericWebApiResult GetDockingPenjurian(int tahun, string gugus)
        {
            try
            {
                using (var result = new GenericWebApiResult<DockingInternalViewDir>())
                {
                    var tables = objRepo.GetDockingPenjurian(tahun, gugus).Tables;
                    if (tables.Count > 1)
                    {
                        result.More = tables;
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = BaseConstants.MESSAGE_RECORD_NOT_FOUND;
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<DockingInternalView>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Data = new DockingInternalView();

                    return result;
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage GetSummary(int periode)
        {
            try
            {
                var datatable = objRepo.GetSummary(periode, repo.UserProfile.Direktorat);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetValueCreation(int periode)
        {
            try
            {
                var datatable = objRepo.GetValueCreation(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage GetSummaryBreakdown(int periode, string kodeRegistrasi)
        {
            try
            {
                kodeRegistrasi = string.IsNullOrEmpty(kodeRegistrasi) ? "" : kodeRegistrasi;

                var datatable = objRepo.GetSummaryBreakdown(periode, kodeRegistrasi);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage GetKoef(int tahun,string kodeJenisCip)
        {
            try
            {
                var periode = ctx.MasterStdDev.SingleOrDefault(x => x.periode ==tahun);
                double stdDev = (double)periode.stdDevKoefisien;
                var datatable = objRepo.GetKoef(tahun,stdDev,repo.UserProfile.Direktorat,kodeJenisCip);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }



        [HttpGet]
        public HttpResponseMessage GetValueCreationExcelReport(int tahun)
        {
            var BytesFile = ExcelGenerator.GenerateValueCreateionExcelDir(tahun,objRepo.UserProfile.Direktorat);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "ValueCreation.xlsx";
            return result;
        }

        [HttpGet] 
        public HttpResponseMessage GetKoefExcelReport(int tahun,string kodeJenisCip) 
        {
            var periode = ctx.MasterStdDev.SingleOrDefault(x => x.periode == tahun);
            double stdDev = (double)periode.stdDevKoefisien;
            Koef koef = objRepo.GetKoef(tahun,stdDev,repo.UserProfile.Direktorat, kodeJenisCip);
            var BytesFile= ExcelGenerator.GenerateKoefExcel(koef);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null; 
            result = Request.CreateResponse(HttpStatusCode.OK); 
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Koefisien.xlsx"; 
            return result; 
        }

        [HttpGet]
        public HttpResponseMessage GetSummaryExcelReport(int periode)
        {
            var BytesFile = ExcelGenerator.GenerateSummaryExcelDir(periode,repo.UserProfile.Direktorat);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Summary.xlsx";
            return result;
        }

        [HttpGet]
        public HttpResponseMessage GetStreamExcelReport(int periode,string stream)
        {
            var BytesFile = ExcelGenerator.GenerateStreamExcelDir(periode, stream);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Stream.xlsx";

            return result;
        }

        [HttpPost]
        public IGenericWebApiResult StartPenjurian(string kode, string kodeRegistrasi)
        {
            try
            {
                using (var result = new GenericWebApiResult<PenjurianDir>())
                {
                    result.Success = objRepo.StartPenjurian(kode, kodeRegistrasi);
                    result.Message = result.Success ? BaseConstants.MESSAGE_START_PENJURIAN_SUCCESS: BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<PenjurianDir>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult FinishPenjurian(string kode, string kodeRegistrasi)
        {
            try
            {
                using (var result = new GenericWebApiResult<PenjurianDir>())
                {
                    result.Success = objRepo.FinishPenjurian(kode, kodeRegistrasi);
                    result.Message = result.Success ? BaseConstants.MESSAGE_FINISH_PENJURIAN_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<PenjurianDir>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }
    }
}
