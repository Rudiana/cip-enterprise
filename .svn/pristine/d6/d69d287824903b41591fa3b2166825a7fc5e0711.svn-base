﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Reposirories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.Core.Repositories.Concretes.Master;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.AdminQA
{
    [RoutePrefix(@"api/StreamJuri")]
    public class StreamJuriController : BaseApiController<StreamJuri>
    {
        private GenericContext ctx;
        private IStreamJuriRepository genRepo;

        public StreamJuriController()
        {
            ctx = new GenericContext();
            genRepo = new StreamJuriRepository(ctx);

            base.repo = genRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = genRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        public override IGenericWebApiResult Post(StreamJuri data)
        {
            try
            {


                using (var result = new GenericWebApiResult<StreamJuri>())
                {
                    int cek = ctx.Set<StreamGugus>().Where(x => x.KodeStream.Equals(data.KodeStream)).Count();
                    if (cek > 0)
                    {
                        data.Kode = Guid.NewGuid().ToString().Substring(0, 20);
                        result.Data = data;
                        data.Unit = repo.UserProfile.Unit;
                        result.Success = repo.Create(result.Data);
                        result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;
                    }
                    else {
                        result.Success = false;
                        result.Message = String.Format(BaseConstants.MESSAGE_STREAM_EMPTY,data.KodeStream);
                    }
                    
                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<StreamJuri>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownTahun()
        {
            try
            {
                using (var result = new GenericWebApiResult<Stream>())
                {

                    result.Dropdown = genRepo.DropdownTahun();
                    result.Success = true;
           
                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Stream>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownJenisCip()
        {
            try
            {
                using (var result = new GenericWebApiResult<MasterJenisCIP>())
                {

                    result.Dropdown = genRepo.DropdownJenisCip();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<MasterJenisCIP>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownStream(int tahun)
        {
            try
            {
                using (var result = new GenericWebApiResult<Stream>())
                {

                    result.Dropdown = genRepo.DropdownStream(tahun);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Stream>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownStreamGugusAvail(int tahun, string stream)
        {
            try
            {
                using (var result = new GenericWebApiResult<Stream>())
                {

                    result.Dropdown = genRepo.DropdownStreamGugusAvail(tahun, stream);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Stream>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }
    }
}
