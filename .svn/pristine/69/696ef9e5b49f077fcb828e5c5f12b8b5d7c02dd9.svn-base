﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IPenjurianDirRepository: IGenericDataRepository<PenjurianHeaderDir>
    {
        new FormatedList<sp_GetPenjurianHeaderDir_Result> DataTables();
        List<sp_GetPenjurianDetailDir_Result> GetPenjurianDetail(string kodePenjurianHeader, string kodeJuri);
        bool UpdatePenjurian(PenjurianHeaderDir modelHeader, List<sp_GetPenjurianDetailDir_Result> modelDetail);
        sp_GetRegistrasiCIP_Result GetRegistrasi(string Kode);
        List<Dropdown> DropdownRegistrasi();
        PenjurianHeaderDir GetByRegistrasi(string Kode, string kodeJuri);
        NilaiPenjurianDir GetNilaiPenjurian(string stream, int tahun);
        DataSet GetDockingPenjurian(int tahun, string gugus);
        FormatedList<sp_GetSummaryDir_Result> GetSummary(int tahun, string direktorat);
        Koef GetKoef(int tahun, double stdDev, string direktorat, string KodeJenisCip);
        double? GetDefaultStdDev();
        bool StartPenjurian(string kode, string kodeRegistrasi);
        bool FinishPenjurian(string kode, string kodeRegistrasi);
        bool ResetPenjurian(int kodePenjurian);
        FormatedList<sp_GetSummaryBreakdownDir_Result> GetSummaryBreakdown(int tahun, string kodeRegistrasi);
        bool SaveKoef(List<KoefStream> KoefStream, string JenisCip);
        bool ResetKoef(List<KoefStream> KoefStream, string JenisCip);
        List<ValueCreationDir> GetValueCreation(int tahun,string stream);
        bool SetValueCreation(ValueCreationDir modelHeader);
    }
}