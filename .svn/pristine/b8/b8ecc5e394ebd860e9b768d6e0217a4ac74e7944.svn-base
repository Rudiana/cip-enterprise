﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Reposirories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using DataTablesParser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace CIP.Core.Repositories.Concretes.Activity
{
    public class AuditRepository : GenericDataRepository<AuditPDCAHeader, IGenericContext>, IAuditRepository
    {
        protected IGenericContext ctx;
        public AuditRepository(IGenericContext ctx) : base(ctx)
        {
            this.ctx = ctx;
        }

        public new FormatedList<sp_GetAuditPDCAHeader_Result> DataTables()
        {
            IQueryable<sp_GetAuditPDCAHeader_Result> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.sp_GetAuditPDCAHeader(this.UserProfile.Kode).ToList().AsQueryable();
            }
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetAuditPDCAHeader_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;
        }

        public bool CreateAuditPDCA(AuditPDCAHeader modelHeader, List<sp_GetAuditPDCADetail_Result> modelDetail)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    p_CreateAuditPDCA(modelHeader, modelDetail);
                    trans.Commit();

                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool UpdateAuditPDCA(AuditPDCAHeader modelHeader, List<sp_GetAuditPDCADetail_Result> modelDetail)
        {
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var header = base.GetSingle(modelHeader.Kode);
                    if (header != null)
                    {
                        header.LingkupKerjaTim = modelHeader.LingkupKerjaTim;
                        header.LingkupKerjaTim_2 = modelHeader.LingkupKerjaTim_2;
                        header.CatatanAudit = modelHeader.CatatanAudit;
                        header.CatatanAudit_2 = modelHeader.CatatanAudit_2;
                        header.EvidenceAudit = modelHeader.EvidenceAudit;
                        header.EvidenceAudit_2 = modelHeader.EvidenceAudit_2;
                        header.Coach = modelHeader.Coach;
                        header.Coach_2 = modelHeader.Coach_2;
                        header.Auditor = modelHeader.Auditor;
                        header.Auditor_2 = modelHeader.Auditor_2;
                        header.Auditee = modelHeader.Auditee;
                        header.Auditee_2 = modelHeader.Auditee_2;

                        base.Update(header);

                        var listDataDetail = new List<AuditPDCADetail>();
                        bool res = false;
                        using (var ctx = new GenericContext())
                        {
                            foreach (sp_GetAuditPDCADetail_Result item in modelDetail)
                            {
                                if (item.Kode.Trim().Length == 0)
                                {
                                    var dataDetial = new AuditPDCADetail()
                                    {
                                        Kode = Guid.NewGuid().ToString(),
                                        KodePDCAHeader = modelHeader.Kode,
                                        KodeKriteria = item.KodeKriteria,
                                        KodeTemplatePDCA = item.KodeTemplatePDCA,
                                        Cek = item.Cek,
                                        StatusAudit = modelHeader.StatusAudit
                                    };
                                    listDataDetail.Add(dataDetial);
                                }
                                else
                                {
                                    var dataDetail = ctx.AuditPDCADetail.Where(x => x.Kode.Equals(item.Kode)).FirstOrDefault();
                                    dataDetail.KodeKriteria = item.KodeKriteria;
                                    dataDetail.KodeTemplatePDCA = item.KodeTemplatePDCA;

                                    var cek = item.Cek.HasValue ? item.Cek.Value == true ? true : false : false;
                                    dataDetail.Cek = cek;
                                    dataDetail.StatusAudit = cek ? (string.IsNullOrEmpty(dataDetail.StatusAudit) ? modelHeader.StatusAudit : dataDetail.StatusAudit) : "";
                                }
                            }
                            ctx.AuditPDCADetail.AddRange(listDataDetail);
                            res = ctx.SaveChanges() > 0;
                        }
                    }
                    trans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }


        public List<Dropdown> DropdownRegistrasi()
        {

            var tableName = typeof(RegistrasiCIP).Name;
            var sql = string.Format(@"select Kode as id, Kode  + ' - ' + NamaGugus as text, Kode as value from 
                                    RegistrasiCIP where Kode not in(select isnull(KodeRegistrasi,'') 
                                    from AuditPDCAHeader where  isnull(CreateBy,'')='{0}')", this.UserProfile.Kode);
            var list = ctx.Database.SqlQuery<Dropdown>(sql).ToList<Dropdown>();

            return list;
        }

        public sp_GetRegistrasiCIP_Result GetRegistrasi(string Kode)
        {
            var res = new sp_GetRegistrasiCIP_Result();
            using (var ctx = new GenericContext())
            {
                res = ctx.sp_GetRegistrasiCIP(Kode).FirstOrDefault();
            }
            return res;
        }

        public AuditPDCAHeader GetByRegistrasi(string Kode)
        {
            var res = new AuditPDCAHeader();
            using (var ctx = new GenericContext())
            {
                res = ctx.AuditPDCAHeader.Where(x => x.KodeRegistrasi.Equals(Kode) && x.CreateBy == this.UserProfile.Kode).FirstOrDefault();
            }
            return res;
        }

        public List<sp_GetAuditPDCADetail_Result> GetPdcaDetail(string kodePDCAHeader)
        {
            List<sp_GetAuditPDCADetail_Result> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.sp_GetAuditPDCADetail(kodePDCAHeader, this.UserProfile.Kode).ToList();
            }

            return queryable;
        }

        public bool StartPDCA_1(string kode, string kodeRegistrasi)
        {
            var rec = base.GetSingle(kode);
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var success = false;
                    if (rec != null)
                    {
                        rec.StatusAudit = "Audit PDCA I";

                        base.Update(rec);

                        using (IRegistrasiCIPRepository repoRegistrasiCIP = new RegistrasiCIPRepository(ctx))
                        {
                            repoRegistrasiCIP.UserProfile = this.UserProfile;

                            success = repoRegistrasiCIP.UpdateStatus(kodeRegistrasi, BaseEnums.EnumRegistrationStatus.Audit_PDCA_I_On_Progress.ReplaceUnderScoreToSpace());

                            trans.Commit();
                        }
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }

                    return success;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool FinishPDCA_1(string kode, string kodeRegistrasi)
        {
            var checkDocument = ctx.Set<RegistrasiCIP>().Where(x =>
                x.Kode == kodeRegistrasi
                && (x.IsUploadRisalah == false || x.IsUploadRisalah == null)).ToList();

            if (checkDocument.Count() > 0)
            {
                throw new Exception("Audit PDCA I tidak dapat di Finish. Silahkan Upload Risalah terlebih dahulu.");
            }

            var rec = base.GetSingle(kode);
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var success = false;
                    if (rec != null)
                    {
                        var recDetail = (from a in ctx.Set<AuditPDCADetail>()
                                         join b in ctx.Set<TemplateAuditPDCA>() on new { a.KodeTemplatePDCA, a.KodeKriteria } equals new { KodeTemplatePDCA = b.Kode, b.KodeKriteria }
                                         where a.KodePDCAHeader == kode && a.KodeKriteria == "1"
                                         select a.Cek).ToList();

                        if (recDetail.Count() > 0)
                        {
                            var cek = recDetail.Where(x => x.HasValue ? x.Value == false : x == null);
                            if (cek.Count() > 0)
                            {
                                throw new Exception("Langkah 1 s/d 4 Mandatori pada Audit PDCA I");
                            }
                        }
                        else
                        {
                            throw new Exception("Langkah 1 s/d 4 Mandatori pada Audit PDCA I");
                        }

                        rec.StatusAudit = BaseEnums.EnumRegistrationStatus.Wait_to_Audit_PDCA_II.ReplaceUnderScoreToSpace();

                        base.Update(rec);

                        using (IRegistrasiCIPRepository repoRegistrasiCIP = new RegistrasiCIPRepository(ctx))
                        {
                            repoRegistrasiCIP.UserProfile = this.UserProfile;

                            success = repoRegistrasiCIP.UpdateStatus(kodeRegistrasi, rec.StatusAudit);

                            trans.Commit();
                        }
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }

                    return success;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool StartPDCA_2(string kode, string kodeRegistrasi)
        {
            var rec = base.GetSingle(kode);
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var success = false;
                    if (rec != null)
                    {
                        rec.StatusAudit = "Audit PDCA II";

                        base.Update(rec);

                        using (IRegistrasiCIPRepository repoRegistrasiCIP = new RegistrasiCIPRepository(ctx))
                        {
                            repoRegistrasiCIP.UserProfile = this.UserProfile;

                            success = repoRegistrasiCIP.UpdateStatus(kodeRegistrasi, BaseEnums.EnumRegistrationStatus.Audit_PDCA_II_On_Progress.ReplaceUnderScoreToSpace());

                            trans.Commit();
                        }
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }

                    return success;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public bool FinishPDCA_2(string kode, string kodeRegistrasi)
        {
            var checkDocument = ctx.Set<RegistrasiCIP>().Where(x =>
                x.Kode == kodeRegistrasi
                && (x.IsUploadAbster == false || x.IsUploadAbster == null)
                && (x.IsUploadPresentasi == false || x.IsUploadPresentasi == null)
                && (x.IsUploadKOMET == false || x.IsUploadKOMET == null)
                && (x.IsUploadRisalah == false || x.IsUploadRisalah == null)
                && (x.IsUploadSTK == false || x.IsUploadSTK == null)
                //&& (x.IsUploadVerifikasiKeuangan == false || x.IsUploadVerifikasiKeuangan == null)
                ).ToList();

            if(checkDocument.Count() > 0)
            {
                throw new Exception("Audit PDCA II tidak dapat di Finish. Silahkan lengkapi semua kelengkapan dokumen.");
            }

            var rec = base.GetSingle(kode);
            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    var success = false;
                    if (rec != null)
                    {
                        rec.StatusApproval = true;

                        base.Update(rec);

                        using (IRegistrasiCIPRepository repoRegistrasiCIP = new RegistrasiCIPRepository(ctx))
                        {
                            repoRegistrasiCIP.UserProfile = this.UserProfile;

                            success = repoRegistrasiCIP.UpdateStatus(kodeRegistrasi, BaseEnums.EnumRegistrationStatus.Ready_to_Show.ReplaceUnderScoreToSpace());

                            trans.Commit();
                        }
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);
                    }

                    return success;
                }
                catch (Exception ex)
                {
                    trans.Rollback();

                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
        }

        public IQueryable<string> DualList(string kodeUser)
        {
            if (string.IsNullOrEmpty(kodeUser))
            {
                var records = (from a in ctx.Set<AuditPDCAHeader>().ToList()
                               join b in ctx.Set<RegistrasiCIP>().ToList()
                                 on a.KodeRegistrasi equals b.Kode
                               select a.KodeRegistrasi).Distinct().AsQueryable();

                return records;
            }
            else
            {
                var records = (from a in ctx.Set<AuditPDCAHeader>().ToList()
                               join b in ctx.Set<RegistrasiCIP>().ToList()
                                 on a.KodeRegistrasi equals b.Kode
                               where a.CreateBy.ToLower().Contains(kodeUser.ToLower())
                               select a.KodeRegistrasi).Distinct().AsQueryable();

                return records;
            }
        }

        public bool AssignAuditorGugus(List<AuditPDCAHeader> headers)
        {
            var modelDetail = new List<sp_GetAuditPDCADetail_Result>();
            using (ITemplatePdcaRepository repoTemplatePDCA = new TemplatePdcaRepository(ctx))
            {
                repoTemplatePDCA.UserProfile = this.UserProfile;
                repoTemplatePDCA.GetAll().ForEach(x =>
                {
                    var auditPDCADtl = new sp_GetAuditPDCADetail_Result()
                    {
                        KodeKriteria = x.KodeKriteria,
                        KodeTemplatePDCA = x.Kode,
                        Langkah = x.Langkah,
                        NoUrut = x.NoUrut
                    };

                    modelDetail.Add(auditPDCADtl);
                });
            }

            using (var trans = ctx.Database.BeginTransaction(IsolationLevel.ReadCommitted))
            {
                try
                {
                    foreach (var modelHeader in headers)
                    {
                        var exist = base.FindBy(x => x.KodeRegistrasi == modelHeader.KodeRegistrasi && x.CreateBy == modelHeader.CreateBy).FirstOrDefault();
                        if (exist == null)
                        {
                            p_CreateAuditPDCA(modelHeader, modelDetail);
                        }
                    }

                    trans.Commit();
                    return true;
                }
                catch (DbEntityValidationException ex)
                {
                    trans.Rollback();

                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    var fullErrorMessage = string.Join("; ", errorMessages);
                    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                    throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                }
            }
        }

        public AuditReport Pdca1FormReport(int Periode, string KodeRegistrasi)
        {
            AuditReport auditReport = new AuditReport();
            using (var ctx = new GenericContext())
            {
                List<sp_GetPdca1Report_Result> Pdca1Detail = ctx.sp_GetPdca1Report(KodeRegistrasi, Periode).ToList();
                AuditPDCAHeader Header = ctx.AuditPDCAHeader.Where(x => x.KodeRegistrasi.Equals(KodeRegistrasi)).FirstOrDefault();
                RegistrasiCIP Registrasi = ctx.RegistrasiCIP.Where(x => x.Kode.Equals(KodeRegistrasi) && x.Tahun == Periode).FirstOrDefault();
                List<AnggotaCIP> Anggota = ctx.AnggotaCIP.Where(x => x.KodeRegistrasi.Equals(KodeRegistrasi)).ToList();

                auditReport.Anggota = Anggota;
                auditReport.PdcaHeader = Header;
                auditReport.Pdca1Detail = Pdca1Detail;
                auditReport.Registrasi = Registrasi;
            }
            return auditReport;
        }

        public AuditReport Pdca2FormReport(int Periode, string KodeRegistrasi)
        {
            AuditReport auditReport = new AuditReport();
            using (var ctx = new GenericContext())
            {
                List<sp_GetPdca2Report_Result> Pdca2Detail = ctx.sp_GetPdca2Report(KodeRegistrasi, Periode).ToList();
                AuditPDCAHeader Header = ctx.AuditPDCAHeader.FirstOrDefault(x => x.KodeRegistrasi.Equals(KodeRegistrasi));
                RegistrasiCIP Registrasi = ctx.RegistrasiCIP.FirstOrDefault(x => x.Kode.Equals(KodeRegistrasi) && x.Tahun == Periode);
                List<AnggotaCIP> Anggota = ctx.AnggotaCIP.Where(x => x.KodeRegistrasi.Equals(KodeRegistrasi)).ToList();

                auditReport.Anggota = Anggota;
                auditReport.PdcaHeader = Header;
                auditReport.Pdca2Detail = Pdca2Detail;
                auditReport.Registrasi = Registrasi;
            }
            return auditReport;
        }

        public FormatedList<sp_GetPdcaList_Result> PdcaListReport(int periode, string status)
        {
            IQueryable<sp_GetPdcaList_Result> queryable;
            using (var ctx = new GenericContext())
            {
                queryable = ctx.Database.SqlQuery<sp_GetPdcaList_Result>(string.Format("exec sp_GetPdcaList {0},'{1}'", periode, status))
                    .ToList().AsQueryable();
            }
            var request = HttpContext.Current.Request;
            var wrapper = new HttpRequestWrapper(request);
            var parser = new DataTablesParser<sp_GetPdcaList_Result>(wrapper, queryable);
            var datatable = parser.Parse();

            return datatable;


        }

        #region Private Method
        private void p_CreateAuditPDCA(AuditPDCAHeader modelHeader, List<sp_GetAuditPDCADetail_Result> modelDetail)
        {
            modelHeader.Kode = Guid.NewGuid().ToString();

            modelHeader.StatusApproval = false;
            modelHeader.StatusAudit = BaseEnums.EnumRegistrationStatus.Wait_to_Audit_PDCA_I.ReplaceUnderScoreToSpace();

            base.Create(modelHeader);
            var listDataDetail = new List<AuditPDCADetail>();
            foreach (sp_GetAuditPDCADetail_Result item in modelDetail)
            {
                var dataDetial = new AuditPDCADetail()
                {
                    Kode = Guid.NewGuid().ToString(),
                    KodePDCAHeader = modelHeader.Kode,
                    KodeKriteria = string.IsNullOrEmpty(item.KodeKriteria) ? "" : item.KodeKriteria,
                    KodeTemplatePDCA = string.IsNullOrEmpty(item.KodeTemplatePDCA) ? "" : item.KodeTemplatePDCA,
                    Cek = item.Cek ?? false,
                    StatusAudit = item.Cek == true ? modelHeader.StatusAudit : "",
                    CreateBy = modelHeader.CreateBy
                };

                listDataDetail.Add(dataDetial);
            }
            bool res = false;
            using (var ctx = new GenericContext())
            {
                ctx.AuditPDCADetail.AddRange(listDataDetail);
                res = ctx.SaveChanges() > 0;
            }

            //using (IRegistrasiCIPRepository repoRegistrasiCIP = new RegistrasiCIPRepository(ctx))
            //{
            //    repoRegistrasiCIP.UserProfile = this.UserProfile;

            //    repoRegistrasiCIP.UpdateStatus(modelHeader.KodeRegistrasi, modelHeader.StatusAudit);
            //}
        }
        #endregion
    }
}
