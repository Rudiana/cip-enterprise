'use strict';

angular.module('app.adminqm').controller('PesertaCipController', function ($scope, $http, $state, $stateParams, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper, $uibModal) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;
    var msgTitle = "Data Peserta";

    me.init = function (obj) {
        me.vm = obj;

        me.isShowList = true;
        me.isNew = false;
        me.allowUpdate = true;
        me.data = {};
        me.filter = {};
        me.Undo();

        me.LoadCombo();

        me.filter.tahun = new Date().getFullYear();

        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('height', 200)
            .withOption('colReorder', true)

            .withFixedColumns({
                leftColumns: 2,
                rightColumns: 1
            })

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('No. Gugus').withClass('text-primary').withOption('width', '80%'),
            DTColumnBuilder.newColumn('Tahun').withTitle('Tahun'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn('Email').withTitle('Email'),
            DTColumnBuilder.newColumn('JenisCIPDes').withTitle('Jenis'),
            DTColumnBuilder.newColumn('KetagoriDes').withTitle('Kategori'),
            DTColumnBuilder.newColumn('FungsiDes').withTitle('Fungsi'),
            DTColumnBuilder.newColumn('LokasiDes').withTitle('Lokasi'),
            DTColumnBuilder.newColumn('Fasilitator').withTitle('Fasilitator'),
            DTColumnBuilder.newColumn('Ketua').withTitle('Ketua'),
            DTColumnBuilder.newColumn('JumlahAnggota').withTitle('Jumlah Anggota').withClass('text-center'),
            DTColumnBuilder.newColumn('Status').withTitle('Status'),
            DTColumnBuilder.newColumn('Keterangan').withTitle('Keterangan').withOption('width', '1000px'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Tgl. Registrasi').renderWith(function (data, type, full, meta) {
                return DateTimeFormat(data);
            }),
            DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').notSortable().renderWith(actionsHtml)
        ];

        me.vm.dtInstance = {};
    };

    function actionsHtml(data, type, full, meta) {
        var dat = data.Kode;
        var dataAnggota = data.Anggota;
        var btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().EditData(\'' + dat + '\',\'' + dataAnggota + '\')"><i class="fa fa-edit"></i> Edit Detail</button>&nbsp';
        btn = btn + '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().ViewData(\'' + dat + '\')"><i class="fa fa-search"></i> View Detail</button>';

        return btn;
    };

    me.Undo = function () {
        me.data = {};
        me.isNew = true;
    };

    me.ShowList = function () {
        me.isShowList = true;
        me.isNew = false;
    };

    me.NewData = function () {
        me.isShowList = false;
        me.Undo();
    };

    me.EditData = function (kode, dataAnggota) {
        me.isShowList = false;
        me.isNew = false;
        me.allowUpdate = true;

        me.loadData(kode, dataAnggota);
    };

    me.dataSource = function () {
        return {
            url: api + 'datatablesbyperiode?periode=' + me.filter.tahun,
            type: 'POST',
            accepts: "application/json",
            headers: function () {
                var headers = authSvc.headers();
                headers.Accept = "application/json";

                return headers;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.reloadData = function () {
        me.vm.tableOptions.withOption('ajax', me.dataSource());
    };

    me.loadData = function (kode, dataAnggota) {
        msgTitle = "Load Data Peserta";

        $http.get(api + "dynamicdata?term=" + kode)
            .success(function (result) {
                if (result.Success) {
                    me.data = result.Data;
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;
        });
    };

    me.ViewData = function (kode) {
        $uibModal.open({
            templateUrl: 'frmModal.html',
            backdrop: true,
            windowClass: 'modal',
            keyboard: true,
            size: "100-pct",
            controller: function ($scope, $uibModalInstance) {
                var loc = window.location;
                var url = loc.origin + loc.pathname + "/#/profil-anggota?kode=" + kode;
                console.log(url);
                $scope.modal = {
                    url: url,
                    kode: kode
                };
                $scope.closeZoom = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {
                dataModal: function () {
                    return $scope.modal;
                }
            }
        });
    };

    me.exportExcel = function () {
        $http.get(api + 'GeneratePeserataCIPExcel?periode=' + me.filter.tahun, { responseType: 'arraybuffer' }).success(function (response) {
            if (response !== null) {
                if (response.Message === undefined) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Daftar_Peserta_CIP_' + me.filter.tahun + '.xlsx');
                }
                else {
                    NotifBoxError("Export " + msgTitle, response.Message || "");
                }
            }
        }).error(function (err, status) {
            NotifBoxError(msgTitle, err.Message);
        });
    };

    me.Save = function () {
        if (this.frmDaftarPeserta.$valid) {
            var arrAnggota = me.data.Anggota.split(',');
            var objAnggota = [];
            angular.forEach(arrAnggota, function (k) {
                var obj = {
                    Nama: k
                };
                objAnggota.push(obj);
            });

            var fd = new FormData();
            fd.append("model", JSON.stringify(me.data));
            fd.append("anggotaCIP", JSON.stringify(objAnggota));
            fd.append("isNew", JSON.stringify(me.isNew));

            var msgTitle = 'Simpan data Registrasi';
            $http.post(api + "register", fd, {
                withCredentials: false,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
                .success(function (result) {
                    if (result.Success) {
                        NotifBoxSuccess(msgTitle, result.Message);
                        me.ShowList();
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });
        }
    };

    me.$watch('data.Anggota', function (n, o) {
        var count = 0;
        if (n != undefined || n != null) {
            count = n.split(',').length;
            me.data.JumlahAnggota = count;
        }
        else {
            me.data.JumlahAnggota = 0;
            me.disabledAnggota = false;
        }
    });

    me.init(this);
});