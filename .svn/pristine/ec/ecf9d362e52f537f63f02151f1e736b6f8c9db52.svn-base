'use strict';

angular.module('app.adminqm').controller('VerifikasiPesertaCIPController', function ($scope, $http, $state, $stateParams, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;

    me.init = function (obj) {
        me.vm = obj;

        me.data = {};
        me.isShowList = true;
        me.isNew = false;
        me.ddlStatus = [
            { value: "", text: "" },
            { value: "Tim_CIP_Approved", text: "Approve" },
            { value: "Tim_CIP_Rejected", text: "Reject" },
        ];

        me.Undo();

        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', {
                // Either you specify the AjaxDataProp here
                // dataSrc: 'data',
                url: api + 'datatableverifikasipeserta',
                type: 'POST',
                accepts: "application/json",
                headers: function () {
                    var headers = authSvc.headers();
                    headers.Accept = "application/json";

                    return headers;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Data Verifikasi Peserta", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            })
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollY', true)
            .withOption('scrollX', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 1
            })

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withOption('rowCallback', me.rowCallback)
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('No. Gugus').withClass('text-primary').notVisible(),
            DTColumnBuilder.newColumn('Tahun').withTitle('Tahun'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn('Email').withTitle('Email'),
            DTColumnBuilder.newColumn('JenisCIPDes').withTitle('Jenis'),
            DTColumnBuilder.newColumn('DirektoratDes').withTitle('Direktorat'),
            DTColumnBuilder.newColumn('UnitDes').withTitle('Unit'),
            DTColumnBuilder.newColumn('SubUnitDes').withTitle('Sub Unit'),
            DTColumnBuilder.newColumn('Fasilitator').withTitle('Fasilitator'),
            DTColumnBuilder.newColumn('Ketua').withTitle('Ketua'),
            DTColumnBuilder.newColumn('JumlahAnggota').withTitle('Jumlah Anggota'),
            DTColumnBuilder.newColumn('Status').withTitle('Status'),
            DTColumnBuilder.newColumn('Keterangan').withTitle('Keterangan').withOption('width', '100px'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Tgl. Registrasi').renderWith(function (data, type, full, meta) {
                return DateTimeFormat(data);
            }),
            DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').notSortable().withOption('width', '380px')
                .renderWith(actionsHtml)

        ];

        function actionsHtml(data, type, full, meta) {
            var btn = full.Status == "Pending Approval Admin QM" ?
                '<button class="btn bg-color-blueDark txt-color-white btn-xs" onclick="angular.element(this).scope().loadData(\'' + data.Kode + '\')"><i class="fa fa-pencil-square-o"></i> Verifikasi</button>&nbsp'
                : '<button class="btn btn-success btn-xs" onclick="angular.element(this).scope().Submit(\'' + data.Kode + '\')"><i class="fa fa-thumbs-o-up"></i> Submit ke PDCA</button>&nbsp';

            return btn;
        }

        me.vm.dtInstance = {};
    };

    me.ShowList = function () {
        me.isShowList = true;
        me.isNew = false;
    };

    me.EditData = function (data) {
        me.isShowList = false;
        me.isNew = false;
        angular.extend(me.data, data);
        console.log(me.data);
    }

    me.Undo = function () {
        me.data = {};
        me.isNew = true;
    };

    me.loadData = function (kode) {
        msgTitle = "Load Data Verifikasi Peserta";
        $http.get(api + "/getsingle?keyvalues=" + kode)
            .success(function (result) {
                if (result.Success) {
                    me.EditData(result.Data);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.UpdateVerifikasiAdminQM = function () {
        if (this.frmPesertaCip.$valid) {
            var msgTitle = 'Verifikasi Data Peserta';
            me.data.Status = isNullOrEmpty(me.data.StatusSelected) ? me.data.Status : me.data.StatusSelected;
            $http.post(api + "updateverifikasiadminqm", me.data)
                .success(function (result) {
                    if (result.Success) {
                        NotifBoxSuccess(msgTitle, result.Message);
                        me.ShowList();
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });
        }
    }

    me.Submit = function (kode) {
        var msgTitle = 'Submit Verifikasi Data Peserta';
        $http.post(api + "submitadminqm?kode=" + kode)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    me.vm.dtInstance.reloadData(function () { }, true);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.init(this);
});