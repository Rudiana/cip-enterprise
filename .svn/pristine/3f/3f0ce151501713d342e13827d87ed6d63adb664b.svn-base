﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using DataTablesParser;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace CIP.Core.Repositories.Abstractions.Activity
{
    public interface IPenjurianRepository: IGenericDataRepository<PenjurianHeader>
    {
        new FormatedList<sp_GetPenjurianHeader_Result> DataTables();
        List<sp_GetPenjurianDetail_Result> GetPenjurianDetail(string kodePenjurianHeader, string kodeJuri);
        bool UpdatePenjurian(PenjurianHeader modelHeader, List<sp_GetPenjurianDetail_Result> modelDetail);
        sp_GetRegistrasiCIP_Result GetRegistrasi(string Kode);
        List<Dropdown> DropdownRegistrasi();
        PenjurianHeader GetByRegistrasi(string Kode, string kodeJuri);
        NilaiPenjurian GetNilaiPenjurian(string stream, int tahun);
        DataSet GetDockingPenjurian(int tahun, string gugus);
        FormatedList<sp_GetSummary_Result> GetSummary(int tahun, string unit);
        Koef GetKoef(int tahun, double stdDev,string unit, string KodeJenisCip);
        double? GetDefaultStdDev();
        bool StartPenjurian(string kode, string kodeRegistrasi);
        bool FinishPenjurian(string kode, string kodeRegistrasi);
        bool ResetPenjurian(int kodePenjurian);
        FormatedList<sp_GetSummaryBreakdown_Result> GetSummaryBreakdown(int tahun, string kodeRegistrasi);
        bool SaveKoef(List<KoefStream> KoefStream, string JenisCip);
        bool ResetKoef(List<KoefStream> KoefStream, string JenisCip);
    }
}