'use strict';

angular.module('app.penjurian').controller('SummaryPenjurianController', function ($scope, $http, $state, $filter, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'Penjurian/';
    var api = BASE_API + module;
    var msgTitle = "Summary Penjurian";

    me.init = function (obj) {
        me.vm = obj;

        me.data = {};
        me.user = authSvc.authenticationData;
        me.tahun = new Date().getFullYear();
        me.kodeRegistrasi = "";

        me.breakdata = {};
     
        var headers = authSvc.headers();
        headers.Accept = "application/json";

        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 3
            })

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('tahun').withTitle('Tahun'),
            DTColumnBuilder.newColumn('stream').withTitle('Stream'),
            DTColumnBuilder.newColumn('jenisCIP').withTitle('Jenis'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('unit').withTitle('Unit'),
            DTColumnBuilder.newColumn('NilaiPdca').withTitle('Nilai PDCA').withClass('text-right').renderWith(function (data, type, full, meta) {
                return IDNumberFormat(data, 2);
            }),
            DTColumnBuilder.newColumn('NilaiRisalah').withTitle('Risalah').withClass('text-right').renderWith(function (data, type, full, meta) {
                return IDNumberFormat(data, 2);
            }),
            DTColumnBuilder.newColumn('NilaiPresentasi').withTitle('Presentasi').withClass('text-right').renderWith(function (data, type, full, meta) {
                return IDNumberFormat(data, 2);
            }),
            DTColumnBuilder.newColumn('NilaiOthers').withTitle('Others').withClass('text-right').renderWith(function (data, type, full, meta) {
                return IDNumberFormat(data, 2);
            }),
            DTColumnBuilder.newColumn('PdcaIntra').withTitle('PDCA Intra').withClass('text-right').renderWith(function (data, type, full, meta) {
                return IDNumberFormat(data, 2);
            }),
            DTColumnBuilder.newColumn('koefisien').withTitle('Koefisien').withClass('text-right').renderWith(function (data, type, full, meta) {
                return IDNumberFormat(data, 2);
            }),
            DTColumnBuilder.newColumn('finalScore').withTitle('Final Score').withClass('text-right').renderWith(function (data, type, full, meta) {
                return IDNumberFormat(data, 2);
            }),
            DTColumnBuilder.newColumn('winnerType').withTitle('Kategori CIP').renderWith(function (data, type, full, meta) {
                return '<span class="to-upper">' + data + '</span>';
            }),
            DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').notSortable().renderWith(actionsHtml)
        ];

        me.vm.dtInstance = {};
       
        me.LoadCombo();

        me.vm.tableOptions2 = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource2())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")

            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns2 = [
            DTColumnBuilder.newColumn(null).withTitle('No.').notSortable().renderWith(function (data, type, full, meta) {
                return (meta.settings._iDisplayStart + meta.row + 1);
            }),
            DTColumnBuilder.newColumn('Kriteria').withTitle('Kriteria'),
            DTColumnBuilder.newColumn('Langkah').withTitle('Langkah'),
            DTColumnBuilder.newColumn('Nilai').withTitle('Nilai')
        ];

        me.vm.dtInstance2 = {};
    };

    function actionsHtml(data, type, full, meta) {

        var dat = data.KodeRegistrasi + '\',\'' + data.tahun;
        var btn = "";
        btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().breakdown(\'' + dat + '\')"><i class="fa fa-search"></i> View Detail</button>&nbsp';
        return btn;
    };

    me.dataSource = function () {
        return {
            url: api + 'GetSummary?periode=' + me.tahun,
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                HideLoader();
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.dataSource2 = function () {
        return {
            url: api + 'GetSummaryBreakdown?periode=' + me.tahun + '&kodeRegistrasi=' + me.kodeRegistrasi,
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.breakdown = function (kode, tahun) {
        if (me.checkPeriode()) {
            me.tahun = tahun;
            me.kodeRegistrasi = kode;

            me.vm.tableOptions2.withOption('ajax', me.dataSource2());

            $('#myModal').modal('show');
        }
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'StreamJuri/DropdownTahun').then(function (result) {
            me.listTahun = result.Dropdown;

            setTimeout(function () {
                var tahun = new Date().getFullYear();
                tahun = $filter('filter')(me.listTahun, { id: tahun }, false);
                if (tahun.length > 0) {
                    tahun[0].obj = angular.copy(tahun[0]);
                    $('#tahun').select2('data', tahun[0]);
                }
            }, 2000);
        });
    };

    me.find = function () {
        if (me.checkPeriode()) {
            me.vm.dtInstance.reloadData(function () { }, true);
        }
    };

    me.exportExcel = function () {
        if (me.checkPeriode()) {
            $http.get(api + 'GetSummaryExcelReport?periode=' + me.tahun, { responseType: 'arraybuffer' }).success(function (response) {

                if (response !== null) {

                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Summary.xlsx');
                }


            }).error(function (err, status) {
                NotifBoxError('Warning', err.Message);
            });
        }
    };

    me.checkPeriode = function () {
        if (isNullOrEmpty(me.tahun)) {
            NotifBoxWarning('Information', 'Silahkan pilih Periode terlebih dahulu.');
            return false;
        }
        else {
            return true;
        }
    };

    me.$watch('tahun', function (n, o) {
        if (isNullOrEmpty(n)) {
            me.tahun = new Date().getFullYear() + 5;
        }
    }, true);

    me.init(this);
});