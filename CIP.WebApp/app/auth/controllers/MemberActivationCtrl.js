'use strict';

angular.module('app.auth').controller('MemberActivationController', function ($scope, $http, $state, $window, $location, BASE_API, User, authSvc) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;
    var msgTitle = 'Aktivasi Peserta';
    
    me.init = function (obj) {
        me.data = {};
        var url = new URL(window.location);
        me.data.Kode = url.href.split('?')[1];

        me.data.Password = "";
    };

    me.activation = function () {
        if (isNullOrEmpty(me.data.Password)) {
            NotifBoxWarning(msgTitle, "Isikan Password terlebih dahulu.");
        }
        else {
            $http.post(api + "activation?kode=" + me.data.Kode + "&x=" + me.data.Password)
                .success(function (result) {
                    if (result.Success) {
                        NotifBoxSuccess(msgTitle, result.Message);

                        me.login(me.data);
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });
        }
    };


    me.login = function (data) {
        if (isNullorEmpty(data.Kode)) {
            NotifBoxWarning("Login", "Username wajib diisi.");
            return;
        }

        var loginData = {
            userName: data.Kode,
            password: data.Password,
            useRefreshTokens: false,
            r: 1
        };

        authSvc.login(loginData).then(function (response) {
            User.userName = response.userName;
            me.data = {};

            var url = $location.$$absUrl;
            url = url.replace("aktivasi-anggota?" + loginData.userName, "profil-anggota");
            $window.location.href = url;
            $window.location.reload();

        }, function (err) {
            if (err.error == "invalid_authorize") {
                me.message = "";
                $.SmartMessageBox({
                    title: "Login Otorisasi",
                    content: err.error_description,
                    buttons: '[OK]',
                    theme: 'bg-warning'
                },
                    function (action) {
                        if (action === "OK") {
                            return;
                        }
                    });
            }
            else {
                me.message = "";
                $.SmartMessageBox({
                    title: "Login",
                    content: err.error_description,
                    buttons: '[OK]',
                    theme: 'bg-danger'
                },
                    function (action) {
                        if (action === "OK") {
                            return;
                        }
                    });
            }
        });
    };

    me.init(this);
});