'use strict';

angular.module('app.peserta').controller('HistorisController', function ($scope, $http, $state, $stateParams, $filter, $window, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;
    var msgTitle = "Data Peserta";
    me.init = function (obj) {
        me.vm = obj;

        me.allowUpdate = true;
        me.data = {};
        me.filter = {};

        me.LoadCombo();

        // UNIT
        me.vm.tableOptions = me.getOptionTable('datatablesbyperiodeanggota');
        me.vm.tableColumns = me.getColumnTable('datatablesbyperiodeanggota');
        me.vm.dtInstance = {};

        me.vm.tableOptions2 = me.getOptionTable('datatablesbyperiode');
        me.vm.tableColumns2 = me.getColumnTable('datatablesbyperiode');
        me.vm.dtInstance2 = {};

        //DIREKTORAT
        me.vm.tableOptionsDir = me.getOptionTable('datatablesbyperiodeanggotaDir');
        me.vm.tableColumnsDir = me.getColumnTable('datatablesbyperiodeanggotaDir');
        me.vm.dtInstanceDir = {};

        me.vm.tableOptionsDir2 = me.getOptionTable('datatablesbyperiodeHistoryDir');
        me.vm.tableColumnsDir2 = me.getColumnTable('datatablesbyperiodeHistoryDir');
        me.vm.dtInstanceDir2 = {};

        //ENTERPRISE
        me.vm.tableOptionsEnter = me.getOptionTable('datatablesbyperiodeanggotaEnter');
        me.vm.tableColumnsEnter = me.getColumnTable('datatablesbyperiodeanggotaEnter');
        me.vm.dtInstanceEnter = {};

        me.vm.tableOptionsEnter2 = me.getOptionTable('datatablesbyperiodeHistoryEnter');
        me.vm.tableColumnsEnter2 = me.getColumnTable('datatablesbyperiodeHistoryEnter');
        me.vm.dtInstanceEnter2 = {};
    };

    me.getOptionTable = function (method) {
        var opts = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource(method))
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('height', 200)
            .withOption('colReorder', true)
            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 1
            })

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        switch (method) {
            case 'datatablesbyperiodeanggota':
            case 'datatablesbyperiode':
                opts.order = [[13, 'asc'], [0, 'asc']];
                break;
            case 'datatablesbyperiodeanggotaDir':
            case 'datatablesbyperiodeHistoryDir':
               
                break;
            case 'datatablesbyperiodeanggotaEnter':
            case 'datatablesbyperiodeHistoryEnter':
                opts.order = [[16, 'asc'], [2, 'asc']];
                break;
            default:
                opts.order = [[17, 'asc'], [3, 'asc']];
                break;
        }

        return opts;
    }

    me.getColumnTable = function (method) {
        var cols = [];
        switch (method) {
            case 'datatablesbyperiodeanggota':
            case 'datatablesbyperiode':
                cols.push(
                    DTColumnBuilder.newColumn('Kode').withTitle('No. Gugus').withClass('text-primary').withOption('width', '80%')
                );
                break;
            case 'datatablesbyperiodeanggotaDir':
            case 'datatablesbyperiodeHistoryDir':
                cols.push(
                    DTColumnBuilder.newColumn('Kode').withTitle('Kode').withClass('text-primary').withOption('width', '80%').notVisible(),
                    DTColumnBuilder.newColumn('KodeRegistrasi').withTitle('No. Gugus Unit').withClass('text-primary').withOption('width', '80%').notVisible(),
                    DTColumnBuilder.newColumn('CipDirektorat').withTitle('No. Gusus').withClass('text-primary').withOption('width', '80%')
                );
                break;
            case 'datatablesbyperiodeanggotaEnter':
            case 'datatablesbyperiodeHistoryEnter':
                cols.push(
                    DTColumnBuilder.newColumn('Kode').withTitle('Kode').withClass('text-primary').withOption('width', '80%').notVisible(),
                    DTColumnBuilder.newColumn('KodeRegistrasi').withTitle('No. Gugus Unit').withClass('text-primary').withOption('width', '80%').notVisible(),
                    DTColumnBuilder.newColumn('CipDirektorat').withTitle('No. Gusus Direktorat').withClass('text-primary').withOption('width', '80%').notVisible(),
                    DTColumnBuilder.newColumn('CipEnterprise').withTitle('No. Gusus').withClass('text-primary').withOption('width', '80%')
                );
                break;
            default:
                cols.push(
                    DTColumnBuilder.newColumn('Kode').withTitle('No. Gugus').withClass('text-primary').withOption('width', '80%')
                );
                break;
        }

        cols.push(
            DTColumnBuilder.newColumn('JenisCIPDes').withTitle('Jenis'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn('Email').withTitle('Email'),
            DTColumnBuilder.newColumn('DirektoratDes').withTitle('Direktorat'),
            DTColumnBuilder.newColumn('UnitDes').withTitle('Unit'),
            DTColumnBuilder.newColumn('SubUnitDes').withTitle('Sub Unit'),
            DTColumnBuilder.newColumn('Ketua').withTitle('Ketua'),
            DTColumnBuilder.newColumn('Fasilitator').withTitle('Fasilitator'),
            DTColumnBuilder.newColumn('JumlahAnggota').withTitle('Jml. Anggota').withClass('text-center'),
            DTColumnBuilder.newColumn('Status').withTitle('Status'),
            DTColumnBuilder.newColumn('Keterangan').withTitle('Keterangan').withOption('width', '1000px'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Tgl. Registrasi').renderWith(function (data, type, full, meta) {
                return DateTimeFormat(data);
            }),
            DTColumnBuilder.newColumn(null).withTitle('#').withClass('text-center').notSortable().withOption('width', '380px')
                .renderWith(actionsHtml)

        );

        function actionsHtml(data, type, full, meta) {
            var kode = data.Kode;
            var no = kode;
            switch (method) {
                case 'datatablesbyperiodeanggotaDir':
                case 'datatablesbyperiodeHistoryDir':
                    no = data.CipDirektorat;
                    break;
                case 'datatablesbyperiodeanggotaEnter':
                case 'datatablesbyperiodeHistoryEnter':
                    no = data.CipEnterprise;
                    break;
                default:
                    kode = kode;
                    no = no;
                    break;
            }


            var btn = '<div class="btn-group dropdown" data-dropdown>' +
                '<div style="width: 140px; text-align: center !importtant;">' +
                '<button class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" style="width: 90px;">Aksi <span class="caret"></span></button>' +
                '<ul class="dropdown-menu" style="left: -11px !important;">' +
                '<li>' +
                '<a onclick="angular.element(this).scope().editDetail(\'' + no + '\',\'' + method +'\')"><i class="fa fa-edit text-warning"></i> Edit Detail</a>' +
                '</li>' +
                '<li>' +
                '<a onclick="angular.element(this).scope().viewDetail(\'' + no + '\',\'' + method +'\')"><i class="fa fa-search text-success"></i> Lihat Detail</a>' +
                '</li>' +
                '<li class="divider"></li>' +
                '<li>' +
                '<a onclick="angular.element(this).scope().downloadRisalah(\'' + kode + '\',\'' + method +'\')"><i class="fa fa-cloud-download text-primary"></i> Unduh Risalah</a>' +
                '</li>' +
                '</ul>' +
                '</div>';

            return btn;
        }

        return cols;
    };

    me.dataSource = function (method, tahun) {
        me.filter.Tahun = me.filter.Tahun || new Date().getFullYear();
        if (!isNullOrEmpty($stateParams.tahun)) {
            me.filter.Tahun = $stateParams.tahun;
        }

        return {
            url: api + method + '?periode=' + me.filter.Tahun,
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.reloadData = function () {
        $stateParams.tahun = "";
        me.vm.tableOptions.withOption('ajax', me.dataSource('datatablesbyperiodeanggota'));
        me.vm.tableOptions2.withOption('ajax', me.dataSource('datatablesbyperiode'));
    };

    me.loadData = function (kode, dataAnggota) {
        msgTitle = "Load Data Peserta";

        $http.get(api + "dynamicdata?term=" + kode)
            .success(function (result) {
                if (result.Success) {
                    me.data = result.Data;
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;

            setTimeout(function () {
                var tahun = isNullOrEmpty($stateParams.tahun) ? new Date().getFullYear() : $stateParams.tahun;
                tahun = $filter('filter')(me.listTahun, { id: tahun }, false);
                if (tahun.length > 0) {
                    tahun[0].obj = angular.copy(tahun[0]);
                    $('#FilterTahun').select2('data', tahun[0]);
                }
            }, 2000);

        });
    };

    me.exportExcel = function () {
        me.filter.Tahun = me.filter.Tahun || new Date().getFullYear();
        $http.get(api + 'GeneratePeserataCIPExcel?periode=' + me.filter.Tahun, { responseType: 'arraybuffer' }).success(function (response) {
            if (response !== null) {
                if (response.Message === undefined) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Daftar_Peserta_CIP_' + me.filter.Tahun + '.xlsx');
                }
                else {
                    NotifBoxError("Export " + msgTitle, response.Message || "");
                }
            }
        }).error(function (err, status) {
            NotifBoxError(msgTitle, err.Message);
        });
    };

    me.editDetail = function (kode, method) {
        var stateGo = 'app.peserta.registrasi';
        switch (method) {
            case 'datatablesbyperiodeanggotaDir':
            case 'datatablesbyperiodeHistoryDir':
                stateGo = 'app.pesertadir.registrasi';
                break;
            case 'datatablesbyperiodeanggotaEnter':
            case 'datatablesbyperiodeHistoryEnter':
                stateGo = 'app.pesertaenter.registrasi';
                break;
            default:
                break;
        }

        $state.go(stateGo, { kode: kode, tahun: me.filter.Tahun, allowEdit: true });
    };

    me.viewDetail = function (kode, method) {
        var stateGo = 'app.peserta.registrasi';
        switch (method) {
            case 'datatablesbyperiodeanggotaDir':
            case 'datatablesbyperiodeHistoryDir':
                stateGo = 'app.pesertadir.registrasi';
                break;
            case 'datatablesbyperiodeanggotaEnter':
            case 'datatablesbyperiodeHistoryEnter':
                stateGo = 'app.pesertaenter.registrasi';
                break;
            default:
                break;
        }

        $state.go(stateGo, { kode: kode, tahun: me.filter.Tahun, allowEdit: false });
    };

    me.refreshAnggotaGugus = function () {
        //me.vm.dtInstance.DataTable.search('2017').draw();
        me.vm.dtInstance.reloadData(function () { }, true);
    };

    me.refreshAllGugus = function () {
        //me.vm.dtInstance.DataTable.search('2017').draw();
        me.vm.dtInstance2.reloadData(function () { }, true);
    };

    me.onClickDir = function () {
        me.vm.dtInstanceDir.reloadData(function () { }, true);;
    };

    me.onClickDirAll = function () {
        me.vm.dtInstanceDir2.reloadData(function () { }, true);;
    };

    me.onClickEnter = function () {
        me.vm.dtInstanceEnter.reloadData(function () { }, true);;
    };

    me.onClickEnterAll = function () {
        me.vm.dtInstanceEnter2.reloadData(function () { }, true);
    };

    me.downloadRisalah = function (kode, method) {
        switch (method) {
            case 'datatablesbyperiodeanggotaDir':
            case 'datatablesbyperiodeHistoryDir':
                method = 'downloadrisalahdir';
                break;
            case 'datatablesbyperiodeanggotaEnter':
            case 'datatablesbyperiodeHistoryEnter':
                method = 'downloadrisalahenter';
                break;
            default:
                method = 'downloadrisalah';
                break;
        }

        $http.get(api + method + "?kode=" + kode)
            .success(function (result) {
                if (result.Success) {
                    $window.open(BASE_API.replace('/api', '') + 'downloads/' + result.Data, '_blank');
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.init(this);
});