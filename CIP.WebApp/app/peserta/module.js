"use strict";


angular.module('app.peserta', ['ui.router', 'datatables', 'datatables.bootstrap', 'ngHandsontable']);

angular.module('app.peserta').config(function ($stateProvider) {

    $stateProvider
        .state('app.peserta', {
            abstract: true,
            data: {
                title: 'Peserta Unit'
            }
        })

        .state('app.peserta.registrasi', {
            url: '/peserta/registrasi',
            data: {
                title: 'Registrasi'
            },
            params: {
                kode: "",
                tahun: "",
                allowEdit: false
            },
            views: {
                "content@app": {
                    controller: 'RegistrasiController as ctrl',
                    templateUrl: 'app/peserta/views/registrasi.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.peserta.historis', {
            url: '/peserta/historis',
            data: {
                title: 'Historis'
            },
            params: {
                tahun: ""
            },
            views: {
                "content@app": {
                    controller: 'HistorisController as ctrl',
                    templateUrl: 'app/peserta/views/historis.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

       
});
