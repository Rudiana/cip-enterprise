"use strict";


angular.module('app.pesertaenter', ['ui.router', 'datatables', 'datatables.bootstrap', 'ngHandsontable']);

angular.module('app.pesertaenter').config(function ($stateProvider) {

    $stateProvider
        .state('app.pesertaenter', {
            abstract: true,
            data: {
                title: 'Peserta Enterprise'
            }
        })

        .state('app.pesertaenter.registrasi', {
            url: '/peserta-enterprise/registrasi',
            data: {
                title: 'Registrasi'
            },
            params: {
                kode: "",
                tahun: "",
                allowEdit: false
            },
            views: {
                "content@app": {
                    controller: 'RegistrasiEnterController as ctrl',
                    templateUrl: 'app/peserta-enter/views/registrasi.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
       
});
