"use strict";


angular.module('app.adminqm', ['ui.router', 'datatables', 'datatables.bootstrap', 'datatables.fixedcolumns', 'datatables.fixedheader']);

angular.module('app.adminqm').config(function ($stateProvider) {

    $stateProvider
        .state('app.adminqm', {
            abstract: true,
            data: {
                title: 'Admin Unit'
            }
        })

        .state('app.adminqm.daftarpeserta', {
            url: '/admin-unit/daftar-peserta',
            data: {
                title: 'Daftar Peserta'
            },
            params: {
                statusRegistrasi: -1,
                tahun: ''
            },
            views: {
                "content@app": {
                    controller: 'PesertaCipController as ctrl',
                    templateUrl: 'app/admin-qm/views/daftar-peserta.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.adminqm.verifkasipeserta', {
           
            url: '/admin-unit/verifikasi-peserta',
            data: {
                title: 'Verifikasi Peserta'
            },
            views: {
                "content@app": {
                    controller: 'VerifikasiPesertaCIPController as ctrl',
                    templateUrl: 'app/admin-qm/views/verifikasi-peserta-cip.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.adminqm.setupdokumen', {
            
            url: '/admin-unit/pengaturan-kelengkapan-dokumen',
            data: {
                title: 'Pengaturan Kelengkapan Dokumen'
            },
            params: {
                'dokemen': 1
            },
            views: {
                "content@app": {
                    controller: 'SetupDokumenController as ctrl',
                    templateUrl: 'app/admin-qm/views/setup-dokumen.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.adminqm.auditorgugus', {
            
            url: '/admin-unit/auditor-gugus',
            data: {
                title: 'Pendaftaran Auditor ke Gugus'
            },
            params: {
                'dokemen': 1
            },
            views: {
                "content@app": {
                    controller: 'AuditorGugusController as ctrl',
                    templateUrl: 'app/admin-qm/views/auditor-gugus.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.adminqm.createstream', {
            
            url: '/admin-unit/createstream',
            data: {
                title: 'Buat Stream Unit'
            },
            views: {
                "content@app": {
                    controller: 'CreateStreamController as ctrl',
                    templateUrl: 'app/admin-qm/views/create-stream.html'
                }
            }
            ,resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.adminqm.createstreamDir', {
            url: '/admin-qm/createstreamdir',
            data: {
                title: 'Buat Stream Direktorat'
            },
            views: {
                "content@app": {
                    controller: 'CreateStreamDirController as ctrl',
                    
                    templateUrl: 'app/admin-qm/views/create-stream-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })


        .state('app.adminqm.createstreamEnter', {
            url: '/admin-qm/createstreamenter',
            data: {
                title: 'Buat Stream Enterprise'
            },
            views: {
                "content@app": {
                    controller: 'CreateStreamEnterController as ctrl',
              
                    templateUrl: 'app/admin-qm/views/create-stream-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.adminqm.streamgugus', {
           
            url: '/admin-unit/streamgugus',
            data: {
                title: 'Pendaftaran Gugus ke Master Stream'
            },
            views: {
                "content@app": {
                    controller: 'StreamGugusController as ctrl',
                    templateUrl: 'app/admin-qm/views/stream-gugus.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.streamgugusDir', {
            url: '/admin-qm/streamgugusdir',
            data: {
                title: 'Pendaftaran Gugus ke Master Stream'
            },
            views: {
                "content@app": {
                    controller: 'StreamGugusDirController as ctrl',
                    
                    templateUrl: 'app/admin-qm/views/stream-gugus-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.streamgugusEnter', {
            url: '/admin-enter/streamgugusenter',
            data: {
                title: 'Pendaftaran Gugus ke Master Stream'
            },
            views: {
                "content@app": {
                    controller: 'StreamGugusEnterController as ctrl',
                    
                    templateUrl: 'app/admin-qm/views/stream-gugus-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.streamjuri', {
            
            url: '/admin-unit/streamjuri',
            data: {
                title: 'Pendaftaran Juri ke Stream'
            },
            views: {
                "content@app": {
                    controller: 'StreamJuriController as ctrl',
                    templateUrl: 'app/admin-qm/views/stream-juri.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.streamjuriDir', {
            url: '/admin-dir/streamjuridir',
            data: {
                title: 'Pendaftaran Juri ke Stream'
            },
            views: {
                "content@app": {
                    controller: 'StreamJuriDirController as ctrl',
                    
                    templateUrl: 'app/admin-qm/views/stream-juri-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.streamjuriEnter', {
            url: '/admin-enter/streamjurienter',
            data: {
                title: 'Pendaftaran Juri ke Stream'
            },
            views: {
                "content@app": {
                    controller: 'StreamJuriEnterController as ctrl',
               
                    templateUrl: 'app/admin-qm/views/stream-juri-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.periodekoefisien', {
            
            url: '/admin-unit/pengaturan-koefisien',
            data: {
                title: 'Std. Deviasi & Koefisien'
            },
            views: {
                "content@app": {
                    controller: 'PeriodeKoefisienController as ctrl',
                    templateUrl: 'app/admin-qm/views/periode-koefisien.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.setpoinkriteria', {
            
            url: '/admin-unit/pengaturan-poin',
            data: {
                title: 'Pengaturan Periode Poin Kriteria'
            },
            views: {
                "content@app": {
                    controller: 'SetPoinKriteriaController as ctrl',
                    templateUrl: 'app/admin-qm/views/set-poin-kriteria.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminqm.assigndirektorat', {
            
            url: '/admin-unit/daftar-gugus-direktorat',
            data: {
            
                title: 'Daftarkan Gugus ke Direktorat'
            },
            views: {
                "content@app": {
                    controller: 'AssignDirektoratController as ctrl',
                    templateUrl: 'app/admin-qm/views/assign-direktorat.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })


        .state('app.adminqm.assignenterprise', {
            url: '/admin-unit/daftar-gugus-enterprise',
            data: {
                title: 'Daftarkan Gugus ke Enterprise'
            },
            views: {
                "content@app": {
                    controller: 'AssignEnterpriseController as ctrl',
                    templateUrl: 'app/admin-qm/views/assign-enterprise.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })


});
