'use strict';

angular.module('app.adminqm').controller('AuditorGugusController', function ($scope, $http, $state, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper ) {
    var me = $scope;
    var apiUser = BASE_API + "administrasiuser/";
    var apiRegistrasiCIP = BASE_API + "registrasicip/";
    var apiAudit = BASE_API + "audit/";

    var xRow = "";

    me.init = function (obj) {
        me.data = {};
        me.detail = {};
        me.detail.lstGugusSelected = [];
        me.filter = {};

        obj.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', {
                // Either you specify the AjaxDataProp here
                // dataSrc: 'data',
                url: apiUser + 'datatablesauditor',
                type: 'POST',
                accepts: "application/json",
                headers: authSvc.headers(),
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Pendaftaran Auditor ke Gugus", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            })
            .withOption('select', true)
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            //.withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withOption('rowCallback', me.rowCallback)
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        obj.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('Kode Auditor').notVisible(),
            DTColumnBuilder.newColumn('Nama').withTitle('Nama Auditor').withClass('text-primary')
        ];

        Select2Helper.GetDataForCombo(BASE_API + 'SetPoinKriteria/DropdownByTahun').then(function (result) {
            me.listPeriod = result.Dropdown;
        });
    };

    me.rowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            if (isNullOrEmpty(me.filter.period)) {
                NotifBoxWarning("Pendaftaran Auditor ke Gugus", "Pilih Periode terlebih dahulu.");
                return;
            }

            me.$apply(function () {
                me.data = aData;
                me.loadGugus(me.loadGugusSelected);

                $(xRow).removeClass('highlight');
                $(nRow).addClass('highlight');  
                xRow = nRow;
            });
        });

        return nRow;
    }

    me.Save = function () {
        var msgTitle = 'Simpan Data Auditor ke Gugus';

        if (me.detail.lstGugusSelected.length == 0) {
            NotifBoxWarning(msgTitle, "Pilih data Gugus");

            return;
        }

        var headers = [];
        angular.forEach(me.detail.lstGugusSelected, function (x) {
            var hdr = {
                CreateBy: me.data.Kode,
                KodeRegistrasi: x
            }

            headers.push(hdr);
        });
        
        $http.post(apiAudit + 'assignauditorgugus', headers)
        .success(function (result) {
            if (result.Success) {
                NotifBoxSuccess(msgTitle, result.Message);
            }
            else {
                NotifBoxWarning(msgTitle, result.Message);
            }
        })
        .error(function (error, status) {
            NotifBoxError(msgTitle, status + " - " + error);
        });
    };

    me.loadGugus = function (callback) {
        me.lstGugus = [];
        me.detail.lstGugusSelected = [];
        $http.post(apiRegistrasiCIP + 'dropdownforauditor', me.filter.period)
        .success(function (result) {
            me.lstGugus = result.Dropdown;
            if (callback != undefined) {
                callback();
            }
        })
        .error(function (error, status) {
            NotifBoxError2("Daftar Gugus/Peserta", error, status);
        });
    }

    me.loadGugusSelected = function () {
        if (!isNullOrEmpty(me.data.Kode)) {
            $http.get(apiAudit + 'duallist?kodeUser=' + me.data.Kode)
            .success(function (result) {
                me.detail.lstGugusSelected = result;
            })
            .error(function (error, status) {
                NotifBoxError2("Pendaftaran Auditor ke Gugus", error, status);
            });
        }
    }

    me.onChangePeriode = function () {
        me.loadGugus(me.loadGugusSelected);
    }

    me.init(this);
});