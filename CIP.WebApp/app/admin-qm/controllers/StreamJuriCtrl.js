'use strict';

angular.module('app.adminqm').controller('StreamJuriController', function ($scope, $http, $state, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper) {
    var me = $scope;
    var module = 'StreamJuri/';
    var api = BASE_API + module;

    me.init = function (obj) {
        me.data = {};
        me.isShowList = true;
        me.isNew = false;

        me.Undo();
        me.user = authSvc.authenticationData;
        var headers = authSvc.headers();
        headers.Accept = "application/json";

        obj.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', {
                // Either you specify the AjaxDataProp here
                // dataSrc: 'data',
                url: api + 'datatables',
                type: 'POST',
                accepts: "application/json",
                headers: headers,
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Data Stream", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            })
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)
            //.withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withOption('rowCallback', me.rowCallback)
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        obj.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('Kode').withClass('text-primary').notVisible(),
            DTColumnBuilder.newColumn('Nama').withTitle('Nama Stream'),
            DTColumnBuilder.newColumn('Tahun').withTitle('Periode'),
            DTColumnBuilder.newColumn('StatusStream').withTitle('Status Stream'),
            DTColumnBuilder.newColumn('KodeJuri').withTitle('Juri'),
            DTColumnBuilder.newColumn('KodeStream').withTitle('').notVisible(),
            DTColumnBuilder.newColumn('CreatedBy').withTitle('').notVisible(),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('').notVisible(),

        ];
    };

    me.rowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            me.$apply(function () {
                me.EditData(aData);
            });
        });
        return nRow;
    }

    me.ShowList = function () {
        me.isShowList = true;
        me.isNew = false;
    };

    me.NewData = function () {
        me.isShowList = false;
        me.Undo();
    };

    me.EditData = function (data) {
        me.isShowList = false;
        me.isNew = false;
        me.data = data;
    }

    me.Undo = function () {
        me.data = {};
        me.isNew = true;
        me.LoadCombo(); 
    };
    
    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'SetPoinKriteria/DropdownByTahun').then(function (result) {
            me.listTahun = result.Dropdown;
        });
    };

    me.Save = function () {
        if (this.frmStreamJuri.$valid) {
            var msgTitle = 'Simpan juri ke stream';
            if (me.isNew) {
                $http.post(api, me.data)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            me.ShowList();
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                         NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
            else {
                msgTitle = 'Update juri ke stream';
                //console.log(me.data);
                $http.put(api, me.data)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            me.ShowList();
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                         NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
        }
    };

    me.Delete = function () {
        var msgTitle = 'Hapus juri';
        if (!me.isNew) {
            var kode = me.data.Kode;
            var msgContent = msgTitle + ": \"" + me.data.KodeJuri + "\" ?";
            var data = [kode];
            $.SmartMessageBox({
                    title: msgTitle,
                    content: msgContent,
                buttons: '[OK][Batal]',
                theme: 'bg-warning'
            },function (action) {
                if (action === "OK") {
                    $http.delete(api, { data })
                        .success(function (result) {
                            if (result.Success) {
                                NotifBoxSuccess(msgTitle, result.Message);
                                me.ShowList();
                            }
                            else {
                                NotifBoxWarning(msgTitle, result.Message);
                            }
                        })
                        .error(function (error, status) {
                             NotifBoxError(msgTitle, status + " - " + error.Message);
                        });
                }
                if (action === "Batal") {
                    return;
                }
            });
        }
    };

     me.changeTahun = function () {
        msgTitle = "Load data Stream Gugus";
        $http.post(BASE_API + 'streamgugus/dropdownbytahunforstreamjuri', me.data.Tahun)
            .success(function (result) {
                if (result.Success) {
                    me.listStream = Select2Helper.GetArrayObjectForCombo(result.Dropdown);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

     me.changeStream = function () {
         Select2Helper.GetDataForCombo(BASE_API + 'administrasiuser/dropdownrolejuri?kodeStream=' + me.data.KodeStream).then(function (result) {
             me.listKodeJuri = result.Dropdown;
         });
     }

    me.init(this);
});