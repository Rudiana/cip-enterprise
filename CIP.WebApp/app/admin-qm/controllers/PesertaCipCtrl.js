'use strict';

angular.module('app.adminqm').controller('PesertaCipController', function ($scope, $http, $state, $stateParams, $filter, $window, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper, $uibModal) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;
    var msgTitle = "Data Peserta";

    me.init = function (obj) {
        me.vm = obj;

        me.isShowList = true;
        me.isNew = false;
        me.allowUpdate = true;
        me.data = {};
        me.filter = {};
        me.Undo();

        me.LoadCombo();

        me.filter.tahun = new Date().getFullYear();

        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('height', 200)
            .withOption('colReorder', true)
            .withOption('order', [[13, 'asc'], [1, 'asc']])

            .withFixedColumns({
                leftColumns: 3,
                rightColumns: 1
            })

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('No. Gugus').withClass('text-primary').withOption('width', '80%'),
            DTColumnBuilder.newColumn('JenisCIPDes').withTitle('Jenis'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn('Email').withTitle('Email'),
            DTColumnBuilder.newColumn('DirektoratDes').withTitle('Direktorat'),
            DTColumnBuilder.newColumn('UnitDes').withTitle('Unit'),
            DTColumnBuilder.newColumn('SubUnitDes').withTitle('Sub Unit'),
            DTColumnBuilder.newColumn('Fasilitator').withTitle('Fasilitator'),
            DTColumnBuilder.newColumn('Ketua').withTitle('Ketua'),
            DTColumnBuilder.newColumn('JumlahAnggota').withTitle('Jumlah Anggota').withClass('text-center'),
            DTColumnBuilder.newColumn('Status').withTitle('Status'),
            DTColumnBuilder.newColumn('Keterangan').withTitle('Keterangan').withOption('width', '1000px'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Tgl. Registrasi').renderWith(function (data, type, full, meta) {
                return DateTimeFormat(data);
            }),
            DTColumnBuilder.newColumn('IsAktif').withTitle('Aktif').withClass('text-center').renderWith(function (data, type, full, meta) {
                return data ? '<span class="text-success">YA</span>' : '<span class="text-danger">TIDAK</span>';
            }),
            DTColumnBuilder.newColumn(null).withTitle('#').withClass('text-center').notSortable().renderWith(actionsHtml)
        ];

        me.vm.dtInstance = {};
    };

    function actionsHtml(data, type, full, meta) {
        var dat = data.Kode;
        //var dataAnggota = data.Anggota;
        var isAktif = data.IsAktif ? false : true;
        var txtAktif = isAktif ? "Set Aktif" : "Set Non Aktif";

        //var btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().EditData(\'' + dat + '\')"><i class="fa fa-edit"></i> Edit Detail</button>&nbsp;';
        //btn = btn + '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().ViewData(\'' + dat + '\')"><i class="fa fa-search"></i> View Detail</button>&nbsp;';
        //btn = btn + '<button id class="btn btn-xs btn-danger" style="min-width: 100px;" onclick="angular.element(this).scope().SetActive(\'' + dat + '\',\'' + isAktif + '\')"><i class="fa fa-warning"></i> ' + txtAktif + '</button>&nbsp;';
        //btn = btn + '<button class="btn btn-primary btn-xs" onclick="angular.element(this).scope().downloadRisalah(\'' + dat + '\')"><i class="fa fa-cloud-download"></i> Unduh Risalah</button>';

        var btn = '<div class="btn-group dropdown" data-dropdown>' +
            '<div style="width: 140px; text-align: center !importtant;">' +
            '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" style="width: 90px;">' +
            ' Aksi ' +
            '<i class="fa fa-caret-down"></i>' +
            '</button > ' +
            '<ul class="dropdown-menu" style="left: -11px !important;">' +
            '<li>' +
            '<a onclick="angular.element(this).scope().EditData(\'' + dat + '\')"><i class="fa fa-edit text-warning"></i> Edit Detail</a>' +
            '</li>' +
            '<li>' +
            '<a onclick="angular.element(this).scope().ViewData(\'' + dat + '\')"><i class="fa fa-search text-success"></i> Lihat Detail</a>' +
            '</li>' +
            '<li>' +
            '<a onclick="angular.element(this).scope().SetActive(\'' + dat + '\',\'' + isAktif + '\')"><i class="fa fa-warning text-danger"></i> ' + txtAktif + '</a>' +
            '</li>' +
            '<li class="divider"></li>' +
            '<li>' +
            '<a onclick="angular.element(this).scope().downloadRisalah(\'' + dat + '\')"><i class="fa fa-cloud-download text-primary"></i> Unduh Risalah</a>' +
            '</li>' +
            '</ul>' +
            '</div>';

        return btn;
    };

    me.Undo = function () {
        me.data = {};
        me.isNew = true;
    };

    me.ShowList = function () {
        me.isShowList = true;
        me.isNew = false;
    };

    me.NewData = function () {
        me.isShowList = false;
        me.Undo();
    };

    me.EditData = function (kode) {
          $state.go('app.peserta.registrasi', { kode: kode, tahun: me.filter.Tahun, allowEdit: true });
    };

    me.dataSource = function () {
        return {
            url: api + 'datatablesbyperiode?periode=' + me.filter.tahun,
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.reloadData = function () {
        me.vm.tableOptions.withOption('ajax', me.dataSource());
    };

    me.loadData = function (kode, dataAnggota) {
        msgTitle = "Load Data Peserta";

        $http.get(api + "dynamicdata?term=" + kode)
            .success(function (result) {
                if (result.Success) {
                    me.data = result.Data;
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;

            setTimeout(function () {
                var tahun = isNullOrEmpty($stateParams.tahun) ? new Date().getFullYear() : $stateParams.tahun;
                var tahun = $filter('filter')(me.listTahun, { id: tahun }, false);
                if (tahun.length > 0) {
                    tahun[0].obj = angular.copy(tahun[0]);
                    $('#FilterTahun').select2('data', tahun[0]);
                }
            }, 2000);

        });
    };

    me.ViewData = function (kode) {
        /* $uibModal.open({
             templateUrl: 'frmModal.html',
             backdrop: true,
             windowClass: 'modal',
             keyboard: true,
             size: "100-pct",
             controller: function ($scope, $uibModalInstance) {
                 var loc = window.location;
                 var url = loc.origin + loc.pathname + "/#/profil-anggota?kode=" + kode;
                 console.log(url);
                 $scope.modal = {
                     url: url,
                     kode: kode
                 };
                 $scope.closeZoom = function () {
                     $uibModalInstance.dismiss('cancel');
                 };
             },
             resolve: {
                 dataModal: function () {
                     return $scope.modal;
                 }
             }
         });*/

        $state.go('app.peserta.registrasi', { kode: kode, tahun: me.filter.Tahun, allowEdit: false });
    };

    me.exportExcel = function () {
        $http.get(api + 'GeneratePeserataCIPExcel?periode=' + me.filter.tahun, { responseType: 'arraybuffer' }).success(function (response) {
            if (response !== null) {
                if (response.Message === undefined) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Daftar_Peserta_CIP_' + me.filter.tahun + '.xlsx');
                }
                else {
                    NotifBoxError("Export " + msgTitle, response.Message || "");
                }
            }
        }).error(function (err, status) {
            NotifBoxError(msgTitle, err.Message);
        });
    };

    me.downloadUploadDokumenStatus = function () {
        $http.get(api + 'GenerateUploadDokumenStatusExcel?tahun=' + me.filter.tahun, { responseType: 'arraybuffer' }).success(function (response) {
            if (response !== null) {
                if (response.Message === undefined) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Upload_Dokumen_' + me.filter.tahun + '.xlsx');
                }
                else {
                    NotifBoxError("Export " + msgTitle, response.Message || "");
                }
            }
        }).error(function (err, status) {
            NotifBoxError(msgTitle, err.Message);
        });
    };

    me.Save = function () {
        if (this.frmDaftarPeserta.$valid) {
            var arrAnggota = me.data.Anggota.split(',');
            var objAnggota = [];
            angular.forEach(arrAnggota, function (k) {
                var obj = {
                    Nama: k
                };
                objAnggota.push(obj);
            });

            var fd = new FormData();
            fd.append("model", JSON.stringify(me.data));
            fd.append("anggotaCIP", JSON.stringify(objAnggota));
            fd.append("isNew", JSON.stringify(me.isNew));

            var msgTitle = 'Simpan data Registrasi';
            $http.post(api + "register", fd, {
                withCredentials: false,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
                .success(function (result) {
                    if (result.Success) {
                        NotifBoxSuccess(msgTitle, result.Message);
                        me.ShowList();
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });
        }
    };

    me.SetActive = function (kode, isAktif) {
        var content = isAktif ? "Non Aktifkan " : " Aktifkan ";
        $.SmartMessageBox({
            title: "Aktif/Non Aktif Gugus",
            content: "Yakin " + content + "data Gugus tersebut?",
            buttons: '[OK][Batal]',
            theme: 'bg-warning'
        },
            function (action) {
                if (action === "OK") {
                    $http.post(api + 'setactive?kode=' + kode + '&isAktif=' + isAktif).success(function (response) {
                        if (response.Success) {
                            me.vm.dtInstance.reloadData(function () { }, true);
                            NotifBoxSuccess(msgTitle, response.Message);
                        }
                        else {
                            NotifBoxError(msgTitle, response.Message);
                        }
                    }).error(function (err, status) {
                        NotifBoxError(msgTitle, err.Message);
                    });
                    return;
                }
                else {
                    return;
                }
            });
    };

    me.downloadRisalah = function (kode) {
        $http.get(api + "downloadrisalah?kode=" + kode)
            .success(function (result) {
                if (result.Success) {
                    $window.open(BASE_API.replace('/api', '') + 'downloads/' + result.Data, '_blank');
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.$watch('data.Anggota', function (n, o) {
        var count = 0;
        if (n != undefined || n != null) {
            count = n.split(',').length;
            me.data.JumlahAnggota = count;
        }
        else {
            me.data.JumlahAnggota = 0;
            me.disabledAnggota = false;
        }
    });

    me.init(this);
});