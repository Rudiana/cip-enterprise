'use strict';

angular.module('app.dashboard').controller('DashboardController', function ($scope, $http, $interval, $state, authSvc, $filter, DTOptionsBuilder, DTColumnBuilder, Language, Select2Helper, BASE_API) {
    var msgTitle = "Dashboard";
    var me = $scope;
    var module = 'Dashboard/';
    var api = BASE_API + module;

    me.init = function (obj) {
        me.userProfile = {};
        me.filter = {};
        me.filter.Tahun = new Date().getFullYear().toString();

        me.dataCountApproval = 0;
        me.dataCountPenjurian = 0;
        me.dataCountAudit = 0;
        me.dataCountNewReg = 0;
        me.dataCountWinner = 0;

        me.vm = obj;
        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)
            .withPaginationType('full_numbers')

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withFixedColumns({
                leftColumns: 2,
                //rightColumns: 1
            })

            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")

            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Tahun').withTitle('Tahun').withClass('text-center'),
            DTColumnBuilder.newColumn('Kode').withTitle('No. Gugus').withClass('text-left'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus').withClass('text-left'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP').withClass('text-left')
        ];

        me.vm.dtInstance = {};

        me.loadDropdown();

    };

    me.loadDropdown = function () {
        $http.get(api + 'getuserprofile').success(function (response) {
            if (response.Success) {
                me.userProfile = response.Data;
            }
        }).error(function (err, status) {
            console.log(err);
        });

        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;
            setTimeout(function () {
                var data = $filter('filter')(result.Dropdown, { id: me.filter.Tahun }, false);
                if (data.length > 0) {
                    $('#Tahun').select2('data', data[0]);

                    me.filter.TahunDes = $('#Tahun').select2('data').text;
                }
            }, 1000);

        });

        Select2Helper.GetDataForCombo(BASE_API + 'masterdirektorat/dropdownbyuser').then(function (result) {
            me.listDirektorat = result.Dropdown;

            if (me.listDirektorat.length === 1) {
                me.filter.Direktorat = me.listDirektorat[0].value;
                setTimeout(function () {
                    me.listDirektorat[0].obj = angular.copy(me.listDirektorat[0]);
                    $('#Direktorat').select2('data', me.listDirektorat[0]);

                    me.submitFilter();
                }, 2000);
            }
            else {
                if (me.listDirektorat.length > 0) {
                    me.filter.Direktorat = me.userProfile.Direktorat;
                    setTimeout(function () {
                        var listCurrentDirektorat = [{
                            id: me.userProfile.Direktorat,
                            value: me.userProfile.Direktorat,
                            text: me.userProfile.DirektoratDes
                        }];

                        $('#Direktorat').select2('data', listCurrentDirektorat[0]);

                        me.submitFilter();
                    }, 2000);
                }
                else {
                    me.filter.Direktorat = '';
                }
            }

        });
    };

    me.dataSource = function () {
        return {
            url: api + 'datatablesbystatus?periode=' + me.filter.Tahun + '&unit=' + me.filter.Unit + '&status=' + me.status,
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.reloadData = function (status) {
        me.status = status;
        me.vm.tableOptions.withOption('ajax', me.dataSource());
        switch (status) {
            case "new":
                me.jumlahGugus = me.dataCountNewReg;
                break;
            case "approval":
                me.jumlahGugus = me.dataCountApproval;
                break;
            case "audit":
                me.jumlahGugus = me.dataCountAudit;
                break;
            case "penjurian":
                me.jumlahGugus = me.dataCountPenjurian;
                break;
            case "winner":
                me.jumlahGugus = me.dataCountWinner;
                break;
            default:
                break;
        }
    };

    function countApproval() {
        $http.get(api + 'GetCountApproval?periode=' + me.filter.Tahun + '&unit=' + me.filter.Unit).success(function (response) {
            me.dataCountApproval = response;

        }).error(function (err, status) {
            console.log(err);
        });
    }

    function countAudit() {
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        $http.get(api + 'GetCountAudit?periode=' + me.filter.Tahun + '&unit=' + me.filter.Unit).success(function (response) {
            me.dataCountAudit = response;

        }).error(function (err, status) {
            console.log(err);
        });
    }

    function countNewReg() {
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        $http.get(api + 'GetCountNewReg?periode=' + me.filter.Tahun + '&unit=' + me.filter.Unit).success(function (response) {
            me.dataCountNewReg = response;

        }).error(function (err, status) {
            console.log(err);
        });
    }

    function countPenjurian() {
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        $http.get(api + 'GetCountPenjurian?periode=' + me.filter.Tahun + '&unit=' + me.filter.Unit).success(function (response) {
            me.dataCountPenjurian = response;

        }).error(function (err, status) {
            console.log(err);
        });
    }

    me.submitFilter = function () {
        countApproval();
        countPenjurian();
        //countAudit();
        countNewReg();

        me.closeFilter();

        if (!isNullOrEmpty($('#Tahun').select2('data'))) {
            me.filter.TahunDes = $('#Tahun').select2('data').text;
        }
        if (!isNullOrEmpty($('#Direktorat').select2('data'))) {
            me.filter.DirektiratDes = $('#Direktorat').select2('data').text;
        }
        if (!isNullOrEmpty($('#Unit').select2('data'))) {
            me.filter.UnitDes = $('#Unit').select2('data').text;
        }
    };

    $("#unfolding").modalAnimate();

    $(document).off('click', '.button');
    $(document).on('click', '.button', function (event) {
        //event.preventDefault();
        var btnEffect = $(this).data('effect');

        $(this).modalAnimate({
            modalTarget: 'modal-container',
            effect: $(this).data('effect'),
            autoEffect: true
        });

        var status = $(this)[0].id;
        angular.element(this).scope().reloadData(status);
    });

    me.$watch('filter.Direktorat', function (n, o) {
        if (!isNullorEmpty(n)) {
            Select2Helper.GetDataForCombo(BASE_API + 'masterunit/dropdownbydirektorat?direktorat=' + n).then(function (result) {
                me.listUnit = result.Dropdown;
                if (me.listUnit.length === 1) {
                    me.filter.Unit = me.listUnit[0].value;
                    setTimeout(function () {
                        me.listUnit[0].obj = angular.copy(me.listUnit[0]);
                        $('#Unit').select2('data', me.listUnit[0]);
                        me.filter.UnitDes = $('#Unit').select2('data').text;
                    }, 3000);
                }
                else {
                    if (me.listUnit.length > 0) {
                        me.filter.Unit = me.userProfile.Unit;
                        setTimeout(function () {
                            var listCurrentUnit = [{
                                id: me.userProfile.Unit,
                                value: me.userProfile.Unit,
                                text: me.userProfile.UnitDes
                            }];

                            $('#Unit').select2('data', listCurrentUnit[0]);
                            me.filter.UnitDes = $('#Unit').select2('data').text;
                        }, 2000);
                    }
                    else {
                        me.filter.Unit = ''
                    }
                }
            });
        }
        else {
            me.listUnit = [];
            me.filter.Unit = '';
        }
    }, true);

    me.closeFilter = function () {
        $('.filter-form').fadeOut(150);
        $('#form-filter').removeClass('active');
    }

    me.init(this);
});