"use strict";

angular.module('app').directive('filterFormToggle', function($log) {

	var link = function($scope,$element, attrs){
		var toggle_form = null;

        $element.on('click', function () {
                var pos = $element.position();
            var t = pos.top;
            var l = pos.left;

            toggle_form = $(this).next('.filter-form');

            t = t + 35;
            toggle_form.css({ top: t + 'px' });

			if (!toggle_form.is(':visible')) {

				toggle_form.fadeIn(150);

				$(this).addClass('active');

			}
			 else {
				
				toggle_form.fadeOut(150);
				
				$(this).removeClass('active');

			}

		})

		$(document).mouseup(function(e) {
			//if (toggle_form && !toggle_form.is(e.target) && toggle_form.has(e.target).length === 0) {
			//	toggle_form.fadeOut(150);
			//	$element.removeClass('active');
			//}
		});
	}
	
	return{
		restrict:'EA',
		link:link
	}
});