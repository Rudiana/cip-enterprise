"use strict";


angular.module('app.adminenter', ['ui.router', 'datatables', 'datatables.bootstrap', 'datatables.fixedcolumns', 'datatables.fixedheader']);

angular.module('app.adminenter').config(function ($stateProvider) {

    $stateProvider
        .state('app.adminenter', {
            abstract: true,
            data: {
                title: 'Admin Enterprise'
            }
        })

        .state('app.adminenter.daftarpeserta', {
            url: '/admin-enterprise/daftar-peserta',
            data: {
                title: 'Daftar Peserta'
            },
            params: {
                statusRegistrasi: -1,
                tahun: ''
            },
            views: {
                "content@app": {
                    controller: 'PesertaCipEnterController as ctrl',
                    templateUrl: 'app/admin-enter/views/daftar-peserta.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.adminenter.verifkasipeserta', {
            url: '/admin-enterprise/verifikasi-peserta',
            data: {
                title: 'Verifikasi Peserta & Submit ke Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'VerifikasiPesertaCIPEnterController as ctrl',
                    templateUrl: 'app/admin-enter/views/verifikasi-peserta-cip.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.adminenter.setpoinkriteria', {

            url: '/admin-enterprise/pengaturan-poin-enterprise',
            data: {
                title: 'Pengaturan Periode Poin Kriteria'
            },
            views: {
                "content@app": {
                    controller: 'SetPoinKriteriaEnterController as ctrl',
                    templateUrl: 'app/admin-enter/views/set-poin-kriteria-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

 });
