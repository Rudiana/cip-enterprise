'use strict';

angular.module('app.adminenter').controller('PesertaCipEnterController', function ($scope, $http, $state, $stateParams, $filter, $window, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper, $uibModal) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;
    var msgTitle = "Data Peserta";

    me.init = function (obj) {
        me.vm = obj;
        me.filter = {};
        me.LoadCombo();
        me.filter.tahun = new Date().getFullYear();

        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('height', 200)
            .withOption('colReorder', true)
            .withOption('order', [[16, 'asc'], [4, 'asc']])

            .withFixedColumns({
                leftColumns: 4,
                rightColumns: 1
            })

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('Kode').notVisible(),
            DTColumnBuilder.newColumn('KodeRegistrasi').withTitle('No. Gugus Unit').withClass('text-primary').withOption('width', '80%').notVisible(),
            DTColumnBuilder.newColumn('CipDirektorat').withTitle('No. Gugus Direktorat').withClass('text-primary').withOption('width', '80%').notVisible(),
            DTColumnBuilder.newColumn('CipEnterprise').withTitle('No. Gugus').withClass('text-primary').withOption('width', '80%'),
            DTColumnBuilder.newColumn('JenisCIPDes').withTitle('Jenis'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn('Email').withTitle('Email'),
            DTColumnBuilder.newColumn('DirektoratDes').withTitle('Direktorat'),
            DTColumnBuilder.newColumn('UnitDes').withTitle('Unit'),
            DTColumnBuilder.newColumn('SubUnitDes').withTitle('Sub Unit'),
            DTColumnBuilder.newColumn('Fasilitator').withTitle('Fasilitator'),
            DTColumnBuilder.newColumn('Ketua').withTitle('Ketua'),
            DTColumnBuilder.newColumn('JumlahAnggota').withTitle('Jumlah Anggota').withClass('text-center'),
            DTColumnBuilder.newColumn('Status').withTitle('Status'),
            DTColumnBuilder.newColumn('Keterangan').withTitle('Keterangan').withOption('width', '1000px'),
            DTColumnBuilder.newColumn('CreatedDate').withTitle('Tgl. Registrasi').renderWith(function (data, type, full, meta) {
                return DateTimeFormat(data);
            }),
            DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').notSortable().renderWith(actionsHtml)
        ];

        me.vm.dtInstance = {};
    };

    function actionsHtml(data, type, full, meta) {
        var no = data.CipEnterprise;
        var kode = data.Kode;

        var btn = '<div class="btn-group dropdown" data-dropdown>' +
            '<div style="width: 140px; text-align: center !importtant;">' +
            '<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" style="width: 90px;">' +
            ' Aksi ' +
            '<i class="fa fa-caret-down"></i>' +
            '</button > ' +
            '<ul class="dropdown-menu" style="left: -11px !important;">' +
            '<li>' +
            '<a onclick="angular.element(this).scope().EditData(\'' + no + '\')"><i class="fa fa-edit text-warning"></i> Edit Detail</a>' +
            '</li>' +
            '<li>' +
            '<a onclick="angular.element(this).scope().ViewData(\'' + no + '\')"><i class="fa fa-search text-success"></i> Lihat Detail</a>' +
            '</li>' +
            '<li class="divider"></li>' +
            '<li>' +
            '<a onclick="angular.element(this).scope().downloadRisalah(\'' + kode + '\')"><i class="fa fa-cloud-download text-success"></i> Unduh Risalah</a>' +
            '</li>' +
            '</ul>' +
            '</div>';

        return btn;
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;

            setTimeout(function () {
                var tahun = isNullOrEmpty($stateParams.tahun) ? new Date().getFullYear() : $stateParams.tahun;
                var tahun = $filter('filter')(me.listTahun, { id: tahun }, false);
                if (tahun.length > 0) {
                    tahun[0].obj = angular.copy(tahun[0]);
                    $('#FilterTahun').select2('data', tahun[0]);
                }
            }, 2000);

        });
    };

    me.dataSource = function () {
        return {
            url: api + 'datatablesbyperiodeEnter?periode=' + me.filter.tahun,
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, "Loading Daftar Peserta", xhr.status, $state, authSvc);
            }
        };
    };

    me.reloadData = function () {
        me.vm.dtInstance.reloadData(function () { }, true);
    };

    me.EditData = function (kode) {
          $state.go('app.pesertaenter.registrasi', { kode: kode, tahun: me.filter.Tahun, allowEdit: true });
    };

    me.ViewData = function (kode) {
        $state.go('app.pesertaenter.registrasi', { kode: kode, tahun: me.filter.Tahun, allowEdit: false });
    };

    me.exportExcel = function () {
        $http.get(api + 'GeneratePeserataCIPExcelEnter?periode=' + me.filter.tahun, { responseType: 'arraybuffer' }).success(function (response) {
            if (response !== null) {
                if (response.Message === undefined) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Daftar_Peserta_CIP_Enter' + me.filter.tahun + '.xlsx');
                }
                else {
                    NotifBoxError("Export " + msgTitle, response.Message || "");
                }
            }
        }).error(function (err, status) {
            NotifBoxError(msgTitle, err.Message);
        });
    };

    me.downloadUploadDokumenStatus = function () {
        $http.get(api + 'GenerateUploadDokumenStatusExcelEnter?tahun=' + me.filter.tahun, { responseType: 'arraybuffer' }).success(function (response) {
            if (response !== null) {
                if (response.Message === undefined) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Upload_Dokumen_Enter_' + me.filter.tahun + '.xlsx');
                }
                else {
                    NotifBoxError("Export " + msgTitle, response.Message || "");
                }
            }
        }).error(function (err, status) {
            NotifBoxError(msgTitle, err.Message);
        });
    };

    me.downloadRisalah = function (kode) {
        $http.get(api + "downloadrisalahEnter?kode=" + kode)
            .success(function (result) {
                if (result.Success) {
                    $window.open(BASE_API.replace('/api', '') + 'downloads/' + result.Data, '_blank');
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.init(this);
});