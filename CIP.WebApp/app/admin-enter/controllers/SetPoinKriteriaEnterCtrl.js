'use strict';

angular.module('app.adminqm').controller('SetPoinKriteriaEnterController', function ($scope, $http, $state, authSvc, Select2Helper, BASE_API, Language) {
    var me = $scope;
    var module = 'SetPoinKriteriaEnter/';
    var api = BASE_API + module;

    me.init = function (obj) {
       
        me.data = {};
        me.isNew = false;
        me.listPeriode = [];
        me.periode = new Date().getFullYear();
        me.user = authSvc.authenticationData;
        me.LoopRowSpan = {};
        me.LoadCombo();
        

    }

    
    function mapRowSpan(data) {
        var finalLoopData = {};
        angular.forEach(data, function (value, key) {
            if (!finalLoopData[value.KodeLangkah]) {
                finalLoopData[value.KodeLangkah] = new Array();
            }
            finalLoopData[value.KodeLangkah].push(value);
        });
        me.LoopRowSpan = finalLoopData;

        //console.log(me.LoopRowSpan);
    }

    

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(api + 'DropdownByTahun').then(function (result) {
            me.listPeriode = result.Dropdown;
        });
    };

    me.Save = function () {
        var msgTitle = 'Pengaturan Poin Kriteria & Langkah';
        $http.post(api + 'CreateRange/' + me.periode,me.data)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    getLangkahLangkah(me.periode);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    };

    me.Load = function () {
        if (me.periode === 0 || me.periode === null) {
            NotifBoxWarning('Information', 'Silahkan pilih periode');
            return;
        }
        else {
            getLangkahLangkah(me.periode);
        }
    };

    me.Copy = function () {
        if (me.periode === 0 || me.periode === null) {
            NotifBoxWarning('Information', 'Silahkan pilih periode');
            return;
        }
        else {
            copyCreate(me.periode);
        }
    };

    function getLangkahLangkah(periode) {
        $http.post(api + 'GetLangkahLangkah?periode=' + periode).success(function (response) {

            if (response !== null) {
                me.data = response;
                mapRowSpan(me.data.LangkahLangkah);
            }

        }).error(function (err, status) {
            NotifBoxError('Warning', err.Message);
        });
    }

    function copyCreate(periode) {
        $http.post(api + 'CopyCreateRange?periode=' + periode).success(function (response) {

            if (response !== null) {
                var success = response.Success;
                if (success) {
                    getLangkahLangkah(periode);
                }
            }

        }).error(function (err, status) {
            NotifBoxError('Warning', err.Message);
        });
    }

    me.init(this);
});