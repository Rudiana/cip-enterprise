"use strict";


angular.module('app.pesertadir', ['ui.router', 'datatables', 'datatables.bootstrap', 'ngHandsontable']);

angular.module('app.pesertadir').config(function ($stateProvider) {

    $stateProvider
        .state('app.pesertadir', {
            abstract: true,
            data: {
                title: 'Peserta Direktorat'
            }
        })

        .state('app.pesertadir.registrasi', {
            url: '/peserta-direktorat/registrasi',
            data: {
                title: 'Registrasi'
            },
            params: {
                kode: "",
                tahun: "",
                allowEdit: false
            },
            views: {
                "content@app": {
                    controller: 'RegistrasiDirController as ctrl',
                    templateUrl: 'app/peserta-dir/views/registrasi.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
       
});
