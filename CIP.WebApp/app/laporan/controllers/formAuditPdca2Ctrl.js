'use strict';

angular.module('app.laporan').controller('formAuditPdca2Controller', function ($scope, $http, $state, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'Audit/';
    var api = BASE_API + module;
    var msgTitle = "Laporan Audit PDCA II";

    me.init = function (obj) {
        me.data = {};
        me.isShowList = true;
        me.user = authSvc.authenticationData;

        me.tahun = 1900;

        me.vm = obj;
        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)
            .withPaginationType('full_numbers')

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withFixedColumns({
                leftColumns: 2,
                rightColumns: 1
            })

            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")

            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Tahun').withTitle('Tahun'),
            DTColumnBuilder.newColumn('KodeRegistrasi').withTitle('Kode Registrasi'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').notSortable()
                .renderWith(actionsHtml)
        ];

        me.vm.dtInstance = {};

        me.LoadComboTahun();
    };

    function actionsHtml(data, type, full, meta) {
        var btn = "";
        btn = '<button class="btn btn-xs btn-success" onclick="angular.element(this).scope().excelGeneratePdca(\'' + data.Tahun + '\',\'' + data.KodeRegistrasi + '\')"><i class="fa fa-file-excel-o"></i> Download Form PDCA II</button>';

        return btn;
    };

    me.LoadComboTahun = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'StreamJuri/DropdownTahun').then(function (result) {
            me.listTahun = result.Dropdown;
        });
    };

    me.excelGeneratePdca = function (tahun, kodeRegistrasi) {
        if (me.checkPeriod()) {
            $http.get(api + 'GetPdca2Report?Periode=' + tahun + '&KodeRegistrasi=' + kodeRegistrasi, { responseType: 'arraybuffer' }).success(function (response) {
                if (response !== null) {

                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Form_Audit_PDCA_II_' + kodeRegistrasi + '.xlsx');
                }
            }).error(function (err, status) {
                NotifBoxError(msgTitle, err.Message);
            });
        }
    };

    me.excelGeneratePdcaList = function () {
        if (me.checkPeriod()) {
            $http.get(api + 'GetPdcaList2Report?Periode=' + me.tahun, { responseType: 'arraybuffer' }).success(function (response) {
                if (response !== null) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Audit_PDCA_II_List_' + me.tahun + '.xlsx');
                }
            }).error(function (err, status) {
                NotifBoxError(msgTitle, err.Message);
            });
        }
    };

    me.dataSource = function () {
        if (!me.checkPeriod()) {
            me.tahun = 1900;
        }

        return {
            url: api + 'PdcaList?Periode=' + me.tahun + '&status=pdca2',
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.checkPeriod = function () {
        if (isNullOrEmpty(me.tahun)) {
            NotifBoxWarning(msgTitle, "Silahkan Pilih Periode terlebih dahulu.");

            return false;
        }
        else {
            return true;
        }
    };

    me.find = function () {
        me.vm.tableOptions.withOption('ajax', me.dataSource());
    };

    me.init(this);
});