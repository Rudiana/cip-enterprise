"use strict";


angular.module('app.laporan', ['ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.laporan').config(function ($stateProvider) {

    $stateProvider
        .state('app.laporan', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.laporan.auditpdca1', {
            url: '/formAuditPdca1',
            data: {
                title: 'Laporan Audit PDCA I '
            },
            views: {
                "content@app": {
                    controller: 'formAuditPdca1Controller as ctrl',
                    templateUrl: 'app/laporan/views/formAuditPdca1.html'
                }
            },
            resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.laporan.auditpdca2', {
            url: '/formAuditPdca2',
            data: {
                title: 'Laporan Audit PDCA II'
            },
            views: {
                "content@app": {
                    controller: 'formAuditPdca2Controller as ctrl',
                    templateUrl: 'app/laporan/views/formAuditPdca2.html'
                }
            }
                , resolve: {
                    srcipts: function (lazyScript) {
                        return lazyScript.register([
                            'build/vendor.ui.js'
                        ]);

                    }
                }
        })
        
});
