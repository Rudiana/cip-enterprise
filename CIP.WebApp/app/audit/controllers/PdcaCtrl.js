'use strict';

angular.module('app.audit').controller('PdcaController', function ($scope, $http, $state, $filter, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, $window) {
    var me = $scope;
    var module = 'audit/';
    var api = BASE_API + module;

    me.init = function (obj) {
        me.vm = obj;
        me.data = {};
        me.filter = {};
        me.filter.tahun = new Date().getFullYear();

        me.isShowList = true;
        me.isNew = false;
        me.allowUpdate = true;

        me.LoopRowSpan = {};
        me.user = authSvc.authenticationData;
        me.isAuditor2 = me.user.role.toLowerCase().includes("auditor2");

        defaultObject();
        me.LoadCombo();

        me.Undo();

        var headers = authSvc.headers();
        headers.Accept = "application/json";

        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource())
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withFixedColumns({
                leftColumns: 2,
                rightColumns: 1
            })

            //.withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            //.withOption('rowCallback', me.rowCallback)

            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Kode').notVisible(),
            DTColumnBuilder.newColumn('KodeRegistrasi').withTitle('No. Gugus').withClass('text-primary').withOption('width', '100px'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus').withOption('width', '180px'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul').withOption('width', '200px'),
            //DTColumnBuilder.newColumn('Tahun').withTitle('Periode').withClass('text-center'),
            DTColumnBuilder.newColumn('Auditor').withTitle('Auditor').withOption('width', '60px'),
            DTColumnBuilder.newColumn('Auditor_2').withTitle('Auditor 2').withOption('width', '60px'),
            DTColumnBuilder.newColumn('Auditee').withTitle('Auditee').withOption('width', '60px'),
            DTColumnBuilder.newColumn('Auditee_2').withTitle('Auditee 2').withOption('width', '60px'),
            DTColumnBuilder.newColumn('Status').withTitle('Status Audit').withClass('text-primary'),
            DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').notSortable().renderWith(actionsHtml).withOption('width', '200px')
        ];

        me.vm.dtInstance = {};

        me.listStatus = [
            { id: '-', value: '-', text: 'Semua' },
            { id: 'Wait to Audit PDCA I', value: 'Wait to Audit PDCA I', text: 'Wait to Audit PDCA I'},
            { id: 'Audit PDCA I On Progress', value: 'Audit PDCA I On Progress', text: 'Audit PDCA I On Progress' },
            { id: 'Wait to Audit PDCA II', value: 'Wait to Audit PDCA II', text: 'Wait to Audit PDCA II' },
            { id: 'Audit PDCA II On Progress', value: 'Audit PDCA II On Progress', text: 'Audit PDCA II On Progress' }
        ]
    };

    function actionsHtml(data, type, full, meta) {
        var btn = "";
        data.JudulCIP = data.JudulCIP.replace(/\"/g, '\\"');
        switch (data.Status) {
            case "Wait to Audit PDCA I":
                btn = me.isAuditor2 ? '' : '<button class="btn btn-xs btn-success" onclick="angular.element(this).scope().StartPDCA_1(\'' + data.Kode + '\',\'' + data.KodeRegistrasi + '\')"><i class="fa fa-play"></i> Start PDCA I</button>';
                break;
            case "Audit PDCA I On Progress":
                var jData = JSON.stringify(data).replace(/"/g, "|");

                btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().EditData(\'' + jData + '\')"><i class="fa fa-edit"></i> Edit Detail</button>&nbsp';
                btn = me.isAuditor2 ? btn : btn + '<button class="btn btn-xs btn-primary" onclick="angular.element(this).scope().FinishPDCA_1(\'' + data.Kode + '\',\'' + data.KodeRegistrasi + '\')"><i class="fa fa-stop"></i> Finish PDCA I</button>';
                break;
            case "Wait to Audit PDCA II":
                btn = me.isAuditor2 ? '' : '<button class="btn btn-xs btn-success" onclick="angular.element(this).scope().StartPDCA_2(\'' + data.Kode + '\',\'' + data.KodeRegistrasi + '\')"><i class="fa fa-play"></i> Start PDCA II</button>';
                break;
            case "Audit PDCA II On Progress":
                var jData = JSON.stringify(data).replace(/"/g, "|");

                btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().EditData(\'' + jData + '\')"><i class="fa fa-edit"></i> Edit Detail</button>&nbsp';
                btn = me.isAuditor2 ? btn : btn + '<button class="btn btn-xs btn-primary" onclick="angular.element(this).scope().FinishPDCA_2(\'' + data.Kode + '\',\'' + data.KodeRegistrasi + '\')"><i class="fa fa-stop"></i> Finish PDCA II</button>';
                break;
            default:
                var jData = JSON.stringify(data).replace(/"/g, "|");
                btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().ViewData(\'' + jData + '\')"><i class="fa fa-search"></i> View Detail</button>&nbsp';
                break;
        }

        return btn;
    }

    me.ShowList = function () {
        me.isShowList = true;
        me.isNew = false;
    };

    me.NewData = function () {
        me.isShowList = false;
        me.Undo();
    };

    me.EditData = function (jData) {
        var data = JSON.parse(jsonEscape(jData.replace(/\|/g, '"')));
        me.data = data;
        me.isAuditPDCA_1 = (me.data.StatusAudit == "Audit PDCA I") ? true : false;

        me.isNew = false;
        me.allowUpdate = true;

        me.ChangeNoPendaftaran(me.data.Kode, me.data.KodeRegistrasi);
    };

    me.ViewData = function (jData) {
        var data = JSON.parse(jsonEscape(jData.replace(/\|/g, '"')));
        me.data = data;
        me.isAuditPDCA_1 = (me.data.StatusAudit == "Audit PDCA I") ? true : false;

        me.isNew = false;
        me.allowUpdate = false;

        me.ChangeNoPendaftaran(me.data.Kode, me.data.KodeRegistrasi);
    }; 

    me.Undo = function () {
        me.data = {};
        me.anggotaCIP = '';
        me.isNew = true;
        defaultObject();
        me.LoadCombo();
        me.loadTemplatePDCA();
        me.allowUpdate = true;
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(api + 'dropdownRegistrasi').then(function (result) {
            me.listKodeRegistrasi = result.Dropdown;
        });

        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;

            setTimeout(function () {
                var tahun = me.filter.tahun;
                var tahun = $filter('filter')(me.listTahun, { id: tahun }, false);
                if (tahun.length > 0) {
                    tahun[0].obj = angular.copy(tahun[0]);
                    $('#FilterTahun').select2('data', tahun[0]);
                }
            }, 2000);

        });
    }

    me.dataSource = function () {
        if (me.isValidPeriode()) {
            me.filter.status = me.filter.status || '-';
            return {
                url: api + 'datatableswithfilter?tahun=' + me.filter.tahun + '&status=' + me.filter.status,
                type: 'POST',
                accepts: "application/json",
                headers: authSvc.headers(),
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Load Data", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            };
        }
    };

    me.isValidPeriode = function () {
        if (!isNullorEmpty(me.filter.tahun)) {
            return true;
        }
        else {
            NotifBoxWarning("Load Data", "Periode harus dipilih.");
            return false;
        }
    }

    me.reloadData = function () {
        if (me.isValidPeriode()) {
            me.vm.tableOptions.withOption('ajax', me.dataSource());
        }
    };

    me.Save = function () {
        if (this.frmAudit.$valid) {
            var msgTitle = 'Simpan Audit PDCA';
            me.pdca_h.KodeRegistrasi = me.data.KodeRegistrasi;
            var data = { "AuditHeader": me.pdca_h, "AuditDetail": me.listTemplate };
            if (me.isNew) {
                $http({
                    method: 'POST',
                    data: data,
                    url: api + 'Create'
                }).success(function (result) {
                    if (result.Success) {
                        NotifBoxSuccess(msgTitle, result.Message);
                        me.ShowList();
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
            else {
                msgTitle = 'Update Audit PDCA';
                me.pdca_h.UpdateBy = me.user.userName;
                me.pdca_h.UpdateDate = new Date();
                var dataAudit = { "AuditHeader": me.pdca_h, "AuditDetail": me.listTemplate };

                $http.post(api + 'Update', dataAudit)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            me.ShowList();
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
        }
    };

    function mapRowSpan(data) {
        var finalLoopData = {};
        angular.forEach(data, function (value, key) {
            if (!finalLoopData[value.KodeLangkah]) {
                finalLoopData[value.KodeLangkah] = new Array();
            }
            finalLoopData[value.KodeLangkah].push(value);
        });
        me.LoopRowSpan = finalLoopData;
       
    }

    me.ChangeNoPendaftaran = function (kode, kodeRegistrasi) {
        $http.get(api + 'GetRegistrasi?keyValues=' + kodeRegistrasi).success(function (response) {
            if (response.Success) {
                angular.extend(me.data, response.Data);

                me.data.IsUploadAbster = (me.data.IsUploadAbster === null || me.data.IsUploadAbster === undefined) ? true : me.data.IsUploadAbster;
                me.data.IsUploadKOMET = (me.data.IsUploadKOMET === null || me.data.IsUploadKOMET === undefined) ? true : me.data.IsUploadKOMET;
                me.data.IsUploadPresentasi = (me.data.IsUploadPresentasi === null || me.data.IsUploadPresentasi || undefined) ? true : me.data.IsUploadPresentasi;
                me.data.IsUploadRisalah = (me.data.IsUploadRisalah === null || me.data.IsUploadRisalah === undefined) ? true : me.data.IsUploadRisalah;
                me.data.IsUploadSTK = (me.data.IsUploadSTK === null || me.data.IsUploadSTK === undefined) ? true : me.data.IsUploadSTK;
                me.data.IsUploadVerifikasiKeuangan = (me.data.IsUploadVerifikasiKeuangan === null || me.data.IsUploadVerifikasiKeuangan === undefined) ? true : me.data.IsUploadVerifikasiKeuangan;

                me.data.Kode = kode;
                me.anggotaCIP = response.More;
                getPdcaHeader(me.data.Kode);
                me.isShowList = false;

                me.classAbster = me.getClass(me.data.Abster);
                me.classKOMET = me.getClass(me.data.KOMET);
                me.classUploadPresentasi = me.getClass(me.data.UploadPresentasi);
                me.classRisalah = me.getClass(me.data.Risalah);
                me.classSTK = me.getClass(me.data.STK);
                me.classVerifikasiKeuangan = me.getClass(me.data.VerifikasiKeuangan);
            }
            else {
                NotifBoxWarning("Edit Audit PDCA Detil", response.Message);
            }
        }).error(function (error, status) {
            NotifBoxError("Edit Audit PDCA Detil", status + " - " + error.Message);
        });
    };

    me.StartPDCA_1 = function (kode, kodeRegistrasi) {
        var msgTitle = 'Start Audit PDCA I';

        $http.post(api + 'startpdca_1?kode=' + kode + '&kodeRegistrasi=' + kodeRegistrasi)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    var resetPaging = false;
                    me.vm.dtInstance.reloadData(function () { }, resetPaging);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.FinishPDCA_1 = function (kode, kodeRegistrasi) {
        var msgTitle = 'Finish Audit PDCA I';

        $http.post(api + 'finishpdca_1?kode=' + kode + '&kodeRegistrasi=' + kodeRegistrasi)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    var resetPaging = false;
                    me.vm.dtInstance.reloadData(function () { }, resetPaging);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.StartPDCA_2 = function (kode, kodeRegistrasi) {
        var msgTitle = 'Start Audit PDCA II';

        $http.post(api + 'startpdca_2?kode=' + kode + '&kodeRegistrasi=' + kodeRegistrasi)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    var resetPaging = false;
                    me.vm.dtInstance.reloadData(function () { }, resetPaging);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.FinishPDCA_2 = function (kode, kodeRegistrasi) {
        var msgTitle = 'Finish Audit PDCA II';

        $http.post(api + 'finishpdca_2?kode=' + kode + '&kodeRegistrasi=' + kodeRegistrasi)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    var resetPaging = false;
                    me.vm.dtInstance.reloadData(function () { }, resetPaging);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.TrigerUploadDocument = function (event) {
        var id = event.target.id;
        var text = $('#' + id).text().trim();

        var msgTitle = 'Kirim Notifikasi ' + text;

        $http.post(BASE_API + 'registrasicip/sendnotificationuploaddocument?kodeRegistrasi=' + me.data.KodeRegistrasi + '&docType=' + id)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    me.data[id] = false;
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.PreviewDocument = function (event) {
        var id = event.target.id;
        var text = $('#' + id).text().trim();

        $window.open(BASE_API.replace('/api', '') + 'uploads/' + me.data[id], 'blank');

    }


    me.SubmitPDCA_1 = function (kode, kodeRegistrasi, callback) {
        var msgTitle = "Submit Audit PDCA I";
        $.SmartMessageBox({
            title: msgTitle,
            content: 'Submit Audit PDCA I, No. Gugus \"' + kodeRegistrasi + '\"',
            buttons: '[OK][Batal]',
            theme: 'bg-warning'
        }, function (action) {
            if (action === "OK") {
                $http.post(api + 'submitpdca_1?kode=' + kode)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            var resetPaging = false;
                            me.vm.dtInstance.reloadData(function () { }, resetPaging);



                            //if (callback != undefined) {
                            //    callback(true);
                            //}
                            return;
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
                return;
            }
            else {
                return;
            }
        });
    }

    me.Approve = function (kode, kodeRegistrasi, callback) {
        var msgTitle = "Approval Audit PDCA II";
        $.SmartMessageBox({
            title: msgTitle,
            content: 'Approval Audit PDCA II, No. Gugus \"' + kodeRegistrasi + '\"',
            buttons: '[OK][Batal]',
            theme: 'bg-warning'
        }, function (action) {
            if (action === "OK") {
                $http.post(api + 'approve?kode=' + kode)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            var resetPaging = false;
                            me.vm.dtInstance.reloadData(function () { }, resetPaging);



                            //if (callback != undefined) {
                            //    callback(true);
                            //}
                            return;
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
                return;
            }
            else {
                return;
            }
        });
    }

    me.loadTemplatePDCA = function () {
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        //$http.get({
        //    method: 'GET',
        //    url: BASE_API + 'masterKriteria/get',
        //    headers: headers
        //})
        $http.get(BASE_API + 'masterKriteria/get').success(function (response) {
            me.listKriteria = response.DataList;
            getPdcaDetail('');
        }).error(function (err, status) {
            console.log(err);
        });
    }

    function getPdcaHeader(kode) {
        $http.get(api + 'getsingle?keyValues=' + kode).success(function (response) {

            if (response.Data != null) {
                me.pdca_h = response.Data;
            }
            getPdcaDetail(me.pdca_h.Kode);
        }).error(function (err, status) {
            console.log(err);
        });
    };

    function getPdcaDetail(kodeHeader) {
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        $http.get(api + 'GetPdcaDetail?Kode=' + kodeHeader).success(function (response) {
            me.listTemplate = response.DataList;
            mapRowSpan(me.listTemplate);
        }).error(function (err, status) {
            console.log(err);
        });
    }

    function defaultObject() {
        me.pdca_h = {
            'Kode': '',
            'KodeRegistrasi': '',
            'LingkupKerjaTim': '',
            'CatatanAudit': '',
            'EvidenceAudit': '',
            'Coach': '',
            'Auditor': me.user.userName,
            'Auditee': '',
            'NoPdca': null,
            'CreateBy': me.user.userName,
            'CreateDate': new Date(),
            'UpdateBy': '',
            'UpdateDate': null
        };
    };

    me.getClass = function (file) {
        var cls = '';
        if (!isNullOrEmpty(file)) {
            cls = 'fa fa-file-code-o text-green';
            if (file.endsWith('xls') || file.endsWith('xlsx')) {
                cls = 'fa fa-file-excel-o color-green';
            }
            if (file.endsWith('doc') || file.endsWith('docx')) {
                cls = 'fa fa-file-word-o color-blue';
            }
            if (file.endsWith('pdf')) {
                cls = 'fa fa-file-pdf-o text-danger';
            }
            if (file.endsWith('ppt') || file.endsWith('pptx')) {
                cls = 'fa fa-file-powerpoint-o color-red';
            }
            if (file.endsWith('png') || file.endsWith('jpg') || file.endsWith('jpeg') || file.endsWith('bmp')) {
                cls = 'fa fa-file-image-o color-green';
            }
        }

        return cls;
    };

    me.init(this);
});