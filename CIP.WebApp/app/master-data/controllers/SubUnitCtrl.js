'use strict';

angular.module('app.masterdata').controller('SubUnitController', function ($scope, $http, $state, $filter, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'mastersubunit/';
    var api = BASE_API + module;

    me.init = function (obj) {
        me.data = {};
        me.isShowList = true;
        me.isNew = false;

        me.Undo();

        var headers = authSvc.headers();
        headers.Accept = "application/json";

        obj.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', {
                // Either you specify the AjaxDataProp here
                // dataSrc: 'data',
                url: api + 'datatables',
                type: 'POST',
                accepts: "application/json",
                headers: headers,
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Master Data Sub Unit", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            })
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)
            //.withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withOption('rowCallback', me.rowCallback)
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        obj.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('Kode').notVisible(),
            DTColumnBuilder.newColumn('Deskripsi').withTitle('Sub Unit').withClass('text-primary'),
            DTColumnBuilder.newColumn('DirektoratDes').withTitle('Direktorat'),
            DTColumnBuilder.newColumn('UnitDes').withTitle('Unit'),
            DTColumnBuilder.newColumn('Direktorat').withTitle('Dir').notVisible(),
            DTColumnBuilder.newColumn('Unit').withTitle('Unit').notVisible(),
        ];
    };

    me.rowCallback = function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', function () {
            me.$apply(function () {
                me.EditData(aData);
            });
        });
        return nRow;
    }

    me.ShowList = function () {
        me.isShowList = true;
        me.isNew = false;
    };

    me.NewData = function () {
        me.isShowList = false;
        me.Undo();
    };

    me.EditData = function (data) {
        me.isShowList = false;
        me.isNew = false;
        me.data = data;

        setTimeout(function () {
            var unit = $filter('filter')(me.listUnit, { id: me.data.Unit }, false);
            if (unit.length > 0) {
                unit[0].obj = angular.copy(unit[0]);
                $('#Unit').select2('data', unit[0]);
            }
        }, 2000);
    }

    me.Undo = function () {
        me.data = {};
        me.isNew = true;
        me.LoadCombo();
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'MasterDirektorat/dropdown').then(function (result) {
            me.listDirektorat = result.Dropdown;
        });
    };

    me.Save = function () {
        if (this.frmUnit.$valid) {
            var msgTitle = 'Simpan Master Data Sub Unit';
            if (me.isNew) {
                $http.post(api, me.data)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            me.ShowList();
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
            else {
                msgTitle = 'Update Master Data Sub Unit';
                $http.put(api, me.data)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            me.ShowList();
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
        }
    };

    me.Delete = function () {
        var msgTitle = 'Hapus Master Data Sub Unit';
        if (!me.isNew) {
            var kode = me.data.Kode;
            var msgContent = msgTitle + ": \"" + me.data.Deskripsi + "\" ?"
            var data = [kode];
            $.SmartMessageBox({
                title: msgTitle,
                content: msgContent,
                buttons: '[OK][Batal]',
                theme: 'bg-warning'
            }, function (action) {
                if (action === "OK") {
                    $http.delete(api, { data })
                        .success(function (result) {
                            if (result.Success) {
                                NotifBoxSuccess(msgTitle, result.Message);
                                me.ShowList();
                            }
                            else {
                                NotifBoxWarning(msgTitle, result.Message);
                            }
                        })
                        .error(function (error, status) {
                            NotifBoxError(msgTitle, status + " - " + error.Message);
                        });
                }
                if (action === "Batal") {
                    return;
                }
            });
        }
    };

    me.$watch('data.Direktorat', function (n, o) {
        if (!isNullorEmpty(n)) {
            Select2Helper.GetDataForCombo(BASE_API + 'masterunit/dropdownbydirektorat?direktorat=' + n).then(function (result) {
                me.listUnit = result.Dropdown;
                if (me.listUnit.length === 1) {
                    setTimeout(function () {
                        me.data.Unit = me.listUnit[0].value;
                        me.listUnit[0].obj = angular.copy(me.listUnit[0]);
                        $('#Unit').select2('data', me.listUnit[0]);
                    }, 2000);
                }
                else {
                    if (me.isNew) {
                        me.data.Unit = '';
                    }
                }
            });
        }
        else {
            me.listUnit = [];
            me.data.Unit = '';
        }
    }, true);
    me.init(this);
});