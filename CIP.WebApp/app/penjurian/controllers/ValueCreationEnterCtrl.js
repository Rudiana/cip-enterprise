'use strict';

angular.module('app.penjurian').controller('ValueCreationEnterController', function ($scope, $http, $state, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'ValueCreationEnter/';
    var api = BASE_API + module;
    var msgTitle = "Value Creation Penjurian";

    me.init = function (obj) {
        me.vm = obj;
        me.data = {};
        me.tahun = new Date().getFullYear();
        me.stream = '';
        me.Undo();
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriEnter/DropdownTahunPenjurian').then(function (result) {
            me.listTahun = result.Dropdown;
            if (me.listTahun.length > 0) {
                if (me.listTahun.length == 1) {
                    me.tahun = me.listTahun[0].id;
                    setTimeout(function () {
                        var tahun = me.listTahun;
                        if (tahun.length > 0) {
                            tahun[0].obj = angular.copy(tahun[0]);
                            $('#tahun').select2('data', tahun[0]);
                        }
                    }, 2000);
                }
                else {
                    setTimeout(function () {
                        var tahun = $filter('filter')(me.listTahun, { id: me.tahun }, false);
                        if (tahun.length > 0) {
                            tahun[0].obj = angular.copy(tahun[0]);
                            $('#tahun').select2('data', tahun[0]);
                        }
                    }, 2000);
                }
            }
        });
    };

    me.changeStream = function (item) {
        me.stream = item;
    };

    me.EditData = function (data) {
        me.data = data;
        me.isShowList = false;
        
    };

    me.Undo = function ()
    {
        me.find();
        me.LoadCombo();
        me.isShowList = true;
    };

    me.ShowList = function () {
        me.find();
        me.isShowList = true;
    };

    me.Save = function () {
        if (this.frmValueCreation.$valid) {

            msgTitle = 'Value Creation';
       
            $http.post(api + 'SetValueCreation', JSON.stringify(me.data))
                .success(function (result) {
                    if (result.Success) {
                        NotifBoxSuccess(msgTitle, result.Message);
                        me.ShowList();
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });

        }
    };

    me.find = function () {
        if (me.checkPeriode()) {
            
            var tahun = me.tahun;
            var stream = me.stream;

            $http.get(api + 'GetValueCreation?periode=' + tahun + '&stream=' + stream).success(function (response) {

                if (response !== null) {
                    me.data = response;
                    console.log(me.data);
                }

            }).error(function (err, status) {
                NotifBoxError('Warning', err.Message);
            });
        }
    };

    me.exportExcel = function () {
        if (me.checkPeriode()) {
            $http.get(api + 'GetValueCreationExcelReport?tahun=' + me.tahun+'&stream='+me.stream, { responseType: 'arraybuffer' }).success(function (response) {

                if (response !== null) {

                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'value-creation.xlsx');
                }


            }).error(function (err, status) {
                NotifBoxError('Warning', err.Message);
            });
        }
    };

    me.onChangeTahun = function () {
        tahun=me.tahun;

        if (!isNullOrEmpty(tahun)) {
            Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriEnter/DropdownStreamByJuri?tahun=' + tahun).then(function (result) {
                me.listStream = result.Dropdown;
            });
        }
    }

    me.checkPeriode = function () {
        if (isNullOrEmpty(me.tahun)) {
            NotifBoxWarning('Information', 'Silahkan pilih Periode terlebih dahulu.');
            return false;
        }
        else {
            return true;
        }
    };

    me.$watch('tahun', function (n, o) {
        if (isNullOrEmpty(n)) {
            me.tahun = new Date().getFullYear();
        }
    }, true);
    me.NumberFormatIDR = function (val) {
        var value = angular.copy(val);
        return IDNumberFormat(value, 2);
    };
    me.init(this);
});