'use strict';

angular.module('app.penjurian').controller('SofiEnterController', function ($scope, $rootScope, $http, $state, $window, $sce, $filter, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'SofiEnter/';
    var api = BASE_API + module;


    me.init = function (obj) {
        me.vm = obj;
        me.data = {};
        me.sofiDetail = {};
        me.tahun = new Date().getFullYear();
        me.stream = '';
        me.Undo();
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriEnter/DropdownTahunPenjurian').then(function (result) {
            me.listTahun = result.Dropdown;
        });
    };

    me.changeStream = function (item) {
        me.stream = item;
    };

    me.EditData = function (data) {
        me.findDetail(data.KodeRegistrasi);
        me.data = data;
        me.isShowList = false;
    };

    me.Undo = function () {
        me.find();
        me.LoadCombo();
        me.isShowList = true;
    };

    me.ShowList = function () {
        me.find();
        me.isShowList = true;
    };

    me.Save = function () {
        if (this.frmSofi.$valid) {

            msgTitle = 'Sofi';

            $http.post(api + 'SetSofi', JSON.stringify(me.sofiDetail))
                .success(function (result) {
                    if (result.Success) {
                        NotifBoxSuccess(msgTitle, result.Message);
                        me.ShowList();
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });

        }
    };

    me.find = function () {
        if (me.checkPeriode()) {

            var tahun = me.tahun;
            var stream = me.stream;

            $http.get(api + 'GetSofi?periode=' + tahun + '&stream=' + stream).success(function (response) {

                if (response !== null) {
                    me.data = response;
                }

            }).error(function (err, status) {
                NotifBoxError('Warning', err.Message);
            });
        }
    };

    me.findDetail = function (KodeRegistrasi) {

            $http.get(api + 'GetSofiDetail?KodeRegistrasi=' + KodeRegistrasi).success(function (response) {

                if (response !== null) {
                    me.sofiDetail = response;
                   
                }

            }).error(function (err, status) {
                NotifBoxError('Warning', err.Message);
            });
        
    };

    me.checkPeriode = function () {
        if (isNullOrEmpty(me.tahun)) {
            NotifBoxWarning('Information', 'Silahkan pilih Periode terlebih dahulu.');
            return false;
        }
        else {
            return true;
        }
    };

    me.$watch('tahun', function (n, o) {
        if (!isNullorEmpty(n)) {
            Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriEnter/DropdownStreamByJuri?tahun=' + n).then(function (result) {
                me.listStream = result.Dropdown;

                if (me.listStream.length == 1) {
                    me.stream = me.listStream[0].id;
                    setTimeout(function () {
                        var stream = $filter('filter')(me.listStream, { id: me.stream }, false);
                        if (stream.length > 0) {
                            stream[0].obj = angular.copy(stream[0]);
                            $('#stream').select2('data', stream[0]);
                        }
                    }, 2000);
                }
                else {
                    me.stream = '';
                }
            });
        }
        else {
            me.stream = '';
            me.tahun = new Date().getFullYear();
        }
    }, true);

    me.init(this);
});