'use strict';

angular.module('app.penjurian').controller('InputPenjurianEnterController', function ($scope, $rootScope, $http, $state, $window, $sce, $filter, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'PenjurianEnter/';
    var api = BASE_API + module;

    me.init = function (obj) {
        me.vm = obj;
        me.data = {};
        me.document =
        me.isShowList = true;
        me.isNew = false;

        console.log($rootScope.isAdminQM);

        me.LoopRowSpan = {};
        me.user = authSvc.authenticationData;
        defaultObject();
        me.Undo();

        var headers = authSvc.headers();
        headers.Accept = "application/json";

        me.vm.tableOptions = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', {
                // Either you specify the AjaxDataProp here
                // dataSrc: 'data',
                url: api + 'datatables',
                type: 'POST',
                accepts: "application/json",
                headers: headers,
                error: function (xhr, ajaxOptions, thrownError) {
                    NotifBoxErrorTable("Template", xhr.responseText.Message, xhr.status, $state, authSvc);
                }
            })
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)
            //.withPaginationType('full_numbers')
            //Add Bootstrap compatibility

            .withOption('responsive', true)
            .withOption('scrollY', true)
            .withOption('scrollX', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', true)
            .withOption('colReorder', true)

            .withFixedColumns({
                leftColumns: 2,
                rightColumns: 1
            })

            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withOption('rowCallback', me.rowCallback)
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath());

        me.vm.tableColumns = [
            DTColumnBuilder.newColumn('Kode').withTitle('Kode').withClass('text-primary').notVisible(),
            DTColumnBuilder.newColumn('KodeRegistrasi').withTitle('No. Gugus').withClass('text-primary'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JenisCIP').withTitle('Jenis'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn('Direktorat').withTitle('Direktorat'),
            DTColumnBuilder.newColumn('Tahun').withTitle('Periode CIP'),
            DTColumnBuilder.newColumn('Juri').withTitle('Juri'),
            DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').notSortable().renderWith(actionsHtml)
        ];

        me.vm.dtInstance = {};
    };

    function actionsHtml(data, type, full, meta) {
        data.JudulCIP = data.JudulCIP.replace(/"/g, "").replace(/\n/g, " ").replace(/\r/g, " ").replace(/\t/g, " ").replace(/\'/g, "~");
        data.NamaGugus = data.NamaGugus.replace(/"/g, "").replace(/\n/g, " ").replace(/\r/g, " ").replace(/\t/g, " ").replace(/\'/g, "~");

        var dat = data.KodeRegistrasi + '\',\'' + data.NamaGugus + '\',\'' + data.JenisCIP + '\',\'' + data.JudulCIP + '\',\'' + data.Direktorat + '\',\'' + data.Tahun + '\',\'' + data.Juri;

        var btn = "";
        switch (data.Status) {
            case "Start":
                if (!$rootScope.isAdminQM) {
                    btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().EditData(\'' + dat + '\')"><i class="fa fa-edit"></i> Edit Detail</button>&nbsp' +
                        '<button class="btn btn-xs btn-primary" onclick="angular.element(this).scope().FinishPenjurian(\'' + data.Kode + '\',\'' + data.KodeRegistrasi + '\')"><i class="fa fa-stop"></i> Finish Penjuruian</button>';
                }
                else {
                    btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().ViewData(\'' + dat + '\')"><i class="fa fa-search"></i> View Detail</button>&nbsp';
                }
                break;
            case "Finish":
                btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().ViewData(\'' + dat + '\')"><i class="fa fa-search"></i> View Detail</button>&nbsp';
                if ($rootScope.isAdminQM) {
                    btn = btn + '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().EditData(\'' + dat + '\')"><i class="fa fa-recycle"></i> Sesuaikan Nilai</button>&nbsp';
                }
                break;
            default:
                if (!$rootScope.isAdminQM) {
                    btn = '<button class="btn btn-xs btn-success" onclick="angular.element(this).scope().StartPenjurian(\'' + data.Kode + '\',\'' + data.KodeRegistrasi + '\')"><i class="fa fa-play"></i> Start Penjurian</button>';
                }
                else {
                    btn = '<button class="btn btn-xs btn-warning" onclick="angular.element(this).scope().ViewData(\'' + dat + '\')"><i class="fa fa-search"></i> View Detail</button>&nbsp';
                }
                break;

        }

        return btn;
    };

    me.ShowList = function () {
        me.isShowList = true;
        me.isNew = false;
    };

    me.NewData = function () {
        me.isShowList = false;
        me.Undo();
    };

    me.EditData = function (KodeRegistrasi, NamaGugus, JenisCIP, JudulCIP, Direktorat, Tahun, kodeJuri) {
        me.allowUpdate = true;

        me.LoadData(KodeRegistrasi, NamaGugus, JenisCIP, JudulCIP, Direktorat, Tahun, kodeJuri);
    };

    me.ViewData = function (KodeRegistrasi, NamaGugus, JenisCIP, JudulCIP, Direktorat, Tahun, kodeJuri) {
        me.allowUpdate = false;

        me.LoadData(KodeRegistrasi, NamaGugus, JenisCIP, JudulCIP, Direktorat, Tahun, kodeJuri);
    };

    me.LoadData = function (KodeRegistrasi, NamaGugus, JenisCIP, JudulCIP, Direktorat, Tahun, kodeJuri) {
        NamaGugus = NamaGugus.replace(/\~/g, "'");
        JudulCIP = JudulCIP.replace(/\~/g, "'");

        me.isShowList = false;
        me.isNew = false;
        me.data.KodeRegistrasi = KodeRegistrasi;
        me.data.NamaGugus = NamaGugus;
        me.data.JenisCIP = JenisCIP;
        me.data.JudulCIP = JudulCIP;
        me.data.Direktorat = Direktorat;
        me.data.Tahun = Tahun;

        getPenjurianHeader(me.data.KodeRegistrasi, kodeJuri);

        $http.get(BASE_API + 'registrasicip/getdocument?kode=' + KodeRegistrasi).success(function (response) {
            if (!isNullOrEmpty(response.Data)) {
                me.document = response.Data;
                me.classAbster = me.getClass(me.document.Abster);
                me.classKOMET = me.getClass(me.document.KOMET);
                me.classUploadPresentasi = me.getClass(me.document.UploadPresentasi);
                me.classRisalah = me.getClass(me.document.Risalah);
                me.classSTK = me.getClass(me.document.STK);
                me.classVerifikasiKeuangan = me.getClass(me.document.VerifikasiKeuangan);
            }

        }).error(function (err, status) {
            console.log(err);
        });
    };

    me.Undo = function () {
        me.data = {};
        me.isNew = true;
        defaultObject();
        me.LoadCombo();
        me.loadTemplatePDCA();
    };

    me.Calculate = function (data) {
        if (data.Skala > 5) {
            data.Skala = 5;
        }

        if (data.Point > 150) {
            data.Point = 150;
        }

        data.Nilai = SetNilai(data.Point, data.Skala, data.KodeTemplatePDCA, data.JenisCIP);
        SumNilai();
    };    

    function SetNilai(point, skala, kodeTemplate, kodeJenis) {
        
        if (skala !== 0) { nilai = skala / 5 * (point); }
        else { nilai = 0; }
        return nilai;

    };


    function SumNilai() {
        var num = 0;
        angular.forEach(me.listTemplate, function (value, key) {
            num = num + parseInt(value.Nilai);
        });

        if (!isNaN(num)) {
            me.total = num;

        }
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(api + 'dropdownRegistrasi').then(function (result) {
            me.listKodeRegistrasi = result.Dropdown;
        });
    };

    me.Save = function () {
        if (this.frmInputPenjurian.$valid) {

            msgTitle = 'Penjurian';
            me.penjurian_h.UpdateBy = me.user.userName;
            me.penjurian_h.UpdateDate = new Date();
            var data = { "PenjurianHeader": me.penjurian_h, "PenjurianDetail": me.listTemplate };
            $http.post(api + 'Update', JSON.stringify(data))
                .success(function (result) {
                    if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                    me.ShowList();
                    }
                    else {
                        NotifBoxWarning(msgTitle, result.Message);
                    }
                })
                .error(function (error, status) {
                    NotifBoxError(msgTitle, status + " - " + error.Message);
                });

        }
    };

    function mapRowSpan(data) {
        var finalLoopData = {};
        angular.forEach(data, function (value, key) {
            if (!finalLoopData[value.KodeLangkah]) {
                finalLoopData[value.KodeLangkah] = new Array();
            }
            finalLoopData[value.KodeLangkah].push(value);
        });
        me.LoopRowSpan = finalLoopData;
    };

    me.loadTemplatePDCA = function () {
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        $http.get(BASE_API + 'masterKriteria/get').success(function (response) {
            me.listKriteria = response.DataList;
            getPenjurianDetail('');
        }).error(function (err, status) {
            console.log(err);
        });
    }

    function getPenjurianHeader(kode, kodeJuri) {

        $http.get(api + 'GetPenjurianHeader?keyValues=' + kode + '&kodeJuri=' + kodeJuri).success(function (response) {

            if (response.Data !== null) {
                me.penjurian_h = angular.copy(response.Data);
                
            }
            getPenjurianDetail(kode, kodeJuri);
        }).error(function (err, status) {
            console.log(err);
        });
    };

    function getPenjurianDetail(kodeHeader, kodeJuri) {
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        $http.get(api + 'GetPenjurianDetail?Kode=' + kodeHeader + '&kodeJuri=' + kodeJuri).success(function (response) {
            me.listTemplate = response.DataList;
            SumNilai();
            mapRowSpan(me.listTemplate);

        }).error(function (err, status) {
            console.log(err);
        });
    };

    

    function defaultObject() {
        me.penjurian_h = {
            'Kode': 0,
            'KodeRegistrasi': '',
            'Juri': me.user.userName,
            'CreateBy': me.user.userName,
            'CreateDate': new Date(),
            'UpdateBy': '',
            'UpdateDate': null
        };
    };

    me.downloadXLS = function () {
        var table = "tblStream";
        var name = "Stream Penjurian";

        var uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }); };

        if (!table.nodeType) table = document.getElementById(table)
        var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML };
        window.location.href = uri + base64(format(template, ctx));
    };

    me.StartPenjurian = function (kode, kodeRegistrasi) {
        var msgTitle = 'Start Penjurian';
        $.SmartMessageBox({
            title: msgTitle,
            content: 'Yakin Start Penjurian: "' + kodeRegistrasi + '" ?',
            buttons: '[OK][Batal]',
            theme: 'bg-success'
        }, function (action) {
            if (action === "OK") {
                $http.post(api + 'startpenjurian?kode=' + kode + '&kodeRegistrasi=' + kodeRegistrasi)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            var resetPaging = false;
                            me.vm.dtInstance.reloadData(function () { }, resetPaging);
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
            else {
                return;
            }
        });
    };

    me.FinishPenjurian = function (kode, kodeRegistrasi) {
        var msgTitle = 'Finish Penjurian';
        $.SmartMessageBox({
            title: msgTitle,
            content: 'Yakin Finish Penjurian: "' + kodeRegistrasi + '" ?',
            buttons: '[OK][Batal]',
            theme: 'bg-success'
        }, function (action) {
            if (action === "OK") {
                $http.post(api + 'finishpenjurian?kode=' + kode + '&kodeRegistrasi=' + kodeRegistrasi)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            var resetPaging = false;
                            me.vm.dtInstance.reloadData(function () { }, resetPaging);
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
            else {
                return;
            }
        });
    };

    me.ResetPenjurian = function (kode) {
        var msgTitle = 'Reset Nilai Penjurian';
        $.SmartMessageBox({
            title: msgTitle,
            content: 'Yakin Reset Penjurian: "' + me.data.KodeRegistrasi + '" ?',
            buttons: '[OK][Batal]',
            theme: 'bg-success'
        }, function (action) {
            if (action === "OK") {
                $http.post(api + 'resetnilaipenjurian?kodePenjurian=' + kode)
                    .success(function (result) {
                        if (result.Success) {
                            NotifBoxSuccess(msgTitle, result.Message);
                            me.ShowList();
                        }
                        else {
                            NotifBoxWarning(msgTitle, result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError(msgTitle, status + " - " + error.Message);
                    });
            }
            else {
                return;
            }
        });
    };

    me.DownloadDocument = function (event) {
        var id = event.target.id;
        if (id.toLowerCase() === 'risalah') {
            me.downloadRisalah(me.data.KodeRegistrasi);
        }
        else {
            $window.open(BASE_API.replace('/api', '') + 'uploads/' + me.document[id], 'blank');
        }
    };

    me.downloadRisalah = function (kode) {
        $http.get(BASE_API + "registrasicip/downloadrisalah?kode=" + kode)
            .success(function (result) {
                if (result.Success) {
                    $window.open(BASE_API.replace('/api', '') + 'downloads/' + result.Data, '_blank');
                }
                else {
                    NotifBoxWarning("Unduh Risalah", result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError("Unduh Risalah", status + " - " + error.Message);
            });
    };

    me.getClass = function (file) {
        var cls = '';
        if (!isNullOrEmpty(file)) {
            cls = 'fa fa-file-code-o text-green';
            if (file.endsWith('xls') || file.endsWith('xlsx')) {
                cls = 'fa fa-file-excel-o color-green';
            }
            if (file.endsWith('doc') || file.endsWith('docx')) {
                cls = 'fa fa-file-word-o color-blue';
            }
            if (file.endsWith('pdf')) {
                cls = 'fa fa-file-pdf-o text-danger';
            }
            if (file.endsWith('ppt') || file.endsWith('pptx')) {
                cls = 'fa fa-file-powerpoint-o color-red';
            }
            if (file.endsWith('png') || file.endsWith('jpg') || file.endsWith('jpeg') || file.endsWith('bmp')) {
                cls = 'fa fa-file-image-o color-green';
            }
        }

        return cls;
    };

    me.popoverHtml = function (val) {
        val = val.replace(/\r/g, "<br/><br/>");
        val = val.replace(/\n/g, "<br/><br/>");
        val = val.replace(/\r\n/g, "<br/><br/>");

        return val;
    };

    me.filterSofi = function (itm) {
        return itm.idx === 1 && itm.Kriteria !== 'Others';
    };

    me.init(this);
});