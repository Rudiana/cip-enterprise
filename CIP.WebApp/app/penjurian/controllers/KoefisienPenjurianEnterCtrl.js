'use strict';

angular.module('app.penjurian').controller('KoefisienPenjurianEnterController', function ($scope, $http, $state, $filter, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'PenjurianEnter/';
    var api = BASE_API + module;
    var msgTitle = "Koefisien Penjurian";

    me.init = function (obj) {
        me.data = {};
        me.user = authSvc.authenticationData;
        me.tahun = new Date().getFullYear();
        me.kodeJenisCip = '%';
        me.editing = false;
        var headers = authSvc.headers();
        headers.Accept = "application/json";
        me.LoadCombo();
        me.LoadComboJenisCip();
    };

    me.editItem = function (item) {

        me.editing = item.No;
        
    }

    me.doneEditing = function (item) {
        me.editing =0;
        
    };

    me.saveKoef = function () {
        var msgTitle = 'Simpan Manual Koefisien';
        var data = {
            header: { 'Kode': me.kodeJenisCip},
            detail: me.data.Stream
        }
        $http.post(api + 'SaveKoef', data)
            .success(function (result) {
                if (result.Success) {
                    NotifBoxSuccess(msgTitle, result.Message);
                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.resetKoef = function () {
        var msgTitle = 'Reset Manual Koefisien';
        var data = {
            header: { 'Kode': me.kodeJenisCip },
            detail: me.data.Stream
        }
        $http.post(api + 'ResetKoef', data)
            .success(function (result) {
                if (result.Success) {
                    me.changeKoef();
                    NotifBoxSuccess(msgTitle, result.Message);

                }
                else {
                    NotifBoxWarning(msgTitle, result.Message);
                }
            })
            .error(function (error, status) {
                NotifBoxError(msgTitle, status + " - " + error.Message);
            });
    }

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriDir/DropdownTahun').then(function (result) {
            me.listTahun = result.Dropdown;

            if (me.listTahun.length > 0) {
                if (me.listTahun.length == 1) {
                    me.tahun = me.listTahun[0].id;
                    setTimeout(function () {
                        var tahun = me.listTahun;
                        if (tahun.length > 0) {
                            tahun[0].obj = angular.copy(tahun[0]);
                            $('#tahun').select2('data', tahun[0]);
                        }
                    }, 2000);
                }
                else {
                    setTimeout(function () {
                        var tahun = $filter('filter')(me.listTahun, { id: me.tahun }, false);
                        if (tahun.length > 0) {
                            tahun[0].obj = angular.copy(tahun[0]);
                            $('#tahun').select2('data', tahun[0]);
                        }
                    }, 2000);
                }
            }
        });
    };

    me.LoadComboJenisCip = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriDir/DropdownJenisCip').then(function (result) {
            me.listJenisCip = result.Dropdown;
        });
    };

    me.changeKoef = function () {
        if (me.checkPeriod()) {
            me.kodeJenisCip = isNullOrEmpty(me.kodeJenisCip) ? '%' : me.kodeJenisCip;

            $http.post(api + 'getkoef?tahun=' + me.tahun+'&kodejenisCip='+me.kodeJenisCip).success(function (response) {

                if (response !== null) {
                    me.data = response;
                }

            }).error(function (err, status) {
                NotifBoxError(msgTitle, err.Message);
            });
        }
    };

    me.exportExcel = function () {
        if (me.checkPeriod()) {
            me.kodeJenisCip = isNullOrEmpty(me.kodeJenisCip) ? '%' : me.kodeJenisCip;

            $http.get(api + 'GetKoefExcelReport?tahun=' + me.tahun + '&kodejenisCip=' + me.kodeJenisCip, { responseType: 'arraybuffer' }).success(function (response) {
                if (response !== null) {

                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Koefisien_' + (me.kodeJenisCip === '%' ? '' : me.kodeJenisCip) + '_' + me.tahun + '.xlsx');
                }
            }).error(function (err, status) {
                NotifBoxError(msgTitle, err.Message);
            });
        }
    };

    me.checkPeriod = function () {
        if (isNullOrEmpty(me.tahun)) {
            NotifBoxWarning(msgTitle, "Pilih Peride terlebih dahulu.");

            return false;
        }
        else {
            return true;
        }
    };

    me.init(this);
});