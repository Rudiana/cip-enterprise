'use strict';

angular.module('app.penjurian').controller('DockingInternalDirController', function ($scope, $http, $state, $filter, authSvc, Select2Helper, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language) {
    var me = $scope;
    var module = 'PenjurianDir/';
    var api = BASE_API + module;

    me.init = function (obj) {
        me.data = {};
        me.tahun = new Date().getFullYear();
        me.stream = '';
        me.LoopRowSpan = {};
        me.user = authSvc.authenticationData;
        me.LoadCombo();

        me.dataList = [];
        me.kolomSkala = [];
        me.kolomNilai = [];
        me.sumNilai = {};
        me.scoreNilai = {};
    };

    function mapRowSpan(data) {
        var finalLoopData = {};
        angular.forEach(data, function (value, key) {
            if (!finalLoopData[value.Kriteria]) {
                finalLoopData[value.Kriteria] = new Array();
            }
            finalLoopData[value.Kriteria].push(value);
        });
        me.LoopRowSpan = finalLoopData;
    }

    me.find = function (callback) {
        var tahun = me.tahun;
        var gugus = me.gugus;

        if (isNullOrEmpty(tahun)) {
            NotifBoxWarning('Information', 'Silahkan pilih Periode terlebih dahulu.');
        }
        else {
            if (isNullOrEmpty(gugus)) {
                NotifBoxWarning('Information', 'Silahkan pilih Gugus terlebih dahulu.');
                return;
            }
            else {
                $http.post(api + 'getDocking?tahun=' + tahun + '&gugus=' + gugus).success(function (response) {

                    if (response.Success) {
                        me.dataList = response.More[0];
                        me.sumNilai = response.More[1][0];
                        me.scoreNilai = response.More[2][0];
                        me.kolomSkala = response.More[3][0].KolomSkala.split(',');
                        me.kolomNilai = response.More[3][0].KolomNilai.split(',');
                        
                    }
                    else {
                        me.dataList = [];
                        NotifBoxError('Warning', response.Message);
                    }
                }).error(function (err, status) {
                    NotifBoxError('Warning', err.Message);
                });

            }
        }
    }

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriDir/DropdownTahunPenjurian').then(function (result) {
            me.listTahun = result.Dropdown;

            if (me.listTahun.length > 0) {
                if (me.listTahun.length == 1) {
                    me.tahun = me.listTahun[0].id;
                    setTimeout(function () {
                        var tahun = me.listTahun;
                        if (tahun.length > 0) {
                            tahun[0].obj = angular.copy(tahun[0]);
                            $('#tahun').select2('data', tahun[0]);
                        }
                    }, 2000);
                }
                else {
                    setTimeout(function () {
                        var tahun = $filter('filter')(me.listTahun, { id: me.tahun }, false);
                        if (tahun.length > 0) {
                            tahun[0].obj = angular.copy(tahun[0]);
                            $('#tahun').select2('data', tahun[0]);
                        }
                    }, 2000);
                }
            }
        });
    }

    me.downloadXLS = function () {
        var stream = me.stream;
        var tahun = me.tahun;

        if (isNullOrEmpty(tahun)) {
            NotifBoxWarning('Information', 'Silahkan pilih Periode terlebih dahulu.');
        }
        else {
            if (isNullOrEmpty(stream)) {
                NotifBoxWarning('Information', 'Silahkan pilih Stream terlebih dahulu.');
                return;
            }
            else {
                $http.get(api + 'GetStreamExcelReport?periode=' + me.tahun + '&stream=' + me.stream, { responseType: 'arraybuffer' }).success(function (response) {

                    if (response !== null) {

                        var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        saveAs(blob, 'Stream.xlsx');
                    }
                }).error(function (err, status) {
                    NotifBoxError('Warning', err.Message);
                });
            }
        }
    };

    me.$watch('tahun', function (n, o) {
        if (!isNullorEmpty(n)) {
            Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriDir/DropdownStreamByJuri?tahun=' + n).then(function (result) {
                me.listStream = result.Dropdown;

                if (me.listStream.length == 1) {
                    me.stream = me.listStream[0].id;
                    setTimeout(function () {
                        var stream = $filter('filter')(me.listStream, { id: me.stream }, false);
                        if (stream.length > 0) {
                            stream[0].obj = angular.copy(stream[0]);
                            $('#stream').select2('data', stream[0]);
                        }
                    }, 2000);
                }
                else {
                    me.stream = '';
                }
            });
        }
        else {
            me.stream = '';
        }
    }, true);

    me.$watch('stream', function (n, o) {
        if (!isNullorEmpty(n)) {
            if (!isNullOrEmpty(me.tahun)) {
                Select2Helper.GetDataForCombo(BASE_API + 'StreamJuriDir/DropdownStreamGugusDocking?tahun=' + me.tahun + '&stream=' + n).then(function (result) {
                    me.listGugus = result.Dropdown;

                    if (me.listGugus.length == 1) {
                        me.gugus = me.listGugus[0].id;
                        setTimeout(function () {
                            var gugus = $filter('filter')(me.listGugus, { id: me.gugus }, false);
                            if (gugus.length > 0) {
                                gugus[0].obj = angular.copy(gugus[0]);
                                $('#gugus').select2('data', gugus[0]);
                            }
                        }, 2000);
                    }
                    else {
                        me.gugus = '';
                    }
                });
            }
            else {
                me.gugus = '';
            }
        }
        else {
            me.gugus = '';
        }
    }, true);

    me.init(this);

});