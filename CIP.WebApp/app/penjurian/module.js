"use strict";


angular.module('app.penjurian', ['ui.router', 'datatables', 'datatables.bootstrap', 'ngHandsontable']);

angular.module('app.penjurian').config(function ($stateProvider) {

    $stateProvider
        .state('app.penjurian', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.inputpenjurian', {
            url: '/penjurian/input/unit',
            data: {
                title: 'Input Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'InputPenjurianController as ctrl',
                    templateUrl: 'app/penjurian/views/input-penjurian.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.inputpenjuriandir', {
            url: '/penjurian/input/direktorat',
            data: {
                title: 'Input Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'InputPenjurianDirController as ctrl',
                    templateUrl: 'app/penjurian/views/input-penjurian-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.inputpenjurianenter', {
            url: '/penjurian/input/enterprise',
            data: {
                title: 'Input Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'InputPenjurianEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/input-penjurian-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.penjuriansofi', {
            url: '/penjurian/sofi',
            data: {
                title: 'Input Sofi'
            },
            views: {
                "content@app": {
                    controller: 'SofiController as ctrl',
                    templateUrl: 'app/penjurian/views/sofi.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.penjuriansofidirektorat', {
            url: '/penjurian/sofi/direktorat',
            data: {
                title: 'Input Sofi'
            },
            views: {
                "content@app": {
                    controller: 'SofiDirController as ctrl',
                    templateUrl: 'app/penjurian/views/sofi-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.penjuriansofienterprise', {
            url: '/penjurian/sofi/enterprise',
            data: {
                title: 'Input Sofi'
            },
            views: {
                "content@app": {
                    controller: 'SofiEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/sofi-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.streampenjurian', {
            url: '/penjurian/stream',
            data: {
                title: 'Stream Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'StreamPenjurianController as ctrl',
                    templateUrl: 'app/penjurian/views/stream-penjurian.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.streampenjuriandir', {
            url: '/penjurian/stream/direktorat',
            data: {
                title: 'Stream Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'StreamPenjurianDirController as ctrl',
                    templateUrl: 'app/penjurian/views/stream-penjurian-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

        .state('app.streampenjurianenter', {
            url: '/penjurian/stream/enterprise',
            data: {
                title: 'Stream Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'StreamPenjurianEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/stream-penjurian-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

        .state('app.dockinginternal', {
            url: '/penjurian/docking/unit',
            data: {
                title: 'Docking Internal'
            },
            views: {
                "content@app": {
                    controller: 'DockingInternalController as ctrl',
                    templateUrl: 'app/penjurian/views/docking-internal.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.dockinginternaldir', {
            url: '/penjurian/docking/direktorat',
            data: {
                title: 'Docking Internal'
            },
            views: {
                "content@app": {
                    controller: 'DockingInternalDirController as ctrl',
                    templateUrl: 'app/penjurian/views/docking-internal-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.dockinginternalenter', {
            url: '/penjurian/docking/enterprise',
            data: {
                title: 'Docking Internal'
            },
            views: {
                "content@app": {
                    controller: 'DockingInternalEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/docking-internal-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.valuecreationEnter', {
            url: '/penjurian/valuecreation/enterprise',
            data: {
                title: 'Value Creation'
            },
            views: {
                "content@app": {
                    controller: 'ValueCreationEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/value-creation-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.valuecreationDir', {
            url: '/penjurian/valuecreation/direktorat',
            data: {
                title: 'Value Creation'
            },
            views: {
                "content@app": {
                    controller: 'ValueCreationDirController as ctrl',
                    templateUrl: 'app/penjurian/views/value-creation-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.valuecreation', {
            url: '/penjurian/valuecreation',
            data: {
                title: 'Value Creation'
            },
            views: {
                "content@app": {
                    controller: 'ValueCreationController as ctrl',
                    templateUrl: 'app/penjurian/views/value-creation.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.koefisienpenjurian', {
            url: '/penjurian/koefisien/unit',
            data: {
                title: 'Koefisien Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'KoefisienPenjurianController as ctrl',
                    templateUrl: 'app/penjurian/views/koefisien-penjurian.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.koefisienpenjuriandir', {
            url: '/penjurian/koefisien/direktorat',
            data: {
                title: 'Koefisien Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'KoefisienPenjurianDirController as ctrl',
                    templateUrl: 'app/penjurian/views/koefisien-penjurian-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

        .state('app.koefisienpenjurianenter', {
            url: '/penjurian/koefisien/enterprise',
            data: {
                title: 'Koefisien Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'KoefisienPenjurianEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/koefisien-penjurian-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

        .state('app.rekapitulasipenjurian', {
            url: '/penjurian/rekapitulasi',
            data: {
                title: 'Summary Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'SummaryPenjurianController as ctrl',
                    templateUrl: 'app/penjurian/views/summary-penjurian.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.rekapitulasipenjuriandetil', {
            url: '/penjurian/rekapitulasidetil',
            data: {
                title: 'Detil Rekapitulasi Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'RekapitulasiPenjurianDetilController as ctrl',
                    templateUrl: 'app/penjurian/views/rekapitulasi-penjurian-detil.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
        .state('app.rekapitulasipenjuriandir', {
            url: '/penjurian/rekapitulasi/direktorat',
            data: {
                title: 'Summary Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'SummaryPenjurianDirController as ctrl',
                    templateUrl: 'app/penjurian/views/summary-penjurian-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

        .state('app.rekapitulasipenjurianenter', {
            url: '/penjurian/rekapitulasi/enterprise',
            data: {
                title: 'Summary Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'SummaryPenjurianEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/summary-penjurian-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })

        .state('app.rekapitulasipenjuriandetildir', {
            url: '/penjurian/rekapitulasi/detil/direktorat',
            data: {
                title: 'Detil Rekapitulasi Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'RekapitulasiPenjurianDetilDirController as ctrl',
                    templateUrl: 'app/penjurian/views/rekapitulasi-penjurian-detil-dir.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        })
        .state('app.rekapitulasipenjuriandetilenter', {
            url: '/penjurian/rekapitulasi/detil/enterprise',
            data: {
                title: 'Detil Rekapitulasi Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'RekapitulasiPenjurianDetilEnterController as ctrl',
                    templateUrl: 'app/penjurian/views/rekapitulasi-penjurian-detil-enter.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ]);

                }
            }
        });
});
