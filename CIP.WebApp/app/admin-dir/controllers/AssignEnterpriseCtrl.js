'use strict';

angular.module('app.admindir').controller('AssignEnterpriseController', function ($scope, $http, $state, $stateParams, $filter, $window, $compile, authSvc, DTOptionsBuilder, DTColumnBuilder, BASE_API, Language, Select2Helper) {
    var me = $scope;
    var module = 'registrasicip/';
    var api = BASE_API + module;
    var msgTitle = "Data Peserta";
    var vm = this;
    var isheaderCompiled = false;

    me.init = function () {
        me.data = {};
        me.filter = {};
        vm.selected = {};
        vm.selectAll = false;
        vm.toggleAll = toggleAll;
        vm.toggleOne = toggleOne;
        vm.listSelectedData = [];
        vm.listSelected = [];


        me.LoadCombo();

        vm.tableOptions = me.getOptionTable('DataTableForAssignToEnter');
        vm.tableColumns = me.getColumnTable('DataTableForAssignToEnter');
        vm.dtInstance = {};

        vm.tableOptions2 = me.getOptionTable('DataTableAssignEnter');
        vm.tableColumns2 = me.getColumnTable('DataTableAssignEnter');
        vm.dtInstance2 = {};
    };

    me.getOptionTable = function (method) {
        var opts = DTOptionsBuilder
            .newOptions()
            .withOption('ajax', me.dataSource(method))
            .withDataProp('data')
            .withOption('processing', false)
            .withOption('serverSide', true)
            .withOption('fnPreDrawCallback', ShowLoader)
            .withOption('fnDrawCallback', HideLoader)

            .withOption('responsive', true)
            .withOption('scrollX', true)
            .withOption('scrollY', true)
            .withOption('scrollCollapse', true)
            .withOption('autoWidth', false)
            .withOption('height', 200)
            .withOption('colReorder', true)
            .withOption('order', [1, 'desc'])

            .withPaginationType('full_numbers')
            //Add Bootstrap compatibility
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            .withBootstrap()
            .withLanguageSource(Language.getLanguagePath())

        if (method === "DataTableForAssignToEnter") {
            opts.withOption('createdRow', function (row, data, dataIndex) {
                // Recompiling so we can bind Angular directive to the DT
                $compile(angular.element(row).contents())($scope);
            });
            opts.withOption('headerCallback', function (header) {
                if (!vm.headerCompiled) {
                    // Use this headerCompiled field to only compile header once
                    vm.headerCompiled = true;
                    $compile(angular.element(header).contents())($scope);
                }
            });
        }

        if (method === "DataTableAssignEnter") {
            opts.withFixedColumns({
                leftColumns: 2,
                rightColumns: 1
            });
        }

        return opts;
    }

    me.getColumnTable = function (method) {
        var titleHtml = '<input type="checkbox" ng-model="ctrl.selectAll"' +
            'ng-click="ctrl.toggleAll(ctrl.selectAll, ctrl.selected)" style="min-width: 60px !important;">';

        var cols = [];
        if (method === "DataTableForAssignToEnter") {
            cols.push(
                DTColumnBuilder.newColumn(null).withTitle(titleHtml).withClass('text-center').notSortable()
                    .renderWith(function (data, type, full, meta) {
                        //vm.selected[full.idx] = false;
                        var idx = full.idx;
                        if (vm.selected.hasOwnProperty(idx)) {
                            vm.selected[idx] = vm.selected[idx];
                        }
                        else {
                            vm.selected[idx] = false;
                        }

                        var obj = {
                            idx: idx,
                            CipDirektorat: full.CipDirektorat
                        };

                        vm.listSelectedData.push(obj);

                        return '<input type="checkbox" ng-model="ctrl.selected[' + data.idx + ']" ng-click="ctrl.toggleOne(ctrl.selected)">';
                    }),
                DTColumnBuilder.newColumn('idx').withTitle('Index').withClass('text-primary').notVisible(),
                DTColumnBuilder.newColumn('CipDirektorat').withTitle('No. Gugus').withClass('text-primary'),
            );
        }
        else {
            cols.push(
                DTColumnBuilder.newColumn('CipDirektorat').withTitle('No. Gugus Direktorat').withClass('text-primary'),
                DTColumnBuilder.newColumn('CipEnterprise').withTitle('No. Gugus Enterprise').withClass('text-primary'),
            );
        }

        cols.push(
            DTColumnBuilder.newColumn('JenisDes').withTitle('Jenis'),
            DTColumnBuilder.newColumn('NamaGugus').withTitle('Nama Gugus'),
            DTColumnBuilder.newColumn('JudulCIP').withTitle('Judul CIP'),
            DTColumnBuilder.newColumn('Direktorat').withTitle('Direktorat').notVisible(),
            DTColumnBuilder.newColumn('Unit').withTitle('Unit').notVisible(),
            DTColumnBuilder.newColumn('SubUnitDes').withTitle('Sub Unit'),
            DTColumnBuilder.newColumn('Tahun').withTitle('Tahun').notVisible()
        );

        if (method === "DataTableAssignEnter") {
            cols.push(
                DTColumnBuilder.newColumn(null).withTitle('Aksi').withClass('text-center').renderWith(function (data, type, full, meta) {
                    var disabled = '';
                    return '<button type="button" class="btn btn-danger btn-xs"' + disabled + ' onclick="angular.element(this).scope().undo(\'' + data.CipDirektorat + '\')"><i class="fa fa-remove"></i>&nbspBatalkan</button>';
                })
            );
        }

        return cols;
    };

    function cekNullOrEmpty(val) {
        return isNullOrEmpty(val);
    }

    function toggleAll(selectAll, selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                selectedItems[id] = selectAll;
            }
        }
    }

    function toggleOne(selectedItems) {
        for (var id in selectedItems) {
            if (selectedItems.hasOwnProperty(id)) {
                if (!selectedItems[id]) {
                    vm.selectAll = false;
                    return;
                }
            }
        }
        vm.selectAll = true;
    }

    me.dataSource = function (method) {
        me.filter.Tahun = me.filter.Tahun || new Date().getFullYear();
        //me.filter.Unit = me.filter.Unit || '~';
        me.filter.Direktorat = me.filter.Direktorat || '~';

        return {
            url: api + method + '?tahun=' + me.filter.Tahun + '&unit=&direktorat=' + me.filter.Direktorat,
            type: 'POST',
            accepts: "application/json",
            headers: authSvc.headers(),
            error: function (xhr, ajaxOptions, thrownError) {
                NotifBoxErrorTable(msgTitle, xhr.responseText.Message, xhr.status, $state, authSvc);
            }
        };
    };

    me.reloadData = function () {
        vm.dtInstance.changeData(me.dataSource('DataTableForAssignToEnter'));
        vm.tableOptions.withOption('createdRow', function (row, data, dataIndex) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        });
        vm.tableOptions.withOption('headerCallback', function (header) {
            vm.headerCompiled = undefined;
            if (!vm.headerCompiled) {
                // Use this headerCompiled field to only compile header once
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        });

        vm.dtInstance2.changeData(me.dataSource('DataTableAssignEnter'));
    };

    me.LoadCombo = function () {
        Select2Helper.GetDataForCombo(BASE_API + 'masterperiode/dropdown').then(function (result) {
            me.listTahun = result.Dropdown;

            setTimeout(function () {
                var tahun = isNullOrEmpty($stateParams.tahun) ? new Date().getFullYear() : $stateParams.tahun;
                tahun = $filter('filter')(me.listTahun, { id: tahun }, false);
                if (tahun.length > 0) {
                    tahun[0].obj = angular.copy(tahun[0]);
                    $('#FilterTahun').select2('data', tahun[0]);
                }
            }, 2000);

        });

        Select2Helper.GetDataForCombo(BASE_API + 'masterdirektorat/dropdownbyuser').then(function (result) {
            me.listDirektorat = result.Dropdown;

            if (me.listDirektorat.length === 1) {
                me.filter.Direktorat = me.listDirektorat[0].value;
                setTimeout(function () {
                    me.listDirektorat[0].obj = angular.copy(me.listDirektorat[0]);
                    $('#Direktorat').select2('data', me.listDirektorat[0]);
                }, 2000);
            }
            else {
                me.filter.Direktorat = '';
            }

        });
    };

    me.exportExcel = function () {
        me.filter.Tahun = me.filter.Tahun || new Date().getFullYear();
        $http.get(api + 'GeneratePeserataCIPExcel?periode=' + me.filter.Tahun, { responseType: 'arraybuffer' }).success(function (response) {
            if (response !== null) {
                if (response.Message === undefined) {
                    var blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                    saveAs(blob, 'Daftar_Peserta_CIP_' + me.filter.Tahun + '.xlsx');
                }
                else {
                    NotifBoxError("Export " + msgTitle, response.Message || "");
                }
            }
        }).error(function (err, status) {
            NotifBoxError(msgTitle, err.Message);
        });
    };


    me.refreshGugusDirektorat = function () {
        me.filter.Tahun = me.filter.Tahun || new Date().getFullYear();
        //me.filter.Unit = me.filter.Unit || '~';
        me.filter.Direktorat = me.filter.Direktorat || '~';

        //vm.dtInstance.DataTable.search('2017').draw();
        vm.dtInstance.reloadData(function () { }, true);
    };

    me.refreshGugusEnterprise = function () {
        me.filter.Tahun = me.filter.Tahun || new Date().getFullYear();
        //me.filter.Unit = me.filter.Unit || '~';
        me.filter.Direktorat = me.filter.Direktorat || '~';

        //vm.dtInstance.DataTable.search('2017').draw();
        vm.dtInstance2.reloadData(function () { }, true);
    };

    me.submit = function () {
        var listData = [];
        vm.listSelected = [];
        var i = 1;
        angular.forEach(vm.selected, function (v, k) {
            if (v) {
                var selectedData = $filter("filter")(vm.listSelectedData, { idx: k }, false);
                if (selectedData.length > 0) {
                    var obj = {
                        CipDirektorat: selectedData[0].CipDirektorat
                    };
                    vm.listSelected.push(obj);

                    listData.push(i + ". " + selectedData[0].CipDirektorat);
                    i++;
                }
            }
        });

        if (vm.listSelected.length > 0) {
            var noGugus = listData.join('\r\n');
            var content = '<div>Yakin daftarkan Gugus Direktorat berikut ke Enterprise?' +
                '<pre style="max-height: 200px;">' + noGugus +'</pre>' +
            '</div>';

            $.SmartMessageBox({
                title: "Submit",
                content: content,
                buttons: '[OK][Batal]',
                theme: 'bg-success'
            }, function (action) {
                if (action === "OK") {
                    $http.post(api + "createasigncipEnter", vm.listSelected)
                        .success(function (result) {
                            if (result.Success) {
                                vm.selected = {};
                                vm.selectAll = false;
                                vm.toggleAll = toggleAll;
                                vm.toggleOne = toggleOne;
                                vm.listSelectedData = [];
                                vm.listSelected = [];
                                me.refreshGugusDirektorat();
                                NotifBoxSuccess("Daftarkan Gugus Direktorat ke Enterprise", result.Message);
                            }
                            else {
                                NotifBoxWarning("Daftarkan Gugus Direktorat ke Enterprise", result.Message);
                            }
                        })
                        .error(function (error, status) {
                            NotifBoxError("Daftarkan Gugus Direktorat ke Enterprise", status + " - " + error.Message);
                        });
                }
                else {
                    return;
                }
            });
        }
        else {
            NotifBoxWarning("Daftarkan Gugus Direktorat ke Enterprise", "Tidak ada Gugus Unit yang dipilih. Silahkan pilih dahulu Gugus Direktorat.");
        }
    };

    me.undo = function (cipDirektorat) {
        $.SmartMessageBox({
            title: "Batalkan",
            content: 'Yakin Batalkan Gugus Direktorat "' + cipDirektorat + '" ke Enterprise?',
            buttons: '[OK][Batal]',
            theme: 'bg-danger'
        }, function (action) {
            if (action === "OK") {
                $http.post(api + "undoasigncipEnter?cipDirektorat=" + cipDirektorat)
                    .success(function (result) {
                        if (result.Success) {
                            me.refreshGugusEnterprise();
                            NotifBoxSuccess("Batalkan Gugus Direktorat ke Enterprise", result.Message);
                        }
                        else {
                            NotifBoxWarning("Batalkan Gugus Direktorat ke Enterprise", result.Message);
                        }
                    })
                    .error(function (error, status) {
                        NotifBoxError("Batalkan Gugus Direktorat ke Enterprise", status + " - " + error.Message);
                    });
            }
            else {
                return;
            }
        });
    };

    me.init();
});