"use strict";


angular.module('app.admindir', ['ui.router', 'datatables', 'datatables.bootstrap', 'datatables.fixedcolumns', 'datatables.fixedheader']);

angular.module('app.admindir').config(function ($stateProvider) {

    $stateProvider
        .state('app.admindir', {
            abstract: true,
            data: {
                title: 'Admin Direktorat'
            }
        })

        .state('app.admindir.daftarpeserta', {
            url: '/admin-direktorat/daftar-peserta',
            data: {
                title: 'Daftar Peserta'
            },
            params: {
                statusRegistrasi: -1,
                tahun: ''
            },
            views: {
                "content@app": {
                    controller: 'PesertaCipDirController as ctrl',
                    templateUrl: 'app/admin-dir/views/daftar-peserta.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })

        .state('app.admindir.verifkasipeserta', {
            url: '/admin-direktorat/verifikasi-peserta',
            data: {
                title: 'Verifikasi Peserta & Submit ke Penjurian'
            },
            views: {
                "content@app": {
                    controller: 'VerifikasiPesertaCIPDirController as ctrl',
                    templateUrl: 'app/admin-dir/views/verifikasi-peserta-cip.html'
                }
            }
            , resolve: {
                srcipts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js'
                    ])

                }
            }
        })
});
