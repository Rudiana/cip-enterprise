﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterKategori")]
    public class MasterKategoriController : BaseApiController<MasterKategori>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterKategori> genRepo;

        public MasterKategoriController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterKategori>(ctx);

            base.repo = genRepo;
        }
    }
}
