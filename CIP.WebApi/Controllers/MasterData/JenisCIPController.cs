﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterJenisCIP")]
    public class MasterJenisCIPController : BaseApiController<MasterJenisCIP>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterJenisCIP> genRepo;

        public MasterJenisCIPController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterJenisCIP>(ctx);

            base.repo = genRepo;
        }
    }
}
