﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Http;
using System.Linq;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterDirektorat")]
    public class MasterDirektoratController : BaseApiController<MasterDirektorat>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterDirektorat> genRepo;

        public MasterDirektoratController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterDirektorat>(ctx);

            base.repo = genRepo;
        }

        [HttpGet]
        public IGenericWebApiResult Dropdown()
        {
            try
            {
                using (var result = new GenericWebApiResult<MasterDirektorat>())
                {
                    result.Dropdown = genRepo.Dropdown("");
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<MasterDirektorat>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByUser()
        {
            try
            {
                using (var result = new GenericWebApiResult<MasterDirektorat>())
                {
                    List<Dropdown> dropdown = new List<Dropdown>();
                    dropdown = ctx.MasterDirektorat.Where(x => x.Kode == (repo.UserProfile.IsAdministrator ? x.Kode : repo.UserProfile.Direktorat)).Select(x => new Dropdown
                    {
                        id = x.Kode,
                        value = x.Kode,
                        text = x.Deskripsi
                    }).ToList();
                    result.Dropdown = dropdown;
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<MasterDirektorat>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }
    }
}
