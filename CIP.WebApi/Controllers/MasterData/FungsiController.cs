﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterFungsi")]
    public class MasterFungsiController : BaseApiController<MasterFungsi>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterFungsi> genRepo;

        public MasterFungsiController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterFungsi>(ctx);

            base.repo = genRepo;
        }
    }
}
