﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Http;
using System.Linq;
using System.Net.Http;
using System.Web;
using DataTablesParser;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterSubUnit")]
    public class MasterSubUnitController : BaseApiController<MasterSubUnit>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterSubUnit> genRepo;

        public MasterSubUnitController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterSubUnit>(ctx);

            base.repo = genRepo;
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByUnit(string unit)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = p_DropdownByUnit(unit);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByUnitUser()
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = p_DropdownByUnit(repo.UserProfile.Unit);

                    dynamic more = new ExpandoObject();
                    more.UnitDes = repo.UserProfile.UnitDes;
                    more.DirektoratDes = repo.UserProfile.DirektoratDes;
                    result.More = more;

                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                IQueryable<MasterSubUnitDto> queryable;
                using (var ctx = new GenericContext())
                {
                    queryable = (from a in ctx.MasterSubUnit.ToList()
                                 join b in ctx.MasterUnit.ToList() on a.Unit equals b.Kode into _b
                                 from b in _b.DefaultIfEmpty()
                                 join c in ctx.MasterDirektorat on b.Direktorat equals c.Kode into _c
                                 from c in _c.DefaultIfEmpty()
                                 select new MasterSubUnitDto
                                 {
                                     Kode = a.Kode,
                                     Deskripsi = a.Deskripsi,
                                     Direktorat = c != null ? c.Kode : "",
                                     DirektoratDes = c != null ? c.Deskripsi : "",
                                     Unit = a.Unit,
                                     UnitDes = b != null ? b.Deskripsi : ""
                                 }).AsQueryable();

                    var request = HttpContext.Current.Request;
                    var wrapper = new HttpRequestWrapper(request);
                    var parser = new DataTablesParser<MasterSubUnitDto>(wrapper, queryable);
                    var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, parser.Parse());

                    return result;
                }
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        #region === Private Methode ===
        private List<Dropdown> p_DropdownByUnit(string unit)
        {
            List<Dropdown> dropdown = new List<Dropdown>();
            dropdown = ctx.MasterSubUnit.Where(x => x.Unit == unit).Select(x => new Dropdown
            {
                id = x.Kode.ToString(),
                value = x.Kode.ToString(),
                text = x.Deskripsi
            }).ToList();

            return dropdown;
        }
#endregion
    }
}
