﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;
using CIP.Core.Repositories.Abstractions.AdminQA;
using CIP.Core.Repositories.Concretes.AdminQA;
using System.Net.Http;
using System;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Common;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterUnit")]
    public class MasterUnitController : BaseApiController<MasterUnit>
    {
        private GenericContext ctx;
        private IUnitRepository genRepo;

        public MasterUnitController()
        {
            ctx = new GenericContext();
            genRepo = new UnitRepository(ctx);

            base.repo = genRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = genRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpGet]
        public IGenericWebApiResult Dropdown()
        {
            try
            {
                using (var result = new GenericWebApiResult<MasterUnit>())
                {
                    result.Dropdown = genRepo.Dropdown("");
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<MasterUnit>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByDirektorat(string direktorat)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = p_DropdownByDirektorat(direktorat);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByDirektoratUser()
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = p_DropdownByDirektorat(repo.UserProfile.Direktorat);

                    dynamic more = new ExpandoObject();
                    more.UnitDes = repo.UserProfile.UnitDes;
                    more.DirektoratDes = repo.UserProfile.DirektoratDes;
                    result.More = more;

                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        private List<Dropdown> p_DropdownByDirektorat(string direktorat)
        {
            List<Dropdown> dropdown = new List<Dropdown>();
            dropdown = ctx.MasterUnit.Where(x => x.Direktorat == direktorat && x.Kode == (repo.UserProfile.IsAdministrator ? x.Kode : repo.UserProfile.Unit)).Select(x => new Dropdown
            {
                id = x.Kode,
                value = x.Kode,
                text = x.Deskripsi
            }).ToList();

            return dropdown;
        }
    }
}
