﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterStatusAnggota")]
    public class MasterStatusAnggotaController : BaseApiController<MasterStatusAnggota>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterStatusAnggota> genRepo;

        public MasterStatusAnggotaController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterStatusAnggota>(ctx);

            base.repo = genRepo;
        }
    }
}
