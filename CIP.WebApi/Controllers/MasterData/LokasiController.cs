﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterLokasi")]
    public class MasterLokasiController : BaseApiController<MasterLokasi>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterLokasi> genRepo;

        public MasterLokasiController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterLokasi>(ctx);

            base.repo = genRepo;
        }
    }
}
