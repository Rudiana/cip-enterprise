﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterKriteria")]
    public class MasterKriteriaController : BaseApiController<MasterKriteria>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterKriteria> genRepo;

        public MasterKriteriaController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterKriteria>(ctx);

            base.repo = genRepo;
        }
    }
}
