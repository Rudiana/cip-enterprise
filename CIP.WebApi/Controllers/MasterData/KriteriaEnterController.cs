﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System.Web.Http;

namespace CIP.WebApi.Controllers.MasterData.StructureOrganization
{
    [RoutePrefix(@"api/MasterKriteriaEnter")]
    public class MasterKriteriaEnterController : BaseApiController<MasterKriteriaEnter>
    {
        private GenericContext ctx;
        private IGenericDataRepository<MasterKriteriaEnter> genRepo;

        public MasterKriteriaEnterController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<MasterKriteriaEnter>(ctx);

            base.repo = genRepo;
        }
    }
}
