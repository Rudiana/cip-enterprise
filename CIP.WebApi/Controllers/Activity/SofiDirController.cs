﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.Core.Repositories.Abstractions;
using System.Dynamic;
using System.Collections.Generic;
using CIP.Core.Common;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http.Headers;
using DataTablesParser;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/SofiDir")]
    public class SofiDirController : BaseApiController<PenjurianDetailDir>
    {
        private GenericContext ctx;
        private ISofiDirRepository objRepo;

        public SofiDirController()
        {
            ctx = new GenericContext();
            objRepo = new SofiDirRepository(ctx);
            base.repo = objRepo;
        }


        [HttpGet]
        public HttpResponseMessage GetSofi(int periode, string stream)
        {
            try
            {
                var datatable = objRepo.GetSofi(periode, stream);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetSofiDetail(string KodeRegistrasi)
        {
            try
            {
                var datatable = objRepo.GetSofiDetail(KodeRegistrasi);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult SetSofi(HttpRequestMessage request)
        {
            try
            {
                using (var result = new GenericWebApiResult<List<SofiDetail>>())
                {

                    var content = request.Content;
                    string jsonContent = content.ReadAsStringAsync().Result;
                    List<SofiDetail> model = JsonConvert.DeserializeObject<List<SofiDetail>>(jsonContent);

                    result.Success = objRepo.SetSofi(model);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<List<SofiDetail>>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }


    }


}
