﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CIP.Core.Repositories.Concretes.Activity;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/TemplateAuditPDCA")]
    public class TemplateAuditPDCAController : BaseApiController<TemplateAuditPDCA>
    {
        private GenericContext ctx;
        private ITemplatePdcaRepository genRepo;

        public TemplateAuditPDCAController()
        {
            ctx = new GenericContext();
            genRepo = new TemplatePdcaRepository(ctx);
            base.repo = genRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = genRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

    }
}
