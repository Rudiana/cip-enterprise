﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.Core.Repositories.Abstractions;
using System.Dynamic;
using System.Collections.Generic;
using CIP.Core.Common;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http.Headers;
using DataTablesParser;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/ValueCreationEnter")]
    public class ValueCreationEnterController : BaseApiController<PenjurianHeaderEnter>
    {
        private GenericContext ctx;
        private IValueCreationEnterRepository objRepo;

        public ValueCreationEnterController()
        {
            ctx = new GenericContext();
            objRepo = new ValueCreationEnterRepository(ctx);
            base.repo = objRepo;
        }

       
        [HttpGet]
        public HttpResponseMessage GetValueCreation(int periode, string stream)
        {
            try
            {
                var datatable = objRepo.GetValueCreation(periode, stream);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }


        [HttpPost]
        public IGenericWebApiResult SetValueCreation(HttpRequestMessage request)
        {
            try
            {
                using (var result = new GenericWebApiResult<ValueCreationEnter>())
                {

                    var content = request.Content;
                    string jsonContent = content.ReadAsStringAsync().Result;
                    ValueCreationEnter model = JsonConvert.DeserializeObject<ValueCreationEnter>(jsonContent);

                    result.Success = objRepo.SetValueCreation(model);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<PenjurianEnter>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public HttpResponseMessage GetValueCreationExcelReport(int tahun, string stream)
        {
            var BytesFile = ExcelGenerator.GenerateValueCreateionExcelEnter(tahun, objRepo.UserProfile.Direktorat, stream);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "ValueCreation.xlsx";
            return result;
        }

    }
}
