﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.Core.Repositories.Abstractions;
using System.Dynamic;
using System.Collections.Generic;
using CIP.Core.Common;
using System.Net;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http.Headers;
using DataTablesParser;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/StreamPenjurianEnter")]
    public class StreamPenjurianEnterController : BaseApiController<PenjurianHeaderEnter>
    {
        private GenericContext ctx;
        private IStreamPenjurianEnterRepository objRepo;

        public StreamPenjurianEnterController()
        {
            ctx = new GenericContext();
            objRepo = new StreamPenjurianEnterRepository(ctx);
            base.repo = objRepo;
        }

        
        [HttpGet, Route("getNilai")]
        public HttpResponseMessage GetNilaiPenjurian(string stream, int tahun)
        {
            try
            {

                var datatable = objRepo.GetNilaiPenjurian(stream, tahun);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        
        [HttpGet]
        public HttpResponseMessage GetStreamExcelReport(int periode,string stream)
        {
            var BytesFile = ExcelGenerator.GenerateStreamExcelEnter(periode, stream);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Stream.xlsx";

            return result;
        }

        
    }
}
