﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.WebApi.Lamp.Controllers;
using Newtonsoft.Json;
using Spire.Pdf;
using Spire.Pdf.Graphics;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Activity
{
    [Authorize]
    [RoutePrefix(@"api/RegistrasiCIP")]
    public class RegistrasiCIPController : BaseApiController<RegistrasiCIP>
    {
        private GenericContext ctx;
        private IRegistrasiCIPRepository objRepo;

        public RegistrasiCIPController()
        {
            ctx = new GenericContext();
            objRepo = new RegistrasiCIPRepository(ctx);

            base.repo = objRepo;
        }

        #region === Private Method ===
        private IGenericWebApiResult p_DownloadRisalah(string fileName)
        {
            try
            {
                using (var result = new GenericWebApiResult<string>())
                {
                    if (string.IsNullOrEmpty(fileName))
                    {
                        throw new Exception("Dokumen Risalah tidak ditemukan.");
                    }

                    string s = fileName;
                    int i = s.LastIndexOf('.');
                    string fName = i < 0 ? s : s.Substring(0, i),
                        ext = i < 0 ? "" : s.Substring(i + 1);

                    if (ext.ToLower() == "pdf")
                    {
                        var dir = HttpContext.Current.Server.MapPath("~/Uploads");
                        var fileUpload = Path.Combine(dir, fileName);

                        if (File.Exists(fileUpload))
                        {
                            dir = HttpContext.Current.Server.MapPath("~/Downloads");
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }

                            var fileDownload = Path.Combine(dir, fileName);
                            if (File.Exists(fileUpload))
                            {
                                File.Delete(fileDownload);
                            }

                            PdfDocument doc = new PdfDocument();
                            doc.LoadFromFile(@"" + fileUpload);
                            var watermark = string.Format("Dokumen Tidak Terkendali, di download oleh: {0}{1} pada {2}", Environment.NewLine, objRepo.UserProfile.Nama, DateTime.Now.ToString("dd MMM yyyy HH:mm:ss"));
                            int n = 1;
                            foreach (PdfPageBase page in doc.Pages)
                            {
                                PdfTilingBrush brush
                                   = new PdfTilingBrush(new SizeF(page.Canvas.ClientSize.Width / 2, page.Canvas.ClientSize.Height / 2));
                                //brush.Graphics.SetTransparency(0.2f);
                                brush.Graphics.SetTransparency(0.3f);
                                brush.Graphics.Save();
                                brush.Graphics.TranslateTransform(brush.Size.Width / 2, brush.Size.Height / 2);
                                brush.Graphics.RotateTransform(-35);
                                brush.Graphics.DrawString(watermark,
                                    new PdfFont(PdfFontFamily.Helvetica, 12), PdfBrushes.Red, 0, 0,
                                    new PdfStringFormat(PdfTextAlignment.Center));
                                brush.Graphics.Restore();
                                page.Canvas.DrawRectangle(brush, new RectangleF(new PointF(0, 0), page.Canvas.ClientSize));

                                if (n == 1)
                                {
                                    PdfBrush brushSolid = new PdfSolidBrush(Color.Red);
                                    page.Canvas.DrawRectangle(brushSolid, new RectangleF(new Point(0, 0), new SizeF(page.Canvas.ClientSize.Width, 15)));
                                }

                                n++;
                            }
                            doc.SaveToFile(fileDownload);

                            result.Data = fileName;
                            result.Success = true;

                            return result;
                        }
                        else
                        {
                            throw new Exception("Dokumen Risalah tidak ditemukan.");
                        }
                    }
                    else
                    {
                        throw new Exception("Dokumen Risalah harus *.PDF");
                    }
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }
        #endregion

        [HttpPost, Route("register")]
        public IGenericWebApiResult Register(ParamHeaderDetail<RegistrasiCIP, AnggotaCIP> data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    var model = data.header;
                    var anggotaCIP = data.detail;
                    result.Success = objRepo.Register(model, anggotaCIP);
                    result.Data = model;
                    result.Message = BaseConstants.MESSAGE_REGISTRATION_SUCCESS;


                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost, Route("UpdateWithDetail")]
        public IGenericWebApiResult UpdateWithDetail(ParamHeaderDetail<RegistrasiCIP, AnggotaCIP> data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    var model = data.header;
                    var anggotaCIP = data.detail;
                    result.Success = objRepo.UpdateWithDetail(model, anggotaCIP);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost, Route("UpdateWithDetailDir")]
        public IGenericWebApiResult UpdateWithDetailDir(ParamHeaderDetail<AsignCipDir, AnggotaCIPDir> data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    var model = data.header;
                    var anggotaCIP = data.detail;
                    result.Success = objRepo.UpdateWithDetailDir(model, anggotaCIP);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }


        [HttpPost, Route("UpdateWithDetailEnter")]
        public IGenericWebApiResult UpdateWithDetailEnter(ParamHeaderDetail<AsignCipEnter, AnggotaCIPEnter> data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    var model = data.header;
                    var anggotaCIP = data.detail;
                    result.Success = objRepo.UpdateWithDetailEnter(model, anggotaCIP);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablePeserta(int statReg)
        {
            try
            {
                var datatable = objRepo.DataTablePeserta(statReg);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablePenjurianSofi(string koderegistrasi)
        {
            try
            {
                var datatable = objRepo.DataTablePenjurianSofi(koderegistrasi);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablePenjurianSofiDir(string koderegistrasi)
        {
            try
            {
                var datatable = objRepo.DataTablePenjurianSofiDir(koderegistrasi);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablePenjurianSofiEnter(string koderegistrasi)
        {
            try
            {
                var datatable = objRepo.DataTablePenjurianSofiEnter(koderegistrasi);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        public HttpResponseMessage DataTableVerifikasiPeserta()
        {
            try
            {
                var datatable = objRepo.DataTableVerifikasiPeserta();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        public HttpResponseMessage DataTableVerifikasiPesertaDir()
        {
            try
            {
                var datatable = objRepo.DataTableVerifikasiPesertaDir();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        public HttpResponseMessage DataTableVerifikasiPesertaEnter()
        {
            try
            {
                var datatable = objRepo.DataTableVerifikasiPesertaEnter();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        public HttpResponseMessage DataTableRegisterLog(string kodeRegistrasi)
        {
            try
            {
                var datatable = objRepo.DataTableRegisterLog(kodeRegistrasi);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownForAuditor([FromBody]int period)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownForAuditor(period);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult UpdateVerifikasiAdminQM(string kode, string status)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateVerifikasiAdminQM(kode, status);
                    result.Message = result.Success ? "Verifikasi Perserta CIP berhasil." : "Verifikasi Perserta CIP tidak berhasil.";

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult UpdateVerifikasiAdminDir(string kode, string status)
        {
            try
            {
                using (var result = new GenericWebApiResult<AsignCipDir>())
                {
                    result.Success = objRepo.UpdateVerifikasiAdminDir(kode, status);
                    result.Message = result.Success ? "Verifikasi Perserta CIP berhasil." : "Verifikasi Perserta CIP tidak berhasil.";

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AsignCipDir>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult UpdateVerifikasiAdminEnter(string kode, string status)
        {
            try
            {
                using (var result = new GenericWebApiResult<AsignCipDir>())
                {
                    result.Success = objRepo.UpdateVerifikasiAdminEnter(kode, status);
                    result.Message = result.Success ? "Verifikasi Perserta CIP berhasil." : "Verifikasi Perserta CIP tidak berhasil.";

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AsignCipDir>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult Activation(string kode, string x)
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    result.Success = objRepo.Activation(kode, x);
                    result.Message = result.Success ? BaseConstants.MESSAGE_ACTIVATION_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult SubmitRegister(RegistrasiCIP data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(data.Kode, BaseEnums.EnumRegistrationStatus.Pending_Approval_Admin_QM.ReplaceUnderScoreToSpace());
                    result.Data = data;
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult SubmitAdminQM(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(kode, BaseEnums.EnumRegistrationStatus.Wait_to_Audit_PDCA_I.ReplaceUnderScoreToSpace());
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult SubmitAdminDir(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatusDir(kode, BaseEnums.EnumRegistrationStatusDirektorat.Ready_to_Show_Direktorat.ReplaceUnderScoreToSpace());
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult SubmitAdminEnter(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatusEnter(kode, BaseEnums.EnumRegistrationStatusEnterprise.Ready_to_Show_Enterprise.ReplaceUnderScoreToSpace());
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        public IGenericWebApiResult StartPenjurian(RegistrasiCIP data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(data.Kode, BaseEnums.EnumRegistrationStatus.Forum_Presentasi_CIP.ReplaceUnderScoreToSpace());
                    result.Data = data;
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        public IGenericWebApiResult FinishPenjurian(RegistrasiCIP data)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Success = objRepo.UpdateStatus(data.Kode, BaseEnums.EnumRegistrationStatus.Result_Forum_CIP.ReplaceUnderScoreToSpace());
                    result.Data = data;
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownForStreamGugus([FromBody]int tahun)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownForStreamGugus(tahun);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownForStreamGugusDir([FromBody]int tahun)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownForStreamGugus(tahun);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost, Route("uploaddocument_ODL")]
        public HttpResponseMessage UploadDocument_OLD()
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    var forms = HttpContext.Current.Request.Form;
                    var formModel = forms.GetValues("model");
                    var docType = forms.GetValues("docType");

                    //var guidString = Guid.NewGuid().ToString();

                    var model = formModel.Length > 0 ? JsonConvert.DeserializeObject<RegistrasiCIP>(formModel[0]) : new RegistrasiCIP();
                    var registrasiCip = repo.GetSingle(model.Kode);
                    var docTypeName = docType.Length > 0 ? docType[0] : "";
                    var colDocType = "IsUpload" + docTypeName;

                    if (!string.IsNullOrEmpty(docTypeName))
                    {
                        if (registrasiCip.GetType().GetProperty(docTypeName) != null)
                        {
                            registrasiCip.GetType().GetProperty(docTypeName).SetValue(registrasiCip, model.GetType().GetProperty(docTypeName).GetValue(model), null);
                        }
                        else
                        {
                            if (registrasiCip.GetType().GetProperty("UploadPresentasi") != null)
                            {
                                registrasiCip.GetType().GetProperty("UploadPresentasi").SetValue(registrasiCip, model.GetType().GetProperty(docTypeName).GetValue(model), null);
                            }
                            else
                            {
                                throw new Exception(string.Format("Kolom Berkas \"{0}\" tidak ditemukan.", docTypeName));
                            }
                        }

                        if (!string.IsNullOrEmpty(colDocType))
                        {
                            var dok = model.GetType().GetProperty(docTypeName).GetValue(model).ToString();
                            if (!string.IsNullOrEmpty(dok))
                            {
                                if (registrasiCip.GetType().GetProperty(colDocType) != null)
                                {
                                    registrasiCip.GetType().GetProperty(colDocType).SetValue(registrasiCip, true, null);
                                }
                                else
                                {
                                    if (registrasiCip.GetType().GetProperty("IsUploadPresentasi") != null)
                                    {
                                        registrasiCip.GetType().GetProperty("IsUploadPresentasi").SetValue(registrasiCip, true, null);
                                    }
                                    else
                                    {
                                        throw new Exception(string.Format("Kolom Berkas \"{0}\" tidak ditemukan.", colDocType));
                                    }
                                }
                            }
                        }
                    }

                    result.Success = objRepo.Update(registrasiCip);
                    result.Data = registrasiCip;
                    result.Message = result.Success ? "Upload Dokumen Berhasil." : BaseConstants.MESSAGE_INVALID_DATA;

                    if (result.Success)
                    {
                        var fileCount = HttpContext.Current.Request.Files.Count;
                        for (int i = 0; i < fileCount; i++)
                        {
                            HttpPostedFile file = HttpContext.Current.Request.Files[i];

                            //var fileName = Path.GetFileName(file.FileName);
                            //var fileExt = Path.GetExtension(file.FileName);
                            //var guid = Guid.NewGuid().ToString("n");

                            var dir = HttpContext.Current.Server.MapPath("~/Uploads");
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }

                            var fileName = string.Format("{0}_{1}_{2}", model.Tahun, docTypeName, file.FileName);
                            var path = Path.Combine(dir, fileName);
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }

                            file.SaveAs(path);
                        }
                    }

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost, Route("uploaddocument")]
        public HttpResponseMessage UploadDocument()
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    var forms = HttpContext.Current.Request.Form;
                    var formModel = forms.GetValues("model");
                    var docType = forms.GetValues("docType");

                    var model = formModel.Length > 0 ? JsonConvert.DeserializeObject<RegistrasiCIP>(formModel[0]) : new RegistrasiCIP();
                    var registrasiCip = repo.GetSingle(model.Kode);
                    var docTypeName = docType.Length > 0 ? docType[0] : "";
                    var fieldIsUpload = (docTypeName == "UploadPresentasi" ? "Is" : "IsUpload") + docTypeName;
                    var fieldUpdatedDate = docTypeName + "UpdatedDate";
                    var fileName = "{0}_{1}_{2}";

                    HttpPostedFile file = null;
                    var fileCount = HttpContext.Current.Request.Files.Count;
                    for (int i = 0; i < fileCount; i++)
                    {
                        file = HttpContext.Current.Request.Files[i];
                        var replaceFileName = docTypeName;
                        switch (docTypeName)
                        {
                            case "KOMET":
                                replaceFileName = "LampiranTambahan";
                                break;
                            case "Abster":
                                replaceFileName = "LampiranWajib";
                                break;
                            default:
                                break;
                        }
                        fileName = string.Format(fileName, model.Tahun, replaceFileName, file.FileName);
                    }

                    CultureInfo ci = new CultureInfo("en-US");
                    Thread.CurrentThread.CurrentCulture = ci;

                    var updatedDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(docTypeName))
                    {
                        if (registrasiCip.GetType().GetProperty(docTypeName) != null)
                        {
                            registrasiCip.GetType().GetProperty(docTypeName).SetValue(registrasiCip, fileName, null);
                            registrasiCip.GetType().GetProperty(fieldIsUpload).SetValue(registrasiCip, true, null);
                            registrasiCip.GetType().GetProperty(fieldUpdatedDate).SetValue(registrasiCip, updatedDate, null);
                        }
                        else
                        {
                            if (registrasiCip.GetType().GetProperty("UploadPresentasi") != null)
                            {
                                registrasiCip.GetType().GetProperty("UploadPresentasi").SetValue(registrasiCip, fileName, null);
                                registrasiCip.GetType().GetProperty("IsUploadPresentasi").SetValue(registrasiCip, true, null);
                                registrasiCip.GetType().GetProperty("UploadPresentasiUpdatedDate").SetValue(registrasiCip, updatedDate, null);
                            }
                            else
                            {
                                throw new Exception(string.Format("Kolom Berkas \"{0}\" tidak ditemukan.", docTypeName));
                            }
                        }
                    }

                    result.Success = objRepo.Update(registrasiCip);
                    dynamic more = new ExpandoObject();
                    more.FileName = fileName;
                    more.UpdatedDate = updatedDate.ToString("yyyy-MM-dd HH:mm:ss");
                    result.More = more;
                    result.Message = result.Success ? "Upload Dokumen Berhasil." : BaseConstants.MESSAGE_INVALID_DATA;

                    if (result.Success)
                    {
                        if (file != null)
                        {
                            var dir = HttpContext.Current.Server.MapPath("~/Uploads");
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }

                            var path = Path.Combine(dir, fileName);
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }

                            file.SaveAs(path);
                        }
                    }

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost, Route("uploaddocumentdir")]
        public HttpResponseMessage UploadDocumentDir()
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    var forms = HttpContext.Current.Request.Form;
                    var formModel = forms.GetValues("model");
                    var docType = forms.GetValues("docType");

                    var model = formModel.Length > 0 ? JsonConvert.DeserializeObject<AsignCipDir>(formModel[0]) : new AsignCipDir();
                    var registrasiCip = ctx.Set<AsignCipDir>().Where(x => x.Kode == model.Kode).FirstOrDefault();
                    var docTypeName = docType.Length > 0 ? docType[0] : "";
                    var fieldIsUpload = (docTypeName == "UploadPresentasi" ? "Is" : "IsUpload") + docTypeName;
                    var fieldUpdatedDate = docTypeName + "UpdatedDate";
                    var fileName = "DIR_{0}_{1}_{2}";

                    HttpPostedFile file = null;
                    var fileCount = HttpContext.Current.Request.Files.Count;
                    for (int i = 0; i < fileCount; i++)
                    {
                        file = HttpContext.Current.Request.Files[i];
                        var replaceFileName = docTypeName;
                        switch (docTypeName)
                        {
                            case "KOMET":
                                replaceFileName = "LampiranTambahan";
                                break;
                            case "Abster":
                                replaceFileName = "LampiranWajib";
                                break;
                            default:
                                break;
                        }
                        fileName = string.Format(fileName, model.Tahun, replaceFileName, file.FileName);
                    }

                    CultureInfo ci = new CultureInfo("en-US");
                    Thread.CurrentThread.CurrentCulture = ci;

                    var updatedDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(docTypeName))
                    {
                        if (registrasiCip.GetType().GetProperty(docTypeName) != null)
                        {
                            registrasiCip.GetType().GetProperty(docTypeName).SetValue(registrasiCip, fileName, null);
                            registrasiCip.GetType().GetProperty(fieldIsUpload).SetValue(registrasiCip, true, null);
                            registrasiCip.GetType().GetProperty(fieldUpdatedDate).SetValue(registrasiCip, updatedDate, null);
                        }
                        else
                        {
                            if (registrasiCip.GetType().GetProperty("UploadPresentasi") != null)
                            {
                                registrasiCip.GetType().GetProperty("UploadPresentasi").SetValue(registrasiCip, fileName, null);
                                registrasiCip.GetType().GetProperty("IsUploadPresentasi").SetValue(registrasiCip, true, null);
                                registrasiCip.GetType().GetProperty("UploadPresentasiUpdatedDate").SetValue(registrasiCip, updatedDate, null);
                            }
                            else
                            {
                                throw new Exception(string.Format("Kolom Berkas \"{0}\" tidak ditemukan.", docTypeName));
                            }
                        }
                    }


                    result.Success = ctx.SaveChanges() >= 0 ? true : false;
                    dynamic more = new ExpandoObject();
                    more.FileName = fileName;
                    more.UpdatedDate = updatedDate.ToString("yyyy-MM-dd HH:mm:ss");
                    result.More = more;
                    result.Message = result.Success ? "Upload Dokumen Berhasil." : BaseConstants.MESSAGE_INVALID_DATA;

                    if (result.Success)
                    {
                        if (file != null)
                        {
                            var dir = HttpContext.Current.Server.MapPath("~/Uploads");
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }

                            var path = Path.Combine(dir, fileName);
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }

                            file.SaveAs(path);
                        }
                    }

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost, Route("uploaddocumententer")]
        public HttpResponseMessage UploadDocumentEnter()
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    var forms = HttpContext.Current.Request.Form;
                    var formModel = forms.GetValues("model");
                    var docType = forms.GetValues("docType");

                    var model = formModel.Length > 0 ? JsonConvert.DeserializeObject<AsignCipEnter>(formModel[0]) : new AsignCipEnter();
                    var registrasiCip = ctx.Set<AsignCipEnter>().Where(x => x.Kode == model.Kode).FirstOrDefault();
                    var docTypeName = docType.Length > 0 ? docType[0] : "";
                    var fieldIsUpload = (docTypeName == "UploadPresentasi" ? "Is" : "IsUpload") + docTypeName;
                    var fieldUpdatedDate = docTypeName + "UpdatedDate";
                    var fileName = "ENTER_{0}_{1}_{2}";

                    HttpPostedFile file = null;
                    var fileCount = HttpContext.Current.Request.Files.Count;
                    for (int i = 0; i < fileCount; i++)
                    {
                        file = HttpContext.Current.Request.Files[i];
                        var replaceFileName = docTypeName;
                        switch (docTypeName)
                        {
                            case "KOMET":
                                replaceFileName = "LampiranTambahan";
                                break;
                            case "Abster":
                                replaceFileName = "LampiranWajib";
                                break;
                            default:
                                break;
                        }
                        fileName = string.Format(fileName, model.Tahun, replaceFileName, file.FileName);
                    }

                    CultureInfo ci = new CultureInfo("en-US");
                    Thread.CurrentThread.CurrentCulture = ci;

                    var updatedDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(docTypeName))
                    {
                        if (registrasiCip.GetType().GetProperty(docTypeName) != null)
                        {
                            registrasiCip.GetType().GetProperty(docTypeName).SetValue(registrasiCip, fileName, null);
                            registrasiCip.GetType().GetProperty(fieldIsUpload).SetValue(registrasiCip, true, null);
                            registrasiCip.GetType().GetProperty(fieldUpdatedDate).SetValue(registrasiCip, updatedDate, null);
                        }
                        else
                        {
                            if (registrasiCip.GetType().GetProperty("UploadPresentasi") != null)
                            {
                                registrasiCip.GetType().GetProperty("UploadPresentasi").SetValue(registrasiCip, fileName, null);
                                registrasiCip.GetType().GetProperty("IsUploadPresentasi").SetValue(registrasiCip, true, null);
                                registrasiCip.GetType().GetProperty("UploadPresentasiUpdatedDate").SetValue(registrasiCip, updatedDate, null);
                            }
                            else
                            {
                                throw new Exception(string.Format("Kolom Berkas \"{0}\" tidak ditemukan.", docTypeName));
                            }
                        }
                    }


                    result.Success = ctx.SaveChanges() >= 0 ? true : false;
                    dynamic more = new ExpandoObject();
                    more.FileName = fileName;
                    more.UpdatedDate = updatedDate.ToString("yyyy-MM-dd HH:mm:ss");
                    result.More = more;
                    result.Message = result.Success ? "Upload Dokumen Berhasil." : BaseConstants.MESSAGE_INVALID_DATA;

                    if (result.Success)
                    {
                        if (file != null)
                        {
                            var dir = HttpContext.Current.Server.MapPath("~/Uploads");
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }

                            var path = Path.Combine(dir, fileName);
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }

                            file.SaveAs(path);
                        }
                    }

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost, Route("sendnotificationuploaddocument")]
        public HttpResponseMessage SendNotificationUploadDocument(string kodeRegistrasi, string docType)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {

                    result.Success = objRepo.SendNotificationUploadDocument(kodeRegistrasi, docType);
                    result.Message = result.Success ? BaseConstants.MESSAGE_NOTIFICATION_UPLOAD_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriode(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriode(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeDir(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeDir(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeEnter(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeEnter(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }


        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeAnggota(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeAnggota(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeAnggotaDir(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeAnggotaDir(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeAnggotaEnter(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeAnggotaEnter(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }


        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeHistoryDir(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeHistoryDir(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByPeriodeHistoryEnter(int? periode)
        {
            try
            {
                var datatable = objRepo.DatatablesByPeriodeHistoryEnter(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpGet]
        public HttpResponseMessage GeneratePeserataCIPExcel(int? periode)
        {
            var BytesFile = ExcelGenerator.GeneratePeserataCIPExcel(objRepo, periode);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Daftar_Peserta_CIP_" + (periode.HasValue ? periode.Value.ToString() : "") + ".xlsx";

            return result;
        }

        [HttpGet]
        public HttpResponseMessage GeneratePeserataCIPExcelDir(int? periode)
        {
            var BytesFile = ExcelGenerator.GeneratePeserataCIPExcelDir(objRepo, periode);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Daftar_Peserta_CIP_DIR_" + (periode.HasValue ? periode.Value.ToString() : "") + ".xlsx";

            return result;
        }

        [HttpGet]
        public HttpResponseMessage GeneratePeserataCIPExcelEnter(int? periode)
        {
            var BytesFile = ExcelGenerator.GeneratePeserataCIPExcelEnter(objRepo, periode);
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Daftar_Peserta_CIP_Enter_" + (periode.HasValue ? periode.Value.ToString() : "") + ".xlsx";

            return result;
        }

        [HttpGet]
        public HttpResponseMessage GenerateUploadDokumenStatusExcel(int tahun)
        {
            try
            {
                var BytesFile = ExcelGenerator.GenerateUploadDokumenStatus(tahun);
                var dataStream = new MemoryStream(BytesFile);
                HttpResponseMessage result = null;
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(dataStream);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                result.Content.Headers.ContentDisposition.FileName = "Upload_Dokumen.xlsx";

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GenerateUploadDokumenStatusExcelDir(int tahun)
        {
            try
            {
                var BytesFile = ExcelGenerator.GenerateUploadDokumenStatusDir(tahun);
                var dataStream = new MemoryStream(BytesFile);
                HttpResponseMessage result = null;
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(dataStream);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                result.Content.Headers.ContentDisposition.FileName = "Upload_Dokumen_Dir.xlsx";

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        public HttpResponseMessage GenerateUploadDokumenStatusExcelEnter(int tahun)
        {
            try
            {
                var BytesFile = ExcelGenerator.GenerateUploadDokumenStatusEnter(tahun);
                var dataStream = new MemoryStream(BytesFile);
                HttpResponseMessage result = null;
                result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(dataStream);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                result.Content.Headers.ContentDisposition.FileName = "Upload_Dokumen_Enter.xlsx";

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        public IGenericWebApiResult GetDocument(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Data = objRepo.GetDocument(kode);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult AllowEdit(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    dynamic more = new ExpandoObject();
                    more.AllowEdit = objRepo.AllowEdit(kode);
                    result.More = more;
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult AllowEditDir(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    dynamic more = new ExpandoObject();
                    more.AllowEdit = objRepo.AllowEditDir(kode);
                    result.More = more;
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult AllowEditEnter(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    dynamic more = new ExpandoObject();
                    more.AllowEdit = objRepo.AllowEditEnter(kode);
                    result.More = more;
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult SetActive(string kode, bool isAktif)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    dynamic more = new ExpandoObject();
                    result.Success = objRepo.SetActive(kode, isAktif);
                    result.Message = result.Success ? string.Format("Gugus tersebut berhasil di {0}", isAktif ? "Aktifkan" : "Non Aktifkan") : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult DownloadRisalah(string kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<string>())
                {
                    var rec = objRepo.GetSingle(kode);

                    if (rec != null)
                    {
                        var fileName = rec.Risalah;
                        return p_DownloadRisalah(fileName);
                    }
                    else
                    {
                        throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                    }
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult DownloadRisalahDir(string kode)
        {
            using (IGenericDataRepository<AsignCipDir> repository = new DataRepository<AsignCipDir>(ctx))
            {
                var rec = repository.GetSingle(kode);

                if (rec != null)
                {
                   var fileName = rec.Risalah;
                   return p_DownloadRisalah(fileName);
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult DownloadRisalahEnter(string kode)
        {
            using (IGenericDataRepository<AsignCipEnter> repository = new DataRepository<AsignCipEnter>(ctx))
            {
                var rec = repository.GetSingle(kode);

                if (rec != null)
                {
                    var fileName = rec.Risalah;
                    return p_DownloadRisalah(fileName);
                }
                else
                {
                    throw new Exception(BaseConstants.MESSAGE_RECORD_NOT_FOUND);
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTableForAssignToDir(int tahun, string unit, string direktorat)
        {
            try
            {
                var datatable = objRepo.DataTableForAssignToDir(tahun, unit, direktorat);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTableAssignDir(int tahun, string unit, string direktorat)
        {
            try
            {
                var datatable = objRepo.DataTableAssignDir(tahun, unit, direktorat);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTableForAssignToEnter(int tahun, string unit, string direktorat)
        {
            try
            {
                var datatable = objRepo.DataTableForAssignToEnter(tahun, unit, direktorat);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTableAssignEnter(int tahun, string unit, string direktorat)
        {
            try
            {
                var datatable = objRepo.DataTableAssignEnter(tahun, unit, direktorat);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult CreateAsignCipDir(List<AsignCipDir> models)
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    dynamic more = new ExpandoObject();
                    result.Success = objRepo.CreateAsignCipDir(models);
                    result.Message = result.Success ? string.Format("Gugus tersebut berhasil didaftarkan ke Direktorat.") : string.Format("Tidak berhasil mendaftarkan Gugus ke Direktorat.");

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult CreateAsignCipEnter(List<AsignCipEnter> models)
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    dynamic more = new ExpandoObject();
                    result.Success = objRepo.CreateAsignCipEnter(models);
                    result.Message = result.Success ? string.Format("Gugus tersebut berhasil didaftarkan ke Enterprise.") : string.Format("Tidak berhasil mendaftarkan Gugus ke Enterprise.");

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }


        [HttpPost]
        public IGenericWebApiResult UndoAsignCipDir(string kodeRegistrasi)
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    dynamic more = new ExpandoObject();
                    result.Success = objRepo.UndoAsignCipDir(kodeRegistrasi);
                    result.Message = result.Success ? string.Format("Batalkan Gugus Unit ke Direktorat berhasil.") : string.Format("Batalkan Gugus Unit ke Direktorat Tidak berhasil.");

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult UndoAsignCipEnter(string cipDirektorat)
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    dynamic more = new ExpandoObject();
                    result.Success = objRepo.UndoAsignCipEnter(cipDirektorat);
                    result.Message = result.Success ? string.Format("Batalkan Gugus Unit ke Direktorat berhasil.") : string.Format("Batalkan Gugus Unit ke Direktorat Tidak berhasil.");

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult DynamicDataDir(string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<dynamic>())
                {
                    result.Data = objRepo.DynamicDataDir(term);
                    if (result.Data == null) throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);

                    result.Success = true;
                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<dynamic>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }

        }

        [HttpGet]
        public IGenericWebApiResult DynamicDataEnter(string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<dynamic>())
                {
                    result.Data = objRepo.DynamicDataEnter(term);
                    if (result.Data == null) throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);

                    result.Success = true;
                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<dynamic>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }

        }

        [HttpGet]
        public IGenericWebApiResult GetSingleDir(string keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<AsignCipDir>())
                {
                    using (IGenericDataRepository<AsignCipDir> repository = new DataRepository<AsignCipDir>(ctx)) {
                        result.Data = repository.GetSingle(keyValues);

                        if (result.Data != null)
                        {
                            result.Success = true;
                        }
                        else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AsignCipDir>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult GetSingleEnter(string keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<AsignCipEnter>())
                {
                    using (IGenericDataRepository<AsignCipEnter> repository = new DataRepository<AsignCipEnter>(ctx))
                    {
                        result.Data = repository.GetSingle(keyValues);

                        if (result.Data != null)
                        {
                            result.Success = true;
                        }
                        else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AsignCipDir>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

    }
}
