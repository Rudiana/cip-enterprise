﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.Core.Repositories.Abstractions;
using System.Dynamic;
using System.Collections.Generic;
using CIP.Core.Common;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http.Headers;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/Audit")]
    public class AuditController : BaseApiController<AuditPDCAHeader>
    {
        private GenericContext ctx;
        private IAuditRepository objRepo;

        public AuditController()
        {
            ctx = new GenericContext();
            objRepo = new AuditRepository(ctx);
            base.repo = objRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = objRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.InnerException.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablesWithFilter(int tahun, string status)
        {
            try
            {
                var datatable = objRepo.DataTablesWithFilter(tahun, status);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.InnerException.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownRegistrasi()
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Dropdown = objRepo.DropdownRegistrasi();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpGet, Route("GetRegistrasi")]
        public IGenericWebApiResult GetRegistrasiCIP(string keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<sp_GetRegistrasiCIP_Result>())
                {
                    result.Data = objRepo.GetRegistrasi(keyValues);

                    if (result.Data != null)
                    {
                        result.Success = true;
                        using (IGenericDataRepository<AnggotaCIP> anggotaCIPRepo = new DataRepository<AnggotaCIP>(ctx))
                        {
                            var anggota = string.Empty;

                            anggotaCIPRepo.UserProfile = base.repo.UserProfile;
                            var anggotaCIPs = anggotaCIPRepo.FindBy(x => x.KodeRegistrasi == keyValues).Select(x => x.KodeUser).ToList();
                            if (anggotaCIPs.Count() > 0)
                            {
                                anggota = string.Join(",", anggotaCIPs);

                                dynamic more = new ExpandoObject();
                                more.Anggota = anggota;
                                result.More = more;
                            }
                        }
                    }
                    else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }

        }

        [HttpGet, Route("GetPdcaHeader")]
        public IGenericWebApiResult GetPdcaHeaderByRegistrasi(string keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>())
                {
                    result.Data = objRepo.GetByRegistrasi(keyValues);

                    if (result.Data != null)
                    {
                        result.Success = true;
                    }
                    else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }

        }

        [HttpGet, Route("GetPdcaDetail")]
        public IGenericWebApiResult GetPdcaDetailByKodeHeader(string Kode)
        {
            try
            {
                using (var result = new GenericWebApiResult<sp_GetAuditPDCADetail_Result>())
                {
                    result.DataList = objRepo.GetPdcaDetail(Kode);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<sp_GetAuditPDCADetail_Result>(ex))
                {
                    result.Success = false;
                    result.DataList = new List<sp_GetAuditPDCADetail_Result>();

                    return result;
                }
            }



        }

        [HttpPost]
        public IGenericWebApiResult Create(HttpRequestMessage request)
        {
            try
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>())
                {
                    var content = request.Content.ReadAsStringAsync();
                    string jsonContent = content.Result;
                    Audit model = JsonConvert.DeserializeObject<Audit>(jsonContent);

                    result.Success = objRepo.CreateAuditPDCA(model.AuditHeader, model.AuditDetail.ToList());
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult Update(HttpRequestMessage request)
        {
            try
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>())
                {
                    var content = request.Content;
                    string jsonContent = content.ReadAsStringAsync().Result;
                    Audit model = JsonConvert.DeserializeObject<Audit>(jsonContent);

                    result.Success = objRepo.UpdateAuditPDCA(model.AuditHeader, model.AuditDetail.ToList());
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult StartPDCA_1(string kode, string kodeRegistrasi)
        {
            try
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>())
                {
                    result.Success = objRepo.StartPDCA_1(kode, kodeRegistrasi);
                    result.Message = result.Success ? BaseConstants.MESSAGE_START_AUDIT_PDCA_I_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult StartPDCA_2(string kode, string kodeRegistrasi)
        {
            try
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>())
                {
                    result.Success = objRepo.StartPDCA_2(kode, kodeRegistrasi);
                    result.Message = result.Success ? BaseConstants.MESSAGE_START_AUDIT_PDCA_II_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult FinishPDCA_1(string kode, string kodeRegistrasi)
        {
            try
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>())
                {
                    result.Success = objRepo.FinishPDCA_1(kode, kodeRegistrasi);
                    result.Message = result.Success ? BaseConstants.MESSAGE_FINISH_AUDIT_PDCA_I_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult FinishPDCA_2(string kode, string kodeRegistrasi)
        {
            try
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>())
                {
                    result.Success = objRepo.FinishPDCA_2(kode, kodeRegistrasi);
                    result.Message = result.Success ? BaseConstants.MESSAGE_FINISH_AUDIT_PDCA_II_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<AuditPDCAHeader>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }
        [HttpGet, Route("duallist")]
        public HttpResponseMessage DualList(string kodeUser)
        {
            try
            {
                var data = objRepo.DualList(kodeUser);
                var result = Request.CreateResponse(HttpStatusCode.OK, data);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult AssignAuditorGugus(List<AuditPDCAHeader> headers)
        {
            try
            {
                using (var result = new GenericWebApiResult<object>())
                {
                    result.Success = objRepo.AssignAuditorGugus(headers);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<object>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public HttpResponseMessage GetPdca1Report(int Periode, string KodeRegistrasi)
        {
            var BytesFile = ExcelGenerator.GeneratePdcaExcel(Periode, KodeRegistrasi, "PDCAI");
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "FormAudit_PDCA_I" + KodeRegistrasi + ".xlsx";
            return result;
        }

        [HttpGet]
        public HttpResponseMessage GetPdca2Report(int Periode, string KodeRegistrasi)
        {
            var BytesFile = ExcelGenerator.GeneratePdcaExcel(Periode, KodeRegistrasi, "PDCAII");
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Form_Audit_PDCA_II" + KodeRegistrasi + ".xlsx";
            return result;
        }

        [HttpGet]
        public HttpResponseMessage GetPdcaList1Report(int Periode)
        {
            var BytesFile = ExcelGenerator.GeneratePdcaListExcel(Periode, "PDCAI", "pdca1");
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Audit_PDCA_I_List" + Periode + ".xlsx";
            return result;
        }

        [HttpGet]
        public HttpResponseMessage GetPdcaList2Report(int Periode)
        {
            var BytesFile = ExcelGenerator.GeneratePdcaListExcel(Periode, "PDCAII", "pdca2");
            var dataStream = new MemoryStream(BytesFile);
            HttpResponseMessage result = null;
            result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(dataStream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            result.Content.Headers.ContentDisposition.FileName = "Audit_PDCA_II_List" + Periode + ".xlsx";
            return result;
        }

        [HttpPost]
        public HttpResponseMessage PdcaList(int periode, string status)
        {
            try
            {
                var datatable = objRepo.PdcaListReport(periode, status);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
    }
}
