﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Dynamic;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/AnggotaCIP")]
    [Authorize]
    public class PesertaCIPController : BaseApiController<AnggotaCIP>
    {
        private GenericContext ctx;
        private IGenericDataRepository<AnggotaCIP> genRepo;

        public PesertaCIPController()
        {
            ctx = new GenericContext();
            genRepo = new DataRepository<AnggotaCIP>(ctx);

            base.repo = genRepo;
        }

    }
}
