﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using CIP.Core.Repositories.Concretes.Activity;

namespace CIP.WebApi.Controllers.Activity
{
    [RoutePrefix(@"api/TemplateAuditPDCAEnter")]
    public class TemplateAuditPDCAEnterController : BaseApiController<TemplateAuditPDCAEnter>
    {
        private GenericContext ctx;
        private ITemplatePdcaEnterRepository genRepo;

        public TemplateAuditPDCAEnterController()
        {
            ctx = new GenericContext();
            genRepo = new TemplatePdcaEnterRepository(ctx);
            base.repo = genRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = genRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

    }
}
