﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.AdminQA.StructureOrganization
{
    [RoutePrefix(@"api/StreamGugusDir")]
    public class StreamGugusDirController : BaseApiController<StreamGugusDir>
    {
        private GenericContext ctx;
        private IStreamGugusDirRepository genRepo;

        public StreamGugusDirController()
        {
            ctx = new GenericContext();
            genRepo = new StreamGugusDirRepository(ctx);

            base.repo = genRepo;
        }


        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = genRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }


        public override IGenericWebApiResult Post(StreamGugusDir data)
        {
            try
            {
                using (var result = new GenericWebApiResult<StreamGugusDir>())
                {
                    data.Kode = Guid.NewGuid().ToString().Substring(0, 20);
                    data.Direktorat = genRepo.UserProfile.Direktorat;
                    result.Data = data;
                    result.Success = genRepo.Create(result.Data);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<StreamGugusDir>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }



        [HttpPost]
        public IGenericWebApiResult DropdownRegistrasi()
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Dropdown = genRepo.DropdownRegistrasi();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownForStreamGugus([FromBody]int tahun)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = genRepo.DropdownForStreamGugus(tahun);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByTahunForStreamJuri([FromBody]string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>())
                {
                    result.Dropdown = genRepo.DropdownByTahunForStreamJuri(term);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<RegistrasiCIP>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }
    }
}
