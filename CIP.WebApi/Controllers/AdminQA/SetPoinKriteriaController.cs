﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.AdminQA;
using CIP.Core.Repositories.Concretes.AdminQA;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.AdminQA
{
    [RoutePrefix(@"api/SetPoinKriteria")]
    public class SetPoinKriteriaController : BaseApiController<TemplateAuditPDCAPoin>
    {
        private GenericContext ctx;
        private ISetPoinKriteriaRepository genRepo;

        public SetPoinKriteriaController()
        {
            ctx = new GenericContext();
            genRepo = new SetPoinKriteriaRepository(ctx);

            base.repo = genRepo;
        }

        [HttpPost]
        public HttpResponseMessage GetLangkahLangkah(int periode)
        {
            try
            {
                var datatable = genRepo.GetLangkahLangkah(periode);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost,Route("CreateRange/{periode:int}")]
        public IGenericWebApiResult CreateRange([FromBody] SetPoinLangkah data, int periode)
        {
            try
            {
                using (var result = new GenericWebApiResult<SetPoinLangkah>())
                {
                    result.Data = data;
                    result.Success = genRepo.CreateRange(result.Data,periode);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<SetPoinLangkah>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult CopyCreateRange(int periode)
        {
            try
            {
                using (var result = new GenericWebApiResult<SetPoinLangkah>())
                {
                    result.Success = genRepo.CopyCreateRange(periode);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<SetPoinLangkah>(ex))
                {

                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByTahun()
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = genRepo.DropdownByTahun();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

    }
}
