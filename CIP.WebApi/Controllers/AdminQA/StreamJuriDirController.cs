﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Concretes.Activity;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.AdminQA
{
    [RoutePrefix(@"api/StreamJuriDir")]
    public class StreamJuriDirController : BaseApiController<StreamJuriDir>
    {
        private GenericContext ctx;
        private IStreamJuriDirRepository genRepo;

        public StreamJuriDirController()
        {
            ctx = new GenericContext();
            genRepo = new StreamJuriDirRepository(ctx);

            base.repo = genRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = genRepo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        public override IGenericWebApiResult Post(StreamJuriDir data)
        {
            try
            {


                using (var result = new GenericWebApiResult<StreamJuriDir>())
                {
                    int cek = ctx.Set<StreamGugusDir>().Where(x => x.KodeStream.Equals(data.KodeStream)).Count();
                    if (cek > 0)
                    {
                        data.Kode = Guid.NewGuid().ToString().Substring(0, 20);
                        result.Data = data;
                        data.Direktorat = repo.UserProfile.Direktorat;
                        result.Success = repo.Create(result.Data);
                        result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;
                    }
                    else {
                        var stream = ctx.Set<StreamDir>().Where(x => x.Kode.Equals(data.KodeStream)).FirstOrDefault();
                        var namaStream = stream != null ? string.Format("Nama Stream: \'{0}\', Tahun: \'{1}\'", stream.Nama, stream.Tahun) : data.KodeStream;
                        result.Success = false;
                        result.Message = String.Format(BaseConstants.MESSAGE_STREAM_EMPTY, namaStream);

                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<StreamJuriDir>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownTahun()
        {
            try
            {
                using (var result = new GenericWebApiResult<StreamDir>())
                {

                    result.Dropdown = genRepo.DropdownTahun();
                    result.Success = true;
           
                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<StreamDir>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownStream(int tahun)
        {
            try
            {
                using (var result = new GenericWebApiResult<StreamDir>())
                {

                    result.Dropdown = genRepo.DropdownStream(tahun);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<StreamDir>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }


        [HttpPost]
        public IGenericWebApiResult DropdownStreamByJuri(int tahun)
        {
            try
            {
                using (var result = new GenericWebApiResult<Stream>())
                {

                    result.Dropdown = genRepo.DropdownStreamByJuri(tahun);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Stream>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownStreamGugusAvail(int tahun, string stream)
        {
            try
            {
                using (var result = new GenericWebApiResult<StreamDir>())
                {

                    result.Dropdown = genRepo.DropdownStreamGugusAvail(tahun, stream);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<StreamDir>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownStreamGugusDocking(int tahun, string stream)
        {
            try
            {
                using (var result = new GenericWebApiResult<Stream>())
                {

                    result.Dropdown = genRepo.DropdownStreamGugusDocking(tahun, stream);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Stream>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownJenisCip()
        {
            try
            {
                using (var result = new GenericWebApiResult<MasterJenisCIP>())
                {

                    result.Dropdown = genRepo.DropdownJenisCip();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<MasterJenisCIP>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownTahunPenjurian()
        {
            try
            {
                using (var result = new GenericWebApiResult<Stream>())
                {

                    result.Dropdown = genRepo.DropdownTahunPenjurian();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Stream>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }
    }
}
