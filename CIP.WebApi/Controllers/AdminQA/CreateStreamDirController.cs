﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Http;
using CIP.Core.Repositories.Abstractions.AdminQA;
using CIP.Core.Repositories.Concretes.AdminQA;

namespace CIP.WebApi.Controllers.AdminQA
{
    [RoutePrefix(@"api/CreateStreamDir")]
    public class CreateStreamDirController : BaseApiController<StreamDir>
    {
        private GenericContext ctx;
        private IStreamDirRepository objRepo;

        public CreateStreamDirController()
        {
            ctx = new GenericContext();
            objRepo = new StreamDirRepository(ctx);

            base.repo = objRepo;
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByTahun([FromBody]string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownByTahun(term);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

    }
}
