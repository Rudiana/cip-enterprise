﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Concretes;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Web.Http;
using CIP.Core.Repositories.Abstractions.AdminQA;
using CIP.Core.Repositories.Concretes.AdminQA;
using System.Net.Http;

namespace CIP.WebApi.Controllers.AdminQA
{
    [RoutePrefix(@"api/CreateStream")]
    public class CreateStreamController : BaseApiController<Stream>
    {
        private GenericContext ctx;
        private IStreamRepository objRepo;

        public CreateStreamController()
        {
            ctx = new GenericContext();
            objRepo = new StreamRepository(ctx);

            base.repo = objRepo;
        }

        [HttpPost]
        public override HttpResponseMessage DataTables()
        {
            try
            {
                var datatable = objRepo.Datatables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        [HttpPost]
        public IGenericWebApiResult DropdownByTahun([FromBody]string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownByTahun(term);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpGet]
        public IGenericWebApiResult KoefisienStatus(int periode)
        {
            try
            {
                using (var result = new GenericWebApiResult<MasterStdDev>())
                {
                    result.Data = objRepo.GetStatusKoefisien(periode);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<MasterStdDev>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    

                    return result;
                }
            }
            
        }
    }
}
