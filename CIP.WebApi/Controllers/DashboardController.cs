﻿using CIP.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CIP.WebApi.Lamp.Controllers;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions.Activity;
using CIP.Core.Repositories.Concretes.Activity;
using System.Net.Http;

namespace CIP.WebApi.Controllers
{
    [RoutePrefix(@"api/Dashboard")]
    public class DashboardController : BaseApiController<RegistrasiCIP>
    {
        private GenericContext ctx;
        private IRegistrasiCIPRepository objRepo;

        public DashboardController()
        {
            ctx = new GenericContext();
            objRepo = new RegistrasiCIPRepository(ctx);

            base.repo = objRepo;
        }

        [HttpGet]
        public System.Net.Http.HttpResponseMessage GetCountApproval(int periode, string unit)
        {
            try
            {
                var count = objRepo.GetCountApprovalStatus(periode, unit);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, count);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpGet]
        public System.Net.Http.HttpResponseMessage GetCountNewReg(int periode, string unit)
        {
            try
            {
                var count = objRepo.GetCountNewRegStatus(periode, unit);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, count);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpGet]
        public System.Net.Http.HttpResponseMessage GetCountAudit(int periode, string unit)
        {
            try
            {
                var count = objRepo.GetCountAuditStatus(periode, unit);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, count);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpGet]
        public System.Net.Http.HttpResponseMessage GetCountPenjurian(int periode, string unit)
        {
            try
            {
                var count = objRepo.GetCountPenjurianStatus(periode, unit);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, count);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public HttpResponseMessage DatatablesByStatus(int periode, string unit, string status)
        {
            try
            {
                var datatable = objRepo.DatatablesByStatus(periode, unit, status);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
    }
}
