﻿using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Administrasi;
using CIP.Core.Repositories.Concretes.Administrasi;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Administrasi
{
    [Authorize]
    [RoutePrefix(@"api/Administrasihakaksestombol")]
    public class AdministrasiHakAksesTombolController : BaseApiController<AdministrasiHakAksesTombol>
    {
        private GenericContext ctx;
        private IAdministrasiHakAksesTombolRepository genRepo;

        public AdministrasiHakAksesTombolController()
        {
            ctx = new GenericContext();
            genRepo = new AdministrasiHakAksesTombolRepository(ctx);
            base.repo = genRepo;

            
        }


        [HttpGet, Route("duallist")]
        public HttpResponseMessage DualList(string kode)
        {
            try
            {
                var data = genRepo.DualList(kode);
                var result = Request.CreateResponse(HttpStatusCode.OK, data);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        

    }
}
