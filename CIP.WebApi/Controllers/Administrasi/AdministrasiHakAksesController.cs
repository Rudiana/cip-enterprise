﻿using CIP.Core.Common;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Administrasi;
using CIP.Core.Repositories.Concretes.Administrasi;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Administrasi
{
    [Authorize]
    [RoutePrefix(@"api/administrasihakakses")]
    public class AdministrasiHakAksesController : BaseApiController<AdministrasiHakAkses>
    {
        private GenericContext ctx;
        private IAdministrasiHakAksesRepository genRepo;

        public AdministrasiHakAksesController()
        {
            ctx = new GenericContext();
            genRepo = new AdministrasiHakAksesRepository(ctx);
            base.repo = genRepo;

            
        }


        [HttpPost, Route("createwithdetail")]
        public IGenericWebApiResult CreateWithDetail(ParamAdministrasiHakAkses data)
        {
            using (var result = new GenericWebApiResult<AdministrasiHakAkses>())
            {
                result.Success = genRepo.CreateWithDetail(data);
                result.Data = data.HakAkses;
                result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                return result;
            }
        }

        [HttpPut, Route("updatewithdetail")]
        public IGenericWebApiResult UpdateWithDetail(ParamAdministrasiHakAkses data)
        {
            using (var result = new GenericWebApiResult<AdministrasiHakAkses>())
            {
                result.Success = genRepo.UpdateWithDetail(data);
                result.Data = data.HakAkses;
                result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                return result;
            }
        }

        [HttpDelete, Route("deletewithdetail")]
        public IGenericWebApiResult DeleteWithDetail(string kode)
        {
            using (var result = new GenericWebApiResult<AdministrasiHakAkses>())
            {
                result.Success = genRepo.DeleteWithDetail(kode);
                result.Message = result.Success ? BaseConstants.MESSAGE_DELETE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                return result;
            }
        }
    }
}
