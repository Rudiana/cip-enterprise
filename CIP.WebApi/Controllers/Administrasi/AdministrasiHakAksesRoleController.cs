﻿using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Administrasi;
using CIP.Core.Repositories.Concretes.Administrasi;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Administrasi
{
    [Authorize]
    [RoutePrefix(@"api/Administrasihakaksesrole")]
    public class AdministrasiHakAksesRoleController : BaseApiController<AdministrasiHakAksesRole>
    {
        private GenericContext ctx;
        private IAdministrasiHakAksesRoleRepository genRepo;

        public AdministrasiHakAksesRoleController()
        {
            ctx = new GenericContext();
            genRepo = new AdministrasiHakAksesRoleRepository(ctx);
            base.repo = genRepo;

            
        }


        [HttpGet, Route("duallist")]
        public HttpResponseMessage DualList(string kode)
        {
            try
            {
                var data = genRepo.DualList(kode);
                var result = Request.CreateResponse(HttpStatusCode.OK, data);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
        

    }
}
