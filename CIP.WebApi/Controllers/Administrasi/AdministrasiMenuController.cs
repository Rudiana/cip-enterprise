﻿using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Administrasi;
using CIP.Core.Repositories.Concretes.Administrasi;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Administrasi
{
    [Authorize]
    [RoutePrefix(@"api/administrasimenu")]
    public class AdministrasiMenuController : BaseApiController<AdministrasiMenu>
    {
        private GenericContext ctx;
        private IAdministrasiMenuRepository genRepo;

        public AdministrasiMenuController()
        {
            ctx = new GenericContext();
            genRepo = new AdministrasiMenuRepository(ctx);
            base.repo = genRepo;
        }

        [HttpGet, Route("getmenubyuser")]
        public HttpResponseMessage GetMenuByUser()
        {
            try
            {
                var menuItem = genRepo.GetMenuByUser();
                dynamic rootMenuItem = new ExpandoObject();
                rootMenuItem.items = menuItem;

                var result = Request.CreateResponse(HttpStatusCode.OK, (object)rootMenuItem);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }
    }
}
