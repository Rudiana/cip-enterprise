﻿using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Administrasi;
using CIP.Core.Repositories.Concretes.Administrasi;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Administrasi
{
    [Authorize]
    [RoutePrefix(@"api/administrasitombol")]
    public class AdministrasiTombolController : BaseApiController<AdministrasiTombol>
    {
        private GenericContext ctx;
        private IGenericDataRepository<AdministrasiTombol> genRepo;

        public AdministrasiTombolController()
        {
            ctx = new GenericContext();
            genRepo = new AdministrasiTombolRepository(ctx);
            base.repo = genRepo;

            
        }
    }
}
