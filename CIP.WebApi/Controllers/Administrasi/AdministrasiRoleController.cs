﻿using CIP.Core.Common;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Administrasi;
using CIP.Core.Repositories.Concretes.Administrasi;
using CIP.WebApi.Lamp.Controllers;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Administrasi
{
    [Authorize]
    [RoutePrefix(@"api/administrasirole")]
    public class AdministrasiRoleController : BaseApiController<AdministrasiRole>
    {
        private GenericContext ctx;
        private IAdministrasiRoleRepository genRepo;

        public AdministrasiRoleController()
        {
            ctx = new GenericContext();
            genRepo = new AdministrasiRoleRepository(ctx);
            base.repo = genRepo;

            
        }

        [HttpPost, Route("createwithdetail")]
        public IGenericWebApiResult CreateWithDetail(ParamHeaderDetail<AdministrasiRole, AdministrasiRoleDtl> data)
        {
            using (var result = new GenericWebApiResult<AdministrasiRole>())
            {
                result.Success = genRepo.CreateWithDetail(data.header, data.detail);
                result.Data = data.header;
                result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                return result;
            }
        }

        [HttpPut, Route("updatewithdetail")]
        public IGenericWebApiResult UpdateWithDetail(ParamHeaderDetail<AdministrasiRole, AdministrasiRoleDtl> data)
        {
            using (var result = new GenericWebApiResult<AdministrasiRole>())
            {
                result.Success = genRepo.UpdateWithDetail(data.header, data.detail);
                result.Data = data.header;
                result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                return result;
            }
        }

        [HttpDelete, Route("deletewithdetail")]
        public IGenericWebApiResult CreateWithDetail(string kode)
        {
            using (var result = new GenericWebApiResult<AdministrasiRole>())
            {
                result.Success = genRepo.DeleteWithDetail(kode);
                result.Message = result.Success ? BaseConstants.MESSAGE_DELETE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                return result;
            }
        }

        [HttpPost, Route("createwithdetailuser")]
        public IGenericWebApiResult CreateWithDetailUser(ParamHeaderDetail<AdministrasiRole, AdministrasiRoleUser> data)
        {
            using (var result = new GenericWebApiResult<AdministrasiRole>())
            {
                result.Success = genRepo.CreateWithDetailUser(data.header, data.detail);
                result.Data = data.header;
                result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                return result;
            }
        }
    }
}
