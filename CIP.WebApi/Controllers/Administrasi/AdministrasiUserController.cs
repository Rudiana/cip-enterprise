﻿using CIP.Core.Common;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using CIP.Core.Repositories.Abstractions.Administrasi;
using CIP.Core.Repositories.Concretes.Administrasi;
using CIP.WebApi.Lamp.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Dynamic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.Http;

namespace CIP.WebApi.Controllers.Administrasi
{
    [Authorize]
    [RoutePrefix(@"api/administrasiuser")]
    public class AdministrasiUserController : BaseApiController<AdministrasiUser>
    {
        private GenericContext ctx;
        private IAdministrasiUserRepository objRepo;

        public AdministrasiUserController()
        {
            ctx = new GenericContext();
            objRepo = new AdministrasiUserRepository(ctx);
            base.repo = objRepo;
        }

        [HttpGet, Route("GetSimpleData")]
        public IGenericWebApiResult GetSimpleData(string keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<UserProfile>())
                {
                    var rec = repo.GetSingle(keyValues);
                    var data = new UserProfile()
                    {
                        Kode = rec.Kode,
                        Nama = rec.Nama,
                        Alamat = rec.Alamat,
                        Telepon = rec.Telepon,
                        Email = rec.Email,
                        Avatar = rec.Avatar ?? "male.png"
                    };

                    var dir = HttpContext.Current.Server.MapPath("~/Avatars");
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }

                    var path = Path.Combine(dir, data.Avatar);
                    if (File.Exists(path))
                    {
                        using (Image image = Image.FromFile(path))
                        {
                            using (MemoryStream m = new MemoryStream())
                            {
                                image.Save(m, image.RawFormat);
                                byte[] imageBytes = m.ToArray();

                                // Convert byte[] to Base64 String
                                string base64String = Convert.ToBase64String(imageBytes).TrimStart(',');
                                data.Avatar = "data:image/png;base64," + base64String;
                            }
                        }
                    }

                    result.Data = data;
                    if (result.Data != null)
                    {
                        result.Success = true;
                    }
                    else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<UserProfile>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult ChangePassword(string oldPass, string newPass)
        {
            try
            {
                using (var result = new GenericWebApiResult<dynamic>())
                {
                    result.Data = new ExpandoObject();
                    result.Success = objRepo.ChangePassword(oldPass, newPass);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CHANGE_PASSWORD_SUCCESS: BaseConstants.MESSAGE_INVALID_OLD_PASSWORD;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<dynamic>(ex))
                {
                    result.Data = new ExpandoObject();
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost, Route("ResetPassword")]
        public IGenericWebApiResult ResetPassword()
        {
            try
            {
                using (var result = new GenericWebApiResult<dynamic>())
                {
                    result.Data = new ExpandoObject();
                    result.Success = objRepo.ResetPassword();
                    result.Message = result.Success ? BaseConstants.MESSAGE_RESET_PASSWORD_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<dynamic>(ex))
                {
                    result.Data = new ExpandoObject();
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPost, Route("UploadAvatar")]
        public HttpResponseMessage UploadAvatar()
        {
            try
            {
                using (var result = new GenericWebApiResult<dynamic>())
                {
                    var forms = HttpContext.Current.Request.Form;
                    var formAvatar = forms.GetValues("avatar");

                    var avatar = formAvatar.Length > 0 ? JsonConvert.DeserializeObject<string>(formAvatar[0]) : "male.png";

                    result.Success = objRepo.UploadAvatar(avatar);
                    result.Data = new ExpandoObject();
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    if (result.Success)
                    {
                        var fileCount = HttpContext.Current.Request.Files.Count;
                        for (int i = 0; i < fileCount; i++)
                        {
                            HttpPostedFile file = HttpContext.Current.Request.Files[i];
                            var dir = HttpContext.Current.Server.MapPath("~/Avatars");
                            if (!Directory.Exists(dir))
                            {
                                Directory.CreateDirectory(dir);
                            }

                            var path = Path.Combine(dir, file.FileName);
                            if (File.Exists(path))
                            {
                                File.Delete(path);
                            }

                            file.SaveAs(path);

                            using (Image image = Image.FromFile(path))
                            {
                                using (MemoryStream m = new MemoryStream())
                                {
                                    image.Save(m, image.RawFormat);
                                    byte[] imageBytes = m.ToArray();

                                    // Convert byte[] to Base64 String
                                    string base64String = Convert.ToBase64String(imageBytes).TrimStart(',');
                                    dynamic data = new ExpandoObject();
                                    data.Avatar = "data:image/png;base64," + base64String;
                                    result.Data = data;
                                }
                            }
                        }
                    }

                    return Request.CreateResponse(result);
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<ExpandoObject>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return Request.CreateResponse(result);
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablesAuditor()
        {
            try
            {
                var queryable = objRepo.DatatablesAuditor();

                return base.DataTables(queryable);
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownRoleJuriEnter(string kodeStream)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownRoleJuriEnter(kodeStream);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownRoleJuriDir(string kodeStream)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownRoleJuriDir(kodeStream);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownRoleJuri(string kodeStream)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownRoleJuri(kodeStream);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public HttpResponseMessage DataTablesDto()
        {
            try
            {
                var datatable = objRepo.DataTablesDto();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        [HttpPost]
        public IGenericWebApiResult DropdownByUnit(string unit)
        {
            try
            {
                using (var result = new GenericWebApiResult<Dropdown>())
                {
                    result.Dropdown = objRepo.DropdownByUnit(unit);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<Dropdown>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }
    }
}
