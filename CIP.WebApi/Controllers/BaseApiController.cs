﻿using CIP.Core.Common;
using CIP.Core.GenericRepositories.Abstractions;
using CIP.Core.Models;
using CIP.Core.Repositories.Concretes;
using CIP.Core.Repositories.Abstractions;
using System.Linq;

using System;
using System.Dynamic;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Routing;
using System.Linq.Expressions;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web;
using DataTablesParser;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;

namespace CIP.WebApi.Lamp.Controllers
{
    public abstract class BaseApiController<T> : ApiController, IGenericWebApiController<T>
        where T : class, new()
    {

        protected IGenericDataRepository<T> repo;
        protected string UserID
        {
            get
            {
                return this.RequestContext.Principal.Identity.Name;
            }
        }

        protected string RoleID
        {
            get
            {
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
                var roleID = identity.Claims.Where(c => c.Type == ClaimTypes.Role)
                                   .Select(c => c.Value).SingleOrDefault();
                return roleID;
            }
        }

        protected string GetIdentityType(string type)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            var claim = identity.Claims.Where(c => c.Type == type)
                                .Select(c => c.Value).SingleOrDefault();
            return claim;
        }

        #region == Base Method ==
        [HttpGet]
        public virtual IGenericWebApiResult New()
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Data = new T();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public virtual IGenericWebApiResult Get()
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.DataList = repo.GetAll();
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        [Route("{id}")]
        public virtual IGenericWebApiResult GetById(int id)
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    //result.Data = repo.GetAll().FirstOrDefault(x => x.id == keyValues);

                    result.Data = repo.SelectById(id);

                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }
        
        [HttpGet]
        public virtual IGenericWebApiResult GetSingle(string keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Data = repo.GetSingle(keyValues);

                    if (result.Data != null)
                    {
                        result.Success = true;
                    }
                    else { throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST); }

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpGet]
        public virtual IGenericWebApiResult DynamicData(string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<dynamic>())
                {
                    result.Data = repo.DynamicData(term);
                    if (result.Data == null) throw new Exception(BaseConstants.MESSAGE_DATA_IS_NOT_EXIST);

                    result.Success = true;
                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<dynamic>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }

        }


        [HttpPost]
        public virtual IGenericWebApiResult Post(T data)
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Data = data;
                    result.Success = repo.Create(result.Data);
                    result.Message = result.Success ? BaseConstants.MESSAGE_CREATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpPut]
        public virtual IGenericWebApiResult Put(T data)
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Data = data;
                    result.Success = repo.Update(result.Data);
                    result.Message = result.Success ? BaseConstants.MESSAGE_UPDATE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Data = data;
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        [HttpDelete]
        public virtual IGenericWebApiResult Delete(params object[] keyValues)
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Success = repo.Delete(keyValues);
                    result.Message = result.Success ? BaseConstants.MESSAGE_DELETE_SUCCESS : BaseConstants.MESSAGE_INVALID_DATA;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;

                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }


        [HttpPost]
        public virtual IGenericWebApiResult Dropdown([FromBody]string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Dropdown = repo.Dropdown(new T(), term);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public virtual IGenericWebApiResult DropdownByKey([FromBody]string term)
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Dropdown = repo.DropdownByKey(new T(), term);
                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;
                    result.Dropdown = new List<Dropdown>();

                    return result;
                }
            }
        }

        [HttpPost]
        public virtual HttpResponseMessage DataTables()
        {
            try
            {
                //var request = HttpContext.Current.Request;
                //var wrapper = new HttpRequestWrapper(request);
                //var parser = new DataTablesParser<T>(wrapper, repo.GetAll().AsQueryable());
                var datatable = repo.DataTables();
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, datatable);

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        #endregion

        #region == Other Method ==
        protected virtual IGenericWebApiResult GetLates()
        {
            try
            {
                using (var result = new GenericWebApiResult<T>())
                {
                    result.Data = repo.GetLates() ?? new T();

                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<T>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }

        protected virtual HttpResponseMessage DataTables(IQueryable<T> queryable)
        {
            try
            {
                var request = HttpContext.Current.Request;
                var wrapper = new HttpRequestWrapper(request);
                var parser = new DataTablesParser<T>(wrapper, queryable);
                var result = Request.CreateResponse(System.Net.HttpStatusCode.OK, parser.Parse());

                return result;
            }
            catch (Exception ex)
            {
                var result = Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, ex.Message);

                return result;
            }
        }

        public IGenericWebApiResult GetUserProfile()
        {
            try
            {
                using (var result = new GenericWebApiResult<UserProfile>())
                {
                    result.Data = repo.UserProfile ?? new UserProfile();

                    result.Success = true;

                    return result;
                }
            }
            catch (Exception ex)
            {
                using (var result = new GenericWebApiResult<UserProfile>(ex))
                {
                    result.Success = false;
                    dynamic more = new ExpandoObject();
                    more.Errors = ex.Message;
                    result.More = more;

                    return result;
                }
            }
        }
        #endregion

        #region == Overide ==
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            UserProfile userProfile = new UserProfile()
            {
                Kode = this.UserID,
                Roles = this.RoleID,
                HakAkses = this.GetIdentityType("HakAkses"),
                Email = this.GetIdentityType("Email"),
                Avatar = this.GetIdentityType("Avatar"),
                IsAdministrator = Convert.ToBoolean(this.GetIdentityType("IsAdministrator")),
                Unit = this.GetIdentityType("Unit"),
                UnitDes = this.GetIdentityType("UnitDes"),
                Direktorat = this.GetIdentityType("Direktorat"),
                DirektoratDes = this.GetIdentityType("DirektoratDes"),
                Nama = this.GetIdentityType("Nama")
            };

            repo.UserProfile = userProfile;
        }
        #endregion
    }
}
