﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
//using CIP.WebApi.Worker;

[assembly: OwinStartup(typeof(CIP.WebApi.Startup))]

namespace CIP.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }
    }
}
