﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using System.Threading.Tasks;
using System.Net;
using System.Web.Http.Cors;
using System.Threading;
using System.Globalization;
using System.Web.Http.Routing;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Web.Http.ExceptionHandling;
using Newtonsoft.Json;
//using Utilization.Agent;

namespace CIP.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Services.Replace(typeof(IExceptionLogger), new CustomExceptionLogger());
            //config.Filters.Add(new CustomActionFilterAttribute());

            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            //Configure enable CORS
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corsAttr);
            config.MessageHandlers.Add(new PreflightRequestsHandler());


            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });
            config.Routes.MapHttpRoute("DefaultApiWithAction", "Api/{controller}/{action}");
            config.Routes.MapHttpRoute("DefaultApiWithActionAndId", "Api/{controller}/{action}/{id}", new { id = RouteParameter.Optional }, new { id = @"\d+" });


            config.Routes.MapHttpRoute("DefaultApiGet", "Api/{controller}", new { action = "Get" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("DefaultApiNew", "Api/{controller}", new { action = "New" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("DefaultApiGetSingle", "Api/{controller}", new { action = "GetSingle" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("DefaultApiDynamicData", "Api/{controller}", new { action = "DynamicData" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("DefaultApiPost", "Api/{controller}", new { action = "Post" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("DefaultApiDropdown", "Api/{controller}", new { action = "Dropdown" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("DefaultApiDropdownByKey", "Api/{controller}", new { action = "DropdownByKey" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("DefaultApiDatatables", "Api/{controller}", new { action = "DataTables" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("DefaultApiPut", "Api/{controller}", new { action = "Put" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("DefaultApiDelete", "Api/{controller}", new { action = "Delete" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Delete) });

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            config.Formatters.JsonFormatter.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings()
            {
                Formatting = Newtonsoft.Json.Formatting.Indented,
                // = new LowercaseContractResolver(),
                Culture = new CultureInfo("en-US"),
                DateFormatHandling = Newtonsoft.Json.DateFormatHandling.IsoDateFormat,
                DateFormatString = "yyyy-MM-dd HH:mm:ss.fff",
                DateParseHandling = Newtonsoft.Json.DateParseHandling.None,
                DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None,
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };

        }
    }

    public class PreflightRequestsHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Headers.Contains("Origin") && request.Method.Method == "OPTIONS")
            {
                var response = new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
                response.Headers.Add("Access-Control-Allow-Origin", "*");
                response.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Authorization");
                response.Headers.Add("Access-Control-Allow-Methods", "*");
                var tcs = new TaskCompletionSource<HttpResponseMessage>();
                tcs.SetResult(response);
                return tcs.Task;
            }
            return base.SendAsync(request, cancellationToken);
        }
    }

    public class LowercaseContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return !string.IsNullOrEmpty(propertyName) ? propertyName.ToLower() : propertyName;
        }

        protected override IValueProvider CreateMemberValueProvider(System.Reflection.MemberInfo member)
        {
            if (member.MemberType == MemberTypes.Property)
            {
                var pi = (PropertyInfo)member;
                if (pi.PropertyType.FullName == "System.String")
                {
                    return new NullableValueProvider(member);
                }
            }


            return base.CreateMemberValueProvider(member);
        }
    }

    public class NullableValueProvider : IValueProvider
    {

        private readonly IValueProvider _underlyingValueProvider;


        public NullableValueProvider(MemberInfo memberInfo)
        {
            _underlyingValueProvider = new DynamicValueProvider(memberInfo);
        }

        public void SetValue(object target, object value)
        {
            _underlyingValueProvider.SetValue(target, value);
        }

        public object GetValue(object target)
        {
            return _underlyingValueProvider.GetValue(target) ?? "";
        }
    }

    //public class CustomExceptionLogger : ExceptionLogger
    //{
    //    public override void Log(ExceptionLoggerContext context)
    //    {
    //        base.Log(context);

    //        try
    //        {
    //            var logModel = new LogModel()
    //            {
    //                KodeAplikasi = "CIP",
    //                LogType = LogType.Error,
    //                RequestUri = context.Request.RequestUri.ToString(),
    //                ExceptionMessage = context.Exception.Message,
    //                ExceptionTrace = context.Exception.StackTrace
    //            };

    //            Client.CreateLog(logModel);
    //        }
    //        catch (Exception ex)
    //        {

    //        }

    //    }
    //}

    //public class CustomActionFilterAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
    //    {
    //        base.OnActionExecuted(actionExecutedContext);

    //        try
    //        {
    //            string requstMethod = actionExecutedContext.Request.Method.ToString();
    //            var ctrlRouteData = actionExecutedContext.ActionContext.ControllerContext.RouteData.Values.Where(x => x.Key == "controller").FirstOrDefault();
    //            string controllerName = ctrlRouteData.Equals(new KeyValuePair<string, object>()) ? "" : ctrlRouteData.Value.ToString();
    //            var actRouteData = actionExecutedContext.ActionContext.ControllerContext.RouteData.Values.Where(x => x.Key == "action").FirstOrDefault();
    //            string actionName = actRouteData.Equals(new KeyValuePair<string, object>()) ? "" : actRouteData.Value.ToString();

    //            actionName = actionName.ToLower();

    //            var logModel = new LogModel();
    //            logModel.KodeAplikasi = "CIP";
    //            logModel.LogType = LogType.Crud;

    //            switch (requstMethod.ToLower())
    //            {
    //                case "get":
    //                    logModel.JenisCrud = "VIEW";
    //                    break;
    //                case "post":
    //                    var jenis = "INSERT";
    //                    if (actionName.Contains("datatable"))
    //                    {
    //                        jenis = "VIEW";
    //                    }

    //                    if (actionName.Contains("star") || actionName.Contains("finish") || actionName.Contains("submit") || actionName.Contains("reset") || actionName.Contains("approv") || actionName.Contains("update"))
    //                    {
    //                        jenis = "UPDATE";
    //                    }


    //                    logModel.JenisCrud = jenis;
    //                    break;
    //                case "put":
    //                    logModel.JenisCrud = "UPDATE";
    //                    break;
    //                case "delete":
    //                    logModel.JenisCrud = "DELETE";
    //                    break;
    //                default:
    //                    logModel.JenisCrud = "VIEW";
    //                    break;
    //            }

    //            Client.CreateLog(logModel);
    //        }
    //        catch (Exception ex)
    //        {

    //        }
    //    }
    //}
}
