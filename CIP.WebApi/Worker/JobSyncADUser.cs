﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Configuration;
using CIP.Core.Models;
using log4net;
using CIP.Core.Repositories.Concretes;
using System.Threading;

namespace CIP.WebApi.Worker
{
    public class JobSyncADUser : IJob
    {
        ILog logger=LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static readonly string SchedulingStatus = ConfigurationManager.AppSettings["LdapExecuteSyncStatus"];
        public Task Execute(IJobExecutionContext context) {
            return Task.Run(()=>
            {
                if (SchedulingStatus.Equals("1"))
                {
                    SyncADUsers();
                }
            }
            );
        }


        public static void SyncADUsers()
        {
            
            try
            {
                using (var context = new PrincipalContext(ContextType.Domain, ConfigurationManager.AppSettings["LdapDomain"], ConfigurationManager.AppSettings["LdapUser"], ConfigurationManager.AppSettings["LdapPassword"]))
                {
                    using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                    {
                        foreach (var result in searcher.FindAll())
                        {
                            DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                            AdministrasiUser user = new AdministrasiUser();
                            if (Convert.ToString(de.Properties["employeeID"].Value).Trim().Length > 0 
                                && Convert.ToString(de.Properties["samAccountName"].Value).Trim().Length > 0
                                && Convert.ToString(de.Properties["displayName"].Value).Trim().Length > 0
                                && Convert.ToString(de.Properties["mail"].Value).Trim().Length > 0
                                && Convert.ToString(de.Properties["title"].Value).Trim().Length > 0
                                && Convert.ToString(de.Properties["displayName"].Value).Trim().Length > 0
                                && !Convert.ToString(de.Properties["employeeID"].Value).Trim().Equals("-")
                                )
                            {

                                using (var ctx = new DB_CIPEntities()) {
                                    var mail = Convert.ToString(de.Properties["mail"].Value)??"";
                                    var noPek = Convert.ToString(de.Properties["employeeID"].Value) ?? "";
                                    var account = Convert.ToString(de.Properties["samAccountName"].Value) ?? "";
                                    var title = Convert.ToString(de.Properties["title"].Value) ?? "";
                                    var displayName = Convert.ToString(de.Properties["displayName"].Value) ?? "";
                                    var telpon= Convert.ToString(de.Properties["telephoneNumber"].Value) ?? "";
                                    var street = Convert.ToString(de.Properties["streetAddress"].Value)??"";
                                    var city = Convert.ToString(de.Properties["l"].Value);
                                    var poscode = Convert.ToString(de.Properties["postalCode"].Value ?? "");
                                    AdministrasiUser userAd =ctx.AdministrasiUser.FirstOrDefault(x => x.Email.Equals(mail) || x.Kode.Equals(account) || x.NoPek.Equals(noPek));
                                    if (userAd==null)
                                    {
                                        userAd = new AdministrasiUser()
                                        {
                                            Kode = account,
                                            Email = mail,
                                            IsLdapAccount = true,
                                            NoPek = noPek,
                                            Nama = displayName,
                                            Title = title,
                                            Aktif=true,
                                            Telepon=telpon,
                                            Alamat=String.Format("{0} {1} {2}",street,poscode,city)
                                        };

                                        ctx.AdministrasiUser.Add(userAd);
                                        Console.WriteLine(String.Format("{0}|{1}|{2}|{3}|{4}|{5}", account, mail, noPek, displayName, title, telpon));
                                        try
                                        {
                                            ctx.SaveChanges();
                                        }
                                        catch (Exception ex) { continue; }
                                    }
                                    else
                                    {
                                       
                                            userAd.Kode = account;
                                            userAd.Email = mail;
                                            userAd.NoPek = noPek;
                                            userAd.Nama = displayName;
                                            userAd.Title = title;
                                            userAd.Telepon = telpon;
                                            userAd.Alamat = String.Format("{0} {1} {2}", street, poscode, city);
                                            Console.WriteLine(String.Format("{0}|{1}|{2}|{3}|{4}|{5}", account, mail, noPek, displayName, title, telpon));
                                        try
                                        {
                                            ctx.SaveChanges();
                                        }
                                        catch (Exception ex) { continue; }
                                        
                                    }



                                    Thread.Sleep(30);
                                    
                                }
                            }
                        }
                        Console.ReadKey();
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex);
                Console.ReadKey();
            }

            
        }

        //static void Main() {
        //    SyncADUsers();
        //}
        
    }
}